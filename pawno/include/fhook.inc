/*
*
* @Author: Found
* @Version: -
* @Realesed: -
*
*/


#include <a_samp>

#define MAX_FUNCTION_NAME	( 32 )
#define MAX_FUNCTIONS		( 16 )

#if defined _f_hook_included
	#endinput
#else
	#define _f_hook_included
#endif

static const
	_f_hook_g_Functions[ MAX_FUNCTIONS ][ MAX_FUNCTION_NAME ] =
	{
		{!"OnGameModeInit"},
		{!"OnGameModeExit"},
		{!"OnDialogResponse"},
		{!"OnPlayerConnect"},
		{!"OnPlayerDisconnect"},
		{!"OnPlayerClickMap"}
	};

forward _f_hook_CreateFunction( func_name[ MAX_FUNCTION_NAME ] );


public _f_hook_CreateFunction( func_name[ MAX_FUNCTION_NAME ] )
{
	static
		_f_function_name	[ MAX_FUNCTION_NAME ];

	for( new _f_i = 0x0; _f_i < sizeof _f_hook_g_Functions; _f_i++ )
	{
		_f_hook_GetFunctionName( _f_i, _f_function_name, sizeof _f_function_name )
	}
}

_f_hook_GetFunctionName( i, const func_name_dest[ MAX_FUNCTION_NAME ], size = sizeof( func_name_dest ) )
{	   
	if ( i < 0 || i > MAX_FUNCTIONS )
        return;

	return strunpack( func_name_dest, _f_hook_g_Functions[ i ], size );
}