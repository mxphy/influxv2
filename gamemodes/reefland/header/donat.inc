#define donat 				"\
								"cBLUE"1. "cWHITE"Informaci�n\n\
								"cBLUE"2. "cWHITE"Cambio de ICoins\n\
								"cBLUE"3. "cWHITE"Cuentas premium\n\
								"cBLUE"4. "cWHITE"Caracter�sticas adicionales\
							"
							
#define donatinfo			"\
								"cBLUE"1. "cWHITE"Informaci�n general\n\
								"cBLUE"2. "cWHITE"Saldo y reposici�n de la cuenta.\n\
								"cBLUE"3. "cWHITE"Mi cuenta premium\
							"
							
#define donatadd			""cWHITE" \t"cWHITE"Costos"cWHITE"\n\
								Cambio de rol\t"cBLUE"%d ICoins"cWHITE"\n\
								Cambio de sexo\t"cBLUE"%d ICoins"cWHITE"\n\
								Decoloraci�n de la piel\t"cBLUE"%d ICoins"cWHITE"\n\
								Cambio de nacionalidad\t"cBLUE"%d ICoins"cWHITE"\n\
								Cambio de pa�s de nacimiento\t"cBLUE"%d ICoins"cWHITE"\n\
								Cambio de edad\t"cBLUE"%d ICoins"cWHITE"\n\
								Cambiar apodo\t"cBLUE"%d ICoins"cWHITE"\n\
								Aprender un estilo de combate\t"cBLUE"%d ICoins"cWHITE"\n\
								Aprender todos los estilos de combate\t"cBLUE"%d ICoins"cWHITE"\n\
								Eliminar una advertencia\t"cBLUE"%d ICoins"cWHITE"\n\
								Eliminar todas las advertencias\t"cBLUE"%d ICoins"cWHITE"\n\
								Cancelar un contrato en el trabajo\t"cBLUE"%d ICoins"cWHITE"\n\
								Cambiar n�mero de tel�fono\t"cBLUE"%d ICoins"cWHITE"\
							"
							
#define donattotal 			""cBLUE"Informaci�n general"cWHITE"\n\n\
								En el servidor existe la posibilidad de transferir dinero real a ICoins, una moneda de juego especial,\n\
								con la que podr�s comprar una cuenta premium, as� como otros servicios adicionales,\n\
								Permiti�ndote hacer tu juego m�s c�modo.\n\n\
								Para agregar ICoins a su cuenta, debe ir a "cBLUE"pcu.influx-rp.eu"cWHITE"\n\
								en la "cBLUE"secci�n de tienda"cWHITE" y especifique la cantidad deseada.\n\n\
								"gbDialogError"Nota:\n\
								"cWHITE"Al comprar una cuenta premium con la capacidad de tener 2 veh�culos, 2 casas o 2 negocios,\n\
								tenga en cuenta que despu�s del vencimiento del per�odo de la compra, el segundo veh�culo / segundo hogar / segundo negocio\n\
								Se vende autom�ticamente y se devuelve su valor de mercado.\n\
								Para evitar esto, extienda su cuenta premium sin salir del servidor.\n\
								"cBLUE"/donar - Cuentas Premium - Renovar cuenta Premium"cWHITE".\n\n\
								Puede saber cu�ndo vence su cuenta premium usando "cBLUE"/tiempopremium"cWHITE".\
							"
							
#define donatbalance		""cBLUE"Saldo y reposici�n de la cuenta."cWHITE"\n\n\
								En su cuenta - "cBLUE"%d"cWHITE" ICoins\n\n\
								Para reponer tu cuenta necesitas ir a "cBLUE"pcu.influx-rp.eu"cWHITE"\n\
								en la "cBLUE"secci�n de tienda"cWHITE" y especifique la cantidad deseada.\n\n\
								"gbDialogError"Nota:\n\
								"cWHITE"Para reponer la cuenta, no es necesario abandonar el servidor, es suficiente minimizar el juego.\n\
								Despu�s de una operaci�n exitosa, expanda el juego y reutilice el comando "cBLUE"/donar"cWHITE".\n\n\
								Si tiene alg�n problema con la reposici�n de ICoins contacte nuestra mesa de soporte.\n\
								"cRED"�Aseg�rese de tomar una captura de pantalla del pago!\
							"
							
#define donatpremium		"\
								"cBLUE"- "cWHITE"Inicial\n\
								"cBLUE"- "cWHITE"B�sico\n\
								"cBLUE"- "cWHITE"Supremo\n\
								"cBLUE"- "cWHITE"Personalizado\n\
								"gbDialog"Renovar cuenta premium\
							"
							
#define donatmoney			""cBLUE"Cambio de moneda"cWHITE"\n\n\
								El cambio de moneda es la transferencia de tus ICoins a la moneda de juego normal.\n\
								La tasa de conversi�n es "cBLUE"1 ICoin = $100"cWHITE".\n\n\
								En su cuenta - "cBLUE"%d"cWHITE" ICoins\n\n\
								"cGRAY"Indica la cantidad de ICoins que quieres intercambiar:\
							"			

// -- Precios --						
const
	PRICE_UNWARN = 60,				// Eliminando Varna
	PRICE_UNWARN_ALL = 100,			// Eliminar todo varnov
	PRICE_CHANGE_ROLE = 45,			// Rol (nacionalidad, edad, pa�s de nacimiento, g�nero)
	PRICE_CHANGE_NATION = 10, 		// nacionalidad
	PRICE_CHANGE_AGE = 20,			// Edad
	PRICE_CHANGE_COUNTRY = 5,		// pa�s de nacimiento
	PRICE_CHANGE_SEX = 15,			// paul
	PRICE_CHANGE_COLOR = 15,		// Color de la piel
	PRICE_CHANGE_NAME = 40,			// Nombre
	PRICE_STYLE = 50,				// estilo de lucha
	PRICE_STYLE_ALL = 150,			// Todos los estilos de combate
	PRICE_UNJOB = 15,				// Contrato de seis horas
	PRICE_NUMBER_CAR = 70,			// N�mero de transporte
	PRICE_NUMBER_PHONE = 60;		// n�mero de tel�fono

							
enum e_PREMIUM
{
	prem_id,				// Incremento de prima
	prem_time,				// Premium Premium Time
	prem_type,				// tipo premium
	
	prem_color,				// Color Premium
	prem_gmoney,			// GMoney por hora
	prem_bank,				// Inter�s para el banco
	prem_salary,			// Porcentaje de salario
	prem_benefit,			// Porcentaje de prestaciones por desempleo
	prem_mass,				// Peso extra en inventario
	
	prem_admins,			// acceso a / admins
	prem_supports,			// Acceso / soportes
	prem_h_payment,			// D�as adicionales a pagar en casa.
	
	prem_car,				// Posibilidad de tener 2 coches.
	prem_house,				// Posibilidad de tener 2 casas.
	prem_business,			// Posibilidad de tener 2 negocios.
	
	prem_house_property,	// Inter�s adicional a la venta de inmuebles.
	
	prem_drop_retreature,	// Reducci�n porcentual de la retextura
	prem_drop_tuning,		// Ajuste de reducci�n porcentual
	prem_drop_repair,		// Porcentaje de reparaci�n reducida
	prem_drop_payment,		// El porcentaje de reducci�n en utilidades.
}

enum e_PREMIUM_INFO
{
	prem_name	[ 16 ],
	prem_price,
	
	prem_color		[2], //+
	prem_gmoney		[2], //+
	prem_bank		[2], //+
	prem_salary		[2], //+
	prem_benefit	[2], //+
	
	prem_mass		[2], //+
	prem_admins		[2], //+
	prem_supports	[2], //+
	prem_h_payment	[2], //+
	
	prem_car		[2], //+
	prem_house		[2], //+
	prem_business	[2], //+
	
	prem_house_property		[2], //+
	
	prem_drop_retreature	[2], //+
	prem_drop_tuning		[2], //+
	prem_drop_repair		[2], //+
	prem_drop_payment		[2], //+
}

enum e_VALUE_PREMIUM
{
	value_amount,		// N�mero de configuraciones seleccionadas
	value_gmoney,		// n�mero de control
	value_days,			// Validez en dias
}

new
	Premium			[ MAX_PLAYERS ][ e_PREMIUM ],
	TimePremium		[ MAX_PLAYERS ][ e_PREMIUM ],
	ValuePremium	[ MAX_PLAYERS ][ e_VALUE_PREMIUM ];
			
new
	premium_info[][ e_PREMIUM_INFO ] = 
	{
		{ "No", 0, 			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },	{ 0, 0 }, { 0, 0 }, 	{ 0, 0 }, { 0, 0 },
		{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },  	{ 0, 0 },  { 0, 0 },  { 0, 0 },  { 0, 0 } },
		
		{ "Inicial", 180, 	{ 1, 5 }, { 0, 0 }, { 1, 10 }, { 7, 15 }, { 20, 5 }, { 2, 30 }, { 0, 0 }, 	{ 1, 10 }, { 0, 0 },
		{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 10, 30 }, 	{ 15, 15 },  { 15, 20 },  { 10, 20 },  { 10, 20 } },
		
		{ "Basico", 400, 	{ 2, 5 }, { 0, 0 }, { 3, 15 }, { 12, 23 }, { 40, 7 }, { 4, 40 }, { 1, 10 }, 	{ 1, 10 }, { 5, 40 }, 
		{ 1, 50 }, { 1, 40 }, { 0, 0 }, { 20, 50 }, { 20, 20 },  { 20, 30 },  { 15, 30 },  { 15, 30 } },
		
		{ "Supremo", 600, 	{ 3, 5 }, { 2, 70 }, { 7, 20 }, { 20, 30 }, { 80, 10 }, { 6, 50 }, { 1, 10 }, 	{ 1, 10 }, { 10, 55 }, 
		{ 1, 50 }, { 1, 40 }, { 1, 40 }, { 30, 60 }, { 30, 30 },  { 30, 40 },  { 20, 40 },  { 20, 40 } },
		
		{ "Personalizado", 0, 	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },	{ 0, 0 }, { 0, 0 }, 	{ 0, 0 },  { 0, 0 },
		{ 0, 0 }, { 0, 0 }, { 0, 0 }, 	{ 0, 0 }, 	{ 0, 0 },  { 0, 0 },  { 0, 0 },  { 0, 0 } }
	},
	
	premium_color[][] = 
	{
		{"{FFFFFF}"},
		{"{46BD38}"},
		{"{E05A5A}"},
		{"{DEB047}"}
	};