
/*new atm_zone[50],
	atm_server;*/
	
enum e_CASHBOX_INFO
{
	c_name[ 32 ],					
	Float: c_cashbox_pos[3],		
}	

//Cajeros Autom�ticos
new const cashbox_info[][ e_CASHBOX_INFO ] = 
{
	{ "Cajero", { 1463.8583, -1006.0172, 2725.8760 } }, // 0
	{ "Cajero", { 1459.5745, -1006.0161, 2725.8760 } }, // 0
	{ "Cajero", { 1455.4482, -1006.0210, 2725.8760 } }, // 0
	{ "Cajero", { 1451.4005, -1006.0151, 2725.8760 } } // 0
};

#define dialog_bank "\
"cBLUE" 1. "cGRAY" Balance \n \
"cBLUE" 2. "cGRAY" Retirar \n \
"cBLUE" 3. "cGRAY" Depositar \n \
"cBLUE" 4. "cGRAY" Transferir \n \
"cBLUE" 5. "cGRAY" Cobrar cheques \n \
"cBLUE" 6. "cGRAY" Servicios de pago \
"

#define dialog_pay "\
"cBLUE" - "cGRAY" Pago de propiedades/expensas \n \
"cBLUE" - "cGRAY" Pago de multas \
"

#define dialog_cashout "\
"cBLUE" Retirar dinero \n \n \
"cWHITE" Ingrese el monto que desea retirar de su cuenta bancaria: \n \
"gbDialog" El monto m�nimo es $ 1, el m�ximo es $ 100,000 \
"
							
#define dialog_cashin "\
"cBLUE" Depositar \n \n \
"cWHITE" Ingrese el monto que desea depositar en la cuenta bancaria: \n \
"gbDialog" El monto m�nimo es $ 1, el m�ximo es $ 100,000 \
"

#define dialog_transfer "\
"cBLUE" Transferir dinero \n \n \
"cWHITE" Ingrese la ID del jugador al que desea transferir dinero a: \
"

#define dialog_transfer_2 "\
"cBLUE" Transferir dinero \n \n \
"cGRAY" Transferencia al jugador: "cBLUE"%s [%d] \n \
"cWHITE" Ingrese el monto a transferir: \n \
"gbDialog" El monto m�nimo es $ 1, el m�ximo es $ 1,000,000 \
"