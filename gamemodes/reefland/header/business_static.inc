#define STATIC_SHOP_GUN			( 0 )
#define STATIC_SHOP_REFILL		( 1 )

const
	MAX_STATIC_ITEMS = 13;

enum e_BUSINESS_STATIC
{
	s_id,		// elemento de identificaci�n
	s_price,	// Precio por art�culo
	s_other,	// Configuraci�n adicional
	s_param,	// Par�metro
}

new static_items[][][ e_BUSINESS_STATIC ] =
{
	{
		{ 105, 150, 0, -1 }, 	// Funda
		{ 32, 400, 0, 1 },		// nudillos de lat�n
		{ 34, 3500, 0, 100 }, 	// Chaleco antibalas
		{ 14, 6000, 1, 17 }, 	// Colt
		{ 17, 8300, 1, 7 }, 	// Deagle
		{ 18, 7500, 2, 8 }, 	// Escopeta
		{ 26, 11000, 2, 16 }, 	// rifle
		{ 22, 10000, 3, 30 }, 	// MP5
		{ 36, 650, 4, 21 },		// Cartuchos Deagle
		{ 35, 750, 4, 51 }, 	// Cartuchos de Colt
		{ 39, 900, 5, 32 }, 	// Rifles de cartucho
		{ 37, 1250, 5, 24 }, 	// Disparo de munici�n
		{ 38, 1400, 6, 60 }		// Cartuchos MP5
	},
	{
		{ 41, 8, 0, -1 },	// Soda
		{ 68, 12, 0, 10 },	// Cigarrillos
		{ 69, 12, 0, 10 },	// Cigarrillos
		{ 55, 12, 0, -1 },	// un vaso de cafe
		{ 91, 20, 0, -1 },	// engrasador
		{ 104, 30, 0, -1 },	// Paquete de carb�n
		{ 89, 40, 0, -1 },	// linterna
		{ 98, 70, 0, -1 },	// Botiqu�n de primeros auxilios
		{ 99, 100, 0, -1 },	// recipiente
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 }
	}
};