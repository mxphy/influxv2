#define FRACTION_CITYHALL		( 1 )
#define FRACTION_POLICE			( 2 )
#define FRACTION_FBI			( 3 )
#define FRACTION_SADOC			( 4 )
#define FRACTION_FIRE			( 5 )
#define FRACTION_HOSPITAL		( 6 )
#define FRACTION_NEWS			( 7 )
#define FRACTION_WOOD			( 8 )

#define CHANNEL_CITYHALL		( 900 )
#define CHANNEL_HOSPITAL		( 903 )
#define CHANNEL_POLICE			( 912 )
#define CHANNEL_FBI				( 913 )
#define CHANNEL_NEWS			( 914 )
#define	CHANNEL_SHERIFF			( 916 )
#define CHANNEL_FIRE			( 991 )
#define CHANNEL_WOOD			( 915 )
#define CHANNEL_SADOC			( 999 )

const
	MAX_POLICECALLS = 20;

// ajustes de rango
enum e_RANK
{
	r_id,				
	r_fracid,				// ID de la facci�n
	r_name[32],				// Nombre del rango
	r_salary,				// Queja
	r_invite,				// �Puede llevar a la facci�n?
	r_uninvite,				// Puede despedir de facci�n
	Float:r_spawn[4],		// El lugar de engendro
	r_world[2],				// El mundo virtual y el interior de la puesta.
	r_attach,				// �Puedes usar archivos adjuntos?
	r_object,				// Puede usar objetos
	r_radio,				// Puede usar ondas de radio
	r_info,					// Puede usar el panel de informaci�n
	r_boot,					// �Puedo usar el ba�l?
	r_spawnveh,
	
	r_mechanic,				// Poderes del mecanico
	r_medic,				// Poderes de un medico
	
	r_gun[10],				// Configuraciones de armas
	r_skin[20],				// ajustes de la piel
	r_vehicles[10],			// Configuraciones para el uso de m�quinas (mantiene modelos de m�quinas)
	r_stock[10],			// Configuraciones de acceso al almac�n
	
	r_add[10],				// Configuraci�n avanzada
}

// Configuraci�n de fracciones
enum e_FRACTION 
{
	f_id,					// ID de la facci�n
	f_name[64],				// El nombre de la facci�n.
	f_short_name[10],		// nombre corto
	f_leader[3],			// l�deres de facciones del uID
	f_skin[20],				// Skins de fracciones - id skins
	f_gun[10],				// Armas de facci�n - ID de armas de gb_inventory
	f_vehicles,				// Limitar el n�mero de veh�culos.
	f_stock[10],			// Almac�n
	f_ranks,				// El n�mero de rangos en la facci�n.
	f_salary,				// Cantidad de la cantidad asignada para salarios
	f_amountveh,			// N�mero de veh�culos creados
	f_members,				// N�mero de jugadores en la facci�n.
}

enum e_MEMBER
{
	m_id,
	m_status,				// en linea o no
	m_name					[ MAX_PLAYER_NAME ],
	m_rank,
	m_lasttime,
}

enum e_VEH
{
	v_id,
	v_number[10],
}

enum e_INFO
{
	i_id,
	i_fracid,
	i_name		[ 26 ],
	i_theme		[ 32 ],
	i_text		[ 1024 ]
}

enum e_POLICE_CALL
{
	p_playerid, 				// ID de la persona que llama
	p_descript[32],				// Descripci�n del lugar de la convocatoria.
	p_zone[28],					// Nombre del distrito de donde vino la llamada.
	Float:p_pos[30],			// Coordenadas del punto de llamada
	p_time,						// tiempo de llamada
	p_number[5],				// N�mero de llamada (aleatorio)
	p_type,						// Tipo de llamada
	p_status,					// Estado de la llamada (aceptado o no)
}

enum e_OBJECT_INFO
{
	obj_name[ 64 ],
	obj_model,
}

enum e_F_OBJECT
{
	f_object_id,
	f_object,
	f_object_model,
	Text3D: f_object_text,
}

enum e_F_ATTACH
{
	f_a_name [ 64 ],
	f_a_object,
}

enum e_FRACTION_STOCK
{
	s_skin,
	s_world,
	s_anim,
	Float:s_pos[4],
	s_member,
}

enum e_THORN
{
	bool:t_status,
	t_object,
	Text3D: t_object_text,
	Float: t_pos[3],
}

// M�quinas fraccionarias disponibles - max 76
new
	const vehicles_available[][] = 
	{
		{//1
			408, 409, 413, 414, 421, 422, 423, 428, 431, 437, 454, 482, 485, 486, 
			487, 489, 490, 497, 507, 524, 525, 530, 552, 560, 572, 574, 579, 580, 
			582, 583, 586, 588, 426, 598, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0
		},
		{//2
			400, 402, 529, 405, 411, 412, 413, 415, 417, 420, 421, 422, 423, 424, 
			426, 427, 429, 430, 431, 437, 438, 439, 442, 445, 448, 460, 466, 468, 
			469, 470, 471, 473, 477, 479, 482, 485, 486, 487, 489, 490, 492, 496, 
			497, 505, 510, 521, 523, 524, 525, 528, 534, 535, 536, 541, 552, 559, 
			560, 561, 563, 566, 567, 568, 574, 579, 582, 583, 588, 593, 596, 597, 
			598, 599, 600, 601, 606, 611
		},
		{//3
			400, 421, 423, 426, 428, 458, 466, 482, 490, 492, 507, 508, 525, 542,
			551, 560, 566, 579, 582, 588, 427, 528, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0
		},
		{//4
			400, 416, 426, 428, 431, 482, 489, 497, 525, 528, 552, 560, 563, 579, 
			597, 599, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0
		},
		{//5
			400, 403, 405, 407, 413, 414, 416, 417, 418, 421, 422, 424, 426, 427, 
			428, 431, 433, 437, 440, 442, 443, 455, 456, 457, 460, 470, 471, 472, 
			473, 482, 485, 486, 487, 489, 490, 495, 497, 498, 499, 500, 505, 514, 
			515, 516, 525, 530, 539, 544, 547, 548, 551, 552, 553, 554, 560, 561, 
			563, 572, 573, 574, 577, 578, 579, 582, 583, 584, 586, 591, 592, 593, 
			599, 609, 0, 0, 0, 0
		},
		{//6
			413, 416, 426, 437, 442, 482, 487, 490, 525, 552, 563, 457, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0
		},
		{//7
			400, 402, 405, 409, 415, 421, 426, 428, 437, 445, 458, 461, 482, 488, 
			489, 496, 500, 507, 518, 521, 525, 526, 529, 533, 550, 551, 552, 554, 
			558, 560, 560, 561, 579, 580, 582, 585, 587, 589, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0
		},
		{//8
			400, 413, 422, 424, 453, 457, 460, 468, 469, 471, 472, 473, 482, 489, 
			490, 500, 525, 552, 554, 568, 572, 579, 598, 427, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0
		}
	},
	// Transporte del spawn al comprar en facci�n.
	Float:vehicles_spawn[][][] = 
	{
		{
			{ 2678.2979,-2375.7632,13.8708,180.9957 },
			{ 2653.7488,-2374.9897,13.8702,181.2036 },
			{ 2628.7720,-2374.6699,13.8684,177.6111 }
		},
		{
			{ 2683.6575,-2305.7805,0.2405,0.7052 },
			{ 2701.9604,-2304.8640,0.2155,1.2437 },
			{ 2721.5989,-2305.5090,0.2678,357.9897 }
		},
		{
			{ 1849.8647,-2424.4675,14.4708,177.9145 },
			{ 2105.8142,-2591.8381,14.4631,91.4890 },
			{ 2104.4827,-2494.0994,14.4591,90.4423 }
		}
	},
	
	vehicles_description[][] = 
	{
		{ "El veh�culo est� en los muelles de Los Santos." },
		{ "El veh�culo est� en los muelles de Los Santos." },
		{ "El veh�culo est� en el aeropuerto de Los Santos." }
	},
	
	FracColor[][] = 
	{ 
		{
			0x7C849900,
			0x1B376D00,
			0x06003600,
			0x5A575200,
			0x620B1C00,
			0x4C75B700,
			0x1E999900,
			0x26420100,
			0xFFFFFF00,
			0xFFFFFF00,
			0xFFFFFF00,
		},
		{
			0xFFFFFF00,
			0xFFFFFF00,
			0xFFFFFF00,
			0xFFFFFF00,
			0xFFFFFF00,
			0xFFFFFF00,
			0xFFFFFF00,
			0xFFFFFF00,
			0xFFFFFF00,
			0xFFFFFF00,
			0xFFFFFF00
		}
	},
	
	Float:zone_gov_repair[][] = 
	{ 
		{ 2695.3782, -2630.6050, 2806.9463, -2330.0547 },
		{ 1886.7932, -2477.8240, 1983.6055, -2230.8225 }
	},
	
	f_objects[][][ e_OBJECT_INFO ] = 
	{
		{//CITYHALL
			{ "Barril", 3632 },
			{ "Reja", 2933 },
			{ "Tuberia gigante", 18983 },
			{ "Tuberia peque�a", 14397 },
			{ "Base de gr�a", 1391 },
			{ "Brazo de gr�a", 1394 },
			{ "Gancho de grua", 1393 },
			{ "Cajas", 2991 },
			{ "Caja grande", 3799 },
			{ "Cono", 1238 },
			{ "Se�al de tr�fico", 16023 },
			{ "Escalera", 1428 },
			{ "Escalera con plataforma", 12950 },
			{ "Basurero", 1339 },
			{ "Basurero grande", 1372 },
			{ "Farola roja", 19124 },
			{ "Se�al de carretera 'Desv�o'", 1425 },
			{ "Valla peque�a", 1427 },
			{ "Valla de concreto", 1422 },
			{ "Valla grande", 1459 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//POLICE
			{ "Farola azul", 19122 },
			{ "Cono", 1238 },
			{ "Valla peque�a", 1427 },
			{ "Valla grande", 1459 },
			{ "Valla de concreto", 1422 },
			{ "Valla con lamparas", 1423 },
			{ "Cinta de interior", 2773 },
			{ "Barril de arena", 1237 },
			{ "Baches", 19425 },
			{ "Se�al de carretera 'Desv�o'", 1425 },
			{ "Se�al de tr�fico 'Izquierda'", 19951 },
			{ "Se�al de tr�fico 'Derecha'", 19952 },
			{ "Se�al de tr�fico 'Cerrado'", 19950 },
			{ "Se�al de tr�fico 'L�mite de velocidad'", 11699 },
			{ "Se�al de tr�fico 'STOP'", 19966 },
			{ "Escalera grande", 1437 },
			{ "Mesa", 2637 },
			{ "Vaso de cafe", 19835 },
			{ "Laptop", 19893 },
			{ "Micr�fono", 19610 },
			{ "Soporte de micr�fono", 19611 },
			{ "Bolsa m�dica roja", 11738 },
			{ "Extintor de incendios", 366 },
			{ "Plano", 3111 },
			{ "Bolsa con un cad�ver", 19944 },
			{ "Bandera de los Estados Unidos", 11245 },
			{ "Maniqu� blanco", 2411 },
			{ "Maniqu� negro", 2407 },
			{ "Luz", 18728 },
			{ "Mancha de sangre", 19836 }
		},
		{//FBI
			{ "Laptop", 19893 },
			{ "Mesa", 19997 },
			{ "Bolsa medica", 11738 },
			{ "Bolso negro", 11745 },
			{ "Logo FBI", 19777 },
			{ "Cinta amarilla", 19834 },
			{ "Mancha de sangre", 19836 },
			{ "Se�al 'DO NOT ENTER'", 19967 },
			{ "Posador de pistola", 19995 },
			{ "Pistola disparada", 346 },
			{ "Cuchillo", 335 },
			{ "Mapa del estado", 19171 },
			{ "Tarjeta de identidad", 19792 },
			{ "Maletin", 1210 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//SADOC
			{ "Farola azul", 19122 },
			{ "Cono", 1238 },
			{ "Valla peque�a", 1427 },
			{ "Valla grande", 1459 },
			{ "Valla de concreto", 1422 },
			{ "Valla con lamparas", 1423 },
			{ "Cinta de interior", 2773 },
			{ "Barril de arena", 1237 },
			{ "Silla de ruedas", 1997 },
			{ "Bolsa m�dica roja", 11738 },
			{ "Bandera de los Estados Unidos", 11245 },
			{ "Vaso de cafe", 19835 },
			{ "Plano", 3111 },
			{ "Bolsa con un cad�ver", 19944 },
			{ "Bandera de los Estados Unidos", 11245 },
			{ "Maniqu� blanco", 2411 },
			{ "Maniqu� negro", 2407 },
			{ "Luz", 18728 },
			{ "Mancha de sangre", 19836 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//FIRE
			{ "Barril", 1237 },
			{ "Ventilador", 919 },
			{ "Motor hidraulico", 920 },
			{ "Grifo", 1366 },
			{ "Barrera de carretera", 1424 },
			{ "Barrera de contenci�n grande", 979 },
			{ "Se�al de tr�fico 'HAZMAT'", 11700 },
			{ "Silla de ruedas", 1997 },
			{ "Bolsa m�dica roja", 11738 },
			{ "Cono", 1238 },
			{ "Alfombra azul", 2632 },
			{ "Escalera 1", 13011 },
			{ "Escalera 2", 12987 },
			{ "Laptop", 19893 },
			{ "Extintor", 2690 },
			{ "Barreras para cerrar carretera", 981 },
			{ "Plano", 3111 },
			{ "Luces grandes con generador", 3864 },
			{ "Luz blanca", 19279 },
			{ "Luz amarilla intermitente", 19150 },
			{ "Mesa", 941 },
			{ "Farola roja", 19124 },
			{ "Escalera de emergencia para edificios.", 9618 },
			{ "Toldo de rescate", 14449 },
			{ "Maniqu� blanco", 2411 },
			{ "Gr�a pesada", 18248 },
			{ "Bandera de los Estados Unidos", 11245 },
			{ "Luz", 18728 },
			{ "Escalera grande", 1437 },
			{ "Contenedor de carga", 2934 }
		},
		{//HOSPITAL
			{ "Cabeza de un cad�ver", 2908 },
			{ "Torso de un cad�ver", 2907 },
			{ "Mano de un cad�ver", 2906 },
			{ "Pierna de un cad�ver", 2905 },
			{ "Limpiapisos", 1778 },
			{ "Laptop", 19893 },
			{ "Bandeja de metal", 19809 },
			{ "Tanque de oxigeno", 19816 },
			{ "Maniqu� negro", 2407 },
			{ "Maniqu� blanco", 2411 },
			{ "Monitor", 14532 },
			{ "Silla de ruedas", 932 },
			{ "Silla", 19994 },
			{ "Silla tapizada", 2776 },
			{ "Proyector", 19279 },
			{ "Pechera", 19919 },
			{ "Bolso cad�ver", 19944 },
			{ "Ata�d de madera", 19339 },
			{ "Silla de ruedas", 1997 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//NEWS
			{ "Teclado", 19808 },
			{ "Vaso de cafe", 19835 },
			{ "Laptop", 19893 },
			{ "Bolsa de viaje", 11745 },
			{ "Soporte de micr�fono", 19611 },
			{ "Micr�fono", 19610 },
			{ "Recipiente", 19621 },
			{ "Cinta interior", 2773 },
			{ "Mesa", 19997 },
			{ "Silla de oficina", 19999 },
			{ "Cajas", 2654 },
			{ "Taburete de bar", 2125 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//WOOD
			{ "Farola azul", 19122 },
			{ "Cono", 1238 },
			{ "Valla peque�a", 1427 },
			{ "Valla grande", 1459 },
			{ "Valla de concreto", 1422 },
			{ "Valla con lamparas", 1423 },
			{ "Cinta interior", 2773 },
			{ "Barril de arena", 1237 },
			{ "Bache", 19425 },
			{ "Se�al de trafico 'Desv�o'", 1425 },
			{ "Se�al de trafico 'Izquierda'", 19951 },
			{ "Se�al de trafico 'Derecha'", 19952 },
			{ "Se�al de tr�fico 'Cerrado'", 19950 },
			{ "Se�al de tr�fico 'L�mite de velocidad'", 11699 },
			{ "Se�al de tr�fico 'STOP'", 19966 },
			{ "Escalera", 1437 },
			{ "Mesa", 2637 },
			{ "Vaso de cafe", 19835 },
			{ "Laptop", 19893 },
			{ "Extintor", 366 },
			{ "Plano", 3111 },
			{ "Bandera de los Estados Unidos", 11245 },
			{ "Luz", 18728 },
			{ "Silla de ruedas", 1997 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		}
	},
	
	f_attach[][][ e_F_ATTACH ] = 
	{
		{//CITYHALL
			{ "Respirador", 19472 },
			{ "Mascarilla", 19036 },
			{ "Casco de construcci�n", 18638 },
			{ "Rastrillo", 18890 },
			{ "Destornillador", 18644 },
			{ "Martillo", 18635 },
			{ "Martillo 2", 19631 },
			{ "Crowbar", 18634 },
			{ "Bolso negro", 2919 },
			{ "Caja roja", 19921 },
			{ "Chaleco de viaje", 19904 },
			{ "Bolsa de basura", 1265 },
			{ "Caja", 1220 },
			{ "Pala", 19626 },
			{ "Llave inglesa", 19627 },
			{ "Trapeador", 19622 },
			{ "Ladrillo rojo", 11708 },
			{ "Walkie talkie", 19942 },
			{ "Bandera de los Estados Unidos", 11245 },
			{ "Linterna", 18641 },
			{ "Tarjeta", 19792 },
			{ "Micr�fono", 19610 },
			{ "Baston", 19348 },
			{ "Bandera roja", 19306 },
			{ "Bandera azul", 19307 },
			{ "Maletin", 1210 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//POLICE
			{ "Armadura de cuerpo SWAT", 19142 },
			{ "Casco SWAT", 19141 },
			{ "Casco de moto", 19200 },
			{ "Chaleco reflectante", 19904 },
			{ "Insignia de oficial", 19775 },
			{ "Insignia de sheriff", 19347 },
			{ "Gorra de b�isbol de la polic�a", 19161 },
			{ "Sombrero de sheriff", 19099 },
			{ "Gorra", 19521 },
			{ "Funda", 19773 },
			{ "Mascara de gas", 19472 },
			{ "Dispositivo de visi�n nocturna", 3070 },
			{ "Walkie talkie", 19942 },
			{ "Micr�fono", 19610 },
			{ "Auriculares", 19421 },
			{ "Bandera de los Estados Unidos", 11245 },
			{ "Puntero laser", 18643 },
			{ "Gafas negras", 19138 },
			{ "Gafas rojas", 19139 },
			{ "Gafas azules", 19140 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//FBI
			{ "Linterna", 18641 },
			{ "Funda", 19773 },
			{ "Esposas", 11750 },
			{ "Walkie talkie", 19942 },
			{ "Armadura de cuerpo", 19142 },
			{ "Certificado del fbi", 19776 },
			{ "Logo del fbi", 19777 },
			{ "Gorra 'policia'", 18636 },
			{ "Tarjeta de identidad", 19792 },
			{ "Maletin", 1210 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//SADOC
			{ "Armadura de cuerpo", 19515 },
			{ "Casco SWAT", 19141 },
			{ "Chaleco reflectante", 19904 },
			{ "Insignia de oficial", 19775 },
			{ "Gorra de b�isbol de la polic�a", 19161 },
			{ "Sombrero de sheriff", 19099 },
			{ "Gorra", 19521 },
			{ "Funda", 19773 },
			{ "Mascara de gas", 19472 },
			{ "Walkie talkie", 19942 },
			{ "Micr�fono", 19610 },
			{ "Auriculares", 19421 },
			{ "Bandera de los Estados Unidos", 11245 },
			{ "Puntero laser", 18643 },
			{ "Gafas negras", 19138 },
			{ "Gafas rojas", 19139 },
			{ "Gafas azules", 19140 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//FIRE
			{ "Casco balistico", 19514 },
			{ "Bolso de mano param�dico", 19559 },
			{ "Armadura de cuerpo", 19515 },
			{ "Llave inglesa", 19627 },
			{ "Bolsa de viaje", 11745 },
			{ "Chaleco reflectante", 19904 },
			{ "Cilindro de oxigeno (espalda)", 1008 },
			{ "Cilindro de ox�geno (simple)", 1009 },
			{ "Cilindro de ox�geno (doble)", 1010 },
			{ "Botiqu�n rojo de primeros auxilios", 11738 },
			{ "Martillo", 19631 },
			{ "Pala", 19626 },
			{ "Lentes de visi�n nocturna", 368 },
			{ "Casco de bombero (negro)", 19331 },
			{ "Casco de bombero (amarillo)", 19330 },
			{ "Crowbar", 18634 },
			{ "Walkie talkie", 19942 },
			{ "Mascara de gas", 19472 },
			{ "Casco de construccion", 18638 },
			{ "Llave de ducha", 18633 },
			{ "Linterna", 18641 },
			{ "Sombrero polic�a", 19520 },
			{ "Gorra", 19521 },
			{ "Casco para pilotos", 19200 },
			{ "Casco HAZMAT (blanco)", 18978 },
			{ "Casco HAZMAT (rojo)", 18977 },
			{ "Casco de rescate", 19116 },
			{ "Casco EMS LT", 19117 },
			{ "Casco EMS CPT", 19118 },
			{ "Casco EMS BC", 19119 }
		},
		{//HOSPITAL
			{ "Botiqu�n de primeros auxilios", 11736 },
			{ "Mascarilla", 18917 },
			{ "Gorra", 18908 },
			{ "Trapeador", 19622 },
			{ "Armadura de cuerpo", 19515 },
			{ "Walkie talkie", 19942 },
			{ "Linterna", 18641 },
			{ "Tarjeta", 19792 },
			{ "Insignia", 19347 },
			{ "Buscador de personas", 18875 },
			{ "Insignia", 1581 },
			{ "Barra metalica", 11716 },
			{ "Casco", 19514 },
			{ "Destornillador", 18644 },
			{ "Llave inglesa", 19627 },
			{ "Recipiente", 19621 },
			{ "Contenedor medico", 11738 },
			{ "Bandeja de metal", 19809 },
			{ "Respirador", 19472 },
			{ "Camara", 19623 },
			{ "Bolsa", 11745 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//NEWS
			{ "Armadura de cuerpo", 19515 },
			{ "Bolso negro", 2919 },
			{ "Linterna", 18641 },
			{ "Chaleco reflectante", 19904 },
			{ "Maletin", 1210 },
			{ "Micr�fono", 19610 },
			{ "Tarjeta", 19792 },
			{ "Camara", 19623 },
			{ "Auriculares", 19421 },
			{ "Llave inglesa", 19627 },
			{ "Insignia", 1581 },
			{ "Taser de mano", 18642 },
			{ "Basura", 18634 },
			{ "Destornillador", 18644 },
			{ "Reloje de pulsera", 19039 },
			{ "Maletin", 19624 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		},
		{//WOOD
			{ "Armadura de cuerpo", 19515 },
			{ "Funda", 19773 },
			{ "Gorra de b�isbol de la polic�a", 19161 },
			{ "Chaleco reflectante", 19904 },
			{ "Insignia del sheriff", 19347 },
			{ "Sombrero de sheriff", 19099 },
			{ "Walkie talkie", 19942 },
			{ "Linterna", 18641 },
			{ "Pala", 19626 },
			{ "Contenedor medico", 11738 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 },
			{ "", 0 }
		}
	},
	
	FStock[][ e_FRACTION_STOCK ] = 
	{
		{ 148, 7, 0, { 1481.7131, -1743.2094, 4510.0859, 271.6391 }, 1 },
		{ 280, 1, 0, { 1545.6333, -1361.8879, 2494.2449, 267.9406 }, 2 },
		{ 280, 2, 0, { -140.4708, -1877.5405, 2501.0859, 270.0072 }, 2 },
		{ 280, 3, 0, { 392.0634, -1882.4570, 2501.0859, 271.7435 }, 2 },
		{ 282, 1, 0, { 641.8748, -573.8699, 1994.0859, 183.7193 }, 2 },
		{ 286, 2, 0, { 316.1080, -133.7561, 999.6016, 92.9351 }, 3 },
		{ 124, 2, 0, { 316.2255, -137.3013, 1004.0625, 86.4441 }, 3 },
		{ 274, 90, 1, { 998.5496, -1601.6605, 5601.0859, 9.2271 }, 5 }, 	//seat
		{ 42, 0, 0, { 1127.2400, -1649.4852, 13.9520, 223.5258 }, 5 },
		{ 268, 0, 2, { 1060.3320, -1694.4542, 13.5490, 187.5155 }, 5 }, 	//lean 3
		{ 276, 88, 3, { 1309.2961, -939.9826, 4001.0840, 272.1163 }, 5 },	//sit
		{ 268, 0, 4, { 1308.3571, -853.6664, 39.5885, 183.7319 }, 5 }, 		//lean 2
		{ 276, 89, 0, { -907.0093, -543.1705, 3501.0859, 153.3383 }, 5 },
		{ 276, 0, 4, { 2781.9429, -1452.9865, 30.4500, 183.1519 }, 5 }, 	//lean 2
		{ 304, 0, 0, { 1892.6017, -2237.4360, 13.5469, 218.8723 }, 5 },
		{ 50, 0, 0, { 2695.1111, -2470.9912, 15.4160, 244.5974 }, 5 },
		{ 71, 63, 0, { 1126.4724, -1351.1367, 2804.4561, 276.6992 }, 6 },
		{ 217, 77, 0, { 1660.0588, -1654.5403, 1701.9738, 94.4047 }, 7 },
		{ 283, 0, 0, { -2197.9624, -2329.6497, 30.6250, 316.1905 }, 8 }
	},
	
	weapon_amount[] = { 102, 102, 3, 42, 40, 22, 42, 200, 120, 150, 400, 200, 30, 30, 100, 20000, 20000, 1 };
	
enum e_GARAGE
{
	g_world,				// mundo virtual
	
	Float:g_entry[3], 		// coordenadas de entrada
	Float:g_exit[3], 		// coordenadas de salida
	
	Float:g_pos_outside[4], // Coordenadas de teletransporte fuera
	Float:g_pos_inside[4], // Las coordenadas del telepuerto interior.
}
	
new
	garage_info[][ e_GARAGE ] = 
	{
		{ 1, { 1588.4397, -1637.8821, 13.4311 }, { -1556.9786, 1492.3080, 2399.7500 }, { 1588.2620, -1633.7195, 13.5251, 359.0667 }, { -1560.7335, 1492.6445, 2399.8813, 90.0176 } }, 		// Departamento de Polic�a
		{ 2, { 2821.5723, -1179.6493, 25.6000 }, { -1556.9786, 1492.3080, 2399.7500 }, { 2822.0562, -1183.6140, 25.7277, 187.6449 }, { -1560.7335, 1492.6445, 2399.8813, 90.0176 } }, 		// 77
		{ 3, { 2191.6809, -2665.8721, 13.5650 }, { -1556.9786, 1492.3080, 2399.7500 }, { 2192.1316, -2661.5378, 13.6881, 0.2470 }, { -1560.7335, 1492.6445, 2399.8813, 90.0176 } },	  		// puerto
		{ 63, { 2012.2611, -1408.4677, 17.0098 }, { -1559.5399, 1469.6260, 2955.7520 }, { 2012.4672, -1412.3362, 17.2375, 179.5124 }, { -1559.5443, 1466.0485, 2955.9880, 179.3363 } },	  	// hospital
		{ 63, { 2038.0995, -1438.2292, 17.2595 }, { -1573.9810, 1484.3020, 2955.7520 }, { 2034.1841, -1437.9784, 17.4837, 89.0610 }, { -1577.5474, 1484.3190, 2955.9880, 90.4615 } }	  	// hospital
	};
	
new
	Fraction				[ MAX_FRACTIONS ][ e_FRACTION ],
	FRank					[ MAX_FRACTIONS ][ MAX_RANKS ][ e_RANK ],
	FNameLeader				[ MAX_FRACTIONS ][ 3 ][ MAX_PLAYER_NAME ],
	FMember					[ MAX_FRACTIONS ][ MAX_MEMBERS ][ e_MEMBER ],
	FVehicle				[ MAX_FRACTIONS ][ MAX_FRACVEHICLES ][ e_VEH ],
	FInfo					[ MAX_FRACTIONS ][ MAX_POSTS ][ e_INFO ],
	CPolice					[ MAX_POLICECALLS ][ e_POLICE_CALL ],
	Object					[ MAX_FRACTIONS ][ MAX_OBJECT ][ e_F_OBJECT ],
	Thorn					[ MAX_PLAYERS ][ e_THORN ];
	
new
	area_gov_repair [ 2 ];

#define fraction_change		"\
								"cWHITE"nombre\n\
								abreviaci�n\n\
								l�mite de coches\n\
								skins de organizaci�n\n\
								armas disponibles\n\
								almac�n\n\
								sueldo\
							"
							
#define fpanel_dialog		"\
								"cBLUE"1. "cWHITE"Lista de empleados\n\
								"cBLUE"2. "cWHITE"Lista de rangos\n\
								"cBLUE"3. "cWHITE"Veh�culos\n\
								"cBLUE"4. "cWHITE"Tablero de informaci�n\
							"							
							
#define fpanel_settings		"\
								"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
								Cambiar nombre\n\
								Salario\t"cBLUE"$%d"cWHITE"\n\
								Invitar jugadores\t"cBLUE"%s"cWHITE"\n\
								Despedir jugadores\t"cBLUE"%s"cWHITE"\n\
								Configuraci�n del tablero de informaci�n\t"cBLUE"%s"cWHITE"\n\
								Uso walkie talkies\t"cBLUE"%s"cWHITE"\n\
								Uso de archivos adjuntos\t"cBLUE"%s"cWHITE"\n\
								Instalaci�n de art�culos\t"cBLUE"%s"cWHITE"\n\
								Uso facci�n\t"cBLUE"%s"cWHITE"\n\
								Spawn de coches\t"cBLUE"%s"cWHITE"\n\
								Poderes mecanicos\t"cBLUE"%s"cWHITE"\n\
								Poderes m�dicos\t"cBLUE"%s"cWHITE"\n\
								Uso de veh�culos\n\
								Uso de skins\n\
								Config. Rangos\n\
								Permisos de stock\n\
								Permisos de armas\n\
								Avanzado\n\
								Establecer rango de jugador\t"cGRAY"/darrango\n\
								"gbDialog"Eliminar rango\
							"
							
#define fpanel_add_pd		"\
								"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
								Servicio 911\t"cBLUE"%s"cWHITE"\n\
								Licencias\t"cBLUE"%s"cWHITE"\n\
								Uso mu�ecos\t"cBLUE"%s"cWHITE"\n\
								Declaraci�n de multas\t"cBLUE"%s"cWHITE"\n\
								Computadora a bordo\t"cBLUE"%s"cWHITE"\n\
								Configurar 'oficial de polic�a'\t"cBLUE"%s"cWHITE"\n\
								Ajuste de 'cadete'\t"cBLUE"%s\
							"

#define fpanel_add_fd		"\
								"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
								Servicio 911\t"cBLUE"%s\n\
								Extinci�n de incendios\t"cBLUE"%s\
							"
							
#define fpanel_add_medic	"\
								"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
								Servicio 911\t"cBLUE"%s\n\
								Seguro de salud\t"cBLUE"%s\
							"
							
#define fpanel_add_wood		"\
								"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
								Servicio 911\t"cBLUE"%s\
							"

#define fpanel_add_fbi		"\
								"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
								Servicio 911\t"cBLUE"%s\
							"
							
							
#define fpanel_add_sadoc	"\
								"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
								Abrir cerraduras\t"cBLUE"%s\n\
								Computadora\t"cBLUE"%s\n\
								Exenci�n de libertad condicional\t"cBLUE"%s\
							"

#define fpanel_add_san		"\
								"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
								Panel de noticias\t"cBLUE"%s\
							"
							
#define fpanel_add_ch		"\
								"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
								Visualizaci�n y gesti�n de casos\t"cBLUE"%s\n\
								Crear documentos\t"cBLUE"%s\
							"
							
#define fpanel_vehicles		"\
								"cWHITE"Acci�n\t"cWHITE"Valor\n\
								Lista de veh�culos\n\
								Compra de vehiculos\n\
								Devolver los veh�culos a la plaza de aparcamiento.\t"cGRAY"/fixcar"cWHITE"\n\
								Devuelve todos los veh�culos a las plazas de aparcamiento.\t"cGRAY"/fixcarall"cWHITE"\n\
								Estacionar un veh�culo\t"cGRAY"/estacionar"cWHITE"\n\
								Borrar veh�culo\
							"	
							
#define attach_bone 		""cGRAY"� Seleccione parte del cuerpo:\n\
							"cBLUE"1."cGRAY" Espalda\n\
							"cBLUE"2."cGRAY" Cabeza\n\
							"cBLUE"3."cGRAY" Brazo izquierdo\n\
							"cBLUE"4."cGRAY" Hombro derecho\n\
							"cBLUE"5."cGRAY" Mano izquierda\n\
							"cBLUE"6."cGRAY" Mano derecha\n\
							"cBLUE"7."cGRAY" Cadera izquierda\n\
							"cBLUE"8."cGRAY" Muslo derecho\n\
							"cBLUE"9."cGRAY" Pierna izquierda\n\
							"cBLUE"10."cGRAY" Pierna derecha\n\
							"cBLUE"11."cGRAY" Pantorrilla derecha\n\
							"cBLUE"12."cGRAY" Pantorrilla izquierda\n\
							"cBLUE"13."cGRAY" Antebrazo izquierdo\n\
							"cBLUE"14."cGRAY" Antebrazo derecho\n\
							"cBLUE"15."cGRAY" Clav�cula izquierda\n\
							"cBLUE"16."cGRAY" Clav�cula derecha\n\
							"cBLUE"17."cGRAY" Cuello\n\
							"cBLUE"18."cGRAY" Mandibula"	

#define fstock_dialog		"\
								"cBLUE"1. "cWHITE"Ropa\n\
								"cBLUE"2. "cWHITE"Armas\n\
								"cBLUE"3. "cWHITE"Otros\
							"	

#define add_news			"\
								"cBLUE"A�adir noticias\n\
								"cWHITE"Tema: "cBLUE"%s\n\n\
								"cWHITE"Introduce el texto de la noticia:\n\n\
								"cGRAY"Ingresa el texto en filas. En una l�nea no m�s de 128 caracteres,\n\
								y no m�s de 1024 caracteres en total.\n\
								Deje el resto en blanco al completar el texto de la noticia.\
							"							