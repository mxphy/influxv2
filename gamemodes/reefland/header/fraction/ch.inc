const
	MAX_RECOURSE = 30,
	MAX_DOCUMENT = 20;
	
enum e_RECOURSE
{
	r_id,								// ID de referencia
	r_date,								// Fecha de apelaci�n
	r_nameto	[ MAX_PLAYER_NAME ],	// a
	r_text		[ 1024 ],				// texto
	r_namefrom	[ MAX_PLAYER_NAME ],	// desde
	r_answer	[ 256 ],				// respuesta
	r_nameans	[ MAX_PLAYER_NAME ],	// quien contest�
	r_status,							// Estado: visto o no
}

enum e_DOCUMENT
{
	d_id,							// ID del documento
	d_type,							// Tipo de documento
	d_date,							// Fecha del documento
	d_text	[ 512 ],				// Documento de texto
	d_name	[ MAX_PLAYER_NAME ],	// Nombre del originador
}

new
	Recourse	[ MAX_RECOURSE ]	[ e_RECOURSE ],
	Document	[ MAX_PLAYERS ]		[ MAX_DOCUMENT ]	[ e_DOCUMENT ];
	
new 
	document_type[][] =	
	{
		{ "Carta de negocios" },
		{ "Reclamaci�n" },
		{ "Protocolo" },
		{ "Acta" },
		{ "Orden" },
		{ "Declaraci�n" },
		{ "Resumen de" },
		{ "Descripci�n del trabajo" },
		{ "Poder de abogado" },
		{ "Memos" }
	}; 
	
#define reception_info				"\
										"cBLUE"Recepci�n del alcalde\n\n\
										"cWHITE"Todo ciudadano puede hablar p�blicamente al alcalde de Los Santos.\n\
										Debe presentar un tipo de solicitud o queja, dejando su explicaci�n correspondiente.\n\
										Ser� revisada lo antes posible.\
									"
	
#define	dialog_reception			"\
										"cWHITE"Dejar solicitud\n\
										"cWHITE"Respuesta a la solicitud.\
									"