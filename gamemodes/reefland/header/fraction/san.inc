const
	NEWS_NUMBER = 777;
	
enum e_AD 
{
	a_text[ 100 ],
	a_name[ MAX_PLAYER_NAME ],
	a_phone,
	bool:a_used,
}

new 
	NETWORK_ZONE,
	NETWORK_COFFER,
	NETWORK_ADPRICE,
	bool:ETHER_CALL,
	bool:ETHER_SMS,
	AD				[ MAX_ADVERT_INFO ][ e_AD ],
	ETHER_STATUS 	= INVALID_PARAM,
	ETHER_CALLID	= INVALID_PARAM;

#define dialog_sanpanel		"\
								"cWHITE"Lista de anuncios - "cBLUE"%d nuevos\n\
								"cWHITE"Transmisión en vivo - %s\n\
								"cWHITE"Invitar un jugador a la transmisión\n\
								"cWHITE"Expulsar un jugador de la transmisión\n\
								"cWHITE"Recibe llamadas en la transmisión - %s\n\
								"cWHITE"Recibe SMS en la transmisión - %s\n\
								"cWHITE"Cambiar el costo del anuncio - "cBLUE"$%d\n\
								"cWHITE"Presupuesto de organizacion - "cBLUE"$%d\
							"

#define dialog_advert		"\
								"cBLUE"1. "cGRAY"Publicar anuncio\n\
								"cBLUE"2. "cGRAY"Editar anuncio\n\
								"cBLUE"3. "cGRAY"Eliminar anuncio\
							"