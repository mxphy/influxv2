// precio
#define REPAIR_MECH_ENGINE 	(3.5)
#define REPAIR_MECH_BODY 	(1)
#define REPAIR_MECH_LIGHT 	(0.5)
#define REPAIR_MECH_WHEELS 	(0.5)
#define REPAIR_MECH_COMPLEX (2.5)

#define REPAIR_ENGINE 		(5)
#define REPAIR_BODY 		(2)
#define REPAIR_LIGHT 		(0.5)
#define REPAIR_WHEELS 		(1)

const
	// Porcentaje de pintura sobre el precio del auto
	PRICE_COLOR_1 = 5,
	PRICE_COLOR_2 = 2,
	PRICE_AERO = 18,
	PRICE_HYDRAULICS = 20;

	// Porcentaje de la reparaci�n del costo de un autom�vil C MEC�NICA
	/*REPAIR_MECH_ENGINE = 7,
	REPAIR_MECH_BODY = 2,
	REPAIR_MECH_LIGHT = 1,
	REPAIR_MECH_WHEELS = 1,
	REPAIR_MECH_COMPLEX = 5,*/
	
	// Porcentaje de reparaciones en el costo de autom�viles SIN MEC�NICA
	/*REPAIR_ENGINE = 10,
	REPAIR_BODY = 4,
	REPAIR_LIGHT = 1,
	REPAIR_WHEELS = 2;*/

new
	Text:car_tuning			[ 9 ],
	PlayerText:tuning_price	[ MAX_PLAYERS ],
	PlayerText:tuning_name	[ MAX_PLAYERS ],
	g_tuning_select			[ MAX_PLAYERS ];
									
enum e_COMPONENTS
{
    c_id,
    c_name[35],
	Float:c_price
};

new ComponentsInfo [][][ e_COMPONENTS ] = 
{
	// Ruedas 17 3%
	{
		{ 1073, "Ruedas Shadow", 5.7 },
		{ 1074, "Ruedas Mega", 6.3 },
		{ 1075, "Ruedas Rimshine", 6.0 },
		{ 1076, "Ruedas Wires", 7.6 },
		{ 1077, "Ruedas Classic", 8.0 },
		{ 1078, "Ruedas Twist", 8.5 },
		{ 1079, "Ruedas Cutter", 6.4 },
		{ 1080, "Ruedas Switch", 6.8 },
		{ 1081, "Ruedas Grove", 6.1 },
		{ 1082, "Ruedas Import", 5.9 },
		{ 1083, "Ruedas Dollar", 6.4 },
		{ 1084, "Ruedas Trance", 6.4 },
		{ 1085, "Ruedas Atomic", 7.4 },
		{ 1025, "Ruedas Offroad", 6.2 },
		{ 1096, "Ruedas Ahab", 5.9 },
		{ 1097, "Ruedas Virtual", 5.5 },
		{ 1098, "Ruedas Access", 5.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 }
	},
	// Parachoques delantero 23 8% -1
	{
		{ 1117, "Parachoques delantero Chrome", 6.8 },
		{ 1152, "Parachoques delantero X-Flow", 10.0 },
		{ 1153, "Parachoques delantero Alien", 12.0 },
		{ 1155, "Parachoques delantero Alien", 11.5 },
		{ 1157, "Parachoques delantero X-Flow", 11.0 },
		{ 1160, "Parachoques delantero Alien", 12.4 },
		{ 1165, "Parachoques delantero X-Flow", 10.7 },
		{ 1166, "Parachoques delantero Alien", 12.7 },
		{ 1169, "Parachoques delantero Alien", 13.2 },
		{ 1170, "Parachoques delantero X-Flow", 11.0 },
		{ 1171, "Parachoques delantero Alien", 12.0 },
		{ 1172, "Parachoques delantero X-Flow", 11.2 },
		{ 1173, "Parachoques delantero X-Flow", 10.4 },
		{ 1174, "Parachoques delantero Chrome", 6.0 },
		{ 1176, "Parachoques delantero Chrome", 5.6 },
		{ 1179, "Parachoques delantero Chrome", 7.0 },
		{ 1181, "Parachoques delantero Slamin", 5.4 },
		{ 1182, "Parachoques delantero Chrome", 5.5 },
		{ 1185, "Parachoques delantero Slamin", 7.4 },
		{ 1188, "Parachoques delantero Slamin", 8.0 },
		{ 1189, "Parachoques delantero Chrome", 7.0 },
		{ 1190, "Parachoques delantero Slamin", 6.0 },
		{ 1191, "Parachoques delantero Chrome", 5.6 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 }
	},
	// Parachoques trasero 22 8%
	{
		{ 1140, "Parachoques trasero X-Flow", 10.2 },
		{ 1141, "Parachoques trasero Alien", 10.0 },
		{ 1148, "Parachoques trasero X-Flow", 10.2 },
		{ 1149, "Parachoques trasero Alien", 10.0 },
		{ 1150, "Parachoques trasero Alien", 9.5 },
		{ 1151, "Parachoques trasero X-Flow", 9.5 },
		{ 1154, "Parachoques trasero Alien", 10.3 },
		{ 1156, "Parachoques trasero X-Flow", 9.7 },
		{ 1159, "Parachoques trasero Alien", 9.0 },
		{ 1161, "Parachoques trasero X-Flow", 10.3 },
		{ 1167, "Parachoques trasero X-Flow", 11.0 },
		{ 1168, "Parachoques trasero Alien", 10.0 },
		{ 1175, "Parachoques trasero Slamin", 5.8 },
		{ 1177, "Parachoques trasero Slamin", 5.4 },
		{ 1178, "Parachoques trasero Slamin", 6.3 },
		{ 1180, "Parachoques trasero Chrome", 5.0 },
		{ 1183, "Parachoques trasero Slamin", 5.8 },
		{ 1184, "Parachoques trasero Chrome", 6.2 },
		{ 1186, "Parachoques trasero Slamin", 6.6 },
		{ 1187, "Parachoques trasero Chrome", 6.3 },
		{ 1192, "Parachoques trasero Chrome", 5.5 },
		{ 1193, "Parachoques trasero Slamin", 5.5 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 }
	},
	// Capucha 4 2%
	{
		{ 1004, "Techo Champ Scoop", 4.5 },
		{ 1005, "Techo Fury Scoop", 4.9 },
		{ 1011, "Techo Race Scoop", 5.0 },
		{ 1012, "Techo Worx Scoop", 5.4 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 }
	},
	// Spoiler 19 4% -1
	{
		{ 1000, "Spoiler Pro", 6.0 },
		{ 1001, "Spoiler Win", 7.0 },
		{ 1002, "Spoiler Drag", 6.0 },
		{ 1003, "Spoiler Alpha", 7.0 },
		{ 1014, "Spoiler Champ", 6.1 },
		{ 1015, "Spoiler Race", 6.2 },
		{ 1016, "Spoiler Worx", 6.0 },
		{ 1049, "Spoiler Alien", 6.1 },
		{ 1050, "Spoiler X-Flow", 5.9 },
		{ 1058, "Spoiler Alien", 6.3 },
		{ 1023, "Spoiler Fury", 7.0 },
		{ 1146, "Spoiler X-Flow", 6.4 },
		{ 1147, "Spoiler Alien", 6.0 },
		{ 1138, "Spoiler Alien", 6.0 },
		{ 1139, "Spoiler X-Flow", 7.0 },
		{ 1060, "Spoiler X-Flow", 5.6 },
		{ 1162, "Spoiler Alien", 5.8 },
		{ 1163, "Spoiler X-Flow", 6.0 },
		{ 1164, "Spoiler Alien", 8.2 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 }
	},
	// Techo 17 2%
	{
		{ 1032, "Techo Alien", 5.1 },
		{ 1006, "Techo Scoop", 4.6 },
		{ 1038, "Techo Alien", 5.1 },
		{ 1035, "Techo X-Flow", 5.2 },
		{ 1033, "Techo X-Flow", 5.2 },
		{ 1053, "Techo X-Flow", 5.2 },
		{ 1054, "Techo Alien", 5.3 },
		{ 1055, "Techo Alien", 5.1 },
		{ 1061, "Techo X-Flow", 5.0 },
		{ 1067, "Techo Alien", 5.1 },
		{ 1068, "Techo X-Flow", 4.8 },
		{ 1088, "Techo Alien", 5.0 },
		{ 1091, "Techo X-Flow", 5.2 },
		{ 1103, "Techo Covertible", 6.0 },
		{ 1128, "Techo Vinyl Hardtop", 9.2 },
		{ 1130, "Techo Hardtop", 8.1 },
		{ 1131, "Techo Softtop", 7.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 }
	},
	// Ventilaci�n 2 1%
	{
		{ 1143, "Ventilaci�n Oval", 4.0 },
		{ 1145, "Ventilaci�n Square", 3.8 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 }
	},
	// Escape 29 3%
	{
		{ 1018, "Escape Upswept", 5.0 },
		{ 1019, "Escape Twin", 4.6 },
		{ 1020, "Escape Large", 4.0 },
		{ 1021, "Escape Medium", 3.9 },
		{ 1022, "Escape Small", 3.7 },
		{ 1028, "Escape Alien", 5.3 },
		{ 1029, "Escape X-Flow", 5.6 },
		{ 1034, "Escape Alien", 5.3 },
		{ 1037, "Escape X-Flow", 5.6 },
		{ 1043, "Escape Slamin", 5.4 },
		{ 1044, "Escape Chrome", 5.0 },
		{ 1045, "Escape X-Flow", 5.8 },
		{ 1046, "Escape Alien", 5.6 },
		{ 1059, "Escape X-Flow", 5.8 },
		{ 1064, "Escape Alien", 5.6 },
		{ 1065, "Escape Alien", 5.5 },
		{ 1066, "Escape X-Flow", 5.6 },
		{ 1092, "Escape Alien", 5.4 },
		{ 1089, "Escape X-Flow", 5.5 },
		{ 1126, "Escape Chrome", 5.7 },
		{ 1127, "Escape Slamin", 6.2 },
		{ 1129, "Escape Chrome", 5.5 },
		{ 1113, "Escape Chrome", 5.1 },
		{ 1114, "Escape Slamin", 5.6 },
		{ 1104, "Escape Chrome", 4.8 },
		{ 1105, "Escape Slamin", 4.3 },
		{ 1132, "Escape Slamin", 6.4 },
		{ 1135, "Escape Slamin", 5.3 },
		{ 1136, "Escape Chrome", 5.8 }
	},
	// Faldas laterales 23 3%
	{
		{ 1007, "Faldas laterales Sideskirt", 4.8 },
		{ 1026, "Faldas laterales Alien", 6.0 },
		{ 1031, "Faldas laterales X-Flow", 6.2 },
		{ 1036, "Faldas laterales Alien", 6.3 },
		//{ 1039, "Faldas laterales X-Flow", 6.4 },
		{ 1041, "Faldas laterales X-Flow", 6.5 },
		{ 1042, "Faldas laterales Chrome", 5.0 },
		{ 1047, "Faldas laterales Alien", 5.7 },
		{ 1048, "Faldas laterales X-Flow", 5.2 },
		{ 1056, "Faldas laterales Alien", 4.9 },
		{ 1057, "Faldas laterales X-Flow", 4.9 },
		{ 1069, "Faldas laterales Alien", 5.4 },
		{ 1070, "Faldas laterales X-Flow", 5.0 },
		{ 1090, "Faldas laterales Alien", 5.4 },
		{ 1093, "Faldas laterales X-Flow", 5.4 },
		{ 1095, "Faldas laterales X-Flow", 5.4 },
		{ 1106, "Faldas laterales Chrome Arches", 4.5 },
		{ 1108, "Faldas laterales Chrome Strip", 4.5 },
		{ 1118, "Faldas laterales Chrome Trim", 3.8 },
		{ 1119, "Faldas laterales Wheelcovers", 4.0 },
		{ 1122, "Faldas laterales Chrome Flames", 5.0 },
		{ 1133, "Faldas laterales Chrome Strip", 4.4 },
		{ 1134, "Faldas laterales Chrome Strip", 4.6 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 }
	},
	// Iluminaci�n adicional 2 1%
	{
		{ 1013, "A�adir iluminaci�n Round Fog", 4.0 },
		{ 1024, "A�adir iluminaci�n Square Fog", 3.8 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 }
	},
	// Protecci�n7 1%
	{
		{ 1100, "Parrilla Chrome Grill", 5.2 },
		{ 1123, "Parrilla Chrome Bars", 3.7 },
		{ 1125, "Parrilla Chrome Lights", 4.0 },
		{ 1109, "Proteccion trasera Chrome", 4.5 },
		{ 1110, "Proteccion trasera Slamin", 3.7 },
		{ 1115, "Protecci�n frontal Chrome", 4.8 },
		{ 1116, "Protecci�n frontal Slamin", 4.2 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 },
		{ 0, " ", 0.0 }
	}	
};

new Float:TuningPosition[][] = 
{
	{ 979.5805, -1369.2102, 13.6812 },
	{ 975.6102, -1369.3257, 13.6812 },
	{ 966.6491, -1371.4917, 13.6812 },
	{ 962.4542, -1371.4739, 13.6812 },
	{ 1354.6115, 201.3747, 19.7012 },
	{ 1367.9880, 196.7904, 19.7012 },
	{ 2507.2783, -1518.6752, 24.1472 },
	{ 2507.2456, -1526.9625, 24.1472 },
	{ 2139.4402, -2183.7778, 13.5544 },
	{ 2160.4895, -2163.6643, 13.5469 }
};

#define tune_dialog					"\	
										"cBLUE"1. "cWHITE"Ruedas\n\
										"cBLUE"2. "cWHITE"Parachoques delantero\n\
										"cBLUE"3. "cWHITE"Parachoques trasero\n\
										"cBLUE"4. "cWHITE"Capuchas\n\
										"cBLUE"5. "cWHITE"Spoilers\n\
										"cBLUE"6. "cWHITE"Techos\n\
										"cBLUE"7. "cWHITE"Ventilaciones\n\
										"cBLUE"8. "cWHITE"Escape\n\
										"cBLUE"9. "cWHITE"Faldas laterales\n\
										"cBLUE"10. "cWHITE"A�adir iluminaci�n\n\
										"cBLUE"11. "cWHITE"Parrillas\n\
										"cBLUE"12. "cWHITE"Suspensi�n neum�tica\
									"
