enum e_SPAWN_INFO
{
	help_dialog	[ 64 ],
	help_text	[ 1256 char ],
}

enum e_COMMAND_INFO
{
	help_index,
	help_member,
	help_criminal,
	help_text		[ 16 ],
	help_descript	[ 128 ],
}

enum e_HELP_INFO
{
	help_dialog,
	help_index,
	help_text	[ 2048 char ],
}

new 
	help_spawn[][ e_SPAWN_INFO ] = 
	{
		{ "{04cabf}1.{ffffff} �Qu� es Influx RolePlay?\n", 
			!"{ffffff}Es un servidor de la plataforma GTA San Andreas Multiplayer (SA:MP).\n\
			El objetivo es simple: jugar para un personaje que t� mismo creas y desarrollas a tu gusto.\n\
			Trabaja, �nete a organizaciones gubernamentales, triunfa en el mundo criminal o desarrolla tu propio negocio.\n\
			Compra bienes ra�ces o vehiculos, descubre nuevos horizontes en Influx RolePlay.\n\
			�Te deseamos �xito y que disfrutes del juego!" },
		
		{ "{04cabf}2.{ffffff} �C�mo empezar a juegar?\n", 
			!"{ffffff}En primer lugar, le recomendamos que se familiarice con las reglas del servidor {04cabf}www.influx-rp.eu/reglas{ffffff}\n\
			y lee ayuda detallada sobre el juego {04cabf}www.influx-rp.eu/ayuda\n\
			{ffffff}Despu�s de eso, te aconsejamos que pienses en lo que har� tu personaje, su car�cter y comportamiento." },	
		
		{ "{04cabf}3.{ffffff} �D�nde puedo ganar dinero?\n", 
			!"{ffffff}Puede ganar dinero en el trabajo de silvicultura o en el servicio de entrega de alimentos, ya que no hay restricciones en el nivel (nivel 0+)\n\
			y al solicitar un trabajo no se requieren documentos. Puede encontrar una oficina forestal o un servicio de entrega utilizando {04cabf}/gps - Trabajo{ffffff}.\n\
			Para otros trabajos necesitar�s los documentos necesarios y al menos 1 nivel." },
		
		{ "{04cabf}4.{ffffff} �C�mo llego a cualquier parte?\n", 
			!"{ffffff}Para esto lo ayudar� el veh�culo p�blico (autobuses), taxi o viajes, que puede tomar en la carretera.\n\
			Si no sabe qu� y d�nde se encuentra, entonces use el comando{04cabf}/gps{ffffff}." },
		
		{ "{04cabf}5.{ffffff} �C�mo me comunico con los jugadores?\n", 
			!"{ffffff}En el juego tienes que interactuar con otros jugadores y lo primero que encuentras es la comunicaci�n.\n\n\
			Para poder comunicarte correctamente, necesitas conocer algunas reglas:\n\
			{04cabf}Chat IC{ffffff} - Es el chat donde se expresa tu personaje, (Ejemplo: �Hola! �Trabajas en la silvicultura?) El chat se abre con {04cabf}F6 o T{ffffff}.\n\
			{04cabf}Chat OOC{ffffff} -  Toda la informaci�n que no sea del juego que no se puede utilizar en el canal IC. Asi que para hablar sobre temas ajenos al juego agrega {04cabf} /b {ffffff} al principio de la oraci�n (por ejemplo: /b Llamame al Discord).\n\n\
			Puede obtener m�s informaci�n sobre las reglas de comunicaci�n e interacci�n con los jugadores en la secci�n de reglas del servidor en {04cabf}www.influx-rp.eu/reglas{ffffff}."},
			
		{ "{04cabf}6.{ffffff}�No encontraste la respuesta a tu pregunta?\n", 
			!"{ffffff}Pide ayuda en el sitio {04cabf}influx-rp.eu/ayuda{ffffff}o pregunte al staff a trav�s de {04cabf}Y - Men� del juego - Enviar duda{ffffff} ({04cabf}/reportar [ Texto ]{ffffff})."}
	},
	
	help_command[][ e_COMMAND_INFO ] = 
	{
		{ 0, -1, -1, "Y", "Abre el men� principal del personaje." },
		{ 0, -1, -1, "F", "Levanta objetos descartados, gestiona texturas del hogar (negocio)" },
		{ 0, -1, -1, "H", "Abrir puertas (barreras), finalizar una llamada telef�nica, administrar texturas en el hogar (negocio)" },
		{ 0, -1, -1, "ALT", "Entrada y salida de lugares, actuaciones en pick-ups." },
		{ 1, -1, -1, "/documento", "Mostrar tarjeta de identificaci�n" },
		{ 1, -1, -1, "/anim", "Lista de animaciones" },
		{ 1, -1, -1, "/pagar", "Dar dinero" },
		{ 1, -1, -1, "/gps", "Navegador GPS abierto" },
		{ 1, -1, -1, "/walk", "Caminata autom�tica" },
		{ 1, -1, -1, "/reportar", "Enviar una duda/reporte" },
		{ 1, -1, -1, "/frecuencia", "Sintoniza la frecuencia de la radio" },
		{ 1, -1, -1, "/canal", "Sintoniza el canal de la radio" },
		{ 1, -1, -1, "/id", "B�squeda de jugadores" },
		{ 1, -1, -1, "/afk", "Averigua si un jugador est� AFK" },
		{ 1, -1, -1, "/estado", "Administrar la descripci�n del personaje" },
		{ 1, -1, -1, "/pame", "Ver descripci�n del jugador" },
		{ 1, -1, -1, "/lideres", "Ver l�deres en l�nea" },
		{ 1, -1, -1, "/tiempo", "Informaci�n sobre la hora y fecha." },
		{ 1, -1, -1, "/limpiarmichat", "Borrar chat" },
		{ 1, -1, -1, "/donar", "Tienda premium" },
		{ 1, -1, -1, "/headmove", "Desactivar la rotaci�n de la cabeza" },
		{ 1, -1, -1, "/blind", "Activar pantalla negra (para crear SS)" },
		{ 1, -1, -1, "/doc", "Vea su lista de documentos" },
		{ 1, -1, -1, "/ayuda", "Ayuda abierta en el juego" },
		{ 2, -1, -1, "/r", "Comunicaci�n en el canal de radio." },
		{ 2, -1, -1, "/rr", "Comunicaci�n en el subcanal de radio." },
		{ 2, -1, -1, "/b", "Chat OOC" },
		{ 2, -1, -1, "/ab", "Chat OOC sobre la cabeza del personaje." },
		{ 2, -1, -1, "/s", "Susurrar" },
		{ 2, -1, -1, "/g", "Gritar" },
		{ 2, -1, -1, "/me", "Acci�n de personaje (mostrada en el chat)" },
		{ 2, -1, -1, "/ame", "Acci�n del personaje (mostrado arriba)" },
		{ 2, -1, -1, "/do", "Descripci�n de una tercera persona" },
		{ 2, -1, -1, "/todo", "La acci�n del personaje al hablar." },
		{ 2, -1, -1, "/moneda", "Tirar una moneda" },
		{ 2, -1, -1, "/w", "Chat OOC (Mensaje privado)" },
		{ 2, -1, -1, "/cnn", "Red de LSTV" },
		{ 2, -1, -1, "/f", "Chat OOC de facci�n" },
		{ 2, -1, -1, "/d", "Canal 911" },
		{ 3, -1, -1, "/dfumar", "Tirar un cigarrillo" },
		{ 3, -1, -1, "/dbeber", "Tirar una botella" },
		{ 3, -1, -1, "/llenar", "Llenar de gasolina con un bote" },
		{ 3, -1, -1, "/curar", "Usa el botiqu�n de primeros auxilios." },
		{ 3, -1, -1, "/esposar", "Poner las esposas" },
		{ 3, -1, -1, "/desesposar", "Quitar las esposas" },
		{ 3, -1, -1, "/pinchos", "Instalar/quitar la cinta con picos" },
		{ 3, -1, -1, "/escudo", "Usar/quitar el escudo de la polic�a" },
		{ 4, -1, -1, "/job", "Men� de trabajo" },
		{ 4, -1, -1, "/descarga", "Comenzar a descargar mercanc�as en el negocio." },
		{ 4, -1, -1, "/enganchar", "Remolcar" },
		{ 4, -1, -1, "/desenganchar", "Soltar remolque" },
		{ 4, -1, -1, "/repair", "Reparar Veh�culo" },
		{ 5, -1, -1, "/pauto", "Panel de control de veh�culo" },
		{ 5, -1, -1, "/pcasa", "Panel de control del hogar" },
		{ 5, -1, -1, "/npanel", "Panel de gestion de negocios" },
		{ 5, -1, -1, "/capo", "Abre el cap�" },
		{ 5, -1, -1, "/maletero", "Abre el maletero" },
		{ 5, -1, -1, "/limite", "Control del limitador de velocidad del veh�culo." },
		{ 5, -1, -1, "/golpear", "Llamar a la puerta de la casa." },
		{ 5, -1, -1, "/desalojar", "Desalojar la casa" },
		{ 5, -1, -1, "/seguro", "Gesti�n de la cerradura de tus propiedades." },
		{ 5, -1, -1, "/ventana", "Abrir una ventana en el veh�culo." },
		{ 6, 0, -1, "/fpanel", "Panel de control de la organizaci�n" },
		{ 6, 0, -1, "/miembros", "Lista de miembros" },
		{ 6, 0, -1, "/f", "Chat OOC de facci�n" },
		{ 6, 0, -1, "/invitar", "Lleva un jugador a la facci�n" },
		{ 6, 0, -1, "/uninvite", "Descartar a un jugador de una facci�n." },
		{ 6, 0, -1, "/setrank", "Establecer rango de jugador" },
		{ 6, 0, -1, "/color", "Ajusta el color del pincel." },
		{ 6, 0, -1, "/mic", "Usar micr�fono" },
		{ 6, 0, -1, "/object", "Lista de objetos de facci�n" },
		{ 6, 0, -1, "/attach", "Adjunto lista de la facci�n." },
		{ 6, 0, -1, "/rb", "Usa un arma como aturdidor" },
		{ 6, 0, -1, "/re", "Canal de chat 911" },
		{ 6, 0, -1, "/m", "Meg�fono" },
		{ 6, 0, -1, "/accept", "Lista de llamadas al 911" },
		{ 6, 0, -1, "/spu", "Instalar se�al especial para el veh�culo." },
		{ 6, 0, -1, "/air", "Entrada de veh�culo a�reo" },
		{ 6, 0, -1, "/sl", "Luces estrobosc�picas" },
		{ 6, 0, -1, "/frepair", "veh�culo fijo" },
		{ 6, 0, -1, "/ftow", "veh�culo de vehiculo" },
		{ 6, 0, -1, "/desenganchar", "Desenganchar el veh�culo" },
		{ 6, 0, -1, "/callsign", "Establecer la descripci�n en el veh�culo." },
		{ 6, 0, -1, "/dsign", "Eliminar la descripci�n del veh�culo" },
		{ 6, 0, -1, "/frisk", "Buscar jugador" },
		{ 6, 2, -1, "/bk", "Enviar solicitud de soporte" },
		{ 6, 5, -1, "/bk", "Enviar solicitud de soporte" },
		{ 6, 2, -1, "/unbk", "Cancelar solicitud de soporte" },
		{ 6, 5, -1, "/unbk", "Cancelar solicitud de soporte" },
		{ 6, 1, -1, "/recept", "Gestion de casos" },
		{ 6, 1, -1, "/createdoc", "Crear un documento para el jugador." },
		{ 6, 2, -1, "/multar", "Escribe una penalizaci�n al jugador." },
		{ 6, 8, -1, "/multar", "Escribe una penalizaci�n al jugador." },
		{ 6, 2, -1, "/cmulta", "Escribe una multa bajo el portero del coche" },
		{ 6, 2, -1, "/liderar", "Empieza a liderar al jugador" },
		{ 6, 3, -1, "/liderar", "Empieza a liderar al jugador" },
		{ 6, 8, -1, "/liderar", "Empieza a liderar al jugador" },
		{ 6, 2, -1, "/dliderar", "Deja de liderar al jugador" },
		{ 6, 3, -1, "/dliderar", "Deja de liderar al jugador" },
		{ 6, 8, -1, "/dliderar", "Deja de liderar al jugador" },
		{ 6, 2, -1, "/meter", "Pon el jugador en el coche." },
		{ 6, 3, -1, "/meter", "Pon el jugador en el coche." },
		{ 6, 8, -1, "/meter", "Pon el jugador en el coche." },
		{ 6, 2, -1, "/sacar", "Echar a un jugador del carro" },
		{ 6, 3, -1, "/sacar", "Echar a un jugador del carro" },
		{ 6, 8, -1, "/sacar", "Echar a un jugador del carro" },
		{ 6, 2, -1, "/darlic", "Administrar licencias de armas." },
		{ 6, 2, -1, "/mdc", "Ordenador de a bordo" },
		{ 6, 3, -1, "/mdc", "Ordenador de a bordo" },
		{ 6, 8, -1, "/mdc", "Ordenador de a bordo" },
		{ 6, 2, -1, "/arrestar", "Pon un jugador en la carcel" },
		{ 6, 8, -1, "/arrestar", "Pon un jugador en al carcel" },
		{ 6, 2, -1, "/liberar", "Libera a un jugador de la carcel" },
		{ 6, 8, -1, "/liberar", "Libera a un jugador de la carcel" },
		{ 6, 8, -1, "/liberar", "Libera a un jugador de la carcel" },
		{ 6, 2, -1, "/su", "Declarar (eliminar) la lista deseada del jugador." },
		{ 6, 3, -1, "/su", "Declarar (eliminar) la lista deseada del jugador." },
		{ 6, 8, -1, "/su", "Declarar (eliminar) la lista deseada del jugador." },
		{ 6, 2, -1, "/wanted", "Declarar (eliminar) al jugador en la b�squeda federal." },
		{ 6, 3, -1, "/wanted", "Declarar (eliminar) al jugador en la b�squeda federal." },
		{ 6, 8, -1, "/wanted", "Declarar (eliminar) al jugador en la b�squeda federal." },
		{ 6, 2, -1, "/carsu", "Anunciar (eliminar) el veh�culo deseado" },
		{ 6, 3, -1, "/carsu", "Anunciar (eliminar) el veh�culo deseado" },
		{ 6, 8, -1, "/carsu", "Anunciar (eliminar) el veh�culo deseado" },
		{ 6, 2, -1, "/takelic", "Retirar licencia" },
		{ 6, 2, -1, "/carrestar", "Enviar veh�culos al aparcamiento." },
		{ 6, 5, -1, "/luke", "Abrir la escotilla de veh�culo a�reo." },
		{ 6, 5, -1, "/oxygen", "Usar una botella de oxigeno" },
		{ 6, 5, -1, "/boat", "Dejar caer el barco" },
		{ 6, 5, -1, "/drop", "Iniciar / detener la descarga de agua" },
		{ 6, 7, -1, "/newspanel", "Panel de organizacion" },
		{ 6, 7, -1, "/ether", "Conectar / desconectar el jugador que est� al aire" },
		{ 7, -1, 0, "/mpanel", "Panel de control de la organizaci�n" },
		{ 7, -1, 0, "/members", "Lista de miembros" },
		{ 7, -1, 0, "/f", "Canal OOC de organizaci�n" },
		{ 7, -1, 0, "/invite", "Invitar a un jugador a la organizaci�n" },
		{ 7, -1, 0, "/uninvite", "Expulsar a un jugador a la organizaci�n" },
		{ 7, -1, 0, "/setrank", "Establecer el rango de un jugador" },
		{ 7, -1, 0, "/attach", "Lista de adjuntos facci�n" },
		{ 8, -1, -1, "/admins", "Lista de administradores en l�nea" },
		{ 8, -1, -1, "/supports", "Lista de soporte en l�nea" }
	},
	
	help_info[][ e_HELP_INFO ] = 
	{
		{ 0, -1,
			!"{04cabf}Informaci�n general{ffffff}\n\n\
			Jugando en el servidor ganar�s dinero. Cada hora de juego recibir�s un PayDay:\n\
			{04cabf}-{ffffff} Salario (si eres miembro de una organizaci�n estatal)\n\
			{04cabf}-{ffffff} Prestaci�n por desempleo (si no trabajas oficialmente)\n\
			{04cabf}-{ffffff} Tickets - control e intereses sobre ahorros/salarios bancarios (si eres)\n\
			{04cabf}-{ffffff} saldo de chequera y cuenta bancaria\n\
			{04cabf}-{ffffff} aumento en el n�mero total de horas jugadas (1 hora = 1 exp)\n\n\
			Si trabajas de manera informal, cada d�a de pago recibir�s una prestaci�n por desempleo de $100, que se acreditar� en tu chequera.\n\
			{04cabf}Talonario de cheques{ffffff} - Esta es una cuenta bancaria por la cual se pagan los salarios. Los cobros pueden ser solo en el banco central\n\
			({04cabf}/gps - Organizaciones del Estado - Banco Central{ffffff}).\n\n\
			Para solicitar ciertos tipos de trabajos, necesitar�s una licencia de conducir y una tarjeta de identificaci�n. En algunos casos, el nivel de juego tambi�n se tiene en cuenta.\n\
			Para obtener los documentos necesarios, debes ir al centro de licencias. ({04cabf}/gps - Otras organizaciones - Centro de licencias{ffffff}).\n\n\
			Para un movimiento m�s conveniente alrededor del estado de SA, hay un navegador GPS en el servidor. ({04cabf}Y - Navegador GPS o /gps{ffffff})." },
		
		{ 1, -1,
			!"{04cabf}Inventario{ffffff}\n\
			El inventario le permite guardar los elementos que necesita e interactuar con ellos.\n\n\
			En el servidor hay varios tipos de inventarios:\n\n\
			{04cabf}Inventario principal{ffffff}\n\
			El inventario incluye 10 celdas para varios ar�culos y 5 ranuras activas para interacci�n directa.\n\
			El peso m�ximo tolerable en el inventario del personaje es de 15 kilogramos.\n\n\
			{04cabf}Ranuras activas{ffffff}\n\
			Se requiere una ranura activa para interactuar con el elemento. Cuando el elemento est� en la ranura activa,\n\
			Se utiliza para su prop�sito previsto o como un archivo adjunto al personaje (cuerpo del personaje).\n\
			La ubicaci�n es totalmente personalizable y siempre permanecer� all�. Se puede configurar multiples veces.\n\n\
			{04cabf}Bolsas de celdas adicionales{ffffff}\n\
			Para simplificar la transferencia de varios ar�culos en el servidor, hay diferentes tipos de bolsas que agregan 5 espacios adicionales al inventario.:\n\
			Bolsa de papel (+ 3 kg), Male�n (+ 5 kg), Maleta (+ 7 kg), Mochila (+ 10 kg), Bolsa de deporte (+ 15 kg), Mochila de viaje (+ 20 kg).\n\n\
			{04cabf}Inventario de vehiculos{ffffff}\n\
			Muchos modelos de coches tienen su propio inventario personal y cada uno tiene un n�mero diferente de celdas para almacenar ar�culos. En coches no hay restricci�n de peso.\n\
			Para usar el inventario del coche, abra el maletero (/maletero) y vaya a �l, luego abra su inventario ({04cabf}Y - Inventario{ffffff}).\n\n\
			{04cabf}Inventario en casa{ffffff}\n\
			[Pr�ximamente]" },
		
		{ 2, -1,
			!"{04cabf}Telefonos{ffffff}\n\n\
			Las torres celulares est�n ubicadas en todo el estado de SA y, en consecuencia, el nivel de red depender� de tu ubicaci�n.\n\
			Si te encuentras fuera del �rea de cobertura de la red, no podr�s comunicarte ni escribir un SMS.\n\n\
			Para usar el tel�fono, colocalo en la ranura de inventario activo y luego presiona {04cabf} Y - Tel�fono celular{ffffff}.\n\
			El tel�fono se puede comprar en cualquier tienda de electr�nica que encuentres en GPS\n\
			({04cabf}/gps - Buscar - Encuentra la tienda m�s cercana - Tienda de electr�nica{ffffff}).\n\n\
			En el servidor hay 2 tipos de tel�fonos: por tecla y t�ctil.\n\
			Con un telefono podr�s:\n\
			{04cabf}- {ffffff}Gestionar agenda/llamadas\n\
			{04cabf}- {ffffff}Gestionar mensajes (guardar/enviar)\n\
			{04cabf}- {ffffff}Personalizar los tonos de llamada y cambiar el color del panel del tel�fono\n\
			{04cabf}- {ffffff}Verificar el estado de la red" },
		{ 3, 0,
			!"{04cabf}Silvicultura{ffffff}\n\n\
			La silvicultura se encuentra en el condado de Redcounty ({04cabf}/gps - Obras - Forestal{ffffff}).\n\
			Es un trabajo que no requiere tu presencia oficial y que firmas un contrato por 6 horas.\n\
			El trabajo consiste en cortar los �rboles cercanos, y m�s aserrados. Luego transferirlos al transportador que conduce al taller forestal.\n\
			Para empezar a trabajar, debes ir al taller principal de silvicultura y subir al segundo piso." },

		{ 3, 1,
			!"{04cabf}Compa�ia de veh�culo de pasajeros{ffffff}\n\n\
			La oficina de la compa��a de veh�culo de pasajeros est� ubicada en el centro de Los Santos en el �rea de 'Market'. ({04cabf}/gps - Obras - Empresa de veh�culo de pasajeros.{ffffff}).\n\
			Al solicitar un trabajo, te ver�s obligado a mantenerlo durante 6 horas (solo puedes renunciar antes siendo un usuario premium).\n\
			Requerido: licencia de conducir, documento de identidad y nivel 1.\n\n\
			Esta empresa incluye 2 tipos de trabajos:\n\n\
			{04cabf}Conductor de taxi{ffffff}\n\
			El trabajo consiste en el veh�culo pagado de personas de un punto a otro. El precio lo pones tu.\n\
			{04cabf}/job{ffffff} (de $1 a $6 por KM)\n\n\
			{04cabf}Conductor de bus{ffffff}\n\
			El trabajo de un conductor de autob�s es viajar por una ruta predeterminada y transportar a las personas de forma gratuita.\n\
			El pago que recibe el conductor es dependiendo de la ruta y se paga al final del d�a laboral.\n\
			Puedes recoger tu dinero en el banco, retir�ndolos de la chequera.\n\
			{04cabf}Atenci�n{ffffff} se te cobrar� una multa a tu cuenta bancaria por los veh�culos da�ados o destruidos."},

		{ 3, 2,
			!"{04cabf}Empresa de veh�culo de mercancias{ffffff}\n\n\
			La oficina de la compa��a de veh�culo de mercancias est� ubicada en los muelles de la ciudad de Los Santos, en el �rea de 'Ocean Docks' ({04cabf}/gps - Obras - Compa��a de veh�culo de mercancias{ffffff}).\n\
			Al solicitar un trabajo, te ver�s obligado a mantenerlo durante 6 horas (solo puedes renunciar antes siendo un usuario premium).\n\
			Requerido: licencia de conducir, documento de identidad.\n\n\
			Esta empresa incluye 2 tipos de trabajo:\n\n\
			{04cabf}Distribuci�n de recursos{ffffff}\n\
			El trabajo consiste en comprar bienes y luego venderlos a un precio m�s rentable en uno de los almacenes.\n\
			La compra y venta de bienes se realiza en 5 almacenes, todos ellos se pueden encontrar en el GPS ({04cabf}/gps - Almac�n{ffffff}).\n\
			En cada uno de los almacenes hay una tabla con informaci�n general sobre las mercanc�as, sus precios por tonelada y la cantidad en stock.\n\
			El precio de un producto depende de su cantidad en stock: si hay una escasez de productos en un almac�n, entonces el precio para la compra y venta se incrementar�,\n\
			y viceversa, si hay un excedente de bienes en el almac�n, entonces se reducir� el precio de compra y venta.\n\
			Los bienes se dividen en 2 tipos: rompibles e irrompibles.\n\n\
			{04cabf}Entrega de productos{ffffff}\n\
			El trabajo consiste en cargar productos en el almac�n central 'Hilltop Farms' y luego entregarlos a las empresas.\n\
			Para poder entregar las mercanc�as, debes cargar tu camioneta con productos. Al llegar al almac�n, presiona (H) y luego ingrese la cantidad deseada de mercanc�as (hasta 2000).\n\
			Luego ingresa el comando {04cabf}/job{ffffff} y selecciona un negocio para entregar los productos a quien te contrate.\n\
			Al llegar al negocio deseado, ingrese el comando {04cabf}/descargar{ffffff} y empezar�s a descargar la mercanica. Ganas por cuanto descargas.\n\n\
			{04cabf}Atenci�n{ffffff} Se te cobrar� una multa a tu cuenta bancaria por los veh�culos da�ados o destruidos." },

		{ 3, 3,
			!"{04cabf}Mec�nicos{ffffff}\n\n\
			Para la reparaci�n y tuneo de los coches debes ir a una estaci�n de servicio 4 est�n disponibles, todas ellas se pueden encontrar en GPS ({04cabf}/gps - Trabajos - CTO{ffffff}):\n\
			Mec�nicos 'Market', Mec�nicos 'Bell', Mec�nicos 'Simpson', Mec�nicos 'Harbor' (En estas estaciones puedes reparar veh�culos grandes, por ejemplo, un autob�s o un tractor.).\n\
			Al solicitar un trabajo, te ver�s obligado a mantenerlo durante 6 horas (solo puedes renunciar antes siendo un usuario premium).\n\
			Requerido: licencia de conducir, documento de identidad y nivel 2.\n\n\
			El trabajo consiste en reparar y remolcar (/reparar - /enganchar)." },
		{ 3, 4,
			!"{04cabf}Servicio de entrega de alimentos{ffffff}\n\n\
			Ubicado en Los Santos, distrito de 'commerce' ({04cabf}/gps - Obras - Servicio de entrega de alimentos{ffffff}).\n\
			Este trabajo no requiere formalizar un contrato por 6 horas.\n\
			El trabajo consiste en la entrega de alimentos al cliente de cualquier manera conveniente: con un servicio de ciclomotor o veh�culo personal.\n\
			El pago depende de la distancia al lugar de entrega y, en algunos casos, del tiempo de entrega (la urgencia del cliente).\n\
			Toma un pedido para la entrega." },
		{ 4, 0,
			!"{04cabf}Casas{ffffff}\n\n\
			Para comprar una casa, debe ponerse en contacto con una agencia de bienes ra�ces ({04cabf}/gps - Otras organizaciones - Agencia inmobiliaria{ffffff}).\n\
			Puedes comprar o alquilar cualquier casa\n\
			En el caso de alquilar, no estar�n disponibles las siguientes opciones: volver a exponer, compartir con otros jugadores, vender bienes ra�ces a otro jugador.\n\
			Puedes pagar los servicios basicos y el alquiler en el Banco Central (Cajero - Pago por servicios - Pago por vivienda).\n\n\
			Ofrecemos 38 interiores unicos para las casas.\n\
			Los interiores se dividen en varios tipos: estudio, 1 habitaci�n, 2 habitaciones, 3 habitaciones, 4 habitaciones, 2 plantas, 3 habitaciones, 2 plantas, 4 habitaciones, mansi�n de clase 1, mansi�n de clase 2.\n\
			Cada tipo tiene un n�mero diferente de paredes, pisos, techos y escaleras para personalizar, as� como un m�ximo diferente de muebles y inquilinos\n\n\
			Para gestionar la casa {04cabf}/pcasa{ffffff} Las opciones son informaci�n, intercambio, transacciones en efectivo, ajustes." },
		
		{ 4, 1,
			!"{04cabf}Empresas{ffffff}\n\n\
			Para adquirir un negocio, necesitas ponerte en contacto con una agencia de bienes ra�ces ({04cabf}/gps - Otras Organizaciones - Agencia Inmobiliaria{ffffff}).\n\
			Ofrecemos 19 interiores unicos para los negocios.\n\
			Los interiores se dividen en varios tipos: bares, restaurantes, bares, tiendas de turismo, tiendas de electr�nica, tiendas de ropa, tiendas de accesorios.\n\
			Cada tipo de negocio tiene diferentes ar�culos en venta,\n\
			un n�mero diferente de paredes, pisos, techos y escaleras para la personalizaci�n, as� como un m�ximo diferente de muebles\n\n\
			Para gestionar el negocio {04cabf}/npanel{ffffff} Las opciones son: innformaci�n, expansi�n comercial, transacciones en efectivo, ajustes." },
		
		{ 4, 2,
			!"{04cabf}Veh�culos{ffffff}\n\n\
			Los veh�culos personales no requieren tener una casa. Para comprarlo debes visitar uno de los tres salones Grotti ({04cabf}/gps - Centros de veh�culo{ffffff}).\n\n\
			Si deseas deshacerte de tu veh�culo personal, puedes:\n\
			- Deshacer tu veh�culo en el chatarrero y te devolver�n el 60%%. ({04cabf}/gps - Centros de veh�culo - Eliminaci�n{ffffff})\n\
			- Intercambiar veh�culos con otro jugador. ({04cabf}/vehpanel{ffffff})\n\
			- Vender veh�culos a un jugador. ({04cabf}/vehpanel{ffffff})\n\n\
			Para gestionar los veh�culos {04cabf}/vehpanel{ffffff} Las opciones son: Informaci�n, gesti�n del veh�culo, devoluci�n de veh�culos al aparcamiento,\n\
			buscar veh�culos en el mapa, estacionar veh�culos, intercambiar veh�culos, vender veh�culos a otro jugador." },
			
		{ 4, 3,
			!"{04cabf}Negocios est�ticos{ffffff}\n\
			Los negocios est�ticos son negocios que no est�n disponibles para la compra de un jugador pero tienen su propia funcionalidad.\n\n\
			{04cabf}Gasolinera{ffffff} (/gps - Buscar - Encuentra la gasolinera m�s cercana)\n\
			Aqu� puede llenar el veh�culo de gasolina (el precio de 1 litro es de $10) y comprar en una tienda:\n\
			Soda, cigarrillos, un vaso de caf�, un plato de mantequilla, una bolsa de carbones, una linterna, un botiqu�n de primeros auxilios, una lata de gasolina.\n\n\
			{04cabf}Tiendas de armas{ffffff} (/gps - Buscar - Encuentra la tienda m�s cercana - Tienda de armas)\n\
			Para comprar armas en la tienda necesitas una licencia de armas, en el servidor hay 3 tipos de licencia de armas,\n\
			cada uno de los cuales permite comprar solo 1 arma (opcional).\n\
			Puedes comprar licencias en la estaci�n de polic�a principal ({04cabf}/gps - Agencias gubernamentales - Departamento de Polic�a LS{ffffff}).\n\
			En la tienda encontrar�s: funda, nudillos de met�l, armadura corporal, M1911, Desert Eagle, escopeta, rifle, HK MP5 y los cartuchos necesarios.\n\n\
			{04cabf}Tiendas de muebles{ffffff} (/gps - Buscar - Encuentra la tienda m�s cercana - Tienda de muebles)\n\
			En la tienda de muebles puedes comprar muebles para tu propiedad. En total m�s de 2000 objetos est�n disponibles. Los precios van desde $5 a $5,000.\n\
			El menu para gestionar las cosas de la casa es /pcasa y para los negocios /npanel." },
			
		{ 5, -1,
			!"{04cabf}Facciones{ffffff}\n\n\
			{04cabf}Estatales{ffffff}\n\
			Gobierno (GOB) - Ayuntamiento\n\
			Police Department (PD) - Departamento de polic�a\n\
			Federal Bureau of Investigation (FBI) - Oficina Federal de Investigaciones\n\
			Fire Department (FD) - Departamento de Bomberos\n\
			Hendersons Medical Department (HMD) - Hospital Central\n\
			San Andreas Network (SAN) - Organizaci�n de noticias\n\
			National Park Service (NPS) - Parque nacional\n\n\
			{04cabf}Criminal:{ffffff}\n\
			Bloods - Pandilla\n\
			Crips - Pandilla\n\
			Mara Salvatrucha - Pandilla\n\
			Norte�os - Pandilla\n\
			Yakuza - Mafia\n\
			Mafia rusa- Mafia\n\
			Mafia Italo-Americana - Mafia\n\
			La Eme - Mafia\n\
			Bandidos - Motoqueros\n\
			Hells Angels - Motoqueros\n\
			Outlaws - Motoqueros" },
			
		{ 5, -1,
			!"{04cabf}Veh�culos{ffffff}\n\n\
			Los veh�culos personales no requieren tener una casa. Para comprarlo debes visitar uno de los tres salones Grotti ({04cabf}/gps - Centros de veh�culo{ffffff}).\n\n\
			Si deseas encender el motor presiona la tecla CTRL. Si deseas encender las luces usa ALT\n\
			Los coches tienen un limite de kilometraje y la velocidad se mide en MPH (Millas por horas. Est� informaci�n la encontrar�s en el velocimetro.\n\
			Recuerda colocar seguro a tu coche para evitar que te lo roben.\n\n\
			Si deseas deshacerte de tu veh�culo personal, puedes:\n\
			- Deshacer tu veh�culo en el chatarrero y te devolver�n el 60%%. ({04cabf}/gps - Centros de veh�culo - Eliminaci�n{ffffff})\n\
			- Intercambiar veh�culos con otro jugador. ({04cabf}/vehpanel{ffffff})\n\
			- Vender veh�culos a un jugador. ({04cabf}/vehpanel{ffffff})\n\n\
			Para gestionar los veh�culos {04cabf}/vehpanel{ffffff} Las opciones son: Informaci�n, gesti�n del veh�culo, devoluci�n de veh�culos al aparcamiento,\n\
			buscar veh�culos en el mapa, estacionar veh�culos, intercambiar veh�culos, vender veh�culos a otro jugador." }
		};


#define d_help_command		"\
								"cBLUE"1."cWHITE" Teclas\n\
								"cBLUE"2."cWHITE" General\n\
								"cBLUE"3."cWHITE" Chat\n\
								"cBLUE"4."cWHITE" Inventario\n\
								"cBLUE"5."cWHITE" Trabajos\n\
								"cBLUE"6."cWHITE" Propiedades\n\
								"cBLUE"7."cWHITE" Organizaciones gubernamentales\n\
								"cBLUE"8."cWHITE" Organizaciones criminales\n\
								"cBLUE"9."cWHITE" Cuenta premium\
							"
							
#define d_help_info			"\
								"cBLUE"1."cWHITE" Informaci�n general\n\
								"cBLUE"2."cWHITE" Inventario\n\
								"cBLUE"3."cWHITE" Telefonos\n\
								"cBLUE"4."cWHITE" Trabajos\n\
								"cBLUE"5."cWHITE" Propiedades\n\
								"cBLUE"6."cWHITE" Facciones\n\
								"cBLUE"7."cWHITE" Veh�culos\
							"
							
#define d_help_job			"\
								Silvicultura\n\
								Compa�ia de veh�culo de pasajeros\n\
								Empresa de veh�culo\n\
								Estaciones de servicio\n\
								Entrega de comida\
							"
							
#define d_help_property		"\
								Casas\n\
								Empresas\n\
								Veh�culos\n\
								Negocios est�ticos\
							"
							
#define d_help_start		"\
								"cBLUE"Ayuda"cWHITE"\n\n\
								Puedes encontrar ayuda m�s detallada en nuestro foro "cBLUE" foro.influx-rp.eu "cWHITE".\
							"