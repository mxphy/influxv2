new 
	Text:phoneOne[28],
    Text:phoneThree[19],
	Text:phoneFonOne[10],
	Text:phoneFonThree[10];
	
const
	MAX_NETWORKS = 12,
	MAX_PHONES = 20,
	MAX_CONTACTS = 50,
	MAX_MESSAGES = 100;
	
new
	zone_network[ MAX_NETWORKS ];
	
// Coordenadas de las torres
new Float:action_network[][] =
{
	{ 2349.5068, -1822.9983, 13.5469, 900.0 }, 		//0
	{ 1480.5703, -1659.7396, 14.0469, 900.0 }, 		//1
	{ 393.4778, -1525.2068, 32.2663, 900.0 }, 		//2
	{ 683.6657, -553.0502, 16.1875, 900.0 }, 		//3
	{ 232.5968, -130.8335, 1.4297, 500.0 }, 		//4
	{ 1267.7789, 296.4348, 19.5547, 500.0 }, 		//5
	{ 2348.8811, 26.1121, 26.3359, 500.0 }, 		//6
	{ -695.64587, -1788.56665, 115.11683, 900.0 }, 		//7
	{ -2159.80713, -2394.24023, 35.45660, 500.0 },	 	//8
	{ -1429.18689, -954.30798, 199.65851, 500.0 },	 	//9
	{ 2266.47729, 2449.57153, 36.42332, 500.0 },	 	//10
	{ 157.93884, 1085.79810, 25.44858, 500.0 }	 		//11
};

enum e_SOUND
{
	s_id,
	s_name[16]
}

new call_sound[][ e_SOUND ] = 
{ 
	{ 0, "Modo silencioso" },
	{ 19600, "Melod�a 1" },
	{ 5203, "Melod�a 2" },
	{ 20600, "Melod�a 3" }, 
	{ 31204, "Melod�a 4" },  
	{ 20804, "Melod�a 5" }, 
	{ 23000, "Melod�a 6" }, 
	{ 31202, "Melod�a 7" },
	{ 19800, "Melod�a 8" } 
};

new color_name[][] =
{
	{ "Azul" },
	{ "Verde" },
	{ "Azul 2" },  
	{ "Amarillo" }, 
	{ "Morado" }, 
	{ "Marr�n" },
	{ "Rojo oscuro" },
	{ "Naranja" },
	{ "Gris" },
	{ "Negro" }
};

enum e_PHONE
{	
	p_id,				// ID de tel�fono
	p_user_id,			// propietario del tel�fono
	p_number,			// n�mero de tel�fono
	p_settings[4],		// Configuraci�n del tel�fono
}

enum e_CONTACTS
{
	p_id,
	p_owner,			// N�mero de tel�fono del propietario
	p_number,			// n�mero registrado
	p_name[25],			// Nombre grabado
}

enum e_MESSAGES
{
	m_id,				// Auto Incremento
	m_number,			// n�mero de remitente
	m_numberto,			// N�mero del destinatario
	m_read,				// Estado del mensaje
	m_date,				// Fecha y hora de salida
	m_text[128],			// mensaje de texto
}

enum e_PHONE_CALL
{
	c_call_id, 			// identificador de llamadas
	bool:c_status,		// El estado de la llamada entrante
	bool:c_accept,		// Estado de aceptaci�n
	c_name[25],			// Nombre de la llamada entrante
	c_sound,			// Sonido de una llamada entrante
}

new
	Phone			[ MAX_PLAYERS ][ MAX_PHONES ][ e_PHONE ],
	PhoneContacts 	[ MAX_PLAYERS ][ MAX_CONTACTS ][ e_CONTACTS ],
	Messages		[ MAX_PLAYERS ][ MAX_MESSAGES ][ e_MESSAGES ],
	Call			[ MAX_PLAYERS ][ e_PHONE_CALL ],
	IsValidPhone	[ MAX_PLAYERS char ]; 								// Verificar un n�mero v�lido en la base de datos