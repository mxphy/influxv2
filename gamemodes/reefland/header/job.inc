#define job_description_prod 		"\
										"gbDefault"Trabajo de "cBLUE"transporte de mercancias"cWHITE".\n\n\
										Al aceptar el trabajo en la empresa de transporte, se firmar� un contrato de seis horas.\n\
										En este trabajo, puedes entregar recursos a f�bricas y almacenes, y transportar\n\
										Productos en establecimientos y empresas que requieren equipamiento especializado.\n\
										Todo el dinero ganado se transfiere a la chequera.\n\n\
										"cBLUE"Este trabajo requiere:"cWHITE"\n\n\
										- Nivel 2\n\
										- Licencia de conducir\n\
										- Tarjeta de identificaci�n\n\n\
										Podr�s encontrar los comandos necesarios en el menu de ayuda\
									"									

#define job_description_bus 		"\
										"gbDefault"Trabajo de "cBLUE"conductor de bus"cWHITE".\n\n\
										Al firmar un contrato de seis horas con la compa��a de pasajeros, comenzar� a trabajar como conductor de autob�s,\n\
										Viajando en una ruta establecida a trav�s del territorio.\n\
										Cuidado con la comodidad y seguridad de tus pasajeros.\n\
										Todo el dinero ganado se transfiere a la chequera.\n\n\
										"cBLUE"Este trabajo requiere:"cWHITE"\n\n\
										- Nivel 1\n\
										- Licencia de conducir\n\
										- Tarjeta de identificaci�n\n\n\
										Podr�s encontrar los comandos necesarios en el menu de ayuda\
									"	
									
#define job_description_taxi 		"\
										"gbDefault"Trabajo de "cBLUE"taxista"cWHITE".\n\n\
										Al firmar un contrato con la compa��a de pasajeros durante seis horas, se te entregar�n las llaves de uno de los autom�viles de la compa��a de taxis.\n\
										Tu tarea ser� recibir llamadas del despachador y transportar a los clientes en todo el estado.\n\
										Todo el dinero que ganas lo recibes inmediatamente.\n\n\
										"cBLUE"Este trabajo requiere:"cWHITE"\n\n\
										- Nivel 1\n\
										- Licencia de conducir\n\
										- Tarjeta de identificaci�n\n\n\
										Podr�s encontrar los comandos necesarios en el menu de ayuda\
									"	
							
#define job_description_mech 		"\
										"gbDefault"Trabajo de "cBLUE"mecanico"cWHITE".\n\n\
										Si firmas un contrato de trabajo con el departamento de servicio de autom�viles ser� durante seis horas,\n\
										Puedes usar la gr�a para moverte por la ciudad atendiendo llamadas del despachador.\n\
										Adem�s deber�s recoger los veh�culos da�ados y repararlos.\n\
										Todo el dinero ganado se transfiere a la chequera.\n\n\
										"cBLUE"Este trabajo requiere:"cWHITE"\n\n\
										- Nivel 2\n\
										- Licencia de conducir\n\
										- Tarjeta de identificaci�n\n\n\
										Podr�s encontrar los comandos necesarios en el menu de ayuda\
									"

#define job_description_wood 		"\
										"gbDefault"Trabajo de "cBLUE"le�ador"cWHITE".\n\n\
										Tu trabajo en la estaci�n de silvicultura ser� cortar y transferir\n\
										madera al transportador que la transporta a la tienda para su posterior procesamiento.\n\
										Todo el dinero ganado se transfiere a la chequera.\
									"	

#define job_description_food 		"\
										"gbDefault"Trabajo de "cBLUE"repartidor de comida"cWHITE".\n\n\
										Este trabajo no requiere un contrato de 6 horas. Tu tarea ser� entregar los alimentos al cliente que realiz� el pedido.\n\
										Puedes realizar la entrega de la forma que m�s te convenga: tanto por transporte personal como por un servicio de ciclomotor.\n\
										Todo el dinero ganado se transfiere a la chequera.\n\n\
										M�s informaci�n "cBLUE"/ayuda - Trabajos - Repartidor de comida\
									"									

// Lista de productos								
#define list_products				"\
										Alcohol FleischBerg\n\
										Sodas Sprunk\n\
										Di�xido de carbono de Solarin Industries\n\
										Ropa para Suburban\n\
										Piezas de Grotti\n\
										Materiales para FinalBuild Construction\
									"
// Di�logo de carga de mercanc�as.
#define load_products				"\
										"cWHITE"Especifique cu�ntos productos desea cargar:\n\n\
										"cRED"El veh�culo puede llevar %d productos\n\n\n\
										"gbDialog"Atenci�n, solamente se puede cargar mercanc�as una vez por pedido.\
									"

#define taxi_dialog					"\	
										"cWHITE"Despachador\n\
										"cWHITE"tarifa %s\n\
										"cWHITE"Cambiar tarifa\n\
										"cWHITE"Contador en cero\
									"

#define JOB_DRIVETAXI				( 1 )
#define JOB_DRIVEBUS				( 2 )									
#define JOB_PRODUCTS				( 3 )
#define JOB_MECHANIC				( 4 )
#define JOB_WOOD					( 5 )
#define JOB_FOOD					( 6 )
#define JOB_FISH					( 7 )

#define CHANNEL_TAXI				( 555 )
#define CHANNEL_MECHANIC			( 577 )
#define CHANNEL_TRUCKER				( 535 )

const
	MAX_COUNT_STOCKS = 100,
	MIN_COUNT_STOCKS = 10,
	
	MAX_STOCKS = 5,
	
	MAX_PRODUCTS_STOCKS = 6,
	
	MAX_LISTITEM_LOAD = 10,
	MAX_PRODUCTS_INCAR = 2000,
	
	MAX_ROUTES = 5,
	MAX_CHECKPOINTS = 100,
	EARN_FOR_CHECKPOINT = 4,
	
	MAX_TAXICALLS = 20,
	MAX_MECHCALLS = 20,
	
	MAX_BOX_PRODUCTS = 200,
	
	MAX_ZONES_REPAIR = 4,
	
	MAX_WOODS = 61,
	EARN_FOR_WOOD = 18, // Salario en silvicultura
	EARN_FOR_FISH = 20; // Salario en la pesca

enum e_JOB_INFO
{
	j_name[ 40 ],					// Nombre del trabajo
	Float: j_start_job_pos[3],		// Coordina el dispositivo de recogida para trabajar.
}

enum e_JOB
{
	j_vehicleid,					// ID del veh�culo de trabajo
	j_time,							// Tiempo de alquiler
	j_trailerid,					// Identificaci�n del trailer
	
	j_count,
	j_count_order,					// Buffer para almacenar productos durante la ejecuci�n del pedido
	j_earn,							// Beneficio de la jornada laboral.
	
	j_point,						// N�mero de puntos de control completados en la ruta.
	bool:j_taxi,					// Estado de la solicitud de taxista
	j_zone_unload,					// �rea de descarga
	
	bool:j_mech,					// Estado de la solicitud de taxista
	
	j_object_wood,					// Objeto de troncos en el transportador.
	j_time_wood,					// temporizador para borrar el registro
}

enum e_JOB_VEHICLE
{
	v_driverid,						// id del conductor
	v_count_tons[6],
	v_count_prod,					// El n�mero de productos en el coche.
	v_lock,							// Puertas del carro de trabajo.
	
	Float:v_damages,				// Da�os en tr�nsito
	Float:v_damages_trailer,
	
	v_bus_text,						// Texto en el parabrisas del bus.
	v_taxi_text,					// Texto en el parabrisas de un taxi.
	v_route,						// Ruta del autob�s
	
	v_rate,							// tarifa de taxi
	Float:v_mileage,				// Kilometraje para un taxi
	v_passenger,					// ID del pasajero que llam� al taxi
}

enum e_SERVER
{
	s_name[32],					// Nombre del almac�n			
	s_value[6],					// valores
	Float:s_pos_unload[3],		// Coordenadas de descarga
	Text3D:s_load_text,			// texto de acoplamiento 3D
}

enum e_BUS
{
	r_id,						// ID de ruta
	r_route,					// n�mero de ruta
	Float:r_pos[3],				// coordenadas del punto de control
	r_param,					// par�metro de parada
}

enum e_BUSROUTE
{
	r_number,					// n�mero de ruta
	r_point,					// Cuenta el n�mero de puntos de control
	r_description[32],			// Descripci�n de la ruta
}

enum e_TAXI
{
	t_playerid, 				// ID de la persona que llama
	t_place[30],				// Descripci�n del lugar de la convocatoria.
	t_zone[28],					// Nombre del distrito de donde vino la llamada.
	Float:t_pos[30],			// Coordenadas del punto de llamada
	t_time,						// tiempo de llamada
}

enum e_MECHANIC
{
	m_playerid, 				// ID de la persona que llama
	m_place[30],				// Descripci�n del lugar de la convocatoria.
	m_zone[28],					// Nombre del distrito de donde vino la llamada.
	Float:m_pos[30],			// Coordenadas del punto de llamada
	m_time,						// tiempo de llamada
}

enum e_WOOD 
{
	w_object,					// Objeto de �rbol
	w_zone,						// �rea de corte de madera
	bool:w_use,
	bool:w_drop,
	w_count,
	w_time						// Tiempo en minutos despu�s de que aparece un nuevo �rbol.
}

new const price_products[6] = { 23, 21, 24, 16, 17, 18 };			// Precio por 1 tonelada en almacenes.

new const name_products[][] = 
{
	{"Alcohol FleischBerg"},
	{"Sodas Sprunk"},
	{"Di�xido de carbono de Solarin Industries"},
	{"Ropa para Suburban"},
	{"Piezas de Grotti"},
	{"Materiales para FinalBuild Construction"}
};

new const name_trucker[][] = 
{
	{"FleishBerg"},
	{"Sprunk"},
	{"S.Industries"},
	{"Suburban"},
	{"Grotti"},
	{"F.Construction"}
};

new const 
	routes[] = { 454, 675, 893, 125, 353 };				// N�meros de rutas de autob�s

new
	Server				[ MAX_STOCKS ][ e_SERVER ],					// Variables del almac�n del servidor
	Job					[ MAX_PLAYERS ][ e_JOB ],					// variables del jugador
	VehicleJob			[ MAX_VEHICLES ][ e_JOB_VEHICLE ],			// coches de trabajo
	job_duty 			[ MAX_PLAYERS char ], 						// Estado de la actividad laboral
	job_load_truck 		[ MAX_STOCKS ][ MAX_LISTITEM_LOAD ],		// Almacena el ID del jugador en la cola para el enganche
	timer_order			[ MAX_PLAYERS ],							// Temporizador de entrega
	Bus					[ MAX_ROUTES ][ MAX_CHECKPOINTS ][ e_BUS ],	// sistema de bus
	Route				[ MAX_ROUTES ][ e_BUSROUTE ],				// Sistema auxiliar de disposici�n de puntos de control.
	Taxi				[ MAX_TAXICALLS ][ e_TAXI ],				// Sistema de llamadas de taxi
	g_dialog_taxi		[ MAX_TAXICALLS ],							
	g_dialog_mech		[ MAX_MECHCALLS ],							
	area_repair			[ MAX_ZONES_REPAIR ],						// Zonas de reparaci�n de transporte
	Mechanic			[ MAX_MECHCALLS ][ e_MECHANIC ],			// Mec�nica del sistema de llamadas.
	WoodInfo			[ MAX_WOODS ][ e_WOOD ];					// Sistema de corte de madera
	
new
	WOOD_ZONE, 		// �rea de aserradero
	WOOD_OBJECT[2];	// El objeto del registro.

// Transporte de trabajo
new
	cars_prod[2],		// Proveedores de productos / recursos.
	cars_taxi[2],		// Taxi
	cars_bus[2],		// Autobuses
	cars_mech[2],		// Mec�nica
	cars_food[2],		// ciclomotores
	cars_auto[3][2],	// concesionarios de autom�viles
	car_salon;			// concesionarios de autom�viles

new
	PlayerText:Taximeter[ MAX_PLAYERS ],	// contador
	PlayerText:Trucker[ MAX_PLAYERS ],		// contador
	PlayerText:Drivebus[ MAX_PLAYERS ],
	Text:TaxiBackground;					// Antecedentes
	
new 
	const job_info[][ e_JOB_INFO ] = 
	{
		{ "Empresa de transporte de mercancias", { 2127.6643, -2275.2849, 20.6719 } }, 		 // 0
		{ "Empresa de pasajeros", { 1147.75, -1341.01, 2201.09 } },  						 // 1
		{ "CTO", { 586.5350, -1540.9489, 2001.0859 } }, 									 // 2	
		{ "CTO", { 2514.3191, -1542.4061, 24.0026 } },										 // 3
		{ "CTO", { 1362.3328, 200.2774, 19.7161 } },										 // 4
		{ "CTO", { 2114.1174, -2151.3284, 13.7768 } },										 // 5
		{ "Silvicultura", { -506.1064, -67.1721, 1805.4299 } },							  	 // 6
		{ "Servicio de reparto de alimentos", { 1686.1561, -1455.9237, 1401.2169 } }		 // 7
	};

new const 
	Float:create_pos_trailer[ MAX_STOCKS ][4] = 
	{
		{ 2394.7578, -2211.6501, 14.5181, -45.0000  },
		{ 1019.5201, -324.6368, 74.9127, 180.0000 },
		{ -23.2184, -274.7552, 6.4114, 180.0000 },
		{ -1724.5596, -120.5365, 4.5092, 135.0000 },
		{ 2367.9763, 2755.7019, 11.7608, 180.0000 }
	},
	
	Float:zone_repair[][] =
	{
		{ 959.0286, -1338.1632, 982.7303, -1376.3478 },
		{ 2537.9460,-1514.6813, 2504.6143, -1546.0717 },
		{ 1347.2704, 212.8908, 1366.6686, 176.8843 },
		{ 2142.7305, -2205.2534, 2151.0955, -2137.0139 }
	},
	
	Float:Woods[][] = 
	{
		{ -467.75409, -111.47410, 61.99763, 76.91998, 7.44001 },
		{ -479.84219, -112.84488, 62.72820, -96.36001, 62.69997 },
		{ -472.50415, -121.78069, 64.18502, 67.85999, 30.24000 },
		{ -460.77368, -118.63423, 62.84046, -97.49996, 50.69994 },
		{ -463.72220, -127.97483, 65.39375, -104.64005, 64.92002 },
		{ -473.98294, -132.01703, 67.15408, 63.83998, 6.24000 },
		{ -492.77859, -110.58039, 62.92606, -7.20000, 88.68001 },
		{ -500.14249, -117.94637, 64.15530, -92.33999, 12.18000 },
		{ -486.78705, -119.29216, 64.11214, -31.20000, 85.67996 },
		{ -494.50360, -126.49559, 66.28484, -100.98004, 8.16000 },
		{ -482.83310, -128.02803, 66.21870, 7.55999, -86.58001 },
		{ -452.57767, -128.65932, 64.70294, 54.23998, -10.68000 },
		{ -443.25406, -124.55906, 63.59114, -14.82000, 81.42002 },
		{ -442.32678, -134.86931, 66.90623, -106.44002, -30.60000 },
		{ -458.27518, -134.94746, 67.62462, -12.36000, -85.67999 },
		{ -467.27386, -139.35941, 69.79500, 67.38002, 64.49999 },
		{ -450.64133, -140.73546, 69.65470, 61.38002, 5.82000 },
		{ -480.16464, -138.74776, 70.20619, 12.72000, -86.34001 },
		{ -488.91891, -134.30923, 68.72164, -61.62001, 91.25999 },
		{ -500.29614, -133.49290, 68.85472, -104.04003, -23.76001 },
		{ -494.79630, -139.86830, 70.97148, 53.04001, 32.58002 },
		{ -500.52927, -144.27196, 72.14676, -105.66007, 10.85999 },
		{ -486.10916, -143.50371, 72.32402, 72.66001, 18.72000 },
		{ -474.51755, -143.94804, 71.96494, 65.28002, 53.58000 },
		{ -479.60022, -149.97545, 73.71490, 39.24002, 83.04003 },
		{ -492.58591, -149.39046, 74.04378, -106.79999, -83.21989 },
		{ -501.51602, -153.76984, 74.15671, 76.01999, 6.35999 },
		{ -525.08569, -113.35001, 63.88410, -98.69998, 21.23999 },
		{ -526.74036, -124.38244, 67.15832, -91.02000, -82.68000 },
		{ -534.29492, -118.15231, 65.86768, -95.52006, 11.52000 },
		{ -541.14722, -112.00261, 63.76771, -95.58000, -19.14000 },
		{ -548.38531, -104.73421, 63.00396, 5.22000, -81.18002 },
		{ -553.06506, -113.69461, 65.25449, -15.60001, -51.83999 },
		{ -546.26062, -119.50565, 66.74438, -100.44003, -21.12000 },
		{ -537.35419, -125.82064, 67.96987, -94.14000, -61.50005 },
		{ -528.91809, -131.42244, 69.25891, -13.32001, -80.46003 },
		{ -538.87909, -134.43553, 70.93632, 62.70000, -18.96000 },
		{ -546.45819, -128.10844, 68.91123, 61.02001, -41.75996 },
		{ -559.85730, -121.62984, 67.78107, -26.64001, 81.59998 },
		{ -527.87512, -140.32082, 72.84001, -112.98000, 35.63999 },
		{ -537.19269, -145.02766, 74.63287, 36.47999, 80.70000 },
		{ -548.41211, -138.32950, 72.90410, 58.32001, -60.17998 },
		{ -554.99670, -129.43347, 69.62330, -33.77999, -83.21998 },
		{ -575.87195, -123.61123, 68.86987, -32.52001, 84.89998 },
		{ -566.14923, -132.29497, 71.16479, -21.72001, -84.66001 },
		{ -547.86865, -148.35999, 75.71080, -14.27999, -82.56001 },
		{ -559.59991, -141.44087, 74.68022, -108.05999, -29.03999 },
		{ -557.34485, -151.55611, 76.16746, -58.98001, -82.97997 },
		{ -578.68695, -133.31316, 72.01100, -3.42000, -83.22001 },
		{ -570.54797, -142.90590, 75.63692, -41.15992, -78.71997 },
		{ -568.43665, -151.76335, 76.42308, -12.77999, -88.32003 },
		{ -550.64233, -160.41142, 77.08839, 7.73999, 79.20001 },
		{ -525.21149, -149.36571, 74.56080, 24.18000, -69.66000 },
		{ -561.21240, -160.73605, 77.23878, 71.94013, 34.91987 },
		{ -588.60266, -124.01651, 69.59834, 72.78002, 66.05999 },
		{ -583.06335, -145.37561, 76.25792, -49.01999, -81.41999 },
		{ -592.10992, -107.61453, 66.07888, 69.72002, 19.20000 },
		{ -593.31763, -139.43323, 74.15447, -93.90004, -111.53999 },
		{ -590.95953, -151.45021, 77.07255, 79.98001, -1.08000 },
		{ -572.10406, -160.40286, 77.16729, 36.54000, -84.77996 },
		{ -436.35913, -142.64870, 69.64687, -104.21999, 11.34000 }
	};