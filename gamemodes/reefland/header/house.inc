#define HBUY_LIST			( 19 )

#define PERCENT_RENT		( 7 )
#define PERCENT_PAYMENT		( 2 )

#define MAX_BOXES 			( 12 )
#define MAX_3DMENUS 		( MAX_PLAYERS )

const
	PRICE_HOUSE_WALL = 600,
	PRICE_HOUSE_FLOOR = 400,
	PRICE_HOUSE_ROOF = 150,
	PRICE_HOUSE_STAIR = 250;
	

enum e_HOUSE 
{
	hID, 
	huID,						// jugador uid
	hType,						// 0 - casa,> 0 - piso
	hRent,						// Alquiler
	
	hOwner				[ 25 ],	// Nombre del propietario
	
	Float:hEnterPos[4],			// iniciar sesi�n
	Float:hExitPos[4],			// Salir
	
	hPrice, 					// Estado. el costo
	hSellDate, 					// Tiempo de alquiler
	
	hMoney,						// Dinero en la caja fuerte en casa
	
	hInterior, 					// n�mero interior
	hLock,						// Estado de la puerta
	
	hWall				[ 10 ],	// Muros
	hFloor				[ 10 ],	// pisos
	hRoof				[ 7 ],	// Techos
	hStairs,					// escaleras
	
	Text3D:hText[2],	// texto de entrada / salida 3D
	hPickup,			// recogida
	
	hCountFurn,			// Cantidad de muebles
	hSettings[3],		// Ajustes en la casa
}

enum e_HOUSE_INTERIOR
{
	h_type,
	Float:h_pos_exit[4],
	h_max_furn,				// N�mero de muebles m�ximo
	h_int_name[32],			// Nombre del interior.
	h_evict,				// El n�mero de personas que se pueden compartir.
	h_wall,					// N�mero de muros
	h_floor,				// N�mero de pisos
	h_roof,					// N�mero de techos
	h_stair,				// Presencia de escaleras
	h_inventory,			// N�mero de celdas de inventario
}

enum e_EVICT
{
	hEvictUID,			// UID de personas que son compartidas
	hEvictName[25],		// Nombres de personas que son compartidas
}

enum e_MENU3DINFO
{
	CurrTextureIndex,
	CurrTextureType,
	CurrPartNumber,
    Menus3D,
}

enum e_MenuParams
{
	Boxes,						// N�mero de cajas
	bool:IsExist,				// comprobar la existencia
	Objects				[12],	// Caja de objetos
	Float:OrigPosX		[12],
	Float:OrigPosY		[12],
	Float:OrigPosZ		[12],
	Float:AddingX,
	Float:AddingY,
	SelectColor			[12],
	UnselectColor		[12],
	MPlayer
}

new 
	HouseInfo		[ MAX_HOUSE ]	[ e_HOUSE ],
	HEvict			[ MAX_HOUSE	]	[ MAX_EVICT ]	[ e_EVICT ],
	HTextureWall 	[ MAX_HOUSE ]	[ MAX_HOUSE_WALL ]			[ 2 ],
	HTextureFloor	[ MAX_HOUSE ]	[ MAX_HOUSE_FLOOR ]			[ 2 ],
	HTextureRoof	[ MAX_HOUSE ]	[ MAX_HOUSE_ROOF ]			[ 2 ],
	HTextureStairs	[ MAX_HOUSE ]	[ 2 ],
	house_object_interior[2];

new 
	Menu3DData		[ MAX_PLAYERS ][ e_MENU3DINFO ],
	MenuInfo		[ MAX_3DMENUS ][ e_MenuParams ],
	SelectedMenu	[ MAX_PLAYERS ] = { -1, ...},
	SelectedBox		[ MAX_PLAYERS ],
	SelectedType	[ MAX_PLAYERS char ] = { 0, ... };
	
new
	hinterior_info[][ e_HOUSE_INTERIOR ] = 
	{
		{ 0, { -505.1115,-2800.2651,1601.0859,272.3192 }, 80, "Estudio", 0, 2, 2, 2, 0, 6 }, 		// 1
		{ 0, { -495.6302,-2796.4771,2001.0859,86.5551 }, 80, "Estudio", 0, 2, 2, 2, 0, 6 }, 			// 2
		{ 0, { -496.7046,-2796.0701,2401.0879,90.9392 }, 80, "Estudio", 0, 2, 2, 2, 0, 6 }, 			// 3
		{ 0, { -494.7816,-2798.8127,3001.0879,90.1270 }, 80, "Estudio", 0, 2, 2, 2, 0, 6 }, 			// 4
		{ 0, { -2706.8123,-2200.3049,2201.0859,266.7627 }, 80, "Estudio", 0, 2, 2, 2, 0, 6 }, 		// 5
		{ 0, { -1706.5236,-1496.2722,2101.0859,268.3687 }, 80, "Estudio", 0, 2, 3, 2, 0, 6 }, 		// 6
		{ 1, { 1.4354,-2798.9832,1601.0859,87.7641 }, 130, "1 habitaci�n", 1, 3, 3, 3, 0, 10 }, 		// 7
		{ 1, { -2.8288,-2799.5706,2001.0859,270.3531 }, 130, "1 habitaci�n", 1, 4, 4, 3, 0, 10 }, 		// 8
		{ 1, { -4.8213,-2799.1030,2401.0859,267.1596 }, 130, "1 habitaci�n", 1, 3, 3, 3, 0, 10 }, 		// 9
		{ 1, { -2704.4302,-1506.0428,1601.0859,267.5750 }, 130, "1 habitaci�n", 1, 4, 4, 4, 0, 10 }, 	// 10
		{ 1, { -2203.4561,-1496.5784,1601.0859,269.1440 }, 130, "1 habitaci�n", 1, 3, 4, 3, 0, 10 }, 	// 11
		{ 1, { -2206.5959,-1496.2676,2101.0859,274.7006 }, 130, "1 habitaci�n", 1, 3, 3, 2, 0, 10 }, 	// 12
		{ 1, { -1707.0074,-1502.9622,2701.0859,267.5565 }, 130, "1 habitaci�n", 1, 3, 3, 3, 0, 10 }, 	// 13
		{ 1, { -2698.2190,-1503.5283,2601.0859,88.9548 }, 130, "1 habitaci�n", 1, 3, 3, 3, 0, 10 }, 	// 14
		{ 2, { 503.1540,-2802.5222,1601.0859,90.5241 }, 170, "2 habitaciones", 2, 4, 4, 4, 0, 12 }, 		// 15
		{ 2, { 503.6868,-2805.9189,2001.0859,86.9704 }, 170, "2 habitaciones", 2, 3, 3, 3, 0, 12 }, 		// 16
		{ 2, { 504.8340,-2796.5630,2401.0859,84.9858 }, 170, "2 habitaciones", 2, 4, 4, 3, 0, 12 }, 		// 17
		{ 2, { -2706.4055,-1497.9352,2001.0859,271.9407 }, 170, "2 habitaciones", 2, 4, 4, 4, 0, 12 }, 	// 18
		{ 2, { -2203.3770,-1501.0178,2601.0859,269.5411 }, 170, "2 habitaciones", 2, 4, 4, 4, 0, 12 }, 	// 19
		{ 2, { -1701.5844,-1500.5055,1601.0859,269.1441 }, 170, "2 habitaciones", 2, 4, 4, 4, 0, 12 }, 	// 20
		{ 3, { 1001.3256,-2801.8030,1601.0859,88.1426 }, 180, "3 habitaciones", 3, 6, 5, 4, 0, 14 }, 	// 21
		{ 3, { 994.6646,-2799.7051,2001.0859,267.9534 }, 180, "3 habitaciones", 3, 5, 5, 5, 0, 14 }, 	// 22
		{ 3, { 996.7646,-2799.2610,2401.0859,269.1441 }, 180, "3 habitaciones", 3, 4, 4, 4, 0, 14 }, 	// 23
		{ 3, { -2708.9065,-2800.4470,2601.0859,267.5565 }, 180, "3 habitaciones", 3, 6, 6, 6, 0, 14 }, 	// 24
		{ 3, { -2705.8848,-2201.3079,1601.0859,266.7810 }, 180, "3 habitaciones", 3, 6, 6, 6, 0, 14 }, 	// 25
		{ 4, { 1497.8357,-2799.4946,1601.0859,269.5409 }, 200, "4 habitaciones", 4, 5, 6, 5, 0, 16 }, 		// 26
		{ 4, { 1497.4849,-2793.8259,2001.0859,269.9378 }, 200, "4 habitaciones", 4, 5, 4, 5, 0, 16 }, 		// 27
		{ 4, { 1492.8431,-2797.0398,2501.0879,266.7627 }, 200, "4 habitaciones", 4, 6, 4, 4, 0, 16 }, 		// 28
		{ 5, { -1011.4072,-2790.4788,2801.0859,269.5595 }, 240, "2 pisos 3 habitaciones", 5, 7, 7, 6, 1, 18 }, // 29
		{ 5, { -1501.1830,-2794.7498,1601.0859,179.4463 }, 240, "2 pisos 3 habitaciones", 5, 8, 7, 6, 1, 18 }, // 30
		{ 6, { -1501.7216,-2792.9841,2201.0859,176.2528 }, 270, "2 pisos 4 habitaciones", 6, 8, 9, 5, 1, 18 }, // 31
		{ 6, { -1498.0194,-2796.5198,2801.0859,177.0649 }, 270, "2 pisos 4 habitaciones", 6, 8, 7, 5, 1, 18 }, // 32
		{ 6, { -2098.9387,-2797.2893,1601.0859,178.6525 }, 270, "2 pisos 4 habitaciones", 6, 8, 8, 4, 1, 18 }, // 33
		{ 7, { -2107.3184,-2801.5286,2101.0859,269.5410 }, 310, "Mansi�n clase 1", 8, 9, 8, 6, 0, 20 }, // 34
		{ 7, { -2707.1895,-2800.3137,1601.0859,269.9378 }, 310, "Mansi�n clase 1", 8, 7, 8, 7, 0, 20 }, // 35
		{ 7, { -2705.5857,-2801.2996,2101.0859,269.1441 }, 310, "Mansi�n clase 1", 8, 10, 8, 6, 0, 20 }, // 36
		{ 8, { -1014.0668,-2800.0930,1601.0879,269.1440 }, 350, "Mansi�n clase 2", 10, 9, 7, 7, 1, 20 }, // 37
		{ 8, { -1003.6544,-2803.1565,2001.0859,358.4634 }, 350, "Mansi�n clase 2", 10, 10, 10, 4, 1, 20 } // 38
	};

// padiki
enum kInfo {
	kID,
	Float: kEnterPos[3],
	Float: kEnterPos_Two[3],
	Float: kExitPos[3],
	Float: kExitPos_Two[3],
	kInt,
	kCity,
	kVirtWorld,
	Float: kCamPos[3]
};
new Hostels[MAX_HOSTEL][kInfo];

	
enum ht_info {
	ht_id, 
	Float:ht_p[4], 
	ht_int, 
	ht_room, 
	ht_name[64],
	ht_list[256]
}		
/*
new Float:hostels_position[][8] = {
	{ 2728.38, -508.715, 883.086, 2734.51, -496.468, 883.086, 3.0 },
	{ 2518.58, -535.958, 883.086, 2530.58, -541.76, 883.086, 6.0 }
};*/

#define dialog_house_buy	""cWHITE"\
								1. Lista de viviendas\n\
								2. Ordenar por costo\n\
								3. Ordenar por tipo de vivienda\n\
								4. Encontrar vivienda\n\
								5. Vender tu casa\
							"	
							
#define dialog_house_type	"\
								"cBLUE"- "cGRAY"estudio\n\    			
								"cBLUE"- "cGRAY"1 habitaci�n\n\			
								"cBLUE"- "cGRAY"2 habitaciones\n\			
								"cBLUE"- "cGRAY"3 habitaciones\n\			
								"cBLUE"- "cGRAY"4 habitaciones\n\			
								"cBLUE"- "cGRAY"2 pisos 3 habitaciones\n\	
								"cBLUE"- "cGRAY"2 pisos 4 habitaciones\n\	
								"cBLUE"- "cGRAY"mansi�n clase 1\n\		
								"cBLUE"- "cGRAY"mansi�n clase 2\		
							"	

#define h_panel				"\
								"cBLUE"-"cWHITE" Informaci�n\n\
								"cBLUE"-"cWHITE" Sub-establecimiento\n\
								"cBLUE"-"cWHITE" Transacciones\n\
								"cBLUE"-"cWHITE" Ajustes\
							"
							
#define h_panel_p3			"\
								"cWHITE"Poner dinero\n\
								"cWHITE"Tomar dinero\
							"
							
#define h_panel_plan		"\
								"cWHITE"Retexturizaci�n\n\
								"cWHITE"Colocaci�n de muebles\n\
								"cWHITE"Muebles adicionales\n\
								"cWHITE"Configuraciones\
							"
							
#define h_panel_evict		"\
								"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
								"cWHITE"Uso de la caja fuerte\t"cBLUE"%s\n\
								"cWHITE"Uso de inventario\t"cBLUE"%s\n\
								"cWHITE"Colocaci�n de muebles\t"cBLUE"%s\
							"
							
#define h_panel_texture		"\
								"cWHITE"Cambiar paredes\n\
								"cWHITE"Cambiar piso\n\
								"cWHITE"Cambiar techo\n\
								"cWHITE"Cambiar escaleras\
							"							