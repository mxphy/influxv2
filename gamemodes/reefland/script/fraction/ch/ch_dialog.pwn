function City_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{	
	switch( dialogid ) 
	{	
		case d_meria:
		{
			if( !response ) return 1;
			
			showPlayerDialog( playerid, d_meria + 1, DIALOG_STYLE_LIST, "Recepci�n del alcalde", dialog_reception, "Seleccionar", "Cerrar" );
		}
		
		case d_meria + 1:
		{
			if( !response ) return 1;
			
			new
				index = INVALID_PARAM,
				year, month, day;
			
			switch( listitem )
			{
				case 0:
				{
					for( new i; i < MAX_RECOURSE; i++ )
					{
						if( Recourse[i][r_namefrom][0] != EOS && !strcmp( Recourse[i][r_namefrom], Player[playerid][uName], true ) )
							return SendClient:( playerid, C_WHITE, !""gbDefault"Ya ha dejado un mensaje antes, intente de nuevo m�s tarde." );
					
						if( Recourse[i][r_nameto][0] == EOS && index == INVALID_PARAM )
							index = i;
					}
					
					if( index == INVALID_PARAM )
						return SendClient:( playerid, C_WHITE, !""gbError"Int�ntalo de nuevo m�s tarde." );
					
					showPlayerDialog( playerid, d_meria + 2, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Dejar apelaci�n\n\n\
						"cWHITE"Indique a qui�n se dirigir� su apelaci�n:", "Siguiente", "Atr�s" );
				}
				
				case 1:
				{
					for( new i; i < MAX_RECOURSE; i++ )
					{
						if( Recourse[i][r_namefrom][0] != EOS && !strcmp( Recourse[i][r_namefrom], Player[playerid][uName], true ) )
						{
							index = i;
							break;
						}
					}
					
					if( index == INVALID_PARAM ) 
						return SendClient:( playerid, C_WHITE, !""gbError"Su b�squeda no fue encontrada." );
						
					clean:<g_big_string>;
					gmtime( Recourse[index][r_date], year, month, day );
					
					format:g_small_string( ""cBLUE"Apelar a %s\n\n", Recourse[index][r_nameto] );
					strcat( g_big_string, g_small_string );
					
					format:g_small_string( ""cWHITE"Fecha: "cBLUE"%02d.%02d.%d\n\n", day, month, year );
					strcat( g_big_string, g_small_string );
					
					format:g_string( ""cWHITE"Texto del mensaje:\n%s", Recourse[index][r_text] );
					strcat( g_big_string, g_string );
					
					if( Recourse[index][r_answer][0] != EOS )
					{
						format:g_string( "\n\n"cWHITE"Respuesta de %s:\n"cBLUE"%s", Recourse[index][r_nameans], Recourse[index][r_answer] );
						strcat( g_big_string, g_string );
					
						if( !Recourse[index][r_status] ) 
						{
							Recourse[index][r_status] = 1;
							
							mysql_format:g_small_string( "UPDATE `"DB_RECOURSE"` SET `r_status` = 1 WHERE `r_id` = %d LIMIT 1", Recourse[index][r_id] );
							mysql_tquery( mysql, g_small_string );
						}
					}
					
					showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Cerrar", "" );
				}
			}
		}
		
		case d_meria + 2:
		{
			if( !response ) return showPlayerDialog( playerid, d_meria + 1, DIALOG_STYLE_LIST, "Recepci�n del alcalde", dialog_reception, "Seleccionar", "Cerrar" );
		
			new
				index = INVALID_PARAM;
				
			if( inputtext[0] == EOS || strlen( inputtext ) > MAX_PLAYER_NAME )
			{
				return showPlayerDialog( playerid, d_meria + 2, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Dejar apelaci�n\n\n\
					"cWHITE"Indique a qui�n se dirigir� su apelaci�n:\n\
					"gbDialogError"Formato de entrada inv�lido.", "Siguiente", "Atr�s" );
			}
				
			for( new i; i < MAX_RECOURSE; i++ )
			{
				if( Recourse[i][r_nameto][0] == EOS  )
				{
					index = i;
					break;
				}
			}
			
			if( index == INVALID_PARAM ) 
			{
				SendClient:( playerid, C_WHITE, !""gbError"Int�ntalo de nuevo m�s tarde." );
				return showPlayerDialog( playerid, d_meria + 1, DIALOG_STYLE_LIST, "Recepci�n del alcalde", dialog_reception, "Seleccionar", "Cerrar" );
			}
			
			strcat( Recourse[index][r_nameto], inputtext, MAX_PLAYER_NAME );
			SetPVarInt( playerid, "CityHall:Recourse", index );
			
			format:g_small_string( "\
				"cBLUE"Apelar a %s\n\n\
				"cWHITE"Ingrese el texto de la apelaci�n:\n\n\
				"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
				el n�mero m�ximo de caracteres es 1024.\n\
				Deje el campo en blanco al completar la apelaci�n.", Recourse[index][r_nameto] );
				
			showPlayerDialog( playerid, d_meria + 3, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
		}
		
		case d_meria + 3:
		{
			if( !response )
			{
				Recourse[ GetPVarInt( playerid, "CityHall:Recourse" ) ][r_nameto][0] = EOS;
				DeletePVar( playerid, "CityHall:Recourse" );
				
				return showPlayerDialog( playerid, d_meria + 2, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Dejar apelaci�n\n\n\
					"cWHITE"Indique a qui�n se dirigir� su apelaci�n:", "Siguiente", "Atr�s" );
			}
			
			new
				index = GetPVarInt( playerid, "CityHall:Recourse" );
			
			if( Recourse[index][r_text][0] == EOS )
			{
				if( inputtext[0] == EOS )
				{
					format:g_string( "\
						"cBLUE"Apelar a %s\n\n\
						"cWHITE"Ingrese el texto de la apelaci�n:\n\n\
						"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por l�nea,\n\
						el n�mero m�ximo de caracteres es 1024.\n\
						Deje el campo en blanco al completar la apelaci�n.\n\n\
						"gbDialogError"El campo de entrada est� vac�o..", Recourse[index][r_nameto] );
						
					return showPlayerDialog( playerid, d_meria + 3, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Atr�s" );
				}
				else if( strlen( inputtext ) > 128 )
				{
					format:g_string( "\
						"cBLUE"Apelar a %s\n\n\
						"cWHITE"Ingrese el texto de la apelaci�n:\n\n\
						"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por l�nea,\n\
						el n�mero m�ximo de caracteres es 1024.\n\
						Deje el campo en blanco al completar la apelaci�n.\n\n\
						"gbDialogError"M�ximo de caracteres permitidos por l�nea.", Recourse[index][r_nameto] );
						
					return showPlayerDialog( playerid, d_meria + 3, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Atr�s" );
				}
				
				strcat( Recourse[index][r_text], inputtext, 1024 );
				
				format:g_big_string( "\
					"cBLUE"Apelar a %s\n\n\
					"cWHITE"Ingrese el texto de la apelaci�n:\n\n\
					"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por l�nea,\n\
					el n�mero m�ximo de caracteres es 1024.\n\
					Deje el campo en blanco al completar la apelaci�n.\n\n\
					"cBLUE"Texto:"cWHITE"\n\
					%s", Recourse[index][r_nameto], Recourse[index][r_text] );
						
				return showPlayerDialog( playerid, d_meria + 3, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Atr�s" );
			}
			else
			{
				if( inputtext[0] != EOS )
				{
					if( strlen( inputtext ) > 128 )
					{
						format:g_big_string( "\
							"cBLUE"Apelar a %s\n\n\
							"cWHITE"Ingrese el texto de la apelaci�n:\n\n\
							"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
							el n�mero m�ximo de caracteres es 1024.\n\
							Deje el campo en blanco al completar la apelaci�n.\n\n\
							"cBLUE"Texto:"cWHITE"\n\
							%s\n\n\
							"gbDialogError"M�ximo de caracteres permitidos por l�nea.", Recourse[index][r_nameto], Recourse[index][r_text] );
								
						return showPlayerDialog( playerid, d_meria + 3, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Atr�s" );
					}
					else if( Recourse[index][r_text][1023] != EOS )
					{
						format:g_big_string( "\
							"cBLUE"Apelar a %s\n\n\
							"cWHITE"Ingrese el texto de la apelaci�n:\n\n\
							"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
							el n�mero m�ximo de caracteres es 1024.\n\
							Deje el campo en blanco al completar la apelaci�n.\n\n\
							"cBLUE"Texto:"cWHITE"\n\
							%s\n\n\
							"gbDialogError"L�mite de caracteres superado.", Recourse[index][r_nameto], Recourse[index][r_text] );
								
						return showPlayerDialog( playerid, d_meria + 3, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Atr�s" );
					}
				
					strcat( Recourse[index][r_text], "\n", 1024 );
					strcat( Recourse[index][r_text], inputtext, 1024 );
				
					format:g_big_string( "\
						"cBLUE"Apelar a %s\n\n\
						"cWHITE"Ingrese el texto de la apelaci�n:\n\n\
						"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
						el n�mero m�ximo de caracteres es 1024.\n\
						Deje el campo en blanco al completar la apelaci�n.\n\n\
						"cBLUE"Texto:"cWHITE"\n\
						%s", Recourse[index][r_nameto], Recourse[index][r_text] );
							
					return showPlayerDialog( playerid, d_meria + 3, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Atr�s" );
				}
			}
			
			format:g_big_string( "\
				"cBLUE"Apelar a %s\n\n\
				"cWHITE"Texto:"cGRAY"\n\
				%s\n\n\
				"cWHITE"�Quieres enviar el mensaje?", Recourse[index][r_nameto], Recourse[index][r_text] );
				
			showPlayerDialog( playerid, d_meria + 4, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Si", "No" );
		}
		
		case d_meria + 4:
		{
			new
				index = GetPVarInt( playerid, "CityHall:Recourse" );
		
			if( !response )
			{
				Recourse[ GetPVarInt( playerid, "CityHall:Recourse" ) ][r_text][0] = EOS;
			
				format:g_small_string( "\
					"cBLUE"Apelar a %s\n\n\
					"cWHITE"Ingrese el texto de la apelaci�n:\n\n\
					"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
					el n�mero m�ximo de caracteres es 1024.\n\
					Deje el campo en blanco al completar la apelaci�n.", Recourse[index][r_nameto] );
					
				return showPlayerDialog( playerid, d_meria + 3, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			strcat( Recourse[index][r_namefrom], Player[playerid][uName], MAX_PLAYER_NAME );
			Recourse[index][r_date] = gettime();
			
			mysql_format:g_big_string( "INSERT INTO `"DB_RECOURSE"`\
				( `r_date`, `r_nameto`, `r_text`, `r_namefrom` ) VALUES \
				( %d, '%s', '%e', '%s' )",
				Recourse[index][r_date],
				Recourse[index][r_nameto],
				Recourse[index][r_text],
				Recourse[index][r_namefrom] );
			mysql_tquery( mysql, g_big_string, "InsertRecourse", "d", index );
			
			pformat:( ""gbSuccess"Dejaste tu apelaci�n a "cBLUE"%s"cWHITE". Tu mensaje ser� revisado lo antes posible.", Recourse[index][r_nameto] );
			psend:( playerid, C_WHITE );
			
			format:g_small_string( ""FRACTION_PREFIX" %s[%d] dej� una apelacio a "cBLUE"%s"cDARKGRAY" en la recepci�n.", Player[playerid][uName], playerid, Recourse[index][r_nameto] );
			
			foreach(new i: Player)
			{
				if( !IsLogged(i) ) continue;
				
				if( Player[playerid][uMember] == FRACTION_CITYHALL )
				{
					SendClient:( i, C_DARKGRAY, g_small_string );
				}
			}
			
			DeletePVar( playerid, "CityHall:Recourse" );
		}
		
		case d_meria + 5:
		{
			if( !response ) return 1;
			
			new
				index = g_dialog_select[playerid][listitem],
				day, month, year;
				
			SetPVarInt( playerid, "CityHall:Recourse", index );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
				
			clean:<g_big_string>;
			gmtime( Recourse[index][r_date], year, month, day );
				
			format:g_small_string( ""cBLUE"Apelar a %s\n\n", Recourse[index][r_nameto] );
			strcat( g_big_string, g_small_string );
					
			format:g_small_string( ""cWHITE"Fecha: "cBLUE"%02d.%02d.%d\n\n", day, month, year );
			strcat( g_big_string, g_small_string );
					
			format:g_string( ""cWHITE"Mensaje:"cGRAY"\n%s", Recourse[index][r_text] );
			strcat( g_big_string, g_string );
					
			if( Recourse[index][r_answer][0] != EOS )
			{
				format:g_string( "\n\n"cWHITE"Respuesta de %s:\n"cBLUE"%s\n\n", Recourse[index][r_nameans], Recourse[index][r_answer] );
				strcat( g_big_string, g_string );
					
				format:g_small_string( ""cWHITE"Estado: "cBLUE"%s", !Recourse[index][r_status] ? ("Sin revisar") : ("Visto") );
				strcat( g_big_string, g_small_string );
			}
			
			showPlayerDialog( playerid, d_meria + 6, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Siguiente", "Atr�s" );
		}
		
		case d_meria + 6:
		{
			if( !response )
			{
				DeletePVar( playerid, "CityHall:Recourse" );
				return cmd_recepcion( playerid );
			}
			
			showPlayerDialog( playerid, d_meria + 7, DIALOG_STYLE_LIST, "Acci�n", "\
				"cGRAY"Responder\n\
				"cGRAY"Eliminar", "Seleccionar", "Atr�s" );
				
		}
		
		case d_meria + 7:
		{
			if( !response )
			{
				DeletePVar( playerid, "CityHall:Recourse" );
				return cmd_recepcion( playerid );
			}
			
			new
				index = GetPVarInt( playerid, "CityHall:Recourse" ),
				day, month, year;
				
			switch( listitem )
			{
				case 0:
				{
					if( Recourse[index][r_answer][0] != EOS )
					{
						SendClient:( playerid, C_WHITE, !""gbError"Esta apelaci�n fue contestada anteriormente." );
					
						return showPlayerDialog( playerid, d_meria + 7, DIALOG_STYLE_LIST, "Acci�n", "\
							"cGRAY"Responder\n\
							"cGRAY"Eliminar", "Seleccionar", "Atr�s" );
					}
					
					format:g_small_string( "\
						"cWHITE"Responder apelaci�n de %s a "cBLUE"%s\n\n\
						"cWHITE"Ingrese su respuesta:\n\n\
						"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
						y el n�mero m�ximo de caracteres en la respuesta es 256.\n\
						Deje el campo en blanco al completar la respuesta.", Recourse[index][r_namefrom], Recourse[index][r_nameto] );
						
					showPlayerDialog( playerid, d_meria + 8, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
				
				case 1:
				{
					gmtime( Recourse[index][r_date], year, month, day );
				
					format:g_small_string( "\
						"cBLUE"Borrando una apelaci�n\n\n\
						"cWHITE"�Quieres eliminar la apelaci�n de "cBLUE"%s escrita el %02d.%02d.%d"cWHITE"?",
						Recourse[index][r_namefrom], day, month, year );
						
					showPlayerDialog( playerid, d_meria + 9, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Si", "No" );
				}
			}
		}
		
		case d_meria + 8:
		{
			if( !response )
			{
				DeletePVar( playerid, "CityHall:Recourse" );
			
				return cmd_recepcion( playerid );
			}
			
			new
				index = GetPVarInt( playerid, "CityHall:Recourse" );
				
				
			if( Recourse[index][r_answer][0] == EOS )
			{
				if( inputtext[0] == EOS )
				{
					format:g_string( "\
						"cWHITE"Responder la apelaci�n de %s a "cBLUE"%s\n\n\
						"cWHITE"Ingrese su respuesta:\n\n\
						"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
						y el n�mero m�ximo de caracteres en la respuesta es 256.\n\
						Deje el campo en blanco al completar la respuesta.\n\n\
						"gbDialogError"El campo de entrada est� vac�o..", Recourse[index][r_namefrom], Recourse[index][r_nameto] );
						
					return showPlayerDialog( playerid, d_meria + 8, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Atr�s" );
				}
				else if( strlen( inputtext ) > 128 )
				{
					format:g_string( "\
						"cWHITE"Responder la apelaci�n de %s a "cBLUE"%s\n\n\
						"cWHITE"Ingrese su respuesta:\n\n\
						"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
						y el n�mero m�ximo de caracteres en la respuesta es 256.\n\
						Deje el campo en blanco al completar la respuesta.\n\n\
						"gbDialogError"M�ximo de caracteres permitidos por l�nea.", Recourse[index][r_namefrom], Recourse[index][r_nameto] );
						
					return showPlayerDialog( playerid, d_meria + 8, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Atr�s" );
				}
				
				strcat( Recourse[index][r_answer], inputtext, 256 );
				
				format:g_big_string( "\
					"cWHITE"Responder la apelaci�n de %s a "cBLUE"%s\n\n\
					"cWHITE"Ingrese su respuesta:\n\n\
					"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
					y el n�mero m�ximo de caracteres en la respuesta es 256.\n\
					Deje el campo en blanco al completar la respuesta.\n\n\
					"cBLUE"Tu respuesta:"cWHITE"\n\
					%s", Recourse[index][r_namefrom], Recourse[index][r_nameto], Recourse[index][r_answer] );

				return showPlayerDialog( playerid, d_meria + 8, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Atr�s" );
			}
			else
			{
				if( inputtext[0] != EOS )
				{
					if( strlen( inputtext ) > 128 )
					{
						format:g_big_string( "\
							"cWHITE"Responder la apelaci�n de %s a "cBLUE"%s\n\n\
							"cWHITE"Ingrese su respuesta:\n\n\
							"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
							y el n�mero m�ximo de caracteres en la respuesta es 256.\n\
							Deje el campo en blanco al completar la respuesta.\n\n\
							"cBLUE"Tu respuesta:"cWHITE"\n\
							%s\n\n\
							"gbDialogError"M�ximo de caracteres permitidos por l�nea.", Recourse[index][r_namefrom], Recourse[index][r_nameto], Recourse[index][r_answer] );

						return showPlayerDialog( playerid, d_meria + 8, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Atr�s" );
					}
					else if( Recourse[index][r_answer][255] != EOS )
					{
						format:g_big_string( "\
							"cWHITE"Responder la apelaci�n de %s a "cBLUE"%s\n\n\
							"cWHITE"Ingrese su respuesta:\n\n\
							"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
							y el n�mero m�ximo de caracteres en la respuesta es 256.\n\
							Deje el campo en blanco al completar la respuesta.\n\n\
							"cBLUE"Tu respuesta:"cWHITE"\n\
							%s\n\n\
							"gbDialogError"Se supera el l�mite de caracteres permitido en la respuesta.", Recourse[index][r_namefrom], Recourse[index][r_nameto], Recourse[index][r_answer] );

						return showPlayerDialog( playerid, d_meria + 8, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Atr�s" );
					}
					
					strcat( Recourse[index][r_answer], "\n", 256 );
					strcat( Recourse[index][r_answer], inputtext, 256 );
				
					format:g_big_string( "\
						"cWHITE"Responder la apelaci�n de %s a "cBLUE"%s\n\n\
						"cWHITE"Ingrese su respuesta:\n\n\
						"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
						y el n�mero m�ximo de caracteres en la respuesta es 256.\n\
						Deje el campo en blanco al completar la respuesta.\n\n\
						"cBLUE"Tu respuesta:"cWHITE"\n\
						%s", Recourse[index][r_namefrom], Recourse[index][r_nameto], Recourse[index][r_answer] );

					return showPlayerDialog( playerid, d_meria + 8, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Atr�s" );
				}
			}
			
			strcat( Recourse[index][r_nameans], Player[playerid][uName], MAX_PLAYER_NAME );
			
			mysql_format:g_string( "UPDATE `"DB_RECOURSE"` \
				SET \
				`r_answer` = '%e', \
				`r_nameans` = '%s' \
				WHERE \
				`r_id` = %d LIMIT 1", 
				Recourse[index][r_answer], 
				Recourse[index][r_nameans], 
				Recourse[index][r_id] );
			mysql_tquery( mysql, g_string );
			
			DeletePVar( playerid, "CityHall:Recourse" );
			
			pformat:( ""gbSuccess"Usted respondi� a la apelaci�n de "cBLUE"%s"cWHITE".", Recourse[index][r_namefrom] );
			psend:( playerid, C_WHITE );
			
			cmd_recepcion( playerid );
		}
		
		case d_meria + 9:
		{
			if( !response )
			{
				DeletePVar( playerid, "CityHall:Recourse" );
				return cmd_recepcion( playerid );
			}
			
			new
				index = GetPVarInt( playerid, "CityHall:Recourse" );
				
			mysql_format:g_small_string( "DELETE FROM `"DB_RECOURSE"` WHERE `r_id` = %d LIMIT 1", Recourse[index][r_id] );
			mysql_tquery( mysql, g_small_string );
			
			pformat:( ""gbSuccess"Eliminaste la apelaci�n de "cBLUE"%s"cWHITE".", Recourse[index][r_namefrom] );
			psend:( playerid, C_WHITE );
			
			Recourse[index][r_id] =
			Recourse[index][r_date] =
			Recourse[index][r_status] = 0;
			
			Recourse[index][r_nameto][0] = 
			Recourse[index][r_namefrom][0] = 
			Recourse[index][r_nameans][0] = 
			Recourse[index][r_text][0] = 
			Recourse[index][r_answer][0] = EOS;
			
			DeletePVar( playerid, "CityHall:Recourse" );
		}
		
		case d_meria + 10:
		{
			if( !response )
			{
				clean:<Document[ GetPVarInt( playerid, "Document:Player" ) ][ GetPVarInt( playerid, "Document:Index" ) ][d_name]>;
				
				DeletePVar( playerid, "Document:Player" );
				DeletePVar( playerid, "Document:Index" );
				
				return 1;
			}
			
			if( !listitem )
			{
				clean:<g_string>;
				strcat( g_string, ""gbDialog"Seleccione el tipo de documento"cWHITE"" );
				
				for( new i; i < sizeof document_type; i++ )
				{
					format:g_small_string( "\n%s", document_type[i] );
					strcat( g_string, g_small_string );
				}
				
				return showPlayerDialog( playerid, d_meria + 10, DIALOG_STYLE_LIST, " ",g_string, "Seleccionar", "Cerrar" );
			}
			
			new
				takeid = GetPVarInt( playerid, "Document:Player" ),
				index = GetPVarInt( playerid, "Document:Index" );
				
			Document[takeid][index][d_type] = listitem - 1;
			
			format:g_string( "\
				"cWHITE"Creaci�n de documentos "cBLUE"%s"cWHITE" para "cBLUE"%s\n\n\
				"cWHITE"Ingrese el texto del documento:\n\n\
				"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
				y el n�mero m�ximo de caracteres en un documento es 512.\n\
				Deje el campo en blanco al finalizar la creaci�n del documento.", document_type[ listitem - 1 ], Player[takeid][uName] );
				
			showPlayerDialog( playerid, d_meria + 11, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Cerrar" );
		}
		
		case d_meria + 11:
		{
			if( !response )
			{
				clean:<Document[ GetPVarInt( playerid, "Document:Player" ) ][ GetPVarInt( playerid, "Document:Index" ) ][d_name]>;
				clean:<Document[ GetPVarInt( playerid, "Document:Player" ) ][ GetPVarInt( playerid, "Document:Index" ) ][d_text]>;
				Document[ GetPVarInt( playerid, "Document:Player" ) ][ GetPVarInt( playerid, "Document:Index" ) ][d_type] = 0;
				
				DeletePVar( playerid, "Document:Player" );
				DeletePVar( playerid, "Document:Index" );
				
				return 1;
			}
			
			new
				takeid = GetPVarInt( playerid, "Document:Player" ),
				index = GetPVarInt( playerid, "Document:Index" );
			
			if( Document[takeid][index][d_text][0] == EOS )
			{
				if( inputtext[0] == EOS )
				{
					format:g_string( "\
						"cWHITE"Creaci�n de documentos "cBLUE"%s"cWHITE" para "cBLUE"%s\n\n\
						"cWHITE"Ingrese el texto del documento:\n\n\
						"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
						y el n�mero m�ximo de caracteres en un documento es 512.\n\
						Deje el campo en blanco al finalizar la creaci�n del documento.\n\n\
						"gbDialogError"El campo de entrada est� vac�o..", document_type[ Document[takeid][index][d_type] ], Player[takeid][uName] );
						
					return showPlayerDialog( playerid, d_meria + 11, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Cerrar" );
				}
				else if( strlen( inputtext ) > 128 )
				{
					format:g_string( "\
						"cWHITE"Creaci�n de documentos "cBLUE"%s"cWHITE" para "cBLUE"%s\n\n\
						"cWHITE"Ingrese el texto del documento:\n\n\
						"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
						y el n�mero m�ximo de caracteres en un documento es 512.\n\
						Deje el campo en blanco al finalizar la creaci�n del documento.\n\n\
						"gbDialogError"M�ximo de caracteres permitidos por l�nea.", document_type[ Document[takeid][index][d_type] ], Player[takeid][uName] );
						
					return showPlayerDialog( playerid, d_meria + 11, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Cerrar" );
				}
				
				strcat( Document[takeid][index][d_text], inputtext, 512 );
				
				format:g_big_string( "\
					"cWHITE"Creaci�n de documentos "cBLUE"%s"cWHITE" para "cBLUE"%s\n\n\
					"cWHITE"Ingrese el texto del documento:\n\n\
					"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
					y el n�mero m�ximo de caracteres en un documento es 512.\n\
					Deje el campo en blanco al finalizar la creaci�n del documento.\n\n\
					"cBLUE"Texto:"cWHITE"\n\
					%s", document_type[ Document[takeid][index][d_type] ], Player[takeid][uName], Document[takeid][index][d_text] );
						
				return showPlayerDialog( playerid, d_meria + 11, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Cerrar" );
			}
			else
			{
				if( inputtext[0] != EOS )
				{
					if( strlen( inputtext ) > 128 )
					{
						format:g_big_string( "\
							"cWHITE"Creaci�n de documentos "cBLUE"%s"cWHITE" para "cBLUE"%s\n\n\
							"cWHITE"Ingrese el texto del documento:\n\n\
							"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
							y el n�mero m�ximo de caracteres en un documento es 512.\n\
							Deje el campo en blanco al finalizar la creaci�n del documento.\n\n\
							"cBLUE"Texto:"cWHITE"\n\
							%s\n\n\
							"gbDialogError"M�ximo de caracteres permitidos por l�nea.", document_type[ Document[takeid][index][d_type] ], Player[takeid][uName], Document[takeid][index][d_text] );
								
						return showPlayerDialog( playerid, d_meria + 11, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Cerrar" );
					}
					else if( Document[takeid][index][d_text][511] != EOS )
					{
						format:g_big_string( "\
							"cWHITE"Creaci�n de documentos "cBLUE"%s"cWHITE" para "cBLUE"%s\n\n\
							"cWHITE"Ingrese el texto del documento:\n\n\
							"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
							y el n�mero m�ximo de caracteres en un documento es 512.\n\
							Deje el campo en blanco al finalizar la creaci�n del documento.\n\n\
							"cBLUE"Texto:"cWHITE"\n\
							%s\n\n\
							"gbDialogError"Se super� el l�mite de caracteres en el documento.", document_type[ Document[takeid][index][d_type] ], Player[takeid][uName], Document[takeid][index][d_text] );
								
						return showPlayerDialog( playerid, d_meria + 11, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Cerrar" );
					}
					
					strcat( Document[takeid][index][d_text], "\n", 512 );
					strcat( Document[takeid][index][d_text], inputtext, 512 );
					
					format:g_big_string( "\
						"cWHITE"Creaci�n de documentos "cBLUE"%s"cWHITE" para "cBLUE"%s\n\n\
						"cWHITE"Ingrese el texto del documento:\n\n\
						"cGRAY"Ingrese el texto en filas, no m�s de 128 caracteres por linea,\n\
						y el n�mero m�ximo de caracteres en un documento es 512.\n\
						Deje el campo en blanco al finalizar la creaci�n del documento.\n\n\
						"cBLUE"Texto:"cWHITE"\n\
						%s", document_type[ Document[takeid][index][d_type] ], Player[takeid][uName], Document[takeid][index][d_text] );
							
					return showPlayerDialog( playerid, d_meria + 11, DIALOG_STYLE_INPUT, " ", g_big_string, "Siguiente", "Cerrar" );
				}
			}
			
			format:g_string( "\
				"cBLUE"Creaci�n de documentos para %s\n\n\
				"cWHITE"Tipo: "cBLUE"%s\n\n\
				"cWHITE"Texto:\n\
				"cGRAY"%s\n\n\
				"cWHITE"�Realmente quieres crear este documento?", 
				Player[takeid][uName], 
				document_type[ Document[takeid][index][d_type] ], 
				Document[takeid][index][d_text] );
				
			showPlayerDialog( playerid, d_meria + 12, DIALOG_STYLE_MSGBOX, " ", g_string, "Si", "No" );
		}
		
		case d_meria + 12:
		{
			new
				takeid = GetPVarInt( playerid, "Document:Player" ),
				index = GetPVarInt( playerid, "Document:Index" );
		
			if( !response )
			{
				clean:<Document[ takeid ][ index ][d_name]>;
				clean:<Document[ takeid ][ index ][d_text]>;
				
				Document[ takeid ][ index ][d_type] = 0;
				
				DeletePVar( playerid, "Document:Player" );
				DeletePVar( playerid, "Document:Index" );
				
				return 1;
			}
			
			if( !IsLogged( takeid ) || GetDistanceBetweenPlayers( playerid, takeid ) > 3.0 || GetPlayerVirtualWorld( playerid ) != GetPlayerVirtualWorld( takeid ) )
			{
				clean:<Document[ takeid ][ index ][d_name]>;
				clean:<Document[ takeid ][ index ][d_text]>;
				
				Document[ takeid ][ index ][d_type] = 0;
				
				DeletePVar( playerid, "Document:Player" );
				DeletePVar( playerid, "Document:Index" );
				
				SendClient:( playerid, C_WHITE, !""gbError"Se produjo un error al crear el documento." );
				
				return 1;
			}
			
			Document[ takeid ][ index ][d_date] = gettime();
			
			mysql_format:g_big_string( "INSERT INTO `"DB_DOCUMENT"`\
				( `d_user_id`, `d_type`, `d_date`, `d_name`, `d_text` ) VALUES \
				( %d, %d, %d, '%s', '%e' )",
				Player[takeid][uID],
				Document[ takeid ][ index ][d_type], 
				Document[ takeid ][ index ][d_date],
				Document[ takeid ][ index ][d_name],
				Document[ takeid ][ index ][d_text] );
				
			mysql_tquery( mysql, g_big_string, "InsertDocument", "dd", takeid, index );
			
			pformat:( ""gbDefault"%s ha firmado su documento. Utilizar "cBLUE"/doc"cWHITE", para abrir sus documentos.", Player[playerid][uName] );
			psend:( takeid, C_WHITE );
			
			pformat:( ""gbSuccess"Usted ha creado un documento para "cBLUE"%s[%d]"cWHITE".", Player[takeid][uName], takeid );
			psend:( playerid, C_WHITE );
			
			DeletePVar( playerid, "Document:Player" );
			DeletePVar( playerid, "Document:Index" );
		}
		//Mis documentos
		case d_meria + 13:
		{
			if( !response ) return 1;
			
			SetPVarInt( playerid, "Document:Index", g_dialog_select[playerid][listitem] );
			
			ShowDocument( playerid, playerid, g_dialog_select[playerid][listitem], d_meria + 14, "Acciones", "Atr�s" );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
		}
		
		case d_meria + 14:
		{
			if( !response ) 
			{
				DeletePVar( playerid, "Document:Index" );
				return cmd_doc( playerid );
			}
			
			showPlayerDialog( playerid, d_meria + 15, DIALOG_STYLE_LIST, "Acciones", ""cGRAY"Mostrar documento\n"cGRAY"Eliminar documento", "Seleccionar", "Atr�s" );
		}
		
		case d_meria + 15:
		{
			if( !response ) 
			{
				DeletePVar( playerid, "Document:Index" );
				return cmd_doc( playerid );
			}
			
			switch( listitem )
			{
				case 0:
				{
					showPlayerDialog( playerid, d_meria + 16, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Mostrar documento\n\n\
						"cWHITE"Ingrese la ID del jugador al que desea mostrar este documento:", "Siguiente", "Atr�s" );
				}
				
				case 1:
				{
					showPlayerDialog( playerid, d_meria + 17, DIALOG_STYLE_MSGBOX, " ", "\
						"cBLUE"Eliminar documento\n\n\
						"cWHITE"�Realmente quieres eliminar este documento?", "Si", "No" );
				}
			}
		}
		
		case d_meria + 16:
		{
			if( !response ) return showPlayerDialog( playerid, d_meria + 15, DIALOG_STYLE_LIST, "Acciones", ""cGRAY"Mostrar documento\n"cGRAY"Eliminar documento", "Seleccionar", "Atr�s" );

			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) > MAX_PLAYERS || strval( inputtext ) < 0 )
			{
				return showPlayerDialog( playerid, d_meria + 16, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Mostrar documento\n\n\
					"cWHITE"Ingrese la ID del jugador al que desea mostrar este documento:\n\
					"gbDialogError"Formato de entrada inv�lido.", "Siguiente", "Atr�s" );
			}
			
			if( !IsLogged( strval( inputtext ) ) || strval( inputtext ) == playerid )
			{
				return showPlayerDialog( playerid, d_meria + 16, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Mostrar documento\n\n\
					"cWHITE"Ingrese la ID del jugador al que desea mostrar este documento:\n\
					"gbDialogError"ID de jugador inv�lida", "Siguiente", "Atr�s" );
			}
			
			if( GetDistanceBetweenPlayers( playerid, strval( inputtext ) ) > 3.0 || GetPlayerVirtualWorld( playerid ) != GetPlayerVirtualWorld( strval( inputtext ) ) )
			{
				return showPlayerDialog( playerid, d_meria + 16, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Mostrar documento\n\n\
					"cWHITE"Ingrese la ID del jugador al que desea mostrar este documento:\n\
					"gbDialogError"Este jugador no est� cerca de ti.", "Siguiente", "Atr�s" );
			}
			
			if( g_player_interaction{ strval( inputtext ) } )
			{
				return showPlayerDialog( playerid, d_meria + 16, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Mostrar documento\n\n\
					"cWHITE"Ingrese la ID del jugador al que desea mostrar este documento:\n\
					"gbDialogError"No puedes interactuar con este jugador.", "Siguiente", "Atr�s" );
			}
			
			ShowDocument( strval( inputtext ), playerid, GetPVarInt( playerid, "Document:Index" ), INVALID_DIALOG_ID, "Cerrar" );
			
			MeAction( playerid, "Mostr� un documento", 1 );
			
			pformat:( ""gbDefault"Le mostraste el documento a "cBLUE"%s[%d]"cWHITE".", Player[ strval( inputtext ) ][uName], strval( inputtext ) );
			psend:( playerid, C_WHITE );
			
			DeletePVar( playerid, "Document:Index" );
		}
		
		case d_meria + 17:
		{
			if( response ) return showPlayerDialog( playerid, d_meria + 15, DIALOG_STYLE_LIST, "Acciones", ""cGRAY"Mostrar documento\n"cGRAY"Eliminar documento", "Seleccionar", "Atr�s" );
			
			new
				index = GetPVarInt( playerid, "Document:Index" );
			
			mysql_format:g_small_string( "DELETE FROM `"DB_DOCUMENT"` WHERE `d_id` = %d LIMIT 1", Document[playerid][index][d_id] );
			mysql_tquery( mysql, g_small_string );
			
			Document[playerid][index][d_id] = 
			Document[playerid][index][d_type] = 
			Document[playerid][index][d_date] = 0;
			
			clean:<Document[playerid][index][d_text]>;
			clean:<Document[playerid][index][d_name]>;
			
			SendClient:( playerid, C_WHITE, ""gbDefault"Has eliminado correctamente el documento." );
			
			DeletePVar( playerid, "Document:Index" );
		}
	}
	
	return 1;
}