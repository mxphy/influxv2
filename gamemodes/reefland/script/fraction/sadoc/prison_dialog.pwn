function Prison_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{
	switch( dialogid ) 
	{
		case d_prison: 
		{
			if( !response ) return 1;
			
			switch( listitem ) 
			{
				// Abrir-cerrar el bloque de c�mara D
				case 0: 
				{
					if( all_block[BLOCK_D] ) 
					{
						SendClient:( playerid, C_GRAY, ""gbDefault"Has cerrado todas las celdas de la unidad 'D'." );
						movePrisonBlock( BLOCK_D, false );
					}	
					else 
					{
						SendClient:( playerid, C_GRAY, ""gbDefault"Has abierto todas las celdas de la unidad 'D'." );
						movePrisonBlock( BLOCK_D, true );
					}	
					
					format:g_string( prison_computer, 
						all_block[BLOCK_D] ? ("Cerrar"):("Abrir"),
						all_block[BLOCK_E] ? ("Cerrar"):("Abrir"),
						prison_alarm ? ("Desactivar"):("Activar")
					);
					showPlayerDialog( playerid, d_prison, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Cerrar" );
				}
				
				// Abrir-Cerrar E Block Cameras
				case 1: 
				{
					if( all_block[BLOCK_E] ) 
					{
						SendClient:( playerid, C_GRAY, ""gbDefault"Has cerrado todas las celdas de la unidad 'E'." );
						movePrisonBlock( BLOCK_E, false );
					}	
					else 
					{
						SendClient:( playerid, C_GRAY, ""gbDefault"Has abierto todas las celdas de la unidad 'E'." );
						movePrisonBlock( BLOCK_E, true );
					}	
					
					format:g_string( prison_computer, 
						all_block[BLOCK_D] ? ("Cerrar"):("Abrir"),
						all_block[BLOCK_E] ? ("Cerrar"):("Abrir"),
						prison_alarm ? ("Desactivar"):("Activar")
						);
					showPlayerDialog( playerid, d_prison, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Cerrar" );
				}
				// Control de c�maras de CCTV
				case 2:
				{
					showPlayerDialog( playerid, d_prison + 1, DIALOG_STYLE_LIST, "Video vigilancia", "\
						"cBLUE"1. "cWHITE"Vigilancia de la calle\n\
						"cBLUE"2. "cWHITE"Vigilancia del bloque A\n\
						"cBLUE"3. "cWHITE"Vigilancia del bloque B\n\
						"cBLUE"4. "cWHITE"Vigilancia del bloque C\n\
						"cBLUE"5. "cWHITE"Vigilancia del bloque D,E\n\
						"cBLUE"6. "cWHITE"Vigilancia del bloque F\n\
						"cBLUE"7. "cWHITE"Vigilancia del sotano", "Seleccionar", "Atr�s" );
				}
				// Base de datos
				case 3:
				{
					showPlayerDialog( playerid, d_prison + 2, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Base de datos\n\n\
						"cWHITE"Ingrese el Nombre y Apellido del Preso:\n\
						"gbDialog"Ejemplo: Logan_McMillan", "Siguiente", "Atr�s" );
				}
				// alarma
				case 4:
				{
					if( prison_alarm )
					{
						prison_alarm = false;
						SendClient:( playerid, C_WHITE, !""gbDefault"Has desactivado la alarma." );
						
						foreach(new i: Player)
						{
							if( !IsLogged(i) ) continue;
							
							if( GetPlayerVirtualWorld(i) == 23 )
							{
								PlayerPlaySound( i, 0, 0.0, 0.0, 0.0 );
							}
							else if( !GetPlayerVirtualWorld(i) && IsPlayerInDynamicArea( i, prison_zone ) )
							{
								PlayerPlaySound( i, 0, 0.0, 0.0, 0.0 );
							}
						}
					}
					else
					{
						prison_alarm = true;
						SendClient:( playerid, C_WHITE, !""gbDefault"Has activado la alarma." );
						
						foreach(new i: Player)
						{
							if( !IsLogged(i) ) continue;
							
							if( GetPlayerVirtualWorld(i) == 23 )
							{
								PlayerPlaySound( i, 3401, 0.0, 0.0, 2.0 );
							}
							else if( !GetPlayerVirtualWorld(i) && IsPlayerInDynamicArea( i, prison_zone ) )
							{
								PlayerPlaySound( i, 3401, 0.0, 0.0, 2.0 );
							}
						}
					}
					
					format:g_string( prison_computer, 
						all_block[BLOCK_D] ? ("Cerrar"):("Abrir"),
						all_block[BLOCK_E] ? ("Cerrar"):("Abrir"),
						prison_alarm ? ("Desactivar"):("Activar")
						);
					showPlayerDialog( playerid, d_prison, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Cerrar" );
				}
			}
		}
		
		case d_prison + 1: 
		{
			if( !response ) 
			{
				format:g_string( prison_computer, 
					all_block[BLOCK_D] ? ("Cerrar"):("Abrir"),
					all_block[BLOCK_E] ? ("Cerrar"):("Abrir"),
					prison_alarm ? ("Desactivar"):("Activar")
					);
					
				return showPlayerDialog( playerid, d_prison, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Cerrar" );
			}
			
			switch( listitem ) 
			{
				case 0: initCCTV( playerid, 6 );
				case 1: initCCTV( playerid, 4 );
				case 2: initCCTV( playerid, 3 );
				case 3: initCCTV( playerid, 2 );
				case 4: initCCTV( playerid, 1 );
				case 5: initCCTV( playerid, 7 );
				case 6: initCCTV( playerid, 5 );
			}
			
			SendClient:( playerid, C_WHITE, !""gbDefault"Utiliza "cBLUE"las flechas"cWHITE" para cambiar entre las videoc�maras, usa "cBLUE"N"cWHITE" para salir del modo de visualizaci�n." );
		}
		
		case d_prison + 2:
		{
			if( !response )
			{
				format:g_string( prison_computer, 
					all_block[BLOCK_D] ? ("Cerrar"):("Abrir"),
					all_block[BLOCK_E] ? ("Cerrar"):("Abrir"),
					prison_alarm ? ("Desactivar"):("Activar")
					);
					
				return showPlayerDialog( playerid, d_prison, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Cerrar" );
			}
			
			if( inputtext[0] == EOS || strlen( inputtext ) > MAX_PLAYER_NAME )
			{
				return showPlayerDialog( playerid, d_prison + 2, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Base de datos\n\n\
					"cWHITE"Ingrese el Nombre y Apellido del Preso:\n\
					"gbDialog"Ejemplo: Logan_McMillan\n\n\
					"gbDialogError"El formato ingresado no es valido.", "Siguiente", "Atr�s" );
			}
			
			format:g_small_string( "\
				"cWHITE"�Deseas hacer una consulta a "cBLUE"%s"cWHITE"?", inputtext );
			showPlayerDialog( playerid, d_prison + 3, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
			
			PrisonTemp[ p_name ][0] =
			PrisonTemp[ p_namearrest ][0] =
			PrisonTemp[ p_reason ][0] =
			PrisonTemp[ p_reasonudo ][0] = EOS;
			
			PrisonTemp[ p_camera ] =
			PrisonTemp[ p_uID ] =
			PrisonTemp[ p_cooler ] =
			PrisonTemp[ p_date ] =
			PrisonTemp[ p_dateexit ] =
			PrisonTemp[ p_dateudo ] = 0;
			
			mysql_format:g_small_string( "SELECT * FROM "DB_PRISON" WHERE `pName` = '%s' AND `pStatus` = 1", inputtext );
			mysql_tquery( mysql, g_small_string, "DownloadArrest" );
		}
		
		case d_prison + 3:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_prison + 2, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Base de datos\n\n\
					"cWHITE"Ingrese el Nombre y Apellido del Preso:\n\
					"gbDialog"Ejemplo: Logan_McMillan", "Siguiente", "Atr�s" );
			}
			
			if( PrisonTemp[ p_name ][0] == EOS )
			{
				return showPlayerDialog( playerid, d_prison + 2, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Base de datos\n\n\
					"cWHITE"Ingrese el Nombre y Apellido del Preso:\n\
					"gbDialog"Ejemplo: Logan_McMillan\n\n\
					"gbDialogError"No se encontr� ningun prisionero con ese nombre.", "Siguiente", "Atr�s" );
			}
			
			new
				year,
				month,
				day;
			
			clean:<g_big_string>;
			
			format:g_small_string( ""cWHITE"Prisionero "cBLUE"#%d\n\n", PrisonTemp[p_uID] );
			strcat( g_big_string, g_small_string );
			
			format:g_small_string( ""cWHITE"Nombre: "cBLUE"%s\n", PrisonTemp[p_name] );
			strcat( g_big_string, g_small_string );
			
			gmtime( PrisonTemp[p_date], year, month, day );
			
			format:g_small_string( ""cWHITE"Desde: "cBLUE"%02d.%02d.%d\n", day, month, year );
			strcat( g_big_string, g_small_string );
			
			format:g_small_string( ""cWHITE"Numero de celda: "cBLUE"%d\n", PrisonTemp[p_camera] );
			strcat( g_big_string, g_small_string );
			
			if( PrisonTemp[p_cooler] )
			{
				format:g_small_string( ""cWHITE"Numero de celda de castigo: "cBLUE"%d\n", PrisonTemp[p_cooler] );
				strcat( g_big_string, g_small_string );
			}
			
			format:g_small_string( ""cWHITE"Raz�n:\n"cBLUE"%s\n\n", PrisonTemp[p_reason] );
			strcat( g_big_string, g_small_string );
			
			
			format:g_small_string( ""cWHITE"Procesado por: "cBLUE"%s\n", PrisonTemp[p_namearrest] );
			strcat( g_big_string, g_small_string );
			
			
			if( PrisonTemp[p_dateexit] == INVALID_PARAM )
			{
				strcat( g_big_string, ""cWHITE"Fecha de salida: perpetua\n\n");
			}
			else
			{
				gmtime( PrisonTemp[p_dateexit], year, month, day );
				
				format:g_small_string( ""cWHITE"Fecha de salida: "cBLUE"%02d.%02d.%d\n\n", day, month, year );
				strcat( g_big_string, g_small_string );
			}
			
			if( PrisonTemp[p_dateudo] )
			{
				gmtime( PrisonTemp[p_dateudo], year, month, day );
				
				format:g_small_string( ""cWHITE"Liberaci�n antes: "cBLUE"%02d.%02d.%d\n", day, month, year );
				strcat( g_big_string, g_small_string );
				
				format:g_small_string( ""cWHITE"Raz�n:\n"cBLUE"%s", PrisonTemp[p_reasonudo] );
				strcat( g_big_string, g_small_string );
			}
			
			showPlayerDialog( playerid, d_prison + 4, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atr�s", "" );
		}
		
		case d_prison + 4:
		{
			format:g_string( prison_computer, 
				all_block[BLOCK_D] ? ("Cerrar"):("Abrir"),
				all_block[BLOCK_E] ? ("Cerrar"):("Abrir"),
				prison_alarm ? ("Desactivar"):("Activar")
			);
					
			return showPlayerDialog( playerid, d_prison, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Cerrar" );
		}

		
		case d_arrest: 
		{
			if( !response ) 
			{
				DeletePVar( playerid, "Arrest:ID" ), DeletePVar( playerid, "Arrest:ar_info" ),
				DeletePVar( playerid, "Arrest:ar_date" ), DeletePVar( playerid, "Arrest:ar_cam" );
				
				g_player_interaction{playerid} = 0;
				
				return 1;
			}
			
			switch( listitem )
			{
				case 0:
				{
					showArrestMenu( playerid );
					return 1;
				}
			
				case 1: 
				{
					showPlayerDialog( playerid, d_arrest + 1, DIALOG_STYLE_INPUT, " ", 
						"Indique el motivo del arresto del jugador:", "Siguiente", "Atr�s" );	
				}
				
				case 2: 
				{
					showPlayerDialog( playerid, d_arrest + 2, DIALOG_STYLE_INPUT, " ", 
						"Ingrese la cantidad de d�as de arresto:\n\
						"gbDialog"-1 para cadena perpetua", "Siguiente", "Atr�s" );	
				}
				
				case 3: 
				{
					showPlayerDialog( playerid, d_arrest + 3, DIALOG_STYLE_INPUT, " ", 
						"Especifique el n�mero de celda para el jugador.\n\
						"gbDialog"Total de celdas: 27", "Siguiente", "Atr�s" );	
				}
				
				case 4: 
				{
					if( !GetPVarInt( playerid, "Arrest:ar_info" ) ||
						!GetPVarInt( playerid, "Arrest:ar_date" ) || 
						!GetPVarInt( playerid, "Arrest:ar_cam" ) ) 
					{
						SendClient:( playerid, C_WHITE, !""gbError"Debes completar todos los campos ." );
						showArrestMenu( playerid );
						return 1;
					}	
					
					new 
						giveplayerid = GetPVarInt( playerid, "Arrest:ID" );
					
					if( !IsLogged( giveplayerid ) || GetDistanceBetweenPlayers( playerid, giveplayerid ) > 3.0 || GetPlayerVirtualWorld(playerid) != GetPlayerVirtualWorld( giveplayerid ) )
					{
						SendClient:( playerid, C_WHITE, !""gbError"Ning�n jugador cerca de ti." );
						
						DeletePVar( playerid, "Arrest:ID" ), DeletePVar( playerid, "Arrest:ar_info_text" ),
						DeletePVar( playerid, "Arrest:ar_info" ), DeletePVar( playerid, "Arrest:ar_date" ), 
						DeletePVar( playerid, "Arrest:ar_cam" );
						
						g_player_interaction{playerid} = 0;
						
						return 1;
					}
					
					Prison[ giveplayerid ][p_date] = gettime();
					
					if( GetPVarInt( playerid, "Arrest:ar_date" ) > INVALID_PARAM )
						Prison[ giveplayerid ][p_dateexit] = gettime() + ( GetPVarInt( playerid, "Arrest:ar_date" ) * 86400 );
					else
						Prison[ giveplayerid ][p_dateexit] = INVALID_PARAM;
					
					Prison[ giveplayerid ][p_camera] = GetPVarInt( playerid, "Arrest:ar_cam" );
					GetPVarString( playerid, "Arrest:ar_info_text", Prison[ giveplayerid ][p_reason], 128 );
					
					format( Prison[ giveplayerid ][p_namearrest], MAX_PLAYER_NAME, "%s", Player[playerid][uName] );
					
					mysql_format:g_string( "INSERT INTO `"DB_PRISON"` \
						( `puID`, `pCamera`, `pName`, `pArrestName`, `pReason`, `pDate`, `pExitDate` ) VALUES \
						( %d, %d, '%s', '%s', '%s', %d, %d )",
						Player[ giveplayerid ][uID],
						Prison[ giveplayerid ][p_camera],
						Player[ giveplayerid ][uName],
						Prison[ giveplayerid ][p_namearrest],
						Prison[ giveplayerid ][p_reason], 
						Prison[ giveplayerid ][p_date],
						Prison[ giveplayerid ][p_dateexit] );
					mysql_tquery( mysql, g_string );
					
					setPlayerPos( giveplayerid, 
						pr_camera[ Prison[ giveplayerid ][p_camera] - 1 ][0],
						pr_camera[ Prison[ giveplayerid ][p_camera] - 1 ][1],
						pr_camera[ Prison[ giveplayerid ][p_camera] - 1 ][2] );
					setPrisonWeather( playerid );
					
					pformat:( ""gbDefault"Oficial "cBLUE"%s"cWHITE" te puso en la prisi�n federal.", Player[playerid][uRPName] );
					psend:( giveplayerid, C_WHITE );
					
					pformat:( ""gbDefault"Has puesto a "cBLUE"%s"cWHITE" en la prisi�n federal.", Player[ giveplayerid ][uRPName] );
					psend:( playerid, C_WHITE );
					
					DeletePVar( playerid, "Arrest:ID" ), DeletePVar( playerid, "Arrest:ar_info_text" ),
					DeletePVar( playerid, "Arrest:ar_info" ), DeletePVar( playerid, "Arrest:ar_date" ), 
					DeletePVar( playerid, "Arrest:ar_cam" );
					
					g_player_interaction{playerid} = 0;
				}
			}
		}
		
		case d_arrest + 1: 
		{
			if( !response ) 
			{
				showArrestMenu( playerid );
				return 1;
			}
			
			if( inputtext[0] == EOS || strlen( inputtext ) > 128 ) 
			{
				return showPlayerDialog( playerid, d_arrest + 1, DIALOG_STYLE_INPUT, " ", 
					"Indique el motivo del arresto del jugador.:\n\
					"gbDialogError"El formato ingresado no es valido.", "Siguiente", "Atr�s" );	
			}
			
			SetPVarInt( playerid, "Arrest:ar_info", 1 );
			SetPVarString( playerid, "Arrest:ar_info_text", inputtext );
			
			showArrestMenu( playerid );
		}
		
		case d_arrest + 2: 
		{
			if( !response ) 
			{
				showArrestMenu( playerid );
				return 1;
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) ) 
			{
				return showPlayerDialog( playerid, d_arrest + 2, DIALOG_STYLE_INPUT, " ", 
					"Ingrese la cantidad de d�as de arresto:\n\
					"gbDialog"-1 para cadena perpetua\n\n\
					"gbDialogError"El formato ingresado no es valido.", "Siguiente", "Atr�s" );	
			}
			
			if( strval( inputtext ) < INVALID_PARAM || strval( inputtext ) == 0 || strval( inputtext ) > 365 )
			{
				return showPlayerDialog( playerid, d_arrest + 2, DIALOG_STYLE_INPUT, " ", 
					"Ingrese la cantidad de d�as de arresto:\n\
					"gbDialog"-1 para cadena perpetua\n\n\
					"gbDialogError"El n�mero de d�as debe ser de -1 a 365 (sin incluir 0).", "Siguiente", "Atr�s" );	
			}
			
			SetPVarInt( playerid, "Arrest:ar_date", strval( inputtext ) );
			
			showArrestMenu( playerid );
		}
		
		
		case d_arrest + 3: 
		{
			if( !response ) 
			{
				showArrestMenu( playerid );
				return 1;
			}
						
			if( !IsNumeric( inputtext ) || strval( inputtext ) < 1 || strval( inputtext ) > 27 ) 
			{
				return showPlayerDialog( playerid, d_arrest + 3, DIALOG_STYLE_INPUT, " ", 
					"Especifique el n�mero de celda para el jugador\n\
					"gbDialog"Total celdas: 27\n\n\
					"gbDialogError"El formato ingresado no es valido.", "Siguiente", "Atr�s" );
			}
			
			SetPVarInt( playerid, "Arrest:ar_cam", strval( inputtext ) );
			showArrestMenu( playerid );
		}
	}
	
	return 1;
}