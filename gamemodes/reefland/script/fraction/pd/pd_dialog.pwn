function Police_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{	
	switch( dialogid ) 
	{	
		case d_police:
		{
			if( !response ) return 1;
				
			switch( listitem )
			{	// multas
				case 0: 
				{
					ShowPlayerPenalties( playerid, d_police + 23, "Atr�s" );
				}
				// Obtener el n�mero
				case 1:
				{
					ShowPlayerVehicleList( playerid, d_police + 1, "Atr�s" );
				}
				// Licencia de armas
				case 2:
				{
					if( GetStatusPlayerLicense( playerid, LICENSE_GUN_3 ) )
					{
						SendClient:( playerid, C_WHITE, !""gbError"`Ya tienes todos los niveles disponibles para licencias de armas." );
						return showPlayerDialog( playerid, d_police, DIALOG_STYLE_LIST, "Mostrador de informaci�n", info_dialog, "Seleccionar", "Cerrar" );
					}
				
				
					showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
						Obtenci�n de licencias\n\
						Informaci�n del recibo", "Seleccionar", "Atr�s" );
				}
			}
		}
		
		case d_police + 1:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_police, DIALOG_STYLE_LIST, "Mostrador de informaci�n", info_dialog, "Seleccionar", "Cerrar" );
			}
			
			if( !listitem ) 
			{
				ShowPlayerVehicleList( playerid, d_police + 1, "Atr�s" );
			}
			else
			{
				if( Vehicle[ g_dialog_select[playerid][listitem] ][vehicle_number][0] != EOS )
				{
					SendClient:( playerid, C_WHITE, !""gbError"Este veh�culo ya tiene matr�cula." );
					ShowPlayerVehicleList( playerid, d_police + 1, "Atr�s" );
					
					return 1;
				}
			
				SetPVarInt( playerid, "Player:VId", g_dialog_select[playerid][listitem] );
				
				format:g_small_string( "\
					"cBLUE"Conseguir una matr�cula\n\n\
					"cWHITE"�Est�s seguro que quieres obtener una matricula para "cBLUE"%s"cWHITE"?\n\
					"gbDefault"Costo de adquisicion $%d.", GetVehicleModelName( Vehicle[ g_dialog_select[playerid][listitem] ][vehicle_model] ), 300 );
					
				showPlayerDialog( playerid, d_police + 2, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
				g_dialog_select[playerid][listitem] = INVALID_PARAM;
			}
		}
		
		case d_police + 2:
		{
			if( !response )
			{
				DeletePVar( playerid, "Player:VId" );
				ShowPlayerVehicleList( playerid, d_police + 1, "Atr�s" );
				
				return 1;
			}
			
			if( Player[playerid][uMoney] < 300 )
			{
				SendClient:( playerid, C_WHITE, !NO_MONEY );
			
				DeletePVar( playerid, "Player:VId" );
				ShowPlayerVehicleList( playerid, d_police + 1, "Atr�s" );
				
				return 1;
			}
			
			new
				vid = GetPVarInt( playerid, "Player:VId" ),
				abcnumber[10] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' },
				abcstring[26] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' },
				number[10];
			
			for( new i; i < 7; i++ )
			{
				if( i < 1 || i > 3 )
				{
					Vehicle[ vid ][vehicle_number][i] = abcnumber[ random(10) ];
					continue;
				}
				
				Vehicle[ vid ][vehicle_number][i] = abcstring[ random(26) ];
			}
			
			SetPlayerCash( playerid, "-", 300 );
			
			format( number, sizeof number, "%s", Vehicle[vid][vehicle_number] );
			SetVehicleNumberPlate( vid, number );
			
			mysql_format:g_small_string( "UPDATE `"DB_VEHICLES"` SET `vehicle_number` = '%e' WHERE `vehicle_id` = %d LIMIT 1", Vehicle[vid][vehicle_number], Vehicle[vid][vehicle_id] );
			mysql_tquery( mysql, g_small_string );
			
			pformat:( ""gbSuccess"Matricula actualizada a "cBLUE"%s"cWHITE" de coche %s.", Vehicle[vid][vehicle_number], GetVehicleModelName( Vehicle[vid][vehicle_model] ) );
			psend:( playerid, C_WHITE );
			
			DeletePVar( playerid, "Player:VId" );
		}
		
		case d_police + 3:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_police, DIALOG_STYLE_LIST, "Mostrador de informaci�n", info_dialog, "Seleccionar", "Cerrar" );
			}
			
			switch( listitem )
			{
				case 0:
				{
					new
						bool:flag = false;
				
					for( new i; i < MAX_APPLICATION; i++ )
					{
						if( PApply[i][a_user_id] == Player[playerid][uID] )
						{
							SendClient:( playerid, C_WHITE, !""gbError"Ya tienes una solicitud para portar armas." );
						
							return showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
								Obtenci�n de licencias\n\
								Informaci�n del recibo", "Seleccionar", "Atr�s" );
						}
					
						if( !PApply[i][a_id] ) flag = true;						
					}
					
					if( !flag )
					{
						SendClient:( playerid, C_WHITE, !""gbError"La cola para las solicitudes de licencia es demasiado grande." );
						
						return showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
							Obtenci�n de licencias\n\
							Informaci�n del recibo", "Seleccionar", "Atr�s" );
					}
				
					for( new i; i < MAX_PENALTIES; i++ )
					{
						if( Penalty[playerid][i][pen_id] && !Penalty[playerid][i][pen_type] )
						{
							SendClient:( playerid, C_WHITE, !""gbError"Tienes multas sin pagar. Debes pagarlas primero." );
						
							return showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
								Obtenci�n de licencias\n\
								Informaci�n del recibo", "Seleccionar", "Atr�s" );
						}
					}
					
					for( new i; i < MAX_LICENSES; i++ )
					{
						if( License[playerid][i][lic_id] && License[playerid][i][lic_take_date] )
						{
							SendClient:( playerid, C_WHITE, !""gbError"Ya has retirado tu licencia." );
						
							return showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
								Obtenci�n de licencias\n\
								Informaci�n del recibo", "Seleccionar", "Atr�s" );
						}
					}
					
					if( !GetStatusPlayerLicense( playerid, LICENSE_GUN_1 ) )
					{
						if( Player[playerid][uMoney] < PRICE_LIC_GUN_1 )
						{
							SendClient:( playerid, C_WHITE, !NO_MONEY );
							
							return showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
								Obtenci�n de licencias\n\
								Informaci�n del recibo", "Seleccionar", "Atr�s" );
						}

						SetPVarInt( playerid, "Player:GunLevel", 1 );
						format:g_small_string( "\
							"cBLUE" \n\n\
							"cWHITE"�Te gustar�a solicitar la licencia de armas clase A?\n\
							"gbDialog"El costo para obtener una licencia es de "cBLUE"$%d"cWHITE".", PRICE_LIC_GUN_1 );
					}
					else if( !GetStatusPlayerLicense( playerid, LICENSE_GUN_2 ) )
					{
						if( Player[playerid][uMoney] < PRICE_LIC_GUN_2 )
						{
							SendClient:( playerid, C_WHITE, !NO_MONEY );
							
							return showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
								Obtenci�n de licencias\n\
								Informaci�n del recibo", "Seleccionar", "Atr�s" );
						}
					
						SetPVarInt( playerid, "Player:GunLevel", 2 );
						format:g_small_string( "\
							"cBLUE"Licencia de armas\n\n\
							"cWHITE"�Te gustar�a solicitar la licencia de armas clase B?\n\
							"gbDialog"El costo para obtener una licencia es de "cBLUE"$%d"cWHITE".", PRICE_LIC_GUN_2 );
					}
					else if( !GetStatusPlayerLicense( playerid, LICENSE_GUN_3 ) )
					{
						if( Player[playerid][uMoney] < PRICE_LIC_GUN_3 )
						{
							SendClient:( playerid, C_WHITE, !NO_MONEY );
							
							return showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
								Obtenci�n de licencias\n\
								Informaci�n del recibo", "Seleccionar", "Atr�s" );
						}
					
						SetPVarInt( playerid, "Player:GunLevel", 3 );
						format:g_small_string( "\
							"cBLUE"Licencia de armas\n\n\
							"cWHITE"�Te gustar�a solicitar la licencia de armas clase C?\n\
							"gbDialog"El costo para obtener una licencia es de "cBLUE"$%d"cWHITE".", PRICE_LIC_GUN_3 );
					}
					
					showPlayerDialog( playerid, d_police + 4, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
					
				}
				//Informaci�n del recibo licencias
				case 1:
				{
					format:g_big_string( info_weapon, PRICE_LIC_GUN_1, PRICE_LIC_GUN_2, PRICE_LIC_GUN_3 );
					showPlayerDialog( playerid, d_police + 24, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atr�s", "" );
				}
			}
		}
		
		case d_police + 4:
		{
			if( !response )
			{
				DeletePVar( playerid, "Player:GunLevel" );
			
				return showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Obtenci�n de licencias\n\
					Informaci�n del recibo", "Seleccionar", "Atr�s" );
			}
			
			new
				index = INVALID_PARAM,
				level = GetPVarInt( playerid, "Player:GunLevel" );
				
			for( new i; i < MAX_APPLICATION; i++ )
			{
				if( !PApply[i][a_id] )
				{
					index = i;
					break;
				}						
			}
					
			if( index == INVALID_PARAM )
			{
				DeletePVar( playerid, "Player:GunLevel" );
				SendClient:( playerid, C_WHITE, !""gbError"La cola para las solicitudes de licencia es demasiado grande." );
				
				return showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Obtenci�n de licencias\n\
					Informaci�n del recibo", "Seleccionar", "Atr�s" );
			}
			
			PApply[index][a_level] = level;
			PApply[index][a_user_id] = Player[playerid][uID];
			PApply[index][a_date] = gettime();
			
			clean:<PApply[index][a_name]>;
			strcat( PApply[index][a_name], Player[playerid][uName], MAX_PLAYER_NAME );
			
			switch( level )
			{
				case 1: SetPlayerCash( playerid, "-", PRICE_LIC_GUN_1 );
				case 2: SetPlayerCash( playerid, "-", PRICE_LIC_GUN_2 );
				case 3: SetPlayerCash( playerid, "-", PRICE_LIC_GUN_3 );
			}
			
			mysql_format:g_small_string( "\
				INSERT INTO `"DB_APPLICATION"` \
				( `a_level`, `a_user_id`, `a_name`, `a_date` ) VALUES \
				( %d, %d, '%s', %d )",
				PApply[index][a_level],
				PApply[index][a_user_id],
				PApply[index][a_name],
				PApply[index][a_date]
			);
			mysql_tquery( mysql, g_small_string, "AddApplycation", "d", index );
			
			DeletePVar( playerid, "Player:GunLevel" );
			
			format:g_small_string( "\
				"cBLUE"Informaci�n\n\n\
				"cWHITE"Has enviado una solicitud para una licencia de armas nivel "cBLUE"%d"cWHITE".\n\
				En caso de que su solicitud sea rechazada se le devolver� la totalidad del dinero a su cuenta bancaria", level );
			showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Cerrar", "" );
		}
		
		case d_police + 5:
		{
			if( !response ) return 1;
			
			SetPVarInt( playerid, "Fraction:SelectApply", g_dialog_select[playerid][listitem] );
			
			format:g_small_string( ""cWHITE"%s nivel %d", PApply[ g_dialog_select[playerid][listitem] ][a_name], PApply[ g_dialog_select[playerid][listitem] ][a_level] );
			showPlayerDialog( playerid, d_police + 6, DIALOG_STYLE_LIST, g_small_string, ""cWHITE"\
				Aprobar\n\
				Rechazar", "Seleccionar", "Atr�s" );
				
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
		}
		
		case d_police + 6:
		{
			if( !response )
			{
				DeletePVar( playerid, "Fraction:SelectApply" );
			
				ShowApplications( playerid );
				return 1;
			}
			
			new
				index = GetPVarInt( playerid, "Fraction:SelectApply" );
				
			if( !PApply[index][a_id] )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Alguien ya revis� esta solicitud antes." );
			
				DeletePVar( playerid, "Fraction:SelectApply" );
			
				ShowApplications( playerid );
				return 1;
			}
				
			switch( listitem )
			{
				case 0:
				{
					format:g_small_string( "\
						"cBLUE"Aprobar la solicitud\n\n\
						"cWHITE"Realmente deseas aprobar la solicitud de "cBLUE"%s"cWHITE" para una licencia de armas nivel "cBLUE"%d",
						PApply[index][a_name], PApply[index][a_level] );
						
					showPlayerDialog( playerid, d_police + 7, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
				}
				
				case 1:
				{
					format:g_small_string( "\
						"cBLUE"Rechazar solicitud\n\n\
						"cWHITE"Realmente deseas aprobar la solicitud de "cBLUE"%s"cWHITE" para una licencia de armas nivel "cBLUE"%d",
						PApply[index][a_name], PApply[index][a_level] );
						
					showPlayerDialog( playerid, d_police + 8, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
				}
			}
		}
		
		case d_police + 7:
		{
			if( !response )
			{
				DeletePVar( playerid, "Fraction:SelectApply" );
			
				ShowApplications( playerid );
				return 1;
			}
			
			new
				index = GetPVarInt( playerid, "Fraction:SelectApply" ),
				bool:flag = false;
				
			if( !PApply[index][a_id] )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Alguien ya revis� esta solicitud antes." );
			
				DeletePVar( playerid, "Fraction:SelectApply" );
			
				ShowApplications( playerid );
				return 1;
			}
			
			mysql_format:g_small_string( "DELETE FROM `"DB_APPLICATION"` WHERE `a_id` = %d LIMIT 1", PApply[index][a_id] );
			mysql_tquery( mysql, g_small_string );
			
			foreach(new i : Player)
			{
				if( !IsLogged( i ) ) continue;
				
				if( Player[i][uID] == PApply[index][a_user_id] )
				{
					for( new j; j < MAX_LICENSES; j++ )
					{
						if( !License[i][j][lic_id] )
						{
							License[i][j][lic_type] = 3 + PApply[index][a_level];
							License[i][j][lic_gave_date] = gettime();
							
							GivePlayerLicense( i, LIC_INSERT, j );
							
							break;
						}
					}
					
					pformat:( ""gbDefault"Fue aprobada tu licencia de armas para nivel %d", PApply[index][a_level] );
					psend:( i, C_WHITE );
					
					flag = true;
					break;
				}
			}
			
			if( !flag )
			{
				mysql_format:g_string(
				"\
					INSERT INTO `"DB_LICENSES"`\
					(\
						`license_user_id`, \
						`license_name`, \
						`license_type`, \
						`license_gave_date` \
					) VALUES (\
						%d,\
						'%s',\
						%d,\
						%d \
					)",
					PApply[index][a_user_id],
					PApply[index][a_name],
					3 + PApply[index][a_level],
					gettime()
				);
				
				mysql_tquery( mysql, g_string );
			}
			
			pformat:( ""gbSuccess"Aprobada la solicitud de %s para portar armas nivel %d", PApply[index][a_name], PApply[index][a_level] );
			psend:( playerid, C_WHITE );
			
			PApply[index][a_id] = 
			PApply[index][a_user_id] =
			PApply[index][a_level] = 
			PApply[index][a_date] = 0;
 			
			clean:<PApply[index][a_name]>;
			
			DeletePVar( playerid, "Fraction:SelectApply" );
		}
		
		case d_police + 8:
		{
			if( !response )
			{
				DeletePVar( playerid, "Fraction:SelectApply" );
			
				ShowApplications( playerid );
				return 1;
			}
			
			new
				index = GetPVarInt( playerid, "Fraction:SelectApply" ),
				bool:flag = false;
				
			if( !PApply[index][a_id] )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Alguien ya revis� esta solicitud antes." );
			
				DeletePVar( playerid, "Fraction:SelectApply" );
			
				ShowApplications( playerid );
				return 1;
			}
			
			mysql_format:g_small_string( "DELETE FROM `"DB_APPLICATION"` WHERE `a_id` = %d LIMIT 1", PApply[index][a_id] );
			mysql_tquery( mysql, g_small_string );
			
			foreach(new i : Player)
			{
				if( !IsLogged( i ) ) continue;
				
				if( Player[i][uID] == PApply[index][a_user_id] )
				{
					switch( PApply[index][a_level] )
					{
						case 1:	SetPlayerBank( i, "+", PRICE_LIC_GUN_1 );
						case 2: SetPlayerBank( i, "+", PRICE_LIC_GUN_2 );
						case 3:	SetPlayerBank( i, "+", PRICE_LIC_GUN_3 );
					}
				
					SendClient:( i, C_WHITE, !""gbDefault"Tu solicitud de licencia de armas fue rechazada. Cantidad pagada devuelta a tu cuenta bancaria." );
				
					flag = true;
					break;
				}
			}
			
			if( !flag )
			{
				new
					price;
				
				switch( PApply[index][a_date] )
				{
					case 1:	price += PRICE_LIC_GUN_1;
					case 2: price += PRICE_LIC_GUN_2;
					case 3:	price += PRICE_LIC_GUN_3;
				}
			
				mysql_format:g_string( "UPDATE `"DB_USERS"` SET `uBank` = `uBank` + %d WHERE `uID` = %d",
					price,
					PApply[index][a_user_id]
				);
				mysql_tquery( mysql, g_string );
			}
			
			pformat:( ""gbSuccess"La solicitud de licencia de armas de %s fue rechazada para el nivel %d.", PApply[index][a_name], PApply[index][a_level] );
			psend:( playerid, C_WHITE );
			
			PApply[index][a_id] = 
			PApply[index][a_user_id] =
			PApply[index][a_level] = 
			PApply[index][a_date] = 0;
 			
			clean:<PApply[index][a_name]>;
			
			DeletePVar( playerid, "Fraction:SelectApply" );
		}
		
		case d_police + 9:
		{
			if( !response ) return 1;
			
			switch( listitem )
			{
				case 0:
				{
					new
						amount;
						
					for( new i; i < MAX_ARREST; i++ )
					{
						if( VArrest[i][arrest_id] )
							amount++;
					}
					
					format:g_string( "\
						"cBLUE"Centro de embargos\n\n\
						"cWHITE"Capacidad: %d/%d"cBLUE"\n\n\
						"cWHITE"Pago de gr�a: "cBLUE"$%d\n\
						"cWHITE"Tarifa por hora: "cBLUE"$%d\n\n\
						"cGRAY"El veh�culo puede estar aparcado por 72 horas,\n\
						si el veh�culo no se retira en las 72 horas ser� subastado.",
						amount,
						MAX_ARREST,
						PRICE_FOR_WRECER,
						PRICE_FOR_ARREST );
						
					showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_string, "Cerrar", "" );
				}
				
				case 1:
				{
					showPlayerDialog( playerid, d_police + 10, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Centro de embargos\n\n\
						"cWHITE"Introduzca la ID del coche (/dl):",
						"Aceptar", "Atr�s" );
						
					g_player_interaction{playerid} = 1;
				}
			}
		}
		
		case d_police + 10:
		{
			if( !response )
			{
				g_player_interaction{playerid} = 0;
				
				return showPlayerDialog( playerid, d_police + 9, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Informaci�n\n\
					Recoge tu veh�culo", "Aceptar", "Cerrar" ); 
			}
			
			if( !IsNumeric( inputtext ) || inputtext[0] == EOS || strval( inputtext ) < 1 || strval( inputtext ) > MAX_VEHICLES )
			{
				return showPlayerDialog( playerid, d_police + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Centro de embargos\n\n\
					"cWHITE"Introduzca la ID del coche (/dl):\n\
					"gbDialogError"El formato ingresado no es valido.",
					"Aceptar", "Atr�s" );
			}
			
			new
				index = INVALID_PARAM,
				year, month, day, hour, minute, 
				price, arresttime;
			
			for( new i; i < MAX_ARREST; i++ )
			{
				if( VArrest[i][arrest_vehid] == Vehicle[strval( inputtext )][vehicle_id] && Vehicle[strval( inputtext )][vehicle_user_id] != INVALID_PARAM )
				{
					index = i;
					break;
				}
			}
			
			if( index == INVALID_PARAM )
			{
				return showPlayerDialog( playerid, d_police + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Centro de embargos\n\n\
					"cWHITE"Introduzca la ID del coche (/dl):\n\
					"gbDialogError"Ese veh�culo no est� embargado.",
					"Aceptar", "Atr�s" );
			}
			
			if( Player[playerid][uID] != Vehicle[strval( inputtext )][vehicle_user_id] )
			{
				return showPlayerDialog( playerid, d_police + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Centro de embargos\n\n\
					"cWHITE"Introduzca la ID del coche (/dl):\n\
					"gbDialogError"Ese veh�culo no te pertenece.",
					"Aceptar", "Atr�s" );
			}
			
			
			arresttime = VArrest[index][arrest_date] - 7 * 86400;
			gmtime( arresttime, year, month, day, hour, minute );
			price = floatround( ( gettime() - arresttime ) / 3600 ) * PRICE_FOR_ARREST;
			
			format:g_string( "\
				"cBLUE"Centro de embargos\n\n\
				"cWHITE"Coche: "cBLUE"%s %s\n\
				"cWHITE"Desde: %02d.%02d.%d %02d:%02d - "cBLUE"%s\n\n\
				"cWHITE"Pago de gr�a: "cBLUE"$%d\n\
				"cWHITE"Pago de multa: "cBLUE"$%d\n\n\
				"cWHITE"�Quieres pagar la multa y retirar tu coche?",
				VehicleInfo[GetVehicleModel( strval( inputtext ) ) - 400][v_type],
				GetVehicleModelName( Vehicle[strval( inputtext )][vehicle_model] ),
				day, month, year, hour, minute, VArrest[index][arrest_name],
				PRICE_FOR_WRECER, 
				price );
				
			showPlayerDialog( playerid, d_police + 11, DIALOG_STYLE_MSGBOX, " ", g_string, "S�", "No" );
			
			SetPVarInt( playerid, "Arrest:Index", index );
			SetPVarInt( playerid, "Arrest:Price", price + PRICE_FOR_WRECER );
			SetPVarInt( playerid, "Arrest:Vehid", strval( inputtext ) );
		}
		
		case d_police + 11:
		{
			if( !response )
			{
				DeletePVar( playerid, "Arrest:Index" );
				DeletePVar( playerid, "Arrest:Price" );
				DeletePVar( playerid, "Arrest:Vehid" );
			
				return showPlayerDialog( playerid, d_police + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Centro de embargos\n\n\
					"cWHITE"Introduzca la ID del coche (/dl):",
					"Aceptar", "Atr�s" );
			}
			
			new
				index = GetPVarInt( playerid, "Arrest:Index" ),
				price = GetPVarInt( playerid, "Arrest:Price" ),
				vehicleid = GetPVarInt( playerid, "Arrest:Vehid" ),
				pos = random( 3 );
				
			if( Player[playerid][uMoney] < price )
			{
				SendClient:( playerid, C_WHITE, !NO_MONEY );
				
				DeletePVar( playerid, "Arrest:Index" );
				DeletePVar( playerid, "Arrest:Price" );
				DeletePVar( playerid, "Arrest:Vehid" );
				g_player_interaction{playerid} = 0;
				
				return showPlayerDialog( playerid, d_police + 9, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Informaci�n\n\
					Recoge tu veh�culo", "Aceptar", "Cerrar" ); 
			}
			
			SetPlayerCash( playerid, "-", price );
			
			mysql_format:g_small_string( "DELETE FROM `"DB_VEHICLES_ARREST"` WHERE `arrest_id` = %d LIMIT 1", VArrest[index][arrest_id] );
			mysql_tquery( mysql, g_small_string );
			
			VArrest[index][arrest_id] =
			VArrest[index][arrest_vehid] =
			VArrest[index][arrest_pos] =
			VArrest[index][arrest_date] = 0;
			
			clean:<VArrest[index][arrest_name]>;
			
			Vehicle[vehicleid][vehicle_pos][0] = vehicle_unarrest_pos[pos][0];
			Vehicle[vehicleid][vehicle_pos][1] = vehicle_unarrest_pos[pos][1];
			Vehicle[vehicleid][vehicle_pos][2] = vehicle_unarrest_pos[pos][2];
			Vehicle[vehicleid][vehicle_pos][3] = vehicle_unarrest_pos[pos][3];
			
			Vehicle[vehicleid][vehicle_arrest] = 
			Vehicle[vehicleid][vehicle_int] =
			Vehicle[vehicleid][vehicle_world] = 0;
			
			/*SetVehicleVirtualWorld( vehicleid, 0 );
			
			SetVehicleZAngle( vehicleid, Vehicle[vehicleid][vehicle_pos][3] );
			setVehiclePos( vehicleid, 
				Vehicle[vehicleid][vehicle_pos][0],
				Vehicle[vehicleid][vehicle_pos][1],
				Vehicle[vehicleid][vehicle_pos][2]
			);*/
			
			mysql_format:g_string( "UPDATE `"DB_VEHICLES"` SET \
				`vehicle_pos` = '%f|%f|%f|%f', \
				`vehicle_world` = 0, \
				`vehicle_arrest` = 0 WHERE `vehicle_id` = %d LIMIT 1", 
				Vehicle[vehicleid][vehicle_pos][0],
				Vehicle[vehicleid][vehicle_pos][1],
				Vehicle[vehicleid][vehicle_pos][2],
				Vehicle[vehicleid][vehicle_pos][3],
				Vehicle[vehicleid][vehicle_id]
			);
			mysql_tquery( mysql, g_string );
			
			SetVehicleToRespawnEx( vehicleid, playerid );
			
			pformat:( ""gbSuccess"Has pagado "cBLUE"$%d"cWHITE". Tu "cBLUE"%s"cWHITE" est� listo para ser retirado en la puerta del centro de embargos", price, GetVehicleModelName( Vehicle[vehicleid][vehicle_model] ) );
			psend:( playerid, C_WHITE );
			
			DeletePVar( playerid, "Arrest:Index" );
			DeletePVar( playerid, "Arrest:Price" );
			DeletePVar( playerid, "Arrest:Vehid" );
			g_player_interaction{playerid} = 0;
		}
		// Ordenador de a bordo
		case d_police + 12:
		{
			if( !response ) return 1;
			
			switch( listitem )
			{
				//Informaci�n o hombre
				case 0:
				{
					showPlayerDialog( playerid, d_police + 13, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Informaci�n sospechosos\n\n\
						"cWHITE"Ingrese el nombre y apellido de la persona:\n\
						"gbDialog"Ejemplo: Logan_McMillan", "Aceptar", "Atr�s" );
				}
				//Informaci�n o transporte
				case 1:
				{
					showPlayerDialog( playerid, d_police + 15, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Informaci�n coches\n\n\
						"cWHITE"Especifique la matricula del veh�culo:", "Aceptar", "Atr�s" );
				}
				//Informaci�n o licencias
				case 2:
				{
					showPlayerDialog( playerid, d_police + 17, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Informaci�n licencias\n\n\
						"cWHITE"Ingrese el n�mero de licencia:", "Aceptar", "Atr�s" );
				}
				//Informaci�n o casa
				case 3:
				{
					showPlayerDialog( playerid, d_police + 25, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Informaci�n casas\n\n\
						"cWHITE"Ingrese el n�mero de la casa:", "Aceptar", "Atr�s" );
				}
				//Lista de autos buscados
				case 4:
				{
					new
						amount,
						year, month, day;
						
					clean:<g_big_string>;
					strcat( g_big_string, ""cWHITE"Lista de coches buscados\n" );
				
					for( new i; i < MAX_SUSPECT; i++ )
					{
						if( SuspectVehicle[i][s_id] )
						{
							gmtime( SuspectVehicle[i][s_date], year, month, day );
							format:g_string( "\n"cBLUE"%d. "cWHITE"Matricula %s\t%02d.%02d.%d\tOficial "cBLUE"%s"cWHITE": %s",
								amount + 1, SuspectVehicle[i][s_name], day, month, year, SuspectVehicle[i][s_officer_name], SuspectVehicle[i][s_descript] );
								
							strcat( g_big_string, g_string );
							amount++;
						}
					}
					
					if( !amount ) strcat( g_big_string, "\nNo hay coches en busqueda" );
					
					showPlayerDialog( playerid, d_police + 14, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atr�s", "" );
				}
				// b�squeda federal
				case 5:
				{
					new
						amount,
						year, month, day;
						
					clean:<g_big_string>;
					strcat( g_big_string, ""cWHITE"Lista de busquedas federales\n" );
				
					for( new i; i < MAX_SUSPECT; i++ )
					{
						if( SuspectPlayer[i][s_id] )
						{
							gmtime( SuspectPlayer[i][s_date], year, month, day );
							format:g_string( "\n"cBLUE"%d. "cWHITE"Sospechoso "cBLUE"%s"cWHITE"\t%02d.%02d.%d\tOficial "cBLUE"%s"cWHITE": "cGRAY"%s",
								amount + 1, SuspectPlayer[i][s_name], day, month, year, SuspectPlayer[i][s_officer_name], SuspectPlayer[i][s_descript] );
								
							strcat( g_big_string, g_string );
							amount++;
						}
					}
					
					if( !amount ) strcat( g_big_string, "\nNo hay sospechoso en busqueda." );
					
					showPlayerDialog( playerid, d_police + 14, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atr�s", "" );
				}
				// Ubicaci�n por n�mero de tel�fono
				case 6:
				{
					showPlayerDialog( playerid, d_police + 20, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Ubicaci�n por n�mero de tel�fono\n\n\
						"cWHITE"Ingrese el n�mero de tel�fono:", "Aceptar", "Atr�s" );
				}
			}
		}
		
		case d_police + 13:
		{
			if( !response ) return showPlayerDialog( playerid, d_police + 12, DIALOG_STYLE_LIST, "Ordenador de a bordo", dialog_computer, "Seleccionar", "Cerrar" );
		
			if( strlen( inputtext ) > MAX_PLAYER_NAME || inputtext[0] == EOS )
			{
				return showPlayerDialog( playerid, d_police + 13, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Informaci�n sospechosos\n\n\
					"cWHITE"Ingrese el nombre y apellido de la persona:\n\
					"gbDialog"Ejemplo: Logan_McMillan\n\n\
					"gbDialogError"Falla en la red. Vuelve a intentarlo.", "Aceptar", "Atr�s" );
			}
			
			new
				index		= INVALID_PARAM;
			
			foreach(new i: Player)
			{
				if( !IsLogged( i ) ) continue;
			
				if( !strcmp( Player[i][uName], inputtext, true ) )
				{
					index = i;
					break;
				}
			}
			
			if( index == INVALID_PARAM )
			{
				return showPlayerDialog( playerid, d_police + 13, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Informaci�n sospechosos\n\n\
					"cWHITE"Ingrese el nombre y apellido de la persona:\n\
					"gbDialog"Ejemplo: Logan_McMillan\n\n\
					"gbDialogError"No se encontr� ninguna persona con este nombre.", "Aceptar", "Atr�s" );
			}
			
			ShowPlayerInformation( playerid, index, d_police + 14 );
		}
		
		case d_police + 14:
		{
			showPlayerDialog( playerid, d_police + 12, DIALOG_STYLE_LIST, "Ordenador de a bordo", dialog_computer, "Seleccionar", "Cerrar" );
		}
		
		case d_police + 15:
		{
			if( !response ) return showPlayerDialog( playerid, d_police + 12, DIALOG_STYLE_LIST, "Ordenador de a bordo", dialog_computer, "Seleccionar", "Cerrar" );
		
			if( strlen( inputtext ) > 7 || inputtext[0] == EOS )
			{
				return showPlayerDialog( playerid, d_police + 15, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Informaci�n coches\n\n\
					"cWHITE"Especifique la matricula del veh�culo:\n\
					"gbDialogError"No se encontr� ninguna persona con este nombre.", "Aceptar", "Atr�s" );
			}
		
			new
				index		= INVALID_PARAM;
			
			for( new i; i < MAX_VEHICLES; i++ )
			{
				if( !Vehicle[i][vehicle_id] ) continue;
			
				if( !strcmp( Vehicle[i][vehicle_number], inputtext, true ) )
				{
					index = i;
					break;
				}
			}
			
			if( index == INVALID_PARAM )
			{
				return showPlayerDialog( playerid, d_police + 15, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Informaci�n coches\n\n\
					"cWHITE"Especifique la matricula del veh�culo:\n\
					"gbDialogError"No se encontraron veh�culos con esta matricula.", "Aceptar", "Atr�s" );
			}
			
			ShowVehiclePInformation( playerid, index, d_police + 14 );
		}
		
		case d_police + 17:
		{
			if( !response ) return showPlayerDialog( playerid, d_police + 12, DIALOG_STYLE_LIST, "Ordenador de a bordo", dialog_computer, "Seleccionar", "Cerrar" );
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 )
			{
				return showPlayerDialog( playerid, d_police + 17, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Informaci�n licencias\n\n\
					"cWHITE"Ingrese el n�mero de licencia:\n\
					"gbDialogError"No se encontr� ninguna persona con este nombre.", "Aceptar", "Atr�s" );
			}
			
			DLicense[ l_id ] = 
			DLicense[ l_type ] =
			DLicense[ l_gave_date ] =
			DLicense[ l_take_date ] = 0;
			
			DLicense[ l_name ][0] = 
			DLicense[ l_taked_by ][0] = 
			DLicense[ l_gun_name ][0] = EOS;
			
			mysql_format:g_small_string( "SELECT * FROM "DB_LICENSES" WHERE `license_id` = %d", strval( inputtext ) );
			mysql_tquery( mysql, g_small_string, "DownloadLicense" );
			
			format:g_small_string( "\
				"cWHITE"�Est�s seguro que quieres buscar la licencia "cBLUE"#%d"cWHITE"?", strval( inputtext ) );
			showPlayerDialog( playerid, d_police + 18, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
		}
		
		case d_police + 18:
		{
			if( !response ) 
			{
				DLicense[ l_id ] = 
				DLicense[ l_type ] =
				DLicense[ l_gave_date ] =
				DLicense[ l_take_date ] = 0;
				
				DLicense[ l_name ][0] = 
				DLicense[ l_taked_by ][0] = 
				DLicense[ l_gun_name ][0] = EOS;
			
				return showPlayerDialog( playerid, d_police + 12, DIALOG_STYLE_LIST, "Ordenador de a bordo", dialog_computer, "Seleccionar", "Cerrar" );
			}
		
			if( !DLicense[l_id] )
			{
				return showPlayerDialog( playerid, d_police + 17, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Informaci�n licencias\n\n\
					"cWHITE"Ingrese el n�mero de licencia:\n\
					"gbDialogError"La licencia no se encontr� en la base de datos.", "Aceptar", "Atr�s" );
			}
			
			new
				year, month, day;
			
			clean:<g_big_string>;
			
			format:g_small_string( ""cWHITE"Informaci�n licencias "cBLUE"#%d\n\n", DLicense[l_id] );
			strcat( g_big_string, g_small_string );
			
			strcat( g_big_string, ""cWHITE"Tipo: " );
			
			switch( DLicense[l_type] )
			{
				case 1: format:g_small_string( "Licencia de conducir" );
				case 2: format:g_small_string( "Licencia de piloto" );
				case 3: format:g_small_string( "Capit�n de barco" );
				case 4: format:g_small_string( "Licencia de armas clase A" );
				case 5: format:g_small_string( "Licencia de armas clase B" );
				case 6: format:g_small_string( "Licencia de armas clase C" );
			}
			
			strcat( g_big_string, g_small_string );
			
			if( DLicense[l_gun_name][0] != EOS )
			{
				format:g_small_string( ""cWHITE"\nModelo de arma: "cBLUE"%s", DLicense[l_gun_name] );
				strcat( g_big_string, g_small_string );
			}
			
			format:g_small_string( ""cWHITE"\nTitular: "cBLUE"%s\n", DLicense[l_name] );
			strcat( g_big_string, g_small_string );
			
			gmtime( DLicense[l_gave_date], year, month, day );
			
			format:g_small_string( ""cWHITE"Adquirida en: %02d.%02d.%d", day, month, year );
			strcat( g_big_string, g_small_string );
			
			if( DLicense[l_take_date] )
			{
				format:g_small_string( "\n\n"cWHITE"Retirada: %02d.%02d.%d", day, month, year );
				strcat( g_big_string, g_small_string );
				
				format:g_small_string( "\n"cWHITE"Por: %s", DLicense[l_taked_by] );
				strcat( g_big_string, g_small_string );
			}
			
			showPlayerDialog( playerid, d_police + 19, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atr�s", "" );
		}
		
		case d_police + 19:
		{
			DLicense[ l_id ] = 
			DLicense[ l_type ] =
			DLicense[ l_gave_date ] =
			DLicense[ l_take_date ] = 0;
				
			DLicense[ l_name ][0] = 
			DLicense[ l_taked_by ][0] = 
			DLicense[ l_gun_name ][0] = EOS;
		
			showPlayerDialog( playerid, d_police + 12, DIALOG_STYLE_LIST, "Ordenador de a bordo", dialog_computer, "Seleccionar", "Cerrar" );
		}
		
		case d_police + 20:
		{
			if( !response ) return showPlayerDialog( playerid, d_police + 12, DIALOG_STYLE_LIST, "Ordenador de a bordo", dialog_computer, "Seleccionar", "Cerrar" );
		
			if( inputtext[0] == EOS || strval( inputtext ) < 1 || !IsNumeric( inputtext ) || strlen( inputtext ) > 6 || strval( inputtext ) == GetPhoneNumber( playerid ) )
			{
				return showPlayerDialog( playerid, d_police + 20, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Ubicaci�n por n�mero de tel�fono\n\n\
					"cWHITE"Ingrese el n�mero de tel�fono:\n\
					"gbDialogError"No se encontr� ninguna persona con este nombre.", "Aceptar", "Atr�s" );
			}
		
			new
				number = strval( inputtext ),
				index = INVALID_PARAM;
		
			foreach(new i : Player)
			{
				if( !IsLogged( i ) ) continue;
				
				if( GetPhoneNumber( i ) != number ) continue;
				
				if( !IsPlayerInNetwork( i ) )
				{
					return showPlayerDialog( playerid, d_police + 20, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Ubicaci�n por n�mero de tel�fono\n\n\
						"cWHITE"Ingrese el n�mero de tel�fono:\n\
						"gbDialogError"El telefono est� fuera de la cobertura de la red.", "Aceptar", "Atr�s" );
				}
				else
				{
					index = i;
					break;
				}
			}
			
			if( index == INVALID_PARAM )
			{
				return showPlayerDialog( playerid, d_police + 20, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Ubicaci�n por n�mero de tel�fono\n\n\
					"cWHITE"Ingrese el n�mero de tel�fono:\n\
					"gbDialogError"El numero de serie no est� registrado en la empresa de telefonos", "Aceptar", "Atr�s" );
			}
			
			g_player_gps{playerid} = 1;
			SetPVarInt( playerid, "Police:GPSuse", 1 );
			SetPVarInt( playerid, "Police:GPSplayer", index );
			
			SetPoliceCheckpoint( playerid, index );
			
			pformat:( ""gbSuccess"Numero de telefono "cBLUE"%d"cWHITE" encontrado, ubicaci�n marcada en GPS. Utiliza "cBLUE"/gps"cWHITE", para detener el seguimiento.", number );
			psend:( playerid, C_WHITE );
		}
		
		case d_police + 21:
		{
			if( !response )
			{
				DeletePVar( playerid, "Police:Takeid" );
				return 1;
			}
			
			new
				takeid = GetPVarInt( playerid, "Police:Takeid" ),
				licid = g_dialog_select[playerid][listitem];
			
			if( License[takeid][licid][lic_take_date] )
			{
				DeletePVar( playerid, "Police:Takeid" );
				return SendClient:( playerid, C_WHITE, !""gbError"La licencia ya ha sido retirada." );
			}
			
			SetPVarInt( playerid, "Police:Licid", licid );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
			
			format:g_string( "\
				"cBLUE"Retiro de licencia\n\n\
				"cWHITE"�Realmente quieres retirar la licencia clase "cBLUE"%s"cWHITE" a "cBLUE"%s"cWHITE"?",
				getLicenseName[ License[takeid][licid][lic_type] - 1 ],
				Player[ takeid ][uRPName] );
				
			showPlayerDialog( playerid, d_police + 22, DIALOG_STYLE_MSGBOX, " ", g_string, "S�", "No" );
		}
		
		case d_police + 22:
		{
			if( !response )
			{
				DeletePVar( playerid, "Police:Takeid" );
				return 1;
			}
			
			new
				takeid = GetPVarInt( playerid, "Police:Takeid" ),
				licid = GetPVarInt( playerid, "Police:Licid" );
				
			if( GetDistanceBetweenPlayers( playerid, takeid ) > 3.0 ) 
			{
				DeletePVar( playerid, "Police:Takeid" );
				DeletePVar( playerid, "Police:Licid" );
			
				return SendClient:( playerid, C_WHITE, !""gbError"No hay ningun jugador cerca tuyo." );
			}
			
			License[takeid][licid][lic_take_date] = gettime();
			License[takeid][licid][lic_taked_by][0] = EOS;
			strcat( License[takeid][licid][lic_taked_by], Player[playerid][uName], MAX_PLAYER_NAME );
			
			GivePlayerLicense( takeid, LIC_UPDATE, licid );
			
			pformat:( ""gbDefault"%s ha eliminado tu licencia clase "cBLUE"%s"cWHITE".", Player[playerid][uRPName], getLicenseName[ License[takeid][licid][lic_type] - 1 ] );
			psend:( takeid, C_WHITE );
			
			pformat:( ""gbDefault"Has retirado la licencia clase "cBLUE"%s"cWHITE" a %s.", getLicenseName[ License[takeid][licid][lic_type] - 1 ], Player[takeid][uRPName] );
			psend:( playerid, C_WHITE );
			
			DeletePVar( playerid, "Police:Takeid" );
			DeletePVar( playerid, "Police:Licid" );
		}
		
		case d_police + 23:
		{
			showPlayerDialog( playerid, d_police, DIALOG_STYLE_LIST, "Mostrador de informaci�n", info_dialog, "Seleccionar", "Cerrar" );
		}
		
		case d_police + 24:
		{
			showPlayerDialog( playerid, d_police + 3, DIALOG_STYLE_LIST, " ", ""cWHITE"\
				Obtenci�n de licencias\n\
				Informaci�n del recibo", "Seleccionar", "Atr�s" );
		}
		
		case d_police + 25:
		{
			if( !response )
				return showPlayerDialog( playerid, d_police + 12, DIALOG_STYLE_LIST, "Ordenador de a bordo", dialog_computer, "Seleccionar", "Cerrar" );
				
			if( inputtext[0] == EOS || strval( inputtext ) < 1 )
			{
				return showPlayerDialog( playerid, d_police + 25, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Informaci�n casas\n\n\
					"cWHITE"Ingrese el n�mero de la casa:\n\
					"gbDialogError"Numero ingresado es erroneo.", "Aceptar", "Atr�s" );
			}

			clean:<g_string>;
			
			strcat( g_string, ""cBLUE"N�mero\t"cBLUE"Due�o\t"cBLUE"Distrito" );
			new
				zone[28];
			
			for( new h; h < MAX_HOUSE; h++ )
			{
				if( !HouseInfo[h][hID] ) continue;
			
				if( strval( inputtext ) == HouseInfo[h][hID] )
				{
					GetPos2DZone( HouseInfo[h][hEnterPos][0], HouseInfo[h][hEnterPos][1], zone, 28 );
					SetPVarInt( playerid, "MDC:House", h );
					
					if( HouseInfo[h][huID] == INVALID_PARAM )
					{
						format:g_small_string( "\n"cWHITE"#%d\tNo conocido\t%s", HouseInfo[h][hID], zone );
						strcat( g_string, g_small_string );
					}
					else
					{
						format:g_small_string( "\n"cWHITE"#%d\t%s\t%s", HouseInfo[h][hID], HouseInfo[h][hOwner], zone );
						strcat( g_string, g_small_string );
					}
					
					return showPlayerDialog( playerid, d_police + 26, DIALOG_STYLE_TABLIST_HEADERS, "Informaci�n casas", g_string, "Encontrar", "Atr�s" );
				}
			}
			
			showPlayerDialog( playerid, d_police + 25, DIALOG_STYLE_INPUT, " ", "\
				"cBLUE"Informaci�n casas\n\n\
				"cWHITE"Ingrese el n�mero de la casa:\n\
				"gbDialogError"No fue encontrada ninguna casa con este numero.", "Aceptar", "Atr�s" );
		}
		
		case d_police + 26:
		{
			if( !response )
			{
				DeletePVar( playerid, "MDC:House" );
				return showPlayerDialog( playerid, d_police + 25, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Informaci�n casas\n\n\
					"cWHITE"Ingrese el n�mero de la casa:", "Aceptar", "Atr�s" );
			}
			
			new
				h = GetPVarInt( playerid, "MDC:House" );
			
			g_player_gps{playerid} = 1;
			SetPlayerCheckpoint( playerid, HouseInfo[h][hEnterPos][0], HouseInfo[h][hEnterPos][1], HouseInfo[h][hEnterPos][2], 3.0 );
			
			DeletePVar( playerid, "MDC:House" );
			SendClient:( playerid, C_WHITE, ""gbDefault"Has puesto la etiqueta en el navegador GPS. Para cancelar use "cBLUE"/gps"cWHITE"");
		}
		
		case d_police + 27:
		{
			if( !response )
			{
				DeletePVar( playerid, "Suspect:Index" );
				return 1;
			}
		
			new
				index = GetPVarInt( playerid, "Suspect:Index" );
				
			if( inputtext[0] == EOS || strlen( inputtext ) > 32 )
			{
				format:g_small_string( "\
					"cWHITE"Emitir en la lista de busquedas federales "cBLUE"%s\n\n\
					"cWHITE"Indicar motivo:\n\
					"gbDialogError"Motivo equivocado.", SuspectPlayer[index][s_time_name] );
					
				return showPlayerDialog( playerid, d_police + 27, DIALOG_STYLE_INPUT, " ", g_small_string, "Anunciar", "Cerrar" );
			}
		
			SuspectPlayer[index][s_date] = gettime();
				
			clean:<SuspectPlayer[index][s_name]>;
			clean:<SuspectPlayer[index][s_officer_name]>;
			clean:<SuspectPlayer[index][s_descript]>;
				
			strcat( SuspectPlayer[index][s_name], SuspectPlayer[index][s_time_name], 24 );
			strcat( SuspectPlayer[index][s_officer_name], Player[ playerid ][uName], MAX_PLAYER_NAME );
			strcat( SuspectPlayer[index][s_descript], inputtext, 32 );
						
			mysql_format:g_string( "INSERT INTO `"DB_SUSPECT"` \
				( `s_name`, `s_officer_name`, `s_date`, `s_descript` ) VALUES \
				( '%s', '%s', '%d', '%e' )",
				SuspectPlayer[index][s_name],
				SuspectPlayer[index][s_officer_name],
				SuspectPlayer[index][s_date],
				SuspectPlayer[index][s_descript] );
			mysql_tquery( mysql, g_string, "InsertSuspectPlayer", "d", index );
			
			pformat:( ""gbSuccess"Bolet�n emitido en la lista de busquedas federales nombre "cBLUE"%s"cWHITE" motivo "cBLUE"%s"cWHITE".", 
				SuspectPlayer[index][s_name],
				SuspectPlayer[index][s_descript] );
			psend:( playerid, C_WHITE );
			
			DeletePVar( playerid, "Suspect:Index" );
		}
		
		case d_police + 28:
		{
			if( !response )
			{
				clean:<SuspectVehicle[GetPVarInt( playerid, "CarSuspect:Index" )][s_name]>;
				DeletePVar( playerid, "CarSuspect:Index" );
				return 1;
			}
			
			new
				index = GetPVarInt( playerid, "CarSuspect:Index" );
				
			if( inputtext[0] == EOS || strlen( inputtext ) > 32 )
			{
				format:g_small_string( "\
					"cWHITE"Emitir en la lista de busquedas federales de veh�culos"cBLUE"%s\n\n\
					"cWHITE"Indicar motivo:\n\n\
					"gbDialogError"Formato incorrecto.", SuspectVehicle[index][s_name] );
					
				return showPlayerDialog( playerid, d_police + 28, DIALOG_STYLE_INPUT, " ", g_small_string, "Anunciar", "Cerrar" );
			}
			
			clean:<SuspectVehicle[index][s_officer_name]>;
			clean:<SuspectVehicle[index][s_descript]>;
			
			SuspectVehicle[index][s_date] = gettime();
			strcat( SuspectVehicle[index][s_officer_name], Player[ playerid ][uName], MAX_PLAYER_NAME );
			strcat( SuspectVehicle[index][s_descript], inputtext, 32 );
				
			mysql_format:g_string( "INSERT INTO `"DB_SUSPECT_VEHICLE"` \
				( `s_name`, `s_officer_name`, `s_date`, `s_descript` ) VALUES \
				( '%s', '%s', '%d', '%e' )",
				SuspectVehicle[index][s_name],
				SuspectVehicle[index][s_officer_name],
				SuspectVehicle[index][s_date],
				SuspectVehicle[index][s_descript] );
			mysql_tquery( mysql, g_string, "InsertSuspectVehicle", "d", index );
			
			pformat:( ""gbSuccess"Emitido un bolet�n para el veh�culo "cBLUE"%s"cWHITE" motivo "cBLUE"%s"cWHITE".", 
				SuspectVehicle[index][s_name], SuspectVehicle[index][s_descript] );
			psend:( playerid, C_WHITE );
		}
	}
	
	return 1;
}