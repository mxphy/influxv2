function San_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{
	switch( dialogid ) 
	{
		case d_cnn: 
		{
			if( !response ) return 1;
			
			if( inputtext[0] == EOS )
			{
				format:g_string( "\
					"cBLUE"Publicar anuncio\n\n\
					"cWHITE"El texto del anuncio debe ser informativo y no debe contener\n\
					Expresiones obscenas. L�mite de caracteres "cBLUE"100"cWHITE".\n\
					"gbDefault"Precio del anuncio $%d.\n\n\
					"gbDialogError"Debes escribir algo", NETWORK_ADPRICE );
				return showPlayerDialog( playerid, d_cnn, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Cerrar" );
			}
			
			if( strlen( inputtext ) > 100 )
			{
				format:g_string( "\
					"cBLUE"Publicar anuncio\n\n\
					"cWHITE"El texto del anuncio debe ser informativo y no debe contener\n\
					Expresiones obscenas. L�mite de caracteres "cBLUE"100"cWHITE".\n\
					"gbDefault"Precio del anuncio $%d.\n\n\
					"gbDialogError"L�mite de caracteres superado.", NETWORK_ADPRICE );
				return showPlayerDialog( playerid, d_cnn, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Cerrar" );
			}
			
			if( COUNT_ADVERTS >= MAX_ADVERT_INFO ) 
				return SendClient:( playerid, C_WHITE, !""gbError"La l�nea de anuncios est� sobrecargada. Intentalo m�s tarde." );
			
			for( new i; i < MAX_ADVERT_INFO; i++ )
			{
				if( AD[i][a_text][0] == EOS )
				{
					strcat( AD[i][a_text], inputtext, 100 );
					strcat( AD[i][a_name], Player[playerid][uName], MAX_PLAYER_NAME );
					
					AD[i][a_phone] = GetPhoneNumber( playerid );
					AD[i][a_used] = false;
					
					COUNT_ADVERTS++;
					break;
				}
			}
			
			SetPlayerCash( playerid, "-", NETWORK_ADPRICE );
			NETWORK_COFFER += floatround( NETWORK_ADPRICE * 0.85 );
			
			mysql_format:g_small_string( "UPDATE `"DB_SAN"` SET `san_coffers` = %d WHERE 1", NETWORK_COFFER );
			mysql_tquery( mysql, g_small_string );
			
			pformat:( ""gbSuccess"Su anuncio ha sido moderado por el personal de CNN. Te han cobrado "cBLUE"$%d"cWHITE".", NETWORK_ADPRICE );
			psend:( playerid, C_WHITE );
		}
		
		case d_cnn + 1:
		{
			if( !response ) 
			{
				g_player_interaction{playerid} = 0;
				return 1;
			}
			
			switch( listitem )
			{
				// Lista de anuncios
				case 0:
				{
					ShowAds( playerid );
				}
				// en vivo
				case 1:
				{
					if( ETHER_STATUS != INVALID_PARAM )
					{
						if( ETHER_STATUS == playerid )
						{
							SendClient:( playerid, C_WHITE, !""gbDefault"Dejaste la transmisi�n en vivo." );
							
							Player[playerid][tEther] = false;
							
							ETHER_STATUS = INVALID_PARAM;
							
							ETHER_CALL = 
							ETHER_SMS = false;
						}
						else
						{
							pformat:( ""gbError"El anfitri�n ya est� al  el aire %s.", Player[ ETHER_STATUS ][uName] );
							psend:( playerid, C_WHITE );
						}
						
						g_player_interaction{playerid} = 0;
						return 1;
					}
				
					if( IsPlayerInAnyVehicle( playerid ) )
					{
						if( GetVehicleModel( GetPlayerVehicleID( playerid ) ) != 488 && GetVehicleModel( GetPlayerVehicleID( playerid ) ) != 582 )
						{
							g_player_interaction{playerid} = 0;
							return SendClient:( playerid, C_WHITE, !""gbError"Para iniciar la emisi�n debes estar en un veh�culo de CNN." );
						}
						if( Vehicle[ GetPlayerVehicleID( playerid ) ][vehicle_member] != FRACTION_NEWS )
						{	
							g_player_interaction{playerid} = 0;
							return SendClient:( playerid, C_WHITE, !NO_VEHICLE_FRACTION );
						}
					}
					else if( !IsPlayerInDynamicArea( playerid, NETWORK_ZONE ) )
					{	
						g_player_interaction{playerid} = 0;
						return SendClient:( playerid, C_WHITE, !""gbError"Para iniciar la emisi�n debes estar en el estudio" );
					}
					
					ETHER_STATUS = playerid;
					SendClient:( playerid, C_WHITE, !""gbDefault"Est�s en vivo. Usa "cBLUE"/n"cWHITE" para enviar un mensaje." );
				
					Player[playerid][tEther] = true;
				}
				// Invitar a transmitir
				case 2:
				{
					if( ETHER_STATUS != playerid )
					{	
						g_player_interaction{playerid} = 0;
						return SendClient:( playerid, C_WHITE, !""gbError"No eres el lider de la emisi�n." );
					}
						
					showPlayerDialog( playerid, d_cnn + 7, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Invitar al jugador al aire\n\n\
						"cWHITE"Ingrese el ID del jugador que desea invitar al aire:",
						"Siguiente", "Atr�s" );
				}
				// Expulsar del aire.
				case 3:
				{
					if( ETHER_STATUS != playerid && !PlayerLeaderFraction( playerid, FRACTION_NEWS - 1 ) )
					{
						g_player_interaction{playerid} = 0;
						return SendClient:( playerid, C_WHITE, !""gbError"No eres el lider de la emisi�n." );
					}
					
					showPlayerDialog( playerid, d_cnn + 9, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Expulsar jugador de la transmisi�n\n\n\
						"cWHITE"Ingresa el ID del jugador que deseas expulsar de la transmisi�n:",
						"Siguiente", "Atr�s" );
				}
				// recibir llamadas
				case 4:
				{
					if( ETHER_STATUS != playerid && !PlayerLeaderFraction( playerid, FRACTION_NEWS - 1 ) )
					{	
						g_player_interaction{playerid} = 0;
						return SendClient:( playerid, C_WHITE, !""gbError"No eres el lider de la emisi�n." );
					}
					
					if( ETHER_CALL )
					{
						ETHER_CALL = false;
					}
					else
					{
						ETHER_CALL = true;
					}
					
					cmd_newspanel( playerid );
				}
				// Recibe SMS
				case 5:
				{
					if( ETHER_STATUS != playerid && !PlayerLeaderFraction( playerid, FRACTION_NEWS - 1 ) )
					{	
						g_player_interaction{playerid} = 0;
						return SendClient:( playerid, C_WHITE, !""gbError"No eres el lider de la emisi�n." );
					}
					
					if( ETHER_SMS )
					{
						ETHER_SMS = false;
					}
					else
					{
						ETHER_SMS = true;
					}
					
					cmd_newspanel( playerid );
				}
				// Costo del anuncio
				case 6:
				{
					if( !PlayerLeaderFraction( playerid, FRACTION_NEWS - 1 ) )
					{	
						g_player_interaction{playerid} = 0;
						return SendClient:( playerid, C_WHITE, !NO_LEADER );
					}
					
					format:g_small_string( "\
						"cBLUE"Cambiar el costo de los anuncios\n\
						"cGRAY"Valor actual: "cBLUE"$%d"cWHITE"\n\n\
						Introduzca el nuevo valor:\n\
						"gbDefault"Minimo 1. M�ximo 100.", NETWORK_ADPRICE );
						
					showPlayerDialog( playerid, d_cnn + 10, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
				// presupuesto de la organizaci�n
				case 7:
				{
					if( !PlayerLeaderFraction( playerid, FRACTION_NEWS - 1 ) )
					{	
						g_player_interaction{playerid} = 0;
						return SendClient:( playerid, C_WHITE, !NO_LEADER );
					}
					
					format:g_small_string( "\
						"cBLUE"Transferencia a una cuenta bancaria\n\
						"cGRAY"Cantidad disponible: "cBLUE"$%d"cWHITE"\n\n\
						Especifique el importe a transferir:", NETWORK_COFFER );
						
					showPlayerDialog( playerid, d_cnn + 11, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
			}
		}
		
		case d_cnn + 2:
		{
			if( !response ) return cmd_newspanel( playerid );
			
			if( AD[ g_dialog_select[playerid][listitem] ][a_used] ) 
				return SendClient:( playerid, C_WHITE, !""gbError"Este anuncio ya est� siendo visto por un miembro de CNN." );
			
			SetPVarInt( playerid, "San:AdvertId", g_dialog_select[playerid][listitem] );
			AD[ g_dialog_select[playerid][listitem] ][a_used] = true;
			
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
			
			showPlayerDialog( playerid, d_cnn + 3, DIALOG_STYLE_LIST, " ", dialog_advert, "Seleccionar", "Atr�s" );
		}
		
		case d_cnn + 3:
		{
			if( !response )
			{
				AD[ GetPVarInt( playerid, "San:AdvertId" ) ][a_used] = false;
				DeletePVar( playerid, "San:AdvertId" );
				
				ShowAds( playerid );
				return 1;
			}
			
			new
				aid = GetPVarInt( playerid, "San:AdvertId" );
			
			switch(listitem) 
			{
				// Leer y publicar un anuncio
				case 0: 
				{
					format:g_small_string( "\
						"cWHITE"Texto:\n\n"cBLUE"%s\n\n\
						"cGRAY"Enviado por: "cBLUE"%s"cGRAY"\n\
						"cGRAY"Telefono: "cBLUE"%d\n\n\
						"cWHITE"�Aprobar este anuncio?", AD[aid][a_text], AD[aid][a_name], AD[aid][a_phone] );
					
					showPlayerDialog( playerid, d_cnn + 4, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
				}
				// respeta el anuncio
				case 1: 
				{
					format:g_small_string( "\
						"cWHITE"Texto:\n\n"cBLUE"%s\n\n\
						"cGRAY"Enviado por: "cBLUE"%s"cGRAY"\n\
						"cGRAY"Telefono: "cBLUE"%d\n\n\
						"cWHITE"Ingresa el texto: ", AD[aid][a_text], AD[aid][a_name], AD[aid][a_phone] );
					
					showPlayerDialog( playerid, d_cnn + 5, DIALOG_STYLE_INPUT, " ", g_small_string, "S�", "Atr�s" );
				}
				// Mostrar el anuncio
				case 2: 
				{
					format:g_small_string( "\
						"cWHITE"�Est�s seguro que quieres aprobar el anuncio enviado por "cBLUE"%s"cWHITE"?", AD[aid][a_name] );
						
					showPlayerDialog( playerid, d_cnn + 7, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
				}
			}
		}
		
		case d_cnn + 4:
		{
			if( !response ) return showPlayerDialog( playerid, d_cnn + 3, DIALOG_STYLE_LIST, " ", dialog_advert, "Seleccionar", "Atr�s" );
		
			new
				aid = GetPVarInt( playerid, "San:AdvertId" ),
				fid = Player[playerid][uMember] - 1,
				rank = getRankId( playerid, fid );
				
			foreach(new i : Player) 
			{
				if( !IsLogged(i) || !Player[i][uSettings][4] ) continue;
			
				pformat:( "[Anuncio] %s. Telefono: %d", AD[aid][a_text], AD[aid][a_phone] );
				psend:( i, C_GREEN );

				pformat:( "Publicado por %s %s.", FRank[fid][rank][r_name], Player[playerid][uRPName] );
				psend:( i, C_GREEN );
			}
			
			clean:<AD[aid][a_text]>;
			clean:<AD[aid][a_name]>;
					
			AD[aid][a_used] = false;
			AD[aid][a_phone] = 0;
			
			COUNT_ADVERTS --;
			DeletePVar( playerid, "San:AdvertId" );
			
			g_player_interaction{playerid} = 0;
		}
		
		case d_cnn + 5:
		{
			if( !response ) return showPlayerDialog( playerid, d_cnn + 3, DIALOG_STYLE_LIST, " ", dialog_advert, "Seleccionar", "Atr�s" );
			
			new
				aid = GetPVarInt( playerid, "San:AdvertId" );
			
			if( inputtext[0] == EOS )
			{
				format:g_small_string( "\
					"cWHITE"Texto:\n\n"cBLUE"%s\n\n\
					"cGRAY"Enviado por: "cBLUE"%s"cGRAY"\n\
					"cGRAY"Telefono: "cBLUE"%d\n\n\
					"cWHITE"Ingrese el texto: \n\
					"gbDialogError"Debes escribir algo.", AD[aid][a_text], AD[aid][a_name], AD[aid][a_phone] );
					
				return showPlayerDialog( playerid, d_cnn + 5, DIALOG_STYLE_INPUT, " ", g_small_string, "S�", "Atr�s" );
			}
			
			if( strlen( inputtext ) > 100 )
			{
				format:g_small_string( "\
					"cWHITE"Texto:\n\n"cBLUE"%s\n\n\
					"cGRAY"Enviado por: "cBLUE"%s"cGRAY"\n\
					"cGRAY"Telefono: "cBLUE"%d\n\n\
					"cWHITE"Ingrese el texto: \n\
					"gbDialogError"L�mite de caracteres superado.", AD[aid][a_text], AD[aid][a_name], AD[aid][a_phone] );
					
				return showPlayerDialog( playerid, d_cnn + 5, DIALOG_STYLE_INPUT, " ", g_small_string, "S�", "Atr�s" );
			}
			
			clean:<AD[aid][a_text]>;
			strcat( AD[aid][a_text], inputtext, 100 );
			
			SendClient:( playerid, C_WHITE, !""gbSuccess"Texto del anuncio editado con �xito." );
			showPlayerDialog( playerid, d_cnn + 3, DIALOG_STYLE_LIST, " ", dialog_advert, "Seleccionar", "Atr�s" );
		}
		
		case d_cnn + 6:
		{
			if( !response ) return showPlayerDialog( playerid, d_cnn + 3, DIALOG_STYLE_LIST, " ", dialog_advert, "Seleccionar", "Atr�s" );
			
			new
				aid = GetPVarInt( playerid, "San:AdvertId" );
				
			pformat:( ""gbSuccess"Anuncio del jugador %s enviado con �xito.", AD[aid][a_name] );
			psend:( playerid, C_WHITE );
				
			clean:<AD[aid][a_text]>;
			clean:<AD[aid][a_name]>;
					
			AD[aid][a_used] = false;
			AD[aid][a_phone] = 0;
			
			COUNT_ADVERTS--;
			DeletePVar( playerid, "San:AdvertId" );
			
			ShowAds( playerid );
		}
		
		case d_cnn + 7:
		{
			if( !response ) return cmd_newspanel( playerid );
			
			if( !IsNumeric( inputtext ) || inputtext[0] == EOS || strval( inputtext ) < 0 || strval( inputtext ) > MAX_PLAYERS || !IsLogged( strval( inputtext ) ) )
			{
				return showPlayerDialog( playerid, d_cnn + 7, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Invitar a un jugador a la transmisi�n\n\n\
					"cWHITE"Ingrese el ID del jugador al que desea invitar:\n\n\
					"gbDialogError"El formato ingresado no es valido.",
					"Siguiente", "Atr�s" );
			}
			
			if( g_player_interaction{ strval( inputtext ) } )
			{
				return showPlayerDialog( playerid, d_cnn + 7, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Invitar a un jugador a la transmisi�n\n\n\
					"cWHITE"Ingrese el ID del jugador al que desea invitar:\n\n\
					"gbDialogError"No puedes interactuar con este jugador.",
					"Siguiente", "Atr�s" );
			}
			
			if( Player[ strval( inputtext ) ][tEther] )
			{
				return showPlayerDialog( playerid, d_cnn + 7, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Invitar a un jugador a la transmisi�n\n\n\
					"cWHITE"Ingrese el ID del jugador al que desea invitar:\n\n\
					"gbDialogError"El jugador ya est� en al aire.",
					"Siguiente", "Atr�s" );
			}
			
			if( GetDistanceBetweenPlayers( playerid, strval( inputtext ) ) > 3.0 || GetPlayerVirtualWorld( playerid ) != GetPlayerVirtualWorld( strval( inputtext ) ) )
			{
				return showPlayerDialog( playerid, d_cnn + 7, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Invitar a un jugador a la transmisi�n\n\n\
					"cWHITE"Ingrese el ID del jugador al que desea invitar:\n\n\
					"gbDialogError"El jugador no est� cerca de ti.",
					"Siguiente", "Atr�s" );
			}
			
			pformat:( ""gbSuccess"Has enviado una invitaci�n a %s para unirse a la transmisi�n. Esperando confirmaci�n.", Player[ strval( inputtext ) ][uName] );
			psend:( playerid, C_WHITE );
			
			format:g_small_string( "\
				"cBLUE"%s"cWHITE" Te invita a participar en la transmisi�n. �Aceptas?", Player[playerid][uRPName] );
			
			showPlayerDialog( strval( inputtext ), d_cnn + 8, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );	
			g_player_interaction{ strval( inputtext ) } = 1;		
				
			cmd_newspanel( playerid );
		}
		
		case d_cnn + 8:
		{
			if( !response )
			{
				if( IsLogged( ETHER_STATUS ) && ETHER_STATUS != INVALID_PARAM )
				{
					pformat:( ""gbError"%s rechaza la solicitud de la transmisi�n.", Player[playerid][uName] );
					psend:( ETHER_STATUS, C_WHITE );
				}
			
				SendClient:( playerid, C_WHITE, !""gbDefault"Rechazas la solicitud de la transmisi�n." );
			
				g_player_interaction{playerid} = 0;
				return 1;
			}
			
			if( IsLogged( ETHER_STATUS ) && ETHER_STATUS != INVALID_PARAM )
			{
				pformat:( "[Transmisi�n %s] %s se uni� a la transmisi�n", Fraction[ FRACTION_NEWS - 1 ][f_short_name], Player[playerid][uName] );
				psend:( ETHER_STATUS, C_LIGHTGREEN );
			}
			
			SendClient:( playerid, C_WHITE, !""gbDefault"Te has unido a la transmisi�n. Usa "cBLUE"/n"cWHITE" para enviar un mensaje." );
			
			Player[playerid][tEther] = true;
			g_player_interaction{playerid} = 0;
		}
		
		case d_cnn + 9:
		{
			if( !response ) return cmd_newspanel( playerid );
		
			if( !IsNumeric( inputtext ) || inputtext[0] == EOS || strval( inputtext ) < 0 || strval( inputtext ) > MAX_PLAYERS || !IsLogged( strval( inputtext ) ) || playerid == strval( inputtext ) )
			{
				return showPlayerDialog( playerid, d_cnn + 9, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Expulsar jugador de la transmisi�n\n\n\
					"cWHITE"Ingresa el ID del jugador que deseas expulsar de la transmisi�n:\n\n\
					"gbDialogError"El formato ingresado no es valido.",
					"Siguiente", "Atr�s" );
			}
			
			if( !Player[ strval( inputtext ) ][tEther] )
			{
				return showPlayerDialog( playerid, d_cnn + 9, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Expulsar jugador de la transmisi�n\n\n\
					"cWHITE"Ingresa el ID del jugador que deseas expulsar de la transmisi�n:\n\n\
					"gbDialogError"El jugador no est� en la transmisi�n.",
					"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) == ETHER_STATUS )
			{
				ETHER_STATUS = INVALID_PARAM;
				
				ETHER_CALL = 
				ETHER_SMS = false;
				
				pformat:( ""gbDialog"Has deshabilitado la transmisi�n para %s.", Player[ strval( inputtext ) ][uName] );
			}
			else
				pformat:( ""gbDialog"Has habilitado la transmisi�n para %s.", Player[ strval( inputtext ) ][uName] );
			
			psend:( playerid, C_WHITE );
			
			pformat:( ""gbDefault"Has sido desconectado de la transmisi�n por %s.", Player[playerid][uName] );
			psend:( strval( inputtext ), C_WHITE );
			
			Player[ strval( inputtext ) ][tEther] = false;
			cmd_newspanel( playerid );
		}
		
		case d_cnn + 10:
		{
			if( !response ) return cmd_newspanel( playerid );
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 || strval( inputtext ) > 100 )
			{
				format:g_small_string( "\
					"cBLUE"Cambiar el costo de los anuncios\n\
					"cGRAY"Valor actual: "cBLUE"$%d"cWHITE"\n\n\
					Introduzca el nuevo valor:\n\
					"gbDefault"Minimo 1. M�ximo 100.\n\n\
					"gbDialogError"El formato ingresado no es valido.", NETWORK_ADPRICE );
						
				return showPlayerDialog( playerid, d_cnn + 10, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}

			if( strval( inputtext ) == NETWORK_ADPRICE )
			{
				format:g_small_string( "\
					"cBLUE"Cambiar el costo de los anuncios\n\
					"cGRAY"Valor actual: "cBLUE"$%d"cWHITE"\n\n\
					Introduzca el nuevo valor:\n\
					"gbDefault"Minimo 1. M�ximo 100.\n\n\
					"gbDialogError"El costo debe ser diferente al actual.", NETWORK_ADPRICE );
						
				return showPlayerDialog( playerid, d_cnn + 10, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			NETWORK_ADPRICE = strval( inputtext );
			
			mysql_format:g_small_string( "UPDATE `"DB_SAN"` SET `san_adprice` = %d WHERE 1", NETWORK_ADPRICE );
			mysql_tquery( mysql, g_small_string );
			
			pformat:( ""gbSuccess"El costo del anuncio cambi� a "cBLUE"$%d"cWHITE".", NETWORK_ADPRICE );
			psend:( playerid, C_WHITE );
			
			cmd_newspanel( playerid );
		}
		
		case d_cnn + 11:
		{
			if( !response ) return cmd_newspanel( playerid );
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 0 || strval( inputtext ) > NETWORK_COFFER )
			{
				format:g_small_string( "\
					"cBLUE"Transferencia a una cuenta bancaria\n\
					"cGRAY"Cantidad disponible: "cBLUE"$%d"cWHITE"\n\n\
					Especifique el importe a transferir:\n\n\
					"gbDialogError"El formato ingresado no es valido.", NETWORK_COFFER );
					
				return showPlayerDialog( playerid, d_cnn + 11, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			NETWORK_COFFER -= strval( inputtext );
			SetPlayerBank( playerid, "+", strval( inputtext ) );
			
			mysql_format:g_small_string( "UPDATE `"DB_SAN"` SET `san_coffers` = %d WHERE 1", NETWORK_COFFER );
			mysql_tquery( mysql, g_small_string );
			
			pformat:( ""gbSuccess"Transferiste "cBLUE"$%d"cWHITE" a una cuenta bancaria.", strval( inputtext ) );
			psend:( playerid, C_WHITE );
	
			cmd_newspanel( playerid );
		}
	}
	
	return 1;
}