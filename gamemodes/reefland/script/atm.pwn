/*
stock CreateBankomat( Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz ) 
{
    CreateDynamicObject( 2754, x, y, z, rx, ry, rz );
    CreateDynamic3DTextLabel( "Cajero autom�tico", 0xFFFFFFFF, x, y, z, 3.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0 );
    atm_zone[ atm_server ] = CreateDynamicSphere( x, y, z, 2.0 );
    atm_server++;
	
	return 1;
}

function Atm_OnPlayerEnterDynamicArea( playerid, areaid ) 
{
	//if( atm_zone[0] <= areaid <= atm_zone[ atm_server - 1 ] ) SetPVarInt( playerid, "ATM:Use", 1 );
	return 1;
}

function Atm_OnPlayerLeaveDynamicArea( playerid, areaid ) 
{
	//if( atm_zone[0] <= areaid <= atm_zone[ atm_server - 1 ] ) DeletePVar( playerid, "ATM:Use" );
	return 1;
}*/

function Atm_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{
	switch( dialogid ) 
	{
		case d_bank + 1: 
		{
		    if( !response ) return 1;
		   
			switch(listitem) 
			{
		        case 0: 
				{
					clean:<g_string>;
					new
						hour, minute;
				
		            format:g_small_string( "\
						"cBLUE"Informaci�n de la cuenta bancaria\n\n\
						"cWHITE"Balance - "cBLUE"$%d", Player[playerid][uBank] );
						
					strcat( g_string, g_small_string );
					
					format:g_small_string( "\n"cWHITE"En cheques - "cBLUE"$%d", Player[playerid][uCheck] );
					strcat( g_string, g_small_string );
						
					strcat( g_string, "\n\n"cGRAY"Ultimas transacciones:" );
						
					for( new i; i < MAX_HISTORY; i++ )
					{
						if( Payment[playerid][i][HistoryTime] )
						{
							gmtime( Payment[playerid][i][HistoryTime], _, _, _, hour, minute );
							format:g_small_string( "\n"cGRAY"%02d:%02d %s", hour, minute, Payment[playerid][i][HistoryName] );
							
							strcat( g_string, g_small_string );
						}
					}

		            showPlayerDialog( playerid, d_bank + 7, DIALOG_STYLE_MSGBOX, " ", g_string, "Atras", "" );
		        }
				
		        case 1: 
				{
					showPlayerDialog( playerid, d_bank + 3, DIALOG_STYLE_INPUT, " ", dialog_cashout, "Siguiente", "Atras" );
				}
				
		        case 2:
				{
					showPlayerDialog( playerid, d_bank + 4, DIALOG_STYLE_INPUT, " ", dialog_cashin, "Siguiente", "Atras" );
				}
				
				case 3: 
				{
					showPlayerDialog( playerid, d_bank + 5, DIALOG_STYLE_INPUT, " ", dialog_transfer, "Siguiente", "Atras" );
				}
				
				case 4: 
				{
					if( job_duty{playerid} ) 
					{
						showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
						return SendClient:( playerid, C_WHITE, ""gbError"No has completado el d�a de trabajo.");
					}
					
					if( !Player[playerid][uCheck] ) 
					{
						showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
						return SendClient:( playerid, C_WHITE, ""gbError"No hay dinero en su chequera.");
					}
					
					SetPlayerBank( playerid, "+", Player[playerid][uCheck] );
					
					Player[playerid][uCheck] = 0;
					UpdatePlayer( playerid, "uCheck", 0 );
					
					SendClient:( playerid, C_WHITE, ""gbSuccess"Usted cobr� su chequera. Todos los fondos son transferidos a una cuenta bancaria." );
					showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
				}
		    
				case 5:
				{
					showPlayerDialog( playerid, d_bank + 8, DIALOG_STYLE_LIST, "Pago de servicios", dialog_pay, "Seleccionar", "Atras" );
				}
			}
		}
		
		
		case d_bank + 3: 
		{
		    if( !response ) 
				return showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
			
		    if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 || strval( inputtext ) > 100000 ) 
			{
				format:g_small_string( "%s\n\n"gbDialogError"Formato de entrada no v�lido.", dialog_cashout );
				
				return showPlayerDialog( playerid, d_bank + 3, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
		   
			if( Player[playerid][uBank] < strval( inputtext ) ) 
			{
				format:g_small_string( "%s\n\n"gbDialogError"No hay suficientes fondos en su cuenta bancaria.", dialog_cashout );
				
				return showPlayerDialog( playerid, d_bank + 3, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			SetPlayerBank( playerid, "-", strval( inputtext ) );
			SetPlayerCash( playerid, "+", strval( inputtext ) );
			
			pformat:( ""gbSuccess"Retiraste "cBLUE"$%d"cWHITE" de tu cuenta bancaria.", strval( inputtext ) );
			psend:( playerid, C_WHITE );
			
			format:g_small_string(  "retir� dinero de su cuenta bancaria");
			MeAction( playerid, g_small_string, 1 );
			
			ApplyAnimation( playerid, "PED", "ATM", 4.0, 0, 1, 1, 0, 0, 1 );
			
			showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
		}
		
		case d_bank + 4:
		{
		    if( !response ) 
				return showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
				
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 || strval( inputtext ) > 100000 ) 
			{
				format:g_small_string( "%s\n\n"gbDialogError"Formato de entrada no v�lido.", dialog_cashin );
				
				return showPlayerDialog( playerid, d_bank + 4, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
		   
			if( Player[playerid][uMoney] < strval( inputtext ) ) 
			{
				format:g_small_string( "%s\n\n"gbDialogError"No tienes suficiente efectivo.", dialog_cashin );
				
				return showPlayerDialog( playerid, d_bank + 4, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
		
			SetPlayerBank( playerid, "+", strval( inputtext ) );
			SetPlayerCash( playerid, "-", strval( inputtext ) );
			
			pformat:( ""gbSuccess"Depositas "cBLUE"$%d"cWHITE" a una cuenta bancaria.", strval( inputtext ) );
			psend:( playerid, C_WHITE );
			
			format:g_small_string(  "deposit� dinero");
			MeAction( playerid, g_small_string, 1 );
			
			ApplyAnimation( playerid, "PED", "ATM", 4.0, 0, 1, 1, 0, 0, 1 );
			
			showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
		}
		
		case d_bank + 5: 
		{
		    if( !response ) 
				return showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
				
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 0 || strval( inputtext ) >= MAX_PLAYERS ) 
			{
				format:g_small_string( "%s\n\n"gbDialogError"Formato de entrada no v�lido.", dialog_transfer );
				
				return showPlayerDialog( playerid, d_bank + 5, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
				
		    if( !IsLogged( strval( inputtext ) ) || strval( inputtext ) == playerid )
			{
				format:g_small_string( "%s\n\n"gbDialogError"ID de jugador inv�lida", dialog_transfer );
				
				return showPlayerDialog( playerid, d_bank + 5, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			SetPVarInt( playerid, "Bank:Playerid", strval( inputtext ) );
			
			format:g_small_string( dialog_transfer_2, Player[strval( inputtext )][uName], strval( inputtext ) );
			showPlayerDialog( playerid, d_bank + 6, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
		}
		
		case d_bank + 6: 
		{
		    if( !response )
			{
				DeletePVar( playerid, "Bank:Playerid" );
				return showPlayerDialog( playerid, d_bank + 5, DIALOG_STYLE_INPUT, " ", dialog_transfer, "Siguiente", "Atras" );
			}
			
			new
				id = GetPVarInt( playerid, "Bank:Playerid" );
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 || strval( inputtext ) > 1000000 )
			{
				format:g_small_string( dialog_transfer_2, Player[id][uName], id );
				strcat( g_small_string, "\n\n"gbDialogError"Formato de entrada no v�lido." );
				
				return showPlayerDialog( playerid, d_bank + 6, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			if( !IsLogged( id ) )
			{
				DeletePVar( playerid, "Bank:Playerid" );
				format:g_small_string( "%s\n\n"gbDialogError"ID de jugador inv�lida", dialog_transfer );
				
				return showPlayerDialog( playerid, d_bank + 5, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			if( Player[playerid][uBank] < strval( inputtext ) )
			{
				format:g_small_string( dialog_transfer_2, Player[id][uName], id );
				strcat( g_small_string, "\n\n"gbDialogError"No hay suficientes fondos en su cuenta bancaria." );
				
				return showPlayerDialog( playerid, d_bank + 6, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			format:g_small_string( ""gbSuccess"Usted transfiri� a %s[%d] - "cBLUE"$%d", Player[id][uName], id, strval( inputtext ) );
			SendClient:( playerid, C_WHITE, g_small_string );
			
			format:g_small_string( ""gbSuccess"%s[%d] hizo una transferencia a tu cuenta bancaria de "cBLUE"$%d", Player[playerid][uName], playerid, strval(inputtext) );
			SendClient:( id, C_WHITE, g_small_string );
			
			SetPlayerBank( playerid, "-", strval( inputtext ) );
			SetPlayerBank( id, "+", strval( inputtext ) );
			
			ApplyAnimation( playerid, "PED", "ATM",4.0, 0, 1, 1, 0, 0, 1 );
			DeletePVar( playerid, "Bank:Playerid" );
			
			log( LOG_TRANSFER_BANK_MONEY, "dinero transferido", Player[playerid][uID], Player[id][uID], strval( inputtext ) );
			format:g_small_string(  
				"[A] %s[%d][%s] transfiere a %s[%d][%s] - $%d", 
				Player[playerid][uName], playerid, Player[playerid][tIP], Player[id][uName], 
				id, Player[id][tIP], strval( inputtext ) );
			SendAdminMessage( C_DARKGRAY, g_small_string );
		}
		
		case d_bank + 7:
		{
			showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
		}
		
		case d_bank + 8:
		{
			if( !response )
				return showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
		
			switch( listitem )
			{
				case 0://Pago a domicilio
				{
					ShowHousePlayerList( playerid, d_bank + 9, "Siguiente", "Atras" );
				}
				
				case 1:// Pago de multas
				{
					ShowPenalties( playerid );
				}
			}
		}
		
		case d_bank + 9:
		{
			if( !response )
				return showPlayerDialog( playerid, d_bank + 8, DIALOG_STYLE_LIST, "Pago de servicios", dialog_pay, "Seleccionar", "Atras" );
				
			new
				h = g_dialog_select[playerid][listitem],
				days = floatround( float( HouseInfo[h][hSellDate] - gettime() ) / float( 86400 ), floatround_ceil ),
				month, day,
				price;
				
			if( days < 0 ) days = 0;
				
			gmtime( HouseInfo[h][hSellDate], _, month, day );
				
			if( 7 + Premium[playerid][prem_h_payment] - days == 0 )
			{
				pformat:( ""gbDefault"Este alquiler ya fue pagado hasta el %02d.%02d", day, month );
				psend:( playerid, C_WHITE );
			
				ShowHousePlayerList( playerid, d_bank + 9, "Siguiente", "Atras" );
				return 1;
			}
				
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
			
			SetPVarInt( playerid, "Bank:hID", h );
			SetPVarInt( playerid, "Bank:Day", days );
			
			if( !HouseInfo[h][hRent] )
			{
				price = GetPricePaymentHouse( h ) - ( GetPricePaymentHouse( h ) * Premium[playerid][prem_drop_payment] / 100 );
			
				format:g_small_string( "\
					"cBLUE"Pago de utilidades\n\n\
					"cWHITE"%s #%d: pagado el "cBLUE"%02d.%02d\n\
					"cWHITE"Tarifa: "cBLUE"%d/dia\n\n\
					"cWHITE"Para extender el alquiler, verifique los dias:\n\
					"gbDialog"M�ximo: %d",
					!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
					HouseInfo[h][hID],
					day, month,
					price,
					7 + Premium[playerid][prem_h_payment] - days );
					
				showPlayerDialog( playerid, d_bank + 10, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			else
			{
				price = GetPriceRentHouse( h ) - ( GetPriceRentHouse( h ) * Premium[playerid][prem_drop_payment] / 100 );
			
				format:g_small_string( "\
					"cBLUE"Pago de la renta\n\n\
					"cWHITE"%s #%d: pagado el "cBLUE"%02d.%02d\n\
					"cWHITE"Tarifa: "cBLUE"%d/dia\n\n\
					"cWHITE"Para extender el alquiler, verifique los dias:\n\
					"gbDialog"M�ximo: %d",
					!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
					HouseInfo[h][hID],
					day, month,
					price,
					7 + Premium[playerid][prem_h_payment] - days );
					
				showPlayerDialog( playerid, d_bank + 12, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
		}
		
		case d_bank + 10:
		{
			if( !response )
			{
				DeletePVar( playerid, "Bank:hID" );
				DeletePVar( playerid, "Bank:Day" );
			
				ShowHousePlayerList( playerid, d_bank + 9, "Siguiente", "Atras" );
				
				return 1;
			}
			
			new
				h = GetPVarInt( playerid, "Bank:hID" ),
				days = GetPVarInt( playerid, "Bank:Day" ),
				month, day,
				price = GetPricePaymentHouse( h ) - ( GetPricePaymentHouse( h ) * Premium[playerid][prem_drop_payment] / 100 );
			
			gmtime( HouseInfo[h][hSellDate], _, month, day );
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 || strval( inputtext ) > 7 + Premium[playerid][prem_h_payment] - days )
			{
				format:g_small_string( "\
					"cBLUE"Pago de utilidades\n\n\
					"cWHITE"%s #%d: pagado el "cBLUE"%02d.%02d\n\
					"cWHITE"Tarifa: "cBLUE"%d/dia\n\n\
					"cWHITE"Para extender el alquiler, verifique los dias:\n\
					"gbDialog"Maximo: %d\n\
					"gbDialogError"Formato de entrada no v�lido.",
					!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
					HouseInfo[h][hID],
					day, month,
					price,
					7 + Premium[playerid][prem_h_payment] - days );
					
				return showPlayerDialog( playerid, d_bank + 10, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			SetPVarInt( playerid, "Bank:PayDays", strval( inputtext ) );
			
			format:g_small_string( "\
				"cWHITE"Factura de servicios p�blicos para "cBLUE"%s #%d:\n\
				"cWHITE"Numero de dias: "cBLUE"%d"cWHITE" por el precio de "cBLUE"$%d\n\n\
				"cWHITE"�Quieres pagar esta cantidad?",
				!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
				HouseInfo[h][hID],
				strval( inputtext ), 
				price * strval( inputtext ) );
				
			showPlayerDialog( playerid, d_bank + 11, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Pagar", "Atras" );
		}
		
		case d_bank + 11:
		{
			new
				h = GetPVarInt( playerid, "Bank:hID" ),
				days = GetPVarInt( playerid, "Bank:Day" ),
				paydays = GetPVarInt( playerid, "Bank:PayDays" ),
				month, day,
				price = GetPricePaymentHouse( h ) - ( GetPricePaymentHouse( h ) * Premium[playerid][prem_drop_payment] / 100 );
			
			gmtime( HouseInfo[h][hSellDate], _, month, day );
			
			if( !response )
			{
				DeletePVar( playerid, "Bank:PayDays" );
			
				format:g_small_string( "\
					"cBLUE"Pago de utilidades\n\n\
					"cWHITE"%s #%d: pagado el "cBLUE"%02d.%02d\n\
					"cWHITE"Tarifa: "cBLUE"%d/dia\n\n\
					"cWHITE"Para extender el alquiler, verifique los dias:\n\
					"gbDialog"Maximo: %d",
					!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
					HouseInfo[h][hID],
					day, month,
					price,
					7 + Premium[playerid][prem_h_payment] - days );
					
				return showPlayerDialog( playerid, d_bank + 10, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			if( Player[playerid][uBank] < paydays * price )
			{
				format:g_small_string( "\
					"cBLUE"Pago de utilidades\n\n\
					"cWHITE"%s #%d: pagado el "cBLUE"%02d.%02d\n\
					"cWHITE"Tarifa: "cBLUE"%d/dia\n\n\
					"cWHITE"Para extender el alquiler, verifique los dias:\n\
					"gbDialog"M�ximo: %d\n\n\
					"gbDialogError"No hay suficientes fondos en su cuenta bancaria.",
					!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
					HouseInfo[h][hID],
					day, month,
					price,
					7 + Premium[playerid][prem_h_payment] - days );
					
				return showPlayerDialog( playerid, d_bank + 10, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			SetPlayerBank( playerid, "-", paydays * price );
			
			HouseInfo[h][hSellDate] += paydays * 86400;
			HouseUpdate( h, "hSellDate", HouseInfo[h][hSellDate] );
			
			pformat:( ""gbSuccess"Usted ha pagado con �xito su "cBLUE"%s #%d"cWHITE" por "cBLUE"%d dias"cWHITE" a "cBLUE"$%d"cWHITE".",
				!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
				HouseInfo[h][hID],
				paydays,
				paydays * price );
			psend:( playerid, C_WHITE );
			
			DeletePVar( playerid, "Bank:hID" );
			DeletePVar( playerid, "Bank:Day" );
			DeletePVar( playerid, "Bank:PayDays" );
			
			showPlayerDialog( playerid, d_bank + 8, DIALOG_STYLE_LIST, "Pago de servicios", dialog_pay, "Seleccionar", "Atras" );
		}
		
		case d_bank + 12:
		{
			if( !response )
			{
				DeletePVar( playerid, "Bank:hID" );
				DeletePVar( playerid, "Bank:Day" );
			
				ShowHousePlayerList( playerid, d_bank + 9, "Siguiente", "Atras" );
				
				return 1;
			}
			
			new
				h = GetPVarInt( playerid, "Bank:hID" ),
				days = GetPVarInt( playerid, "Bank:Day" ),
				month, day,
				price = GetPriceRentHouse( h ) - ( GetPriceRentHouse( h ) * Premium[playerid][prem_drop_payment] / 100 );
			
			gmtime( HouseInfo[h][hSellDate], _, month, day );
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 || strval( inputtext ) > 7 + Premium[playerid][prem_h_payment] - days )
			{
				format:g_small_string( "\
					"cBLUE"Pago de la renta\n\n\
					"cWHITE"%s #%d: pagado el "cBLUE"%02d.%02d\n\
					"cWHITE"Tarifa: "cBLUE"%d/dia\n\n\
					"cWHITE"Para extender el alquiler, verifique los dias:\n\
					"gbDialog"M�ximo: %d\n\
					"gbDialogError"Formato de entrada no v�lido.",
					!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
					HouseInfo[h][hID],
					day, month,
					price,
					7 + Premium[playerid][prem_h_payment] - days );
					
				return showPlayerDialog( playerid, d_bank + 12, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			SetPVarInt( playerid, "Bank:PayDays", strval( inputtext ) );
			
			format:g_small_string( "\
				"cWHITE"Factura de alquiler "cBLUE"%s #%d:\n\
				"cWHITE"Numero de dias:"cBLUE"%d"cWHITE" por el precio de "cBLUE"$%d\n\n\
				"cWHITE"�Deseas pagar el alquiler?",
				!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
				HouseInfo[h][hID],
				strval( inputtext ), 
				price * strval( inputtext ) );
				
			showPlayerDialog( playerid, d_bank + 13, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Pagar", "Atras" );
		}
		
		case d_bank + 13:
		{
			new
				h = GetPVarInt( playerid, "Bank:hID" ),
				days = GetPVarInt( playerid, "Bank:Day" ),
				paydays = GetPVarInt( playerid, "Bank:PayDays" ),
				month, day,
				price = GetPriceRentHouse( h ) - ( GetPriceRentHouse( h ) * Premium[playerid][prem_drop_payment] / 100 );
			
			gmtime( HouseInfo[h][hSellDate], _, month, day );
			
			if( !response )
			{
				DeletePVar( playerid, "Bank:PayDays" );
			
				format:g_small_string( "\
					"cBLUE"Pago de la renta\n\n\
					"cWHITE"%s #%d: pagado el "cBLUE"%02d.%02d\n\
					"cWHITE"Tarifa: "cBLUE"%d/dia\n\n\
					"cWHITE"Para extender el alquiler, verifique los dias:\n\
					"gbDialog"M�ximo: %d",
					!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
					HouseInfo[h][hID],
					day, month,
					price,
					7 + Premium[playerid][prem_h_payment] - days );
					
				showPlayerDialog( playerid, d_bank + 12, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			if( Player[playerid][uBank] < paydays * price )
			{
				format:g_small_string( "\
					"cBLUE"Pago de la renta\n\n\
					"cWHITE"%s #%d: pagado el "cBLUE"%02d.%02d\n\
					"cWHITE"Tarifa: "cBLUE"%d/dia\n\n\
					"cWHITE"Para extender el alquiler, verifique los dias:\n\
					"gbDialog"M�ximo: %d\n\n\
					"gbDialogError"No hay suficientes fondos en su cuenta bancaria.",
					!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
					HouseInfo[h][hID],
					day, month,
					price,
					7 + Premium[playerid][prem_h_payment] - days );
					
				return showPlayerDialog( playerid, d_bank + 12, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			SetPlayerBank( playerid, "-", paydays * price );
			
			HouseInfo[h][hSellDate] += paydays * 86400;
			HouseUpdate( h, "hSellDate", HouseInfo[h][hSellDate] );
			
			pformat:( ""gbSuccess"Usted ha pagado con �xito su "cBLUE"%s #%d"cWHITE" por "cBLUE"%d dia/s"cWHITE" a "cBLUE"$%d"cWHITE".",
				!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
				HouseInfo[h][hID],
				paydays,
				paydays * price );
			psend:( playerid, C_WHITE );
			
			DeletePVar( playerid, "Bank:hID" );
			DeletePVar( playerid, "Bank:Day" );
			DeletePVar( playerid, "Bank:PayDays" );
			
			showPlayerDialog( playerid, d_bank + 8, DIALOG_STYLE_LIST, "Pago de servicios", dialog_pay, "Seleccionar", "Atras" );
		}
		
		case d_bank + 14:
		{
			if( !response )
				return showPlayerDialog( playerid, d_bank + 8, DIALOG_STYLE_LIST, "Pago de servicios", dialog_pay, "Seleccionar", "Atras" );
			
			new
				pen = g_dialog_select[playerid][listitem];
				
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
			
			if( Player[playerid][uBank] < Penalty[playerid][pen][pen_price] )
			{
				SendClient:( playerid, C_WHITE, !""gbError"No hay suficientes fondos en su cuenta bancaria." );
			
				ShowPenalties( playerid );
				return 1;
			}
			
			SetPlayerBank( playerid, "-", Penalty[playerid][pen][pen_price] );
			Penalty[playerid][pen][pen_type] = 1;
			
			mysql_format:g_small_string( "UPDATE `"DB_PENALTIES"` SET `pen_type` = 1 WHERE `pen_id` = %d LIMIT 1", Penalty[playerid][pen][pen_id] );
			mysql_tquery( mysql, g_small_string );
			
			pformat:( ""gbSuccess"Propiedad "cBLUE"#%d"cWHITE" pagada con �xito, el dinero se debita de la cuenta bancaria.", Penalty[playerid][pen][pen_id] );
			psend:( playerid, C_WHITE );
			
			showPlayerDialog( playerid, d_bank + 8, DIALOG_STYLE_LIST, "Pago de servicios", dialog_pay, "Seleccionar", "Atras" );
		}
	}
	
	return 1;
}

function Atm_OnPlayerKeyStateChange( playerid, newkeys, oldkeys ) 
{
	if( PRESSED(KEY_WALK) ) 
	{
		for( new i; i < sizeof cashbox_info; i++ )
		{
			if( IsPlayerInRangeOfPoint( playerid, 1.0, cashbox_info[i][c_cashbox_pos][0], cashbox_info[i][c_cashbox_pos][1], cashbox_info[i][c_cashbox_pos][2] ) ) 
			{
				return showPlayerDialog( playerid, d_bank + 1, DIALOG_STYLE_LIST, "Gesti�n de cuenta personal", dialog_bank, "Siguiente", "Cancelar" );
			}
		}
	}
	
	return 1;
}