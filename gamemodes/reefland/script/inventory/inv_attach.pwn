#include <YSI\y_hooks>

hook OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) {
	switch( dialogid ) {
		case d_inv_attach: {
			switch( listitem ) {
				case 0: {
					showPlayerDialog( playerid, d_inv_attach + 1, DIALOG_STYLE_LIST, "Ajustar la posición",
						"1. Establecer posición\n2. Editar posición\n3. Restablecer la configuración",
						"Aceptar", "Salir" );
				}
			}
		}
	}
	return true;
}	