function showDonatPanel( playerid )
{
	Player[playerid][uGMoney] = cache_get_field_content_int( 0, "uGMoney", mysql );
	showPlayerDialog( playerid, d_donate, DIALOG_STYLE_LIST, "Tienda", donat, "Seleccionar", "Cerrar" );

	return 1;
}

function Donate_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{
	switch( dialogid ) 
	{
		case d_donate:
		{
			if( !response )
				return 1;
				
			switch( listitem )
			{
				case 0: //Informaci�n
				{
					showPlayerDialog( playerid, d_donate + 1, DIALOG_STYLE_LIST, "Informaci�n", donatinfo, "Seleccionar", "Atras" );
				}
				
				case 1: //Cambio de moneda
				{
					format:g_small_string( donatmoney, Player[playerid][uGMoney] );
					showPlayerDialog( playerid, d_donate + 4, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
				}
				
				case 2: //Cuentas premium
				{
					showPlayerDialog( playerid, d_donate + 2, DIALOG_STYLE_LIST, "Cuentas premium", donatpremium, "Seleccionar", "Atras" );
				}
				
				case 3: //Caracter�sticas adicionales
				{
					ShowDonatAdd( playerid );
				}
			}
		}
		
		case d_donate + 1:
		{
			if( !response )
				return showPlayerDialog( playerid, d_donate, DIALOG_STYLE_LIST, "Tienda", donat, "Seleccionar", "Cerrar" );
				
			switch( listitem )
			{
				case 0:
				{
					showPlayerDialog( playerid, d_donate + 25, DIALOG_STYLE_MSGBOX, " ", donattotal, "Atras", "" );
				}
				
				case 1:
				{
					format:g_string( donatbalance, Player[playerid][uGMoney] );
					showPlayerDialog( playerid, d_donate + 25, DIALOG_STYLE_MSGBOX, " ", g_string, "Atras", "" );
				}
				
				case 2:
				{
					if( !Premium[playerid][prem_id] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"No tienes una cuenta premium." );
						return showPlayerDialog( playerid, d_donate + 1, DIALOG_STYLE_LIST, "Informaci�n", donatinfo, "Seleccionar", "Atras" );
					}
					
					ShowMyPremiumInfo( playerid );
				}
			}
		}
		
		case d_donate + 2:
		{
			if( !response )
				return showPlayerDialog( playerid, d_donate, DIALOG_STYLE_LIST, "Tienda", donat, "Seleccionar", "Cerrar" );
				
			if( listitem == 4 ) //Extensi�n
			{
				if( !Premium[playerid][prem_id] )
				{
					SendClient:( playerid, C_WHITE, !""gbError"No tienes una cuenta premium." );
					return showPlayerDialog( playerid, d_donate + 2, DIALOG_STYLE_LIST, "Cuentas premium", donatpremium, "Seleccionar", "Atras" );
				}
				
				if( Premium[playerid][prem_type] == 4 )
				{
					SendClient:( playerid, C_WHITE, !""gbError"No puede renovar su cuenta premium 'Personalizada'." );
					return showPlayerDialog( playerid, d_donate + 2, DIALOG_STYLE_LIST, "Cuentas premium", donatpremium, "Seleccionar", "Atras" );
				}
				
				new
					days,
					Float:interval;
					
				interval = float( Premium[playerid][prem_time] - gettime() ) / 86400.0;
				days = floatround( interval, floatround_floor );
				
				if( days > 7 )
				{
					SendClient:( playerid, C_WHITE, !""gbError"Puede renovar su cuenta premium 7 d�as antes de la fecha de vencimiento." );
					return showPlayerDialog( playerid, d_donate + 2, DIALOG_STYLE_LIST, "Cuentas premium", donatpremium, "Seleccionar", "Atras" );
				}
				
				if( Player[playerid][uGMoney] < premium_info[ Premium[playerid][prem_type] ][prem_price] || !Player[playerid][uGMoney] )
				{
					SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					return showPlayerDialog( playerid, d_donate + 2, DIALOG_STYLE_LIST, "Cuentas premium", donatpremium, "Seleccionar", "Atras" );
				}
				
				Premium[playerid][prem_time] = Premium[playerid][prem_time] + 30 * 86400;
				Player[playerid][uGMoney] -= premium_info[ Premium[playerid][prem_type] ][prem_price];
				UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
				
				mysql_format:g_small_string( "UPDATE `"DB_PREMIUM"` SET `prem_time` = %d WHERE `prem_id` = %d LIMIT 1", 
					Premium[playerid][prem_time], Premium[playerid][prem_id] );
				mysql_tquery( mysql, g_small_string );
				
				pformat:( ""gbSuccess"Has ampliado con �xito tu cuenta premium %s%s"cWHITE" (%d ICoin) en "cBLUE"30"cWHITE" d�as.", GetPremiumColor( Premium[playerid][prem_color] ), 
					GetPremiumName( Premium[playerid][prem_type] ), premium_info[ Premium[playerid][prem_type] ][prem_price] );
				psend:( playerid, C_WHITE );
				
				showPlayerDialog( playerid, d_donate, DIALOG_STYLE_LIST, "Tienda", donat, "Seleccionar", "Cerrar" );
			}
			else if( listitem == 3 ) //Personalizable
			{
				TimePremium[playerid][prem_id] =
				TimePremium[playerid][prem_time] = 
				TimePremium[playerid][prem_type] = 
				TimePremium[playerid][prem_color] = 
				TimePremium[playerid][prem_gmoney] = 
				TimePremium[playerid][prem_bank] = 
				TimePremium[playerid][prem_salary] = 
				TimePremium[playerid][prem_benefit] = 
				TimePremium[playerid][prem_mass] = 
				TimePremium[playerid][prem_admins] = 
				TimePremium[playerid][prem_supports] = 
				TimePremium[playerid][prem_h_payment] = 
				TimePremium[playerid][prem_house] = 
				TimePremium[playerid][prem_car] = 
				TimePremium[playerid][prem_business] = 
				TimePremium[playerid][prem_house_property] = 
				TimePremium[playerid][prem_drop_retreature] = 
				TimePremium[playerid][prem_drop_tuning] = 
				TimePremium[playerid][prem_drop_repair] = 
				TimePremium[playerid][prem_drop_payment] = 0;
				
				ValuePremium[playerid][value_amount] =
				ValuePremium[playerid][value_gmoney] = 0;
				ValuePremium[playerid][value_days] = 30;
			
				SetPVarInt( playerid, "Premium:Type", listitem + 1 );
				ShowPremiumSettings( playerid );
			}
			else
			{
				SetPVarInt( playerid, "Premium:Type", listitem + 1 );
				ShowPremiumInfo( playerid, listitem + 1 );
			}
		}
		
		case d_donate + 3: //Inicio de compra, c�modo, �ptimo
		{
			if( !response )
			{
				DeletePVar( playerid, "Premium:Type" );
				return showPlayerDialog( playerid, d_donate + 2, DIALOG_STYLE_LIST, "Cuentas premium", donatpremium, "Seleccionar", "Atras" );
			}
			
			new
				type = GetPVarInt( playerid, "Premium:Type" );
				
			if( Premium[playerid][prem_id] )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Ya tienes una cuenta premium." );
				
				DeletePVar( playerid, "Premium:Type" );
				return showPlayerDialog( playerid, d_donate + 2, DIALOG_STYLE_LIST, "Cuentas premium", donatpremium, "Seleccionar", "Atras" );
			}
				
			if( Player[playerid][uGMoney] < premium_info[type][prem_price] )
			{
				SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
				
				DeletePVar( playerid, "Premium:Type" );
				return showPlayerDialog( playerid, d_donate + 2, DIALOG_STYLE_LIST, "Cuentas premium", donatpremium, "Seleccionar", "Atras" );
			}
			
			Player[playerid][uGMoney] -= premium_info[type][prem_price];
			UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
				
			Premium[playerid][prem_time] 		= gettime() + 30 * 86400;
			Premium[playerid][prem_type] 		= type;
			Premium[playerid][prem_color] 		= premium_info[type][prem_color][0];
			Premium[playerid][prem_gmoney]	 	= premium_info[type][prem_gmoney][0];
			Premium[playerid][prem_bank] 		= premium_info[type][prem_bank][0];
			Premium[playerid][prem_salary]		= premium_info[type][prem_salary][0];
			Premium[playerid][prem_benefit]	 	= premium_info[type][prem_benefit][0];
			Premium[playerid][prem_mass]	 	= premium_info[type][prem_mass][0];
			Premium[playerid][prem_admins]		= premium_info[type][prem_admins][0];
			Premium[playerid][prem_supports] 	= premium_info[type][prem_supports][0];
			Premium[playerid][prem_h_payment]	= premium_info[type][prem_h_payment][0];
			Premium[playerid][prem_car]			= premium_info[type][prem_car][0];
			Premium[playerid][prem_house]		= premium_info[type][prem_house][0];
			Premium[playerid][prem_business]	= premium_info[type][prem_business][0];
			Premium[playerid][prem_house_property]	= premium_info[type][prem_house_property][0];
			Premium[playerid][prem_drop_retreature]	= premium_info[type][prem_drop_retreature][0];
			Premium[playerid][prem_drop_tuning]		= premium_info[type][prem_drop_tuning][0];
			Premium[playerid][prem_drop_repair]		= premium_info[type][prem_drop_repair][0];
			Premium[playerid][prem_drop_payment]	= premium_info[type][prem_drop_payment][0];
			
			mysql_format:g_string( "\
				INSERT INTO `"DB_PREMIUM"`\
					( `prem_user_id`, `prem_type`, `prem_time`, `prem_color`, `prem_gmoney`, `prem_bank`, `prem_salary`,\
					`prem_benefit`, `prem_mass`, `prem_admins`, `prem_supports`, `prem_h_payment`, `prem_house`,\
					`prem_car`, `prem_business`, `prem_house_property`, `prem_drop_retreature`, `prem_drop_tuning`,\
					`prem_drop_repair`, `prem_drop_payment`\
					) \
				VALUES \
					( %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d )",
				Player[playerid][uID], 
				Premium[playerid][prem_type], 
				Premium[playerid][prem_time],
				Premium[playerid][prem_color],
				Premium[playerid][prem_gmoney],
				Premium[playerid][prem_bank],
				Premium[playerid][prem_salary],
				Premium[playerid][prem_benefit],
				Premium[playerid][prem_mass],
				Premium[playerid][prem_admins],
				Premium[playerid][prem_supports],
				Premium[playerid][prem_h_payment],
				Premium[playerid][prem_house],
				Premium[playerid][prem_car],
				Premium[playerid][prem_business],
				Premium[playerid][prem_house_property],
				Premium[playerid][prem_drop_retreature],
				Premium[playerid][prem_drop_tuning],
				Premium[playerid][prem_drop_repair],
				Premium[playerid][prem_drop_payment]
			);
			
			mysql_tquery( mysql, g_string, "AddPremium", "d", playerid );
			
			pformat:( ""gbSuccess"Has comprado una cuenta premium %s%s"cWHITE" (%d ICoins), por un pazo de "cBLUE"30"cWHITE" d�as.", GetPremiumColor( Premium[playerid][prem_color] ), premium_info[type][prem_price], GetPremiumName( type ) );
			psend:( playerid, C_WHITE );
			
			log( LOG_BUY_PREMIUM, "compr� una cuenta premium", Player[playerid][uID], type );
			
			DeletePVar( playerid, "Premium:Type" );
			showPlayerDialog( playerid, d_donate, DIALOG_STYLE_LIST, "Tienda", donat, "Seleccionar", "Cerrar" );
		}
		
		case d_donate + 4: //Cambio de moneda
		{
			if( !response )
				return showPlayerDialog( playerid, d_donate, DIALOG_STYLE_LIST, "Tienda", donat, "Seleccionar", "Cerrar" );
				
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1  )
			{
				clean:<g_string>;
			
				format:g_small_string( donatmoney, Player[playerid][uGMoney] ), strcat( g_string, g_small_string );
				strcat( g_string, "\n"gbDialogError"Formato de entrada inv�lido." );
				
				return showPlayerDialog( playerid, d_donate + 4, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Atras" );
			}
			
			if( strval( inputtext ) > Player[playerid][uGMoney] )
			{
				clean:<g_string>;
			
				format:g_small_string( donatmoney, Player[playerid][uGMoney] ), strcat( g_string, g_small_string );
				strcat( g_string, "\n"gbDialogError"No tienes ICoins suficientes." );
				
				return showPlayerDialog( playerid, d_donate + 4, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Atras" );
			}
			
			SetPVarInt( playerid, "Donat:Money", strval( inputtext ) );
			
			format:g_small_string( "\
				"cBLUE"Cambio de moneda"cWHITE"\n\n\
				Confirmaci�n: Usted intercambia "cBLUE"%d ICoins"cWHITE" por "cBLUE"$%d"cWHITE".\n\
				�Est�s seguro?",
				strval( inputtext ),
				strval( inputtext ) * 100 );
			showPlayerDialog( playerid, d_donate + 5, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Si", "No" );
		}
		
		case d_donate + 5:
		{
			if( !response )
			{
				DeletePVar( playerid, "Donat:Money" );
				
				format:g_small_string( donatmoney, Player[playerid][uGMoney] );
				return showPlayerDialog( playerid, d_donate + 4, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atras" );
			}
			
			new
				money = GetPVarInt( playerid, "Donat:Money" );
				
			if( money > Player[playerid][uGMoney] || !Player[playerid][uGMoney] )
			{
				clean:<g_string>;
			
				format:g_small_string( donatmoney, Player[playerid][uGMoney] ), strcat( g_string, g_small_string );
				strcat( g_string, "\n"gbDialogError"No tienes suficientes ICoins." );
				
				return showPlayerDialog( playerid, d_donate + 4, DIALOG_STYLE_INPUT, " ", g_string, "Siguiente", "Atras" );
			}
			
			Player[playerid][uGMoney] -= money;
			
			UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
			SetPlayerCash( playerid, "+", money * 100 );
			
			DeletePVar( playerid, "Donat:Money" );
			
			pformat:( ""gbSuccess"Intercambiaste "cBLUE"%d"cWHITE" ICoins por "cBLUE"$%d"cWHITE".", money, money * 100 );
			psend:( playerid, C_WHITE );
			
			log( LOG_TRANSFER_RCOIN, "moneda cambiada", Player[playerid][uID], money, money * 100 );
			
			showPlayerDialog( playerid, d_donate, DIALOG_STYLE_LIST, "Tienda", donat, "Seleccionar", "Cerrar" );
		}
		
		case d_donate + 6: //Di�logo con ajustes premium
		{
			if( !response )
			{
				TimePremium[playerid][prem_id] =
				TimePremium[playerid][prem_time] = 
				TimePremium[playerid][prem_type] = 
				TimePremium[playerid][prem_color] = 
				TimePremium[playerid][prem_gmoney] = 
				TimePremium[playerid][prem_bank] = 
				TimePremium[playerid][prem_salary] = 
				TimePremium[playerid][prem_benefit] = 
				TimePremium[playerid][prem_mass] = 
				TimePremium[playerid][prem_admins] = 
				TimePremium[playerid][prem_supports] = 
				TimePremium[playerid][prem_h_payment] = 
				TimePremium[playerid][prem_house] = 
				TimePremium[playerid][prem_car] = 
				TimePremium[playerid][prem_business] = 
				TimePremium[playerid][prem_house_property] = 
				TimePremium[playerid][prem_drop_retreature] = 
				TimePremium[playerid][prem_drop_tuning] = 
				TimePremium[playerid][prem_drop_repair] = 
				TimePremium[playerid][prem_drop_payment] = 0;
				
				ValuePremium[playerid][value_amount] =
				ValuePremium[playerid][value_gmoney] = 0;
				ValuePremium[playerid][value_days] = 30;
			
				DeletePVar( playerid, "Premium:Type" );
				return showPlayerDialog( playerid, d_donate + 2, DIALOG_STYLE_LIST, "Cuentas premium", donatpremium, "Seleccionar", "Atras" );
			}
			
			clean:<g_string>;
			
			new
				count = 0;
			
			switch( listitem )
			{
				case 0: //Colorear
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						format:g_small_string( "\n%sSeleccionar color", GetPremiumColor( i ) );
						strcat( g_string, g_small_string );
					}
					
					showPlayerDialog( playerid, d_donate + 7, DIALOG_STYLE_LIST, "Color premium", g_string, "Seleccionar", "Atras" );
				}
				
				case 1: //ICoins
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_gmoney ][0] )
						{
							format:g_small_string( "\n%d ICoins", premium_info[i][ prem_gmoney ][0] );
							strcat( g_string, g_small_string );
							
							count++;
							g_dialog_select[playerid][count] = i;
						}
					}
					
					showPlayerDialog( playerid, d_donate + 8, DIALOG_STYLE_LIST, "ICoin a PayDay", g_string, "Seleccionar", "Atras" );
				}
				
				case 2: //Banco
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_bank ][0] )
						{
							format:g_small_string( "\n0.%d %%", premium_info[i][ prem_bank ][0] );
							strcat( g_string, g_small_string );
						}
					}
					
					showPlayerDialog( playerid, d_donate + 9, DIALOG_STYLE_LIST, "Ahorros bancarios", g_string, "Seleccionar", "Atras" );
				}
				
				case 3: //Salario
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_salary ][0] )
						{
							format:g_small_string( "\n%d %%", premium_info[i][ prem_salary ][0] );
							strcat( g_string, g_small_string );
						}
					}
					
					showPlayerDialog( playerid, d_donate + 10, DIALOG_STYLE_LIST, "Salario", g_string, "Seleccionar", "Atras" );
				}
				
				case 4: //Prestaci�n por desempleo
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_benefit ][0] )
						{
							format:g_small_string( "\n%d %%", premium_info[i][ prem_benefit ][0] );
							strcat( g_string, g_small_string );
							
							count++;
							g_dialog_select[playerid][count] = i;
						}
					}
					
					showPlayerDialog( playerid, d_donate + 11, DIALOG_STYLE_LIST, "Prestaci�n por desempleo", g_string, "Seleccionar", "Atras" );
				}
				
				case 5: //Peso
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_mass ][0] )
						{
							format:g_small_string( "\n%d kg", premium_info[i][ prem_mass ][0] );
							strcat( g_string, g_small_string );
							
							count++;
							g_dialog_select[playerid][count] = i;
						}
					}
					
					showPlayerDialog( playerid, d_donate + 12, DIALOG_STYLE_LIST, "Espacio extra en el inventario", g_string, "Seleccionar", "Atras" );
				}
				
				case 6: //admins 
				{
					showPlayerDialog( playerid, d_donate + 13, DIALOG_STYLE_LIST, "Equipo /admins", "No\nSi", "Seleccionar", "Atras" );
				}
				
				case 7: //supports 
				{
					showPlayerDialog( playerid, d_donate + 14, DIALOG_STYLE_LIST, "Equipo /supports", "No\nSi", "Seleccionar", "Atras" );
				}
				
				case 8: //D�as para pagar en casa.
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_h_payment ][0] )
						{
							format:g_small_string( "\n+ %d d�as", premium_info[i][ prem_h_payment ][0] );
							strcat( g_string, g_small_string );
							
							count++;
							g_dialog_select[playerid][count] = i;
						}
					}
					
					showPlayerDialog( playerid, d_donate + 15, DIALOG_STYLE_LIST, "Pago a domicilio", g_string, "Seleccionar", "Atras" );
				}
				
				case 9: //Transporte
				{
					showPlayerDialog( playerid, d_donate + 16, DIALOG_STYLE_LIST, "Posibilidad de tener 2 coches", "No\nSi", "Seleccionar", "Atras" );
				}
				
				case 10: //Casas
				{
					showPlayerDialog( playerid, d_donate + 17, DIALOG_STYLE_LIST, "Posibilidad de tener 2 casas", "No\nSi", "Seleccionar", "Atras" );
				}
				
				case 11: //Las empresas
				{
					showPlayerDialog( playerid, d_donate + 18, DIALOG_STYLE_LIST, "Posibilidad de tener 2 negocios", "No\nSi", "Seleccionar", "Atras" );
				}
				
				case 12: //Venta de propiedades
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_house_property ][0] )
						{
							format:g_small_string( "\n+ %d %%", premium_info[i][ prem_house_property ][0] );
							strcat( g_string, g_small_string );
							
							count++;
							g_dialog_select[playerid][count] = i;
						}
					}
					
					showPlayerDialog( playerid, d_donate + 19, DIALOG_STYLE_LIST, "PROPUESTA DE PROPIEDAD AL ESTADO", g_string, "Seleccionar", "Atras" );
				}
				
				case 13: //Retexter
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_drop_retreature ][0] )
						{
							format:g_small_string( "\n- %d %%", premium_info[i][ prem_drop_retreature ][0] );
							strcat( g_string, g_small_string );
							
							count++;
							g_dialog_select[playerid][count] = i;
						}
					}
					
					showPlayerDialog( playerid, d_donate + 20, DIALOG_STYLE_LIST, "Costo de reexpresion", g_string, "Seleccionar", "Atras" );
				}
				
				case 14: //Puesta a punto
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_drop_tuning ][0] )
						{
							format:g_small_string( "\n- %d %%", premium_info[i][ prem_drop_tuning ][0] );
							strcat( g_string, g_small_string );
							
							count++;
							g_dialog_select[playerid][count] = i;
						}
					}
					
					showPlayerDialog( playerid, d_donate + 21, DIALOG_STYLE_LIST, "Costo de tuning", g_string, "Seleccionar", "Atras" );
				}
				
				case 15: //Reparaci�n
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_drop_repair ][0] )
						{
							format:g_small_string( "\n- %d %%", premium_info[i][ prem_drop_repair ][0] );
							strcat( g_string, g_small_string );
							
							count++;
							g_dialog_select[playerid][count] = i;
						}
					}
					
					showPlayerDialog( playerid, d_donate + 22, DIALOG_STYLE_LIST, "Costo de reparacion", g_string, "Seleccionar", "Atras" );
				}
				
				case 16: //Pagos de servicios
				{
					strcat( g_string, "No" );
				
					for( new i = 1; i < 4; i++ )
					{
						if( premium_info[i][ prem_drop_payment ][0] )
						{
							format:g_small_string( "\n- %d %%", premium_info[i][ prem_drop_payment ][0] );
							strcat( g_string, g_small_string );
							
							count++;
							g_dialog_select[playerid][count] = i;
						}
					}
					
					showPlayerDialog( playerid, d_donate + 23, DIALOG_STYLE_LIST, "Pago a domicilio", g_string, "Seleccionar", "Atras" );
				}
				
				case 17: //Al comprar, seleccione el plazo de la prima.
				{
					if( Premium[playerid][prem_id] )
					{
						ShowPremiumSettings( playerid );
						return SendClient:( playerid, C_WHITE, !""gbError"Ya tienes una cuenta premium." );
					}
				
					if( Player[playerid][uGMoney] < ValuePremium[playerid][value_gmoney] || !Player[playerid][uGMoney] )
					{
						ShowPremiumSettings( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					if( ValuePremium[playerid][value_gmoney] < premium_info[1][prem_price] || ValuePremium[playerid][value_amount] < 11 )
					{
						ShowPremiumSettings( playerid );
						
						pformat:( ""gbError"No hay suficientes ajustes ("cBLUE"%d/11"cWHITE") comprar una cuenta premium.", ValuePremium[playerid][value_amount] );
						psend:( playerid, C_WHITE );
						return 1;
					}
				
					format:g_small_string( "\
						"cWHITE"Numero de dias\t"cWHITE"Precio\n\
						"cWHITE"30 d�as\t"cBLUE"%d ICoins\n\
						"cWHITE"60 d�as\t"cBLUE"%d ICoins "cGREEN"(Descuento 6 %%)\n\
						"cWHITE"90 d�as\t"cBLUE"%d ICoins "cGREEN"(Descuento 12 %%)\n",
						ValuePremium[playerid][value_gmoney], 
						floatround( ValuePremium[playerid][value_gmoney] * 2 * 0.94 ),
						floatround( ValuePremium[playerid][value_gmoney] * 3 * 0.88 ) );
			
					showPlayerDialog( playerid, d_donate + 24, DIALOG_STYLE_TABLIST_HEADERS, "Duraci�n del premium", g_small_string, "Comprar", "Atras" );
				}
				
				case 18: //Comprar
				{
					if( Premium[playerid][prem_id] )
					{
						ShowPremiumSettings( playerid );
						return SendClient:( playerid, C_WHITE, !""gbError"Ya tienes una cuenta premium." );
					}
				
					if( Player[playerid][uGMoney] < ValuePremium[playerid][value_gmoney] || !Player[playerid][uGMoney] )
					{
						ShowPremiumSettings( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					if( ValuePremium[playerid][value_gmoney] < premium_info[1][prem_price] || ValuePremium[playerid][value_amount] < 11 )
					{
						ShowPremiumSettings( playerid );
						
						pformat:( ""gbError"No hay suficientes ajustes ("cBLUE"%d/11"cWHITE") para comprar una cuenta premium.", ValuePremium[playerid][value_amount] );
						psend:( playerid, C_WHITE );
						return 1;
					}
					
					Player[playerid][uGMoney] -= ValuePremium[playerid][value_gmoney];
					UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
						
					Premium[playerid][prem_time] 		= gettime() + ValuePremium[playerid][value_days] * 86400;
					Premium[playerid][prem_type] 		= 4;
					Premium[playerid][prem_color] 		= TimePremium[playerid][prem_color];
					Premium[playerid][prem_gmoney]	 	= TimePremium[playerid][prem_gmoney];
					Premium[playerid][prem_bank] 		= TimePremium[playerid][prem_bank];
					Premium[playerid][prem_salary]		= TimePremium[playerid][prem_salary];
					Premium[playerid][prem_benefit]	 	= TimePremium[playerid][prem_benefit];
					Premium[playerid][prem_mass]	 	= TimePremium[playerid][prem_mass];
					Premium[playerid][prem_admins]		= TimePremium[playerid][prem_admins];
					Premium[playerid][prem_supports] 	= TimePremium[playerid][prem_supports];
					Premium[playerid][prem_h_payment]	= TimePremium[playerid][prem_h_payment];
					Premium[playerid][prem_car]			= TimePremium[playerid][prem_car];
					Premium[playerid][prem_house]		= TimePremium[playerid][prem_house];
					Premium[playerid][prem_business]	= TimePremium[playerid][prem_business];
					Premium[playerid][prem_house_property]	= TimePremium[playerid][prem_house_property];
					Premium[playerid][prem_drop_retreature]	= TimePremium[playerid][prem_drop_retreature];
					Premium[playerid][prem_drop_tuning]		= TimePremium[playerid][prem_drop_tuning];
					Premium[playerid][prem_drop_repair]		= TimePremium[playerid][prem_drop_repair];
					Premium[playerid][prem_drop_payment]	= TimePremium[playerid][prem_drop_payment];
					
					mysql_format:g_string( "\
						INSERT INTO `"DB_PREMIUM"`\
							( `prem_user_id`, `prem_type`, `prem_time`, `prem_color`, `prem_gmoney`, `prem_bank`, `prem_salary`,\
							`prem_benefit`, `prem_mass`, `prem_admins`, `prem_supports`, `prem_h_payment`, `prem_house`,\
							`prem_car`, `prem_business`, `prem_house_property`, `prem_drop_retreature`, `prem_drop_tuning`,\
							`prem_drop_repair`, `prem_drop_payment`\
							) \
						VALUES \
							( %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d )",
						Player[playerid][uID], 
						Premium[playerid][prem_type], 
						Premium[playerid][prem_time],
						Premium[playerid][prem_color],
						Premium[playerid][prem_gmoney],
						Premium[playerid][prem_bank],
						Premium[playerid][prem_salary],
						Premium[playerid][prem_benefit],
						Premium[playerid][prem_mass],
						Premium[playerid][prem_admins],
						Premium[playerid][prem_supports],
						Premium[playerid][prem_h_payment],
						Premium[playerid][prem_house],
						Premium[playerid][prem_car],
						Premium[playerid][prem_business],
						Premium[playerid][prem_house_property],
						Premium[playerid][prem_drop_retreature],
						Premium[playerid][prem_drop_tuning],
						Premium[playerid][prem_drop_repair],
						Premium[playerid][prem_drop_payment]
					);
					
					mysql_tquery( mysql, g_string, "AddPremium", "d", playerid );
					
					pformat:( ""gbSuccess"Has comprado una cuenta premium %sPersonalizable"cWHITE", por  "cBLUE"%d"cWHITE" d�as.", GetPremiumColor( Premium[playerid][prem_color] ), ValuePremium[playerid][value_days] );
					psend:( playerid, C_WHITE );
					
					log( LOG_BUY_PREMIUM, "compr� una cuenta premium", Player[playerid][uID], 4 );
					
					TimePremium[playerid][prem_id] =
					TimePremium[playerid][prem_time] = 
					TimePremium[playerid][prem_type] = 
					TimePremium[playerid][prem_color] = 
					TimePremium[playerid][prem_gmoney] = 
					TimePremium[playerid][prem_bank] = 
					TimePremium[playerid][prem_salary] = 
					TimePremium[playerid][prem_benefit] = 
					TimePremium[playerid][prem_mass] = 
					TimePremium[playerid][prem_admins] = 
					TimePremium[playerid][prem_supports] = 
					TimePremium[playerid][prem_h_payment] = 
					TimePremium[playerid][prem_house] = 
					TimePremium[playerid][prem_car] = 
					TimePremium[playerid][prem_business] = 
					TimePremium[playerid][prem_house_property] = 
					TimePremium[playerid][prem_drop_retreature] = 
					TimePremium[playerid][prem_drop_tuning] = 
					TimePremium[playerid][prem_drop_repair] = 
					TimePremium[playerid][prem_drop_payment] = 0;
					
					ValuePremium[playerid][value_amount] =
					ValuePremium[playerid][value_gmoney] = 0;
					ValuePremium[playerid][value_days] = 30;
					
					showPlayerDialog( playerid, d_donate, DIALOG_STYLE_LIST, "Tienda", donat, "Seleccionar", "Cerrar" );
				}
			}
		}
		
		/* - - - - - - - Cuenta premium personalizable - - - - - - - */
		
		case d_donate + 7: //Color
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_color] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_color] == premium_info[i][ prem_color ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_color ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_color] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_color] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_color] == premium_info[i][ prem_color ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_color ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[listitem][ prem_color ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_color] = listitem;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 8: //ICoin
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_gmoney] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_gmoney] == premium_info[i][ prem_gmoney ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_gmoney ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_gmoney] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_gmoney] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_gmoney] == premium_info[i][ prem_gmoney ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_gmoney ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[ g_dialog_select[playerid][listitem] ][ prem_gmoney ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_gmoney] = premium_info[ g_dialog_select[playerid][listitem] ][ prem_gmoney ][0];
				g_dialog_select[playerid][listitem] = INVALID_PARAM;
			}
			
			ShowPremiumSettings( playerid );
		}

		case d_donate + 9: //Banco
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_bank] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_bank] == premium_info[i][ prem_bank ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_bank ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_bank] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_bank] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_bank] == premium_info[i][ prem_bank ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_bank ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[listitem][ prem_bank ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_bank] = premium_info[listitem][ prem_bank ][0];
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 10: //Salario
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_salary] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_salary] == premium_info[i][ prem_salary ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_salary ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_salary] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_salary] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_salary] == premium_info[i][ prem_salary ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_salary ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[listitem][ prem_salary ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_salary] = premium_info[listitem][ prem_salary ][0];
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 11: //Prestaci�n por desempleo
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_benefit] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_benefit] == premium_info[i][ prem_benefit ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_benefit ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_benefit] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_benefit] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_benefit] == premium_info[i][ prem_benefit ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_benefit ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[ g_dialog_select[playerid][listitem] ][ prem_benefit ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_benefit] = premium_info[ g_dialog_select[playerid][listitem] ][ prem_benefit ][0];
				g_dialog_select[playerid][listitem] = INVALID_PARAM;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 12: //Peso
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_mass] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_mass] == premium_info[i][ prem_mass ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_mass ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_mass] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_mass] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_mass] == premium_info[i][ prem_mass ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_mass ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[ g_dialog_select[playerid][listitem] ][ prem_mass ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_mass] = premium_info[ g_dialog_select[playerid][listitem] ][ prem_mass ][0];
				g_dialog_select[playerid][listitem] = INVALID_PARAM;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 13: //admins
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_admins] )
				{
					ValuePremium[playerid][value_amount] --;
					ValuePremium[playerid][value_gmoney] -= premium_info[3][ prem_admins ][1];
				}
				
				TimePremium[playerid][prem_admins] = 0;
			}
			else
			{
				if( !TimePremium[playerid][prem_admins] )
				{
					ValuePremium[playerid][value_amount] ++;
					ValuePremium[playerid][value_gmoney] += premium_info[ 3 ][ prem_admins ][1];
				}
				
				TimePremium[playerid][prem_admins] = 1;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 14: //supports
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_supports] )
				{
					ValuePremium[playerid][value_amount] --;
					ValuePremium[playerid][value_gmoney] -= premium_info[3][ prem_supports ][1];
				}
				
				TimePremium[playerid][prem_supports] = 0;
			}
			else
			{
				if( !TimePremium[playerid][prem_supports] )
				{
					ValuePremium[playerid][value_amount] ++;
					ValuePremium[playerid][value_gmoney] += premium_info[3][ prem_supports ][1];
				}
				
				TimePremium[playerid][prem_supports] = 1;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 15: //D�as para pagar en casa.
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_h_payment] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_h_payment] == premium_info[i][ prem_h_payment ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_h_payment ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_h_payment] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_h_payment] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_h_payment] == premium_info[i][ prem_h_payment ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_h_payment ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[ g_dialog_select[playerid][listitem] ][ prem_h_payment ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_h_payment] = premium_info[ g_dialog_select[playerid][listitem] ][ prem_h_payment ][0];
				g_dialog_select[playerid][listitem] = INVALID_PARAM;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 16: //Autos
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_car] )
				{
					ValuePremium[playerid][value_amount] --;
					ValuePremium[playerid][value_gmoney] -= premium_info[3][ prem_car ][1];
				}
				
				TimePremium[playerid][prem_car] = 0;
			}
			else
			{
				if( !TimePremium[playerid][prem_car] )
				{
					ValuePremium[playerid][value_amount] ++;
					ValuePremium[playerid][value_gmoney] += premium_info[3][ prem_car ][1];
				}
				
				TimePremium[playerid][prem_car] = 1;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 17: //Casas
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_house] )
				{
					ValuePremium[playerid][value_amount] --;
					ValuePremium[playerid][value_gmoney] -= premium_info[3][ prem_house ][1];
				}
				
				TimePremium[playerid][prem_house] = 0;
			}
			else
			{
				if( !TimePremium[playerid][prem_house] )
				{
					ValuePremium[playerid][value_amount] ++;
					ValuePremium[playerid][value_gmoney] += premium_info[3][ prem_house ][1];
				}
				
				TimePremium[playerid][prem_house] = 1;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 18: //Negocios
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_business] )
				{
					ValuePremium[playerid][value_amount] --;
					ValuePremium[playerid][value_gmoney] -= premium_info[3][ prem_business ][1];
				}
				
				TimePremium[playerid][prem_business] = 0;
			}
			else
			{
				if( !TimePremium[playerid][prem_business] )
				{
					ValuePremium[playerid][value_amount] ++;
					ValuePremium[playerid][value_gmoney] += premium_info[3][ prem_business ][1];
				}
				
				TimePremium[playerid][prem_business] = 1;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 19: //Venta de propiedades
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_house_property] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_house_property] == premium_info[i][ prem_house_property ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_house_property ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_house_property] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_house_property] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_house_property] == premium_info[i][ prem_house_property ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_house_property ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[ g_dialog_select[playerid][listitem] ][ prem_house_property ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_house_property] = premium_info[ g_dialog_select[playerid][listitem] ][ prem_house_property ][0];
				g_dialog_select[playerid][listitem] = INVALID_PARAM;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 20: //Retexter
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_drop_retreature] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_drop_retreature] == premium_info[i][ prem_drop_retreature ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_drop_retreature ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_drop_retreature] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_drop_retreature] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_drop_retreature] == premium_info[i][ prem_drop_retreature ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_drop_retreature ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[ g_dialog_select[playerid][listitem] ][ prem_drop_retreature ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_drop_retreature] = premium_info[ g_dialog_select[playerid][listitem] ][ prem_drop_retreature ][0];
				g_dialog_select[playerid][listitem] = INVALID_PARAM;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 21: //Tuning
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_drop_tuning] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_drop_tuning] == premium_info[i][ prem_drop_tuning ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_drop_tuning ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_drop_tuning] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_drop_tuning] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_drop_tuning] == premium_info[i][ prem_drop_tuning ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_drop_tuning ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[ g_dialog_select[playerid][listitem] ][ prem_drop_tuning ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_drop_tuning] = premium_info[ g_dialog_select[playerid][listitem] ][ prem_drop_tuning ][0];
				g_dialog_select[playerid][listitem] = INVALID_PARAM;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 22: //Reparaci�n
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_drop_repair] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_drop_repair] == premium_info[i][ prem_drop_repair ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_drop_repair ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_drop_repair] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_drop_repair] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_drop_repair] == premium_info[i][ prem_drop_repair ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_drop_repair ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[ g_dialog_select[playerid][listitem] ][ prem_drop_repair ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_drop_repair] = premium_info[ g_dialog_select[playerid][listitem] ][ prem_drop_repair ][0];
				g_dialog_select[playerid][listitem] = INVALID_PARAM;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 23: //Pago a domicilio
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				if( TimePremium[playerid][prem_drop_payment] )
					ValuePremium[playerid][value_amount] --;
			
				for( new i; i < 4; i++ )
				{
					if( TimePremium[playerid][prem_drop_payment] == premium_info[i][ prem_drop_payment ][0] )
					{
						ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_drop_payment ][1];
						break;
					}
				}
				
				TimePremium[playerid][prem_drop_payment] = 0;
			}
			else
			{
				if( TimePremium[playerid][prem_drop_payment] )
				{
					ValuePremium[playerid][value_amount] --;
					
					for( new i = 1; i < 4; i++ )
					{
						if( TimePremium[playerid][prem_drop_payment] == premium_info[i][ prem_drop_payment ][0] )
						{
							ValuePremium[playerid][value_gmoney] -= premium_info[i][ prem_drop_payment ][1];
							break;
						}
					}
				}
			
				ValuePremium[playerid][value_gmoney] += premium_info[ g_dialog_select[playerid][listitem] ][ prem_drop_payment ][1];
				ValuePremium[playerid][value_amount] ++;
			
				TimePremium[playerid][prem_drop_payment] = premium_info[ g_dialog_select[playerid][listitem] ][ prem_drop_payment ][0];
				g_dialog_select[playerid][listitem] = INVALID_PARAM;
			}
			
			ShowPremiumSettings( playerid );
		}
		
		case d_donate + 24: //Tiempo
		{
			if( !response )
			{
				ShowPremiumSettings( playerid );
				return 1;
			}
			
			new
				price;
			
			ValuePremium[playerid][value_days] = 30 * (listitem + 1);
			
			switch( listitem )
			{
				case 0: price = ValuePremium[playerid][value_gmoney];
				case 1: price = floatround( ValuePremium[playerid][value_gmoney] * 2 * 0.94 );
				case 2: price = floatround( ValuePremium[playerid][value_gmoney] * 3 * 0.88 );
			}
			
			if( Premium[playerid][prem_id] )
			{
				ShowPremiumSettings( playerid );
				return SendClient:( playerid, C_WHITE, !""gbError"Ya tienes una cuenta premium." );
			}
				
			if( Player[playerid][uGMoney] < price || !Player[playerid][uGMoney] )
			{
				ShowPremiumSettings( playerid );
				return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
			}
					
			if( ValuePremium[playerid][value_gmoney] < premium_info[1][prem_price] || ValuePremium[playerid][value_amount] < 11 )
			{
				ShowPremiumSettings( playerid );
						
				pformat:( ""gbError"No hay suficientes ajustes ("cBLUE"%d/11"cWHITE") para comprar una cuenta premium.", ValuePremium[playerid][value_amount] );
				psend:( playerid, C_WHITE );
				return 1;
			}
					
			Player[playerid][uGMoney] -= price;
			UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
						
			Premium[playerid][prem_time] 		= gettime() + ValuePremium[playerid][value_days] * 86400;
			Premium[playerid][prem_type] 		= 4;
			Premium[playerid][prem_color] 		= TimePremium[playerid][prem_color];
			Premium[playerid][prem_gmoney]	 	= TimePremium[playerid][prem_gmoney];
			Premium[playerid][prem_bank] 		= TimePremium[playerid][prem_bank];
			Premium[playerid][prem_salary]		= TimePremium[playerid][prem_salary];
			Premium[playerid][prem_benefit]	 	= TimePremium[playerid][prem_benefit];
			Premium[playerid][prem_mass]	 	= TimePremium[playerid][prem_mass];
			Premium[playerid][prem_admins]		= TimePremium[playerid][prem_admins];
			Premium[playerid][prem_supports] 	= TimePremium[playerid][prem_supports];
			Premium[playerid][prem_h_payment]	= TimePremium[playerid][prem_h_payment];
			Premium[playerid][prem_car]			= TimePremium[playerid][prem_car];
			Premium[playerid][prem_house]		= TimePremium[playerid][prem_house];
			Premium[playerid][prem_business]	= TimePremium[playerid][prem_business];
			Premium[playerid][prem_house_property]	= TimePremium[playerid][prem_house_property];
			Premium[playerid][prem_drop_retreature]	= TimePremium[playerid][prem_drop_retreature];
			Premium[playerid][prem_drop_tuning]		= TimePremium[playerid][prem_drop_tuning];
			Premium[playerid][prem_drop_repair]		= TimePremium[playerid][prem_drop_repair];
			Premium[playerid][prem_drop_payment]	= TimePremium[playerid][prem_drop_payment];
					
			mysql_format:g_string( "\
				INSERT INTO `"DB_PREMIUM"`\
					( `prem_user_id`, `prem_type`, `prem_time`, `prem_color`, `prem_gmoney`, `prem_bank`, `prem_salary`,\
					`prem_benefit`, `prem_mass`, `prem_admins`, `prem_supports`, `prem_h_payment`, `prem_house`,\
					`prem_car`, `prem_business`, `prem_house_property`, `prem_drop_retreature`, `prem_drop_tuning`,\
					`prem_drop_repair`, `prem_drop_payment`\
					) \
				VALUES \
					( %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d )",
				Player[playerid][uID], 
				Premium[playerid][prem_type], 
				Premium[playerid][prem_time],
				Premium[playerid][prem_color],
				Premium[playerid][prem_gmoney],
				Premium[playerid][prem_bank],
				Premium[playerid][prem_salary],
				Premium[playerid][prem_benefit],
				Premium[playerid][prem_mass],
				Premium[playerid][prem_admins],
				Premium[playerid][prem_supports],
				Premium[playerid][prem_h_payment],
				Premium[playerid][prem_house],
				Premium[playerid][prem_car],
				Premium[playerid][prem_business],
				Premium[playerid][prem_house_property],
				Premium[playerid][prem_drop_retreature],
				Premium[playerid][prem_drop_tuning],
				Premium[playerid][prem_drop_repair],
				Premium[playerid][prem_drop_payment]
			);
					
			mysql_tquery( mysql, g_string, "AddPremium", "d", playerid );
			
			pformat:( ""gbSuccess"Compraste una cuenta premium %sPersonalizada"cWHITE" (%d ICoins), por "cBLUE"%d"cWHITE" d�as.", GetPremiumColor( Premium[playerid][prem_color] ), price, ValuePremium[playerid][value_days] );
			psend:( playerid, C_WHITE );
					
			TimePremium[playerid][prem_id] =
			TimePremium[playerid][prem_time] = 
			TimePremium[playerid][prem_type] = 
			TimePremium[playerid][prem_color] = 
			TimePremium[playerid][prem_gmoney] = 
			TimePremium[playerid][prem_bank] = 
			TimePremium[playerid][prem_salary] = 
			TimePremium[playerid][prem_benefit] = 
			TimePremium[playerid][prem_mass] = 
			TimePremium[playerid][prem_admins] = 
			TimePremium[playerid][prem_supports] = 
			TimePremium[playerid][prem_h_payment] = 
			TimePremium[playerid][prem_house] = 
			TimePremium[playerid][prem_car] = 
			TimePremium[playerid][prem_business] = 
			TimePremium[playerid][prem_house_property] = 
			TimePremium[playerid][prem_drop_retreature] = 
			TimePremium[playerid][prem_drop_tuning] = 
			TimePremium[playerid][prem_drop_repair] = 
			TimePremium[playerid][prem_drop_payment] = 0;
					
			ValuePremium[playerid][value_amount] =
			ValuePremium[playerid][value_gmoney] = 0;
			ValuePremium[playerid][value_days] = 30;
					
			showPlayerDialog( playerid, d_donate, DIALOG_STYLE_LIST, "Tienda", donat, "Seleccionar", "Cerrar" );
		}
		
		case d_donate + 25:
		{
			showPlayerDialog( playerid, d_donate + 1, DIALOG_STYLE_LIST, "Informaci�n", donatinfo, "Seleccionar", "Atras" );
		}
		
		/* - - - - - - - - - - Caracter�sticas adicionales - - - - - - - - - - */
		
		case d_donate + 26:
		{
			if( !response )
				return showPlayerDialog( playerid, d_donate, DIALOG_STYLE_LIST, "Tienda", donat, "Seleccionar", "Cerrar" );
		
			switch( listitem )
			{
				case 0: //Cambio de roles
				{
					if( Player[playerid][uGMoney] < PRICE_CHANGE_ROLE )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
				
					showPlayerDialog( playerid, d_donate + 27, DIALOG_STYLE_LIST, ""cBLUE"Editar: g�nero", "\
							"gbDialog"Selecciona el g�nero de tu personaje:\n\
							"cBLUE"- "cWHITE"Hombre\n\
							"cBLUE"- "cWHITE"Hombre\
						", "Seleccionar", "Atras" );
				}
				
				case 1: //Cambio de sexo
				{
					if( Player[playerid][uGMoney] < PRICE_CHANGE_SEX )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					showPlayerDialog( playerid, d_donate + 33, DIALOG_STYLE_LIST, ""cBLUE"Editar: g�nero", "\
							"gbDialog"Selecciona el g�nero de tu personaje:\n\
							"cBLUE"- "cWHITE"Hombre\n\
							"cBLUE"- "cWHITE"Mujer\
						", "Seleccionar", "Atras" );
				}
				
				case 2: //Cambio de color de piel
				{
					if( Player[playerid][uGMoney] < PRICE_CHANGE_COLOR )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					showPlayerDialog( playerid, d_donate + 34, DIALOG_STYLE_LIST, ""cBLUE"Cambio: Color de piel", "\
							"gbDialog"Elige el color de piel de tu personaje:\n\
							"cBLUE"- "cWHITE"Oscuro\n\
							"cBLUE"- "cWHITE"Claro\
						", "Seleccionar", "Atras" );
				}
				
				case 3: //Cambio de nacionalidad
				{
					if( Player[playerid][uGMoney] < PRICE_CHANGE_NATION )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					showPlayerDialog( playerid, d_donate + 35, DIALOG_STYLE_LIST, ""cBLUE"Cambio: nacionalidad", "\
							"gbDialog"Elige la nacionalidad de tu personaje:\n\
							"cBLUE"- "cWHITE"Australiano\n\
							"cBLUE"- "cWHITE"Alban�s\n\
							"cBLUE"- "cWHITE"Americano\n\
							"cBLUE"- "cWHITE"Ingl�s\n\
							"cBLUE"- "cWHITE"Armenio\n\
							"cBLUE"- "cWHITE"Brasile�a\n\
							"cBLUE"- "cWHITE"Vietnamita\n\
							"cBLUE"- "cWHITE"Holand�s\n\
							"cBLUE"- "cWHITE"Jud�o\n\
							"cBLUE"- "cWHITE"Espa�ol\n\
							"cBLUE"- "cWHITE"Italiano\n\
							"cBLUE"- "cWHITE"Chino\n\
							"cBLUE"- "cWHITE"Colombiano\n\
							"cBLUE"- "cWHITE"Coreano\n\
							"cBLUE"- "cWHITE"Latino\n\
							"cBLUE"- "cWHITE"Mexicano\n\
							"cBLUE"- "cWHITE"Alem�n\n\
							"cBLUE"- "cWHITE"Polo\n\
							"cBLUE"- "cWHITE"Portugues\n\
							"cBLUE"- "cWHITE"Ruso\n\
							"cBLUE"- "cWHITE"Turco\n\
							"cBLUE"- "cWHITE"Ucraniano\n\
							"cBLUE"- "cWHITE"Franc�s\n\
							"cBLUE"- "cWHITE"Checo\n\
							"cBLUE"- "cWHITE"Japones\
						", "Seleccionar", "Atras" );
				}
				
				case 4: //Cambio de pa�s de nacimiento
				{
					if( Player[playerid][uGMoney] < PRICE_CHANGE_COUNTRY )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					showPlayerDialog( playerid, d_donate + 36, DIALOG_STYLE_LIST, ""cBLUE"Cambio: Pa�s de nacimiento", "\
							"gbDialog"Selecciona el pa�s de nacimiento de tu personaje:\n\
							"cBLUE" - "cWHITE" Australia \n \
							"cBLUE" - "cWHITE" Albania \n \
							"cBLUE" - "cWHITE" Inglaterra \n \
							"cBLUE" - "cWHITE" Armenia \n \
							"cBLUE" - "cWHITE" Brasil \n \
							"cBLUE" - "cWHITE" Vietnam \n \
							"cBLUE" - "cWHITE" Alemania \n \
							"cBLUE" - "cWHITE" Israel \n \
							"cBLUE" - "cWHITE" Espa�a \n \
							"cBLUE" - "cWHITE" Italia \n \
							"cBLUE" - "cWHITE" China \n \
							"cBLUE" - "cWHITE" Colombia \n \
							"cBLUE" - "cWHITE" Corea \n \
							"cBLUE" - "cWHITE" Mexico \n \
							"cBLUE" - "cWHITE" Pa�ses Bajos \n \
							"cBLUE" - "cWHITE" Polonia \n \
							"cBLUE" - "cWHITE" Portugal \n \
							"cBLUE" - "cWHITE" Rusia \n \
							"cBLUE" - "cWHITE" US \n \
							"cBLUE" - "cWHITE" Turqu�a \n \
							"cBLUE" - "cWHITE" Francia \n \
							"cBLUE" - "cWHITE" Ucrania \n \
							"cBLUE" - "cWHITE" Rep�blica Checa \n \
							"cBLUE" - "cWHITE" Jap�n \
						", "Seleccionar", "Atras" );
				}
				
				case 5: //Cambio de edad
				{
					if( Player[playerid][uGMoney] < PRICE_CHANGE_AGE )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					showPlayerDialog( playerid, d_donate + 37, DIALOG_STYLE_INPUT, " ", "\
							"cBLUE"Cambio: Edad\n\n\
							"cWHITE"Especifica la edad de tu personaje:\n\
							"gbDialog"16 a 70 a�os",
						"Siguiente", "Atras" );
				}
				
				case 6: //Cambiar apodo
				{
					if( Player[playerid][uGMoney] < PRICE_CHANGE_NAME )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					showPlayerDialog( playerid, d_donate + 39, DIALOG_STYLE_INPUT, " ", "\
							"cBLUE"Edici�n: apodo\n\n\
								"cWHITE"Especifique un nuevo nombre y apellido:\n\n\
								Considere la ortograf�a del Nombre/Apellido:\n\
								"cBLUE"- "cGRAY"el apodo debe consistir en letras latinas solamente,\n\
								"cBLUE"- "cGRAY"entre el Nombre y el Apellido se debe usar el signo '_'\n\
								"cBLUE"- "cGRAY"el apodo no debe contener m�s de 24 y menos de 6 caracteres,\n\
								"cBLUE"- "cGRAY"Nombre y/o Apellido no debe contener menos de 3 caracteres,\n\
								"cBLUE"- "cGRAY"El nombre y el apellido deben comenzar con letras may�sculas.",
						"Siguiente", "Atras" );
				}
				
				case 7: //Aprende el estilo de combate.
				{
					if( Player[playerid][uGMoney] < PRICE_STYLE )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
				
					showPlayerDialog( playerid, d_donate + 40, DIALOG_STYLE_LIST, "Aprende el estilo de combate.", "\
						"gbDialog"Elige un estilo de batalla:\n\
						"cBLUE"- "cWHITE"Boxeo\n\
						"cBLUE"- "cWHITE"Kung Fu\n\
						"cBLUE"- "cWHITE"Kneehead\n\
						"cBLUE"- "cWHITE"GrabKick\n\
						"cBLUE"- "cWHITE"ElBow", "Seleccionar", "Atras" );
				}
				
				case 8: //Examina todos los estilos de combate
				{
					if( Player[playerid][uGMoney] < PRICE_STYLE_ALL )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					new
						amount = 5;
					
					for( new i; i < 5; i++ )
					{
						if( Player[playerid][uStyle][i] )
							amount--;
					}
					
					if( !amount )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !""gbError"Todos los estilos de combate ya han sido estudiados." );
					}
					
					clean:<g_string>;
					
					strcat( g_string, ""cBLUE"Examina todos los estilos de combate\n\n"cWHITE"Usted aprender�:" );
					
					if( !Player[playerid][uStyle][0] )
						strcat( g_string, "\n"cBLUE"Boxeo" );
					
					if( !Player[playerid][uStyle][1] )
						strcat( g_string, "\n"cBLUE"Kung Fu" );
						
					if( !Player[playerid][uStyle][2] )
						strcat( g_string, "\n"cBLUE"KneeHead" );
						
					if( !Player[playerid][uStyle][3] )
						strcat( g_string, "\n"cBLUE"Grab Kick" );
					
					if( !Player[playerid][uStyle][4] )
						strcat( g_string, "\n"cBLUE"Elbow" );
						
					format:g_small_string( "\n\n"cWHITE"Esto cuesta "cBLUE"%d"cWHITE" ICoins, �Est�s seguro?", PRICE_STYLE_ALL );
					strcat( g_string, g_small_string );
					
					showPlayerDialog( playerid, d_donate + 41, DIALOG_STYLE_MSGBOX, " ", g_string, "Si", "No" );
				}
				
				case 9: //Quitar la advertencia
				{
					if( Player[playerid][uGMoney] < PRICE_UNWARN )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					if( !Player[playerid][uWarn] )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !""gbError"No tienes alertas." );
					}
					
					format:g_small_string( "\
						"cBLUE"Quitar una advertencia\n\n\
						"cWHITE"Tienes "cBLUE"%d/3"cWHITE" advertencias\n\
						"cWHITE"El costo para eliminar una advertencia es de"cBLUE"%d"cWHITE" ICoins, �Est�s seguro?", 
						Player[playerid][uWarn], PRICE_UNWARN );
					
					showPlayerDialog( playerid, d_donate + 42, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Si", "No" );
				}
				
				case 10: //Eliminar todas las advertencias
				{
					if( Player[playerid][uGMoney] < PRICE_UNWARN_ALL )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					if( !Player[playerid][uWarn] )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !""gbError"Usted no tiene advertencias" );
					}
					
					format:g_small_string( "\
						"cBLUE"Eliminar todas las advertencias\n\n\
						"cWHITE"Tienes "cBLUE"%d/3 "cWHITE" advertencias\n\
						"cWHITE"El costo para eliminarlas es de "cBLUE"%d"cWHITE" ICoins, �Est�s seguro?", 
						Player[playerid][uWarn], PRICE_UNWARN_ALL );
					
					showPlayerDialog( playerid, d_donate + 43, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Si", "No" );
				}
				
				case 11: //Cancelar un contrato
				{
					if( Player[playerid][uGMoney] < PRICE_UNJOB )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					if( !Player[ playerid ][uJob] )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !""gbError"Usted no est� empleado." );
					}
						
					if( job_duty{playerid} )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !""gbError"Debes completar el d�a de trabajo." );
					}
					
					format:g_small_string( "\
						"cBLUE"Terminar un contrato de trabajo de seis horas\n\n\
						"cWHITE"Para terminar con el contrato debes pagar "cBLUE"%d"cWHITE" ICoins, �Est�s seguro?",
						PRICE_UNJOB );
					
					showPlayerDialog( playerid, d_donate + 44, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Si", "No" );
				}
				
				case 12: //Cambiar n�mero de tel�fono
				{
					if( Player[playerid][uGMoney] < PRICE_NUMBER_PHONE )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
					}
					
					if( !GetPhoneNumber( playerid ) )
					{
						ShowDonatAdd( playerid );
						return SendClient:( playerid, C_WHITE, !""gbError"Ponga el tel�fono en el que desea cambiar el n�mero a la ranura activa del inventario." );
					}
					
					showPlayerDialog( playerid, d_donate + 45, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Cambiar n�mero de tel�fono\n\n\
						"cWHITE"Especifique el n�mero de tel�fono deseado:\n\
						"gbDialog"El n�mero de tel�fono debe ser de 6 d�gitos.", "Siguiente", "Atras" );
				}
			}
		}
		
		case d_donate + 27:
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
		
			if( !listitem )
			{
				return showPlayerDialog( playerid, d_donate + 27, DIALOG_STYLE_LIST, ""cBLUE"Editar: g�nero", "\
						"gbDialog"Selecciona el g�nero de tu personaje:\n\
						"cBLUE"- "cWHITE"Hombre\n\
						"cBLUE"- "cWHITE"Mujer\
					", "Seleccionar", "Atras" );
			}
			
			SetPVarInt( playerid, "Change:Sex", listitem );
			
			showPlayerDialog( playerid, d_donate + 28, DIALOG_STYLE_LIST, ""cBLUE"Cambio: Color de piel", "\
					"gbDialog"Elige el color de piel de tu personaje:\n\
					"cBLUE"- "cWHITE"Oscuro\n\
					"cBLUE"- "cWHITE"Claro\
				", "Seleccionar", "Atras" );
		}
		
		case d_donate + 28: 
		{
			if( !response )
			{
				DeletePVar( playerid, "Change:Sex" );
				return showPlayerDialog( playerid, d_donate + 27, DIALOG_STYLE_LIST, ""cBLUE"Editar: g�nero", "\
						"gbDialog"Selecciona el g�nero de tu personaje:\n\
						"cBLUE"- "cWHITE"Hombre\n\
						"cBLUE"- "cWHITE"Mujer\
					", "Seleccionar", "Atras" );
			}
		
			if( !listitem )
			{
				return showPlayerDialog( playerid, d_donate + 28, DIALOG_STYLE_LIST, ""cBLUE"Cambio: Color de piel", "\
						"gbDialog"Elige el color de piel de tu personaje:\n\
						"cBLUE"- "cWHITE"Oscuro\n\
						"cBLUE"- "cWHITE"Claro\
					", "Seleccionar", "Atras" );
			}
		
			SetPVarInt( playerid, "Change:Color", listitem );
		
			showPlayerDialog( playerid, d_donate + 29, DIALOG_STYLE_LIST, ""cBLUE"Cambio: nacionalidad", "\
					"gbDialog"Elige la nacionalidad de tu personaje:\n\
					"cBLUE"- "cWHITE"Australiano\n\
					"cBLUE"- "cWHITE"Alban�s\n\
					"cBLUE"- "cWHITE"Americano\n\
					"cBLUE"- "cWHITE"Ingl�s\n\
					"cBLUE"- "cWHITE"Armenio\n\
					"cBLUE"- "cWHITE"Brasile�a\n\
					"cBLUE"- "cWHITE"Vietnamita\n\
					"cBLUE"- "cWHITE"Holand�s\n\
					"cBLUE"- "cWHITE"Jud�o\n\
					"cBLUE"- "cWHITE"Espa�ol\n\
					"cBLUE"- "cWHITE"Italiano\n\
					"cBLUE"- "cWHITE"Chino\n\
					"cBLUE"- "cWHITE"Colombiano\n\
					"cBLUE"- "cWHITE"Coreano\n\
					"cBLUE"- "cWHITE"Latino\n\
					"cBLUE"- "cWHITE"Mexicano\n\
					"cBLUE"- "cWHITE"Alem�n\n\
					"cBLUE"- "cWHITE"Polo\n\
					"cBLUE"- "cWHITE"Portugues\n\
					"cBLUE"- "cWHITE"Ruso\n\
					"cBLUE"- "cWHITE"Turco\n\
					"cBLUE"- "cWHITE"Ucraniano\n\
					"cBLUE"- "cWHITE"Franc�s\n\
					"cBLUE"- "cWHITE"Checo\n\
					"cBLUE"- "cWHITE"Japones\
				", "Seleccionar", "Atras" );
		}
		
		case d_donate + 29: 
		{
			if( !response )
			{
				DeletePVar( playerid, "Change:Color" );
				return showPlayerDialog( playerid, d_donate + 28, DIALOG_STYLE_LIST, ""cBLUE"Cambio: Color de piel", "\
						"gbDialog"Elige el color de piel de tu personaje:\n\
						"cBLUE"- "cWHITE"Oscuro\n\
						"cBLUE"- "cWHITE"Claro\
					", "Seleccionar", "Atras" );
			}
			
			if( !listitem )
			{
				return showPlayerDialog( playerid, d_donate + 29, DIALOG_STYLE_LIST, ""cBLUE"Cambio: nacionalidad", "\
						"gbDialog"Elige la nacionalidad de tu personaje:\n\
						"cBLUE"- "cWHITE"Australiano\n\
						"cBLUE"- "cWHITE"Alban�s\n\
						"cBLUE"- "cWHITE"Americano\n\
						"cBLUE"- "cWHITE"Ingl�s\n\
						"cBLUE"- "cWHITE"Armenio\n\
						"cBLUE"- "cWHITE"Brasile�a\n\
						"cBLUE"- "cWHITE"Vietnamita\n\
						"cBLUE"- "cWHITE"Holand�s\n\
						"cBLUE"- "cWHITE"Jud�o\n\
						"cBLUE"- "cWHITE"Espa�ol\n\
						"cBLUE"- "cWHITE"Italiano\n\
						"cBLUE"- "cWHITE"Chino\n\
						"cBLUE"- "cWHITE"Colombiano\n\
						"cBLUE"- "cWHITE"Coreano\n\
						"cBLUE"- "cWHITE"Latino\n\
						"cBLUE"- "cWHITE"Mexicano\n\
						"cBLUE"- "cWHITE"Alem�n\n\
						"cBLUE"- "cWHITE"Polo\n\
						"cBLUE"- "cWHITE"Portugues\n\
						"cBLUE"- "cWHITE"Ruso\n\
						"cBLUE"- "cWHITE"Turco\n\
						"cBLUE"- "cWHITE"Ucraniano\n\
						"cBLUE"- "cWHITE"Franc�s\n\
						"cBLUE"- "cWHITE"Checo\n\
						"cBLUE"- "cWHITE"Japones\
					", "Seleccionar", "Atras" );
			}
			
			SetPVarInt( playerid, "Change:Nation", listitem );
			
			showPlayerDialog( playerid, d_donate + 30, DIALOG_STYLE_LIST, ""cBLUE"Cambio: Pa�s de nacimiento", "\
					"gbDialog"Selecciona el pa�s de nacimiento de tu personaje:\n\
					"cBLUE" - "cWHITE" Australia \n \
					"cBLUE" - "cWHITE" Albania \n \
					"cBLUE" - "cWHITE" Inglaterra \n \
					"cBLUE" - "cWHITE" Armenia \n \
					"cBLUE" - "cWHITE" Brasil \n \
					"cBLUE" - "cWHITE" Vietnam \n \
					"cBLUE" - "cWHITE" Alemania \n \
					"cBLUE" - "cWHITE" Israel \n \
					"cBLUE" - "cWHITE" Espa�a \n \
					"cBLUE" - "cWHITE" Italia \n \
					"cBLUE" - "cWHITE" China \n \
					"cBLUE" - "cWHITE" Colombia \n \
					"cBLUE" - "cWHITE" Corea \n \
					"cBLUE" - "cWHITE" Mexico \n \
					"cBLUE" - "cWHITE" Pa�ses Bajos \n \
					"cBLUE" - "cWHITE" Polonia \n \
					"cBLUE" - "cWHITE" Portugal \n \
					"cBLUE" - "cWHITE" Rusia \n \
					"cBLUE" - "cWHITE" US \n \
					"cBLUE" - "cWHITE" Turqu�a \n \
					"cBLUE" - "cWHITE" Francia \n \
					"cBLUE" - "cWHITE" Ucrania \n \
					"cBLUE" - "cWHITE" Rep�blica Checa \n \
					"cBLUE" - "cWHITE" Jap�n \
				", "Seleccionar", "Atras" );
		}
		
		case d_donate + 30:
		{
			if( !response )
			{
				DeletePVar( playerid, "Change:Nation" );
				return showPlayerDialog( playerid, d_donate + 29, DIALOG_STYLE_LIST, ""cBLUE"Cambio: nacionalidad", "\
						"gbDialog"Elige la nacionalidad de tu personaje:\n\
						"cBLUE"- "cWHITE"Australiano\n\
						"cBLUE"- "cWHITE"Alban�s\n\
						"cBLUE"- "cWHITE"Americano\n\
						"cBLUE"- "cWHITE"Ingl�s\n\
						"cBLUE"- "cWHITE"Armenio\n\
						"cBLUE"- "cWHITE"Brasile�a\n\
						"cBLUE"- "cWHITE"Vietnamita\n\
						"cBLUE"- "cWHITE"Holand�s\n\
						"cBLUE"- "cWHITE"Jud�o\n\
						"cBLUE"- "cWHITE"Espa�ol\n\
						"cBLUE"- "cWHITE"Italiano\n\
						"cBLUE"- "cWHITE"Chino\n\
						"cBLUE"- "cWHITE"Colombiano\n\
						"cBLUE"- "cWHITE"Coreano\n\
						"cBLUE"- "cWHITE"Latino\n\
						"cBLUE"- "cWHITE"Mexicano\n\
						"cBLUE"- "cWHITE"Alem�n\n\
						"cBLUE"- "cWHITE"Polo\n\
						"cBLUE"- "cWHITE"Portugues\n\
						"cBLUE"- "cWHITE"Ruso\n\
						"cBLUE"- "cWHITE"Turco\n\
						"cBLUE"- "cWHITE"Ucraniano\n\
						"cBLUE"- "cWHITE"Franc�s\n\
						"cBLUE"- "cWHITE"Checo\n\
						"cBLUE"- "cWHITE"Japones\
					", "Seleccionar", "Atras" );
			}
			
			if( !listitem )
			{
				return showPlayerDialog( playerid, d_donate + 30, DIALOG_STYLE_LIST, ""cBLUE"Cambio: Pa�s de nacimiento", "\
						"gbDialog"Selecciona el pa�s de nacimiento de tu personaje:\n\
						"cBLUE" - "cWHITE" Australia \n \
						"cBLUE" - "cWHITE" Albania \n \
						"cBLUE" - "cWHITE" Inglaterra \n \
						"cBLUE" - "cWHITE" Armenia \n \
						"cBLUE" - "cWHITE" Brasil \n \
						"cBLUE" - "cWHITE" Vietnam \n \
						"cBLUE" - "cWHITE" Alemania \n \
						"cBLUE" - "cWHITE" Israel \n \
						"cBLUE" - "cWHITE" Espa�a \n \
						"cBLUE" - "cWHITE" Italia \n \
						"cBLUE" - "cWHITE" China \n \
						"cBLUE" - "cWHITE" Colombia \n \
						"cBLUE" - "cWHITE" Corea \n \
						"cBLUE" - "cWHITE" Mexico \n \
						"cBLUE" - "cWHITE" Pa�ses Bajos \n \
						"cBLUE" - "cWHITE" Polonia \n \
						"cBLUE" - "cWHITE" Portugal \n \
						"cBLUE" - "cWHITE" Rusia \n \
						"cBLUE" - "cWHITE" US \n \
						"cBLUE" - "cWHITE" Turqu�a \n \
						"cBLUE" - "cWHITE" Francia \n \
						"cBLUE" - "cWHITE" Ucrania \n \
						"cBLUE" - "cWHITE" Rep�blica Checa \n \
						"cBLUE" - "cWHITE" Jap�n \
					", "Seleccionar", "Atras" );
			}
			
			SetPVarInt( playerid, "Change:Country", listitem );
			
			showPlayerDialog( playerid, d_donate + 31, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Cambio: Edad\n\n\
					"cWHITE"Especifica la edad de tu personaje:\n\
					"gbDialog"16 a 70 a�os",
				"Siguiente", "Atras" );
		}
		
		case d_donate + 31:
		{
			if( !response )
			{
				DeletePVar( playerid, "Change:Country" );
				return showPlayerDialog( playerid, d_donate + 30, DIALOG_STYLE_LIST, ""cBLUE"Cambio: Pa�s de nacimiento", "\
						"gbDialog"Selecciona el pa�s de nacimiento de tu personaje:\n\
						"cBLUE" - "cWHITE" Australia \n \
						"cBLUE" - "cWHITE" Albania \n \
						"cBLUE" - "cWHITE" Inglaterra \n \
						"cBLUE" - "cWHITE" Armenia \n \
						"cBLUE" - "cWHITE" Brasil \n \
						"cBLUE" - "cWHITE" Vietnam \n \
						"cBLUE" - "cWHITE" Alemania \n \
						"cBLUE" - "cWHITE" Israel \n \
						"cBLUE" - "cWHITE" Espa�a \n \
						"cBLUE" - "cWHITE" Italia \n \
						"cBLUE" - "cWHITE" China \n \
						"cBLUE" - "cWHITE" Colombia \n \
						"cBLUE" - "cWHITE" Corea \n \
						"cBLUE" - "cWHITE" Mexico \n \
						"cBLUE" - "cWHITE" Pa�ses Bajos \n \
						"cBLUE" - "cWHITE" Polonia \n \
						"cBLUE" - "cWHITE" Portugal \n \
						"cBLUE" - "cWHITE" Rusia \n \
						"cBLUE" - "cWHITE" US \n \
						"cBLUE" - "cWHITE" Turqu�a \n \
						"cBLUE" - "cWHITE" Francia \n \
						"cBLUE" - "cWHITE" Ucrania \n \
						"cBLUE" - "cWHITE" Rep�blica Checa \n \
						"cBLUE" - "cWHITE" Jap�n \
					", "Seleccionar", "Atras" );
			}
			
			if( !IsNumeric( inputtext ) || inputtext[0] == EOS || strval( inputtext ) < 16 || strval( inputtext ) > 70 )
			{
				return showPlayerDialog( playerid, d_donate + 31, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Cambio: Edad\n\n\
						"cWHITE"Especifica la edad de tu personaje:\n\
						"gbDialog"16 a 70 a�os\n\n\
						"gbDialogError"Formato incorrecto",
					"Siguiente", "Atras" );
			}
			 
			SetPVarInt( playerid, "Change:Age", strval( inputtext ) );
			
			format:g_string( ""cBLUE"Confirmar el cambio de rol\n\n\
				"cWHITE"Papel actual:\n\
				"cWHITE"Sexo - "cBLUE"%s,\n\
				"cWHITE"Color de piel - "cBLUE"%s,\n\
				"cWHITE"Nacionalidad - "cBLUE"%s,\n\
				"cWHITE"Pa�s de nacimiento - "cBLUE"%s,\n\
				"cWHITE"Edad - "cBLUE"%d %s\n\n\
				"cWHITE"Nuevo rol:\n\
				"cWHITE"Sexo - "cBLUE"%s,\n\
				"cWHITE"Color de piel - "cBLUE"%s,\n\
				"cWHITE"Nacionalidad - "cBLUE"%s,\n\
				"cWHITE"Pa�s de nacimiento - "cBLUE"%s,\n\
				"cWHITE"Edad - "cBLUE"%d %s\n\n\
				"cWHITE"Este cambio cuesta "cBLUE"%d"cWHITE" ICoins, �Est�s seguro?",
				GetSexName( Player[playerid][uSex] ),
				Player[playerid][uColor] == 2 ? ("Claro") : ("Oscuro"),
				GetNationName( Player[playerid][uNation] ),
				GetCountryName( Player[playerid][uCountry] ),
				Player[playerid][uAge], AgeTextEnd( Player[playerid][uAge]%10 ), 
				GetSexName( GetPVarInt( playerid, "Change:Sex" ) ),
				GetPVarInt( playerid, "Change:Color" ) == 2 ? ("Claro") : ("Oscuro"),
				GetNationName( GetPVarInt( playerid, "Change:Nation" ) ),
				GetCountryName( GetPVarInt( playerid, "Change:Country" ) ),
				GetPVarInt( playerid, "Change:Age" ), AgeTextEnd( GetPVarInt( playerid, "Change:Age" )%10 ),
				PRICE_CHANGE_ROLE );
				
			showPlayerDialog( playerid, d_donate + 32, DIALOG_STYLE_MSGBOX, " ", g_string, "Si", "No" );
		}
		
		case d_donate + 32:
		{
			if( !listitem )
			{
				DeletePVar( playerid, "Change:Sex" );
				DeletePVar( playerid, "Change:Color" );
				DeletePVar( playerid, "Change:Nation" );
				DeletePVar( playerid, "Change:Country" );
				DeletePVar( playerid, "Change:Age" );
				
				ShowDonatAdd( playerid );
				return SendClient:( playerid, C_WHITE, !""gbDefault"Te negaste a cambiar el papel." );
			}
			
			if( Player[playerid][uGMoney] < PRICE_CHANGE_ROLE )
			{
				DeletePVar( playerid, "Change:Sex" );
				DeletePVar( playerid, "Change:Color" );
				DeletePVar( playerid, "Change:Nation" );
				DeletePVar( playerid, "Change:Country" );
				DeletePVar( playerid, "Change:Age" );
			
				ShowDonatAdd( playerid );
				return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
			}
			
			Player[playerid][uGMoney] -= PRICE_CHANGE_ROLE;
			
			Player[playerid][uSex] = GetPVarInt( playerid, "Change:Sex" );
			Player[playerid][uColor] = GetPVarInt( playerid, "Change:Color" );
			Player[playerid][uNation] = GetPVarInt( playerid, "Change:Nation" );
			Player[playerid][uCountry] = GetPVarInt( playerid, "Change:Country" );
			Player[playerid][uAge] = GetPVarInt( playerid, "Change:Age" );
			
			DeletePVar( playerid, "Change:Sex" );
			DeletePVar( playerid, "Change:Color" );
			DeletePVar( playerid, "Change:Nation" );
			DeletePVar( playerid, "Change:Country" );
			DeletePVar( playerid, "Change:Age" );
			
			mysql_format:g_small_string( "UPDATE `"DB_USERS"` SET \
				`uSex` = %d, `uColor` = %d, `uNation` = %d, `uCountry` = %d, `uAge` = %d, `uGMoney` = %d \
				WHERE `uID` = %d LIMIT 1", 
				Player[playerid][uSex],
				Player[playerid][uColor],
				Player[playerid][uNation],
				Player[playerid][uCountry],
				Player[playerid][uAge],
				Player[playerid][uGMoney],
				Player[playerid][uID] );
			mysql_tquery( mysql, g_small_string );
			
			pformat:( ""gbSuccess"Has cambiado exitosamente el rol del personaje (%d ICoins). Consigue ropa adecuada para el nuevo rol.", PRICE_CHANGE_ROLE );
			psend:( playerid, C_WHITE );
			
			ShowDonatAdd( playerid );
		}
		
		case d_donate + 33: //Cambio de sexo
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				return showPlayerDialog( playerid, d_donate + 33, DIALOG_STYLE_LIST, ""cBLUE"Editar: g�nero", "\
						"gbDialog"Selecciona el g�nero de tu personaje:\n\
						"cBLUE"- "cWHITE"Hombre\n\
						"cBLUE"- "cWHITE"Mujer\
					", "Seleccionar", "Atras" );
			}
			
			SetPVarInt( playerid, "Change:Sex", listitem );
			ConfirmChangePlayer( playerid, 0 );
		}
		
		case d_donate + 34: //Cambio de color de piel
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				return showPlayerDialog( playerid, d_donate + 34, DIALOG_STYLE_LIST, ""cBLUE"Cambio: Color de piel", "\
							"gbDialog"Elige el color de piel de tu personaje:\n\
							"cBLUE"- "cWHITE"Oscuro\n\
							"cBLUE"- "cWHITE"Claro\
						", "Seleccionar", "Atras" );
			}
			
			SetPVarInt( playerid, "Change:Color", listitem );
			ConfirmChangePlayer( playerid, 1 );
		}
		
		case d_donate + 35: //Cambio de nacionalidad
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				return showPlayerDialog( playerid, d_donate + 35, DIALOG_STYLE_LIST, ""cBLUE"Cambio: nacionalidad", "\
							"gbDialog"Elige la nacionalidad de tu personaje:\n\
							"cBLUE"- "cWHITE"Australiano\n\
							"cBLUE"- "cWHITE"Alban�s\n\
							"cBLUE"- "cWHITE"Americano\n\
							"cBLUE"- "cWHITE"Ingl�s\n\
							"cBLUE"- "cWHITE"Armenio\n\
							"cBLUE"- "cWHITE"Brasile�a\n\
							"cBLUE"- "cWHITE"Vietnamita\n\
							"cBLUE"- "cWHITE"Holand�s\n\
							"cBLUE"- "cWHITE"Jud�o\n\
							"cBLUE"- "cWHITE"Espa�ol\n\
							"cBLUE"- "cWHITE"Italiano\n\
							"cBLUE"- "cWHITE"Chino\n\
							"cBLUE"- "cWHITE"Colombiano\n\
							"cBLUE"- "cWHITE"Coreano\n\
							"cBLUE"- "cWHITE"Latino\n\
							"cBLUE"- "cWHITE"Mexicano\n\
							"cBLUE"- "cWHITE"Alem�n\n\
							"cBLUE"- "cWHITE"Polo\n\
							"cBLUE"- "cWHITE"Portugues\n\
							"cBLUE"- "cWHITE"Ruso\n\
							"cBLUE"- "cWHITE"Turco\n\
							"cBLUE"- "cWHITE"Ucraniano\n\
							"cBLUE"- "cWHITE"Franc�s\n\
							"cBLUE"- "cWHITE"Checo\n\
							"cBLUE"- "cWHITE"Japones\
						", "Seleccionar", "Atras" );
			}
			
			SetPVarInt( playerid, "Change:Nation", listitem );
			ConfirmChangePlayer( playerid, 2 );
		}
		
		case d_donate + 36: //Cambio de pa�s de nacimiento
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				return showPlayerDialog( playerid, d_donate + 36, DIALOG_STYLE_LIST, ""cBLUE"Cambio: Pa�s de nacimiento", "\
							"gbDialog"Selecciona el pa�s de nacimiento de tu personaje:\n\
							"cBLUE" - "cWHITE" Australia \n \
							"cBLUE" - "cWHITE" Albania \n \
							"cBLUE" - "cWHITE" Inglaterra \n \
							"cBLUE" - "cWHITE" Armenia \n \
							"cBLUE" - "cWHITE" Brasil \n \
							"cBLUE" - "cWHITE" Vietnam \n \
							"cBLUE" - "cWHITE" Alemania \n \
							"cBLUE" - "cWHITE" Israel \n \
							"cBLUE" - "cWHITE" Espa�a \n \
							"cBLUE" - "cWHITE" Italia \n \
							"cBLUE" - "cWHITE" China \n \
							"cBLUE" - "cWHITE" Colombia \n \
							"cBLUE" - "cWHITE" Corea \n \
							"cBLUE" - "cWHITE" Mexico \n \
							"cBLUE" - "cWHITE" Pa�ses Bajos \n \
							"cBLUE" - "cWHITE" Polonia \n \
							"cBLUE" - "cWHITE" Portugal \n \
							"cBLUE" - "cWHITE" Rusia \n \
							"cBLUE" - "cWHITE" US \n \
							"cBLUE" - "cWHITE" Turqu�a \n \
							"cBLUE" - "cWHITE" Francia \n \
							"cBLUE" - "cWHITE" Ucrania \n \
							"cBLUE" - "cWHITE" Rep�blica Checa \n \
							"cBLUE" - "cWHITE" Jap�n \
						", "Seleccionar", "Atras" );
			}
			
			SetPVarInt( playerid, "Change:Country", listitem );
			ConfirmChangePlayer( playerid, 3 );
		}
		
		case d_donate + 37: //Cambio de edad
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( !IsNumeric( inputtext ) || inputtext[0] == EOS || strval( inputtext ) < 16 || strval( inputtext ) > 70 )
			{
				return showPlayerDialog( playerid, d_donate + 37, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Cambio: Edad\n\n\
						"cWHITE"Especifica la edad de tu personaje:\n\
						"gbDialog"16 a 70 a�os\n\n\
						"gbDialogError"Formato incorrecto",
					"Siguiente", "Atras" );
			}
			
			SetPVarInt( playerid, "Change:Age", strval( inputtext ) );
			ConfirmChangePlayer( playerid, 4 );
		}
		
		case d_donate + 38: //Cuadro de di�logo de confirmaci�n de cambio
		{
			if( !response )
			{
				DeletePVar( playerid, "Change:Sex" );
				DeletePVar( playerid, "Change:Color" );
				DeletePVar( playerid, "Change:Nation" );
				DeletePVar( playerid, "Change:Country" );
				DeletePVar( playerid, "Change:Age" );
				DeletePVar( playerid, "Change:Price" );
				DeletePVar( playerid, "Change:Type" );
				DeletePVar( playerid, "Change:Name" );
			
				ShowDonatAdd( playerid );
				return SendClient:( playerid, C_WHITE, !""gbError"Cancelaste los cambios." );
			}
			
			new
				price = GetPVarInt( playerid, "Change:Price" ),
				type = GetPVarInt( playerid, "Change:Type" );
				
			if( Player[playerid][uGMoney] < price )
			{
				DeletePVar( playerid, "Change:Sex" );
				DeletePVar( playerid, "Change:Color" );
				DeletePVar( playerid, "Change:Nation" );
				DeletePVar( playerid, "Change:Country" );
				DeletePVar( playerid, "Change:Age" );
				DeletePVar( playerid, "Change:Price" );
				DeletePVar( playerid, "Change:Type" );
				DeletePVar( playerid, "Change:Name" );
			
				ShowDonatAdd( playerid );
				return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
			}
			
			Player[playerid][uGMoney] -= price;
			UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
			
			switch( type )
			{
				case 0:
				{
					Player[playerid][uSex] = GetPVarInt( playerid, "Change:Sex" );
					pformat:( ""gbSuccess"Has cambiado exitosamente tu g�nero a "cBLUE"%s"cWHITE" (%d ICoins).", GetSexName( Player[playerid][uSex] ), price );
					UpdatePlayer( playerid, "uSex", Player[playerid][uSex] );
				}
				
				case 1:
				{
					Player[playerid][uColor] = GetPVarInt( playerid, "Change:Color" );
					pformat:( ""gbSuccess"Has cambiado exitosamente el color de la piel a "cBLUE"%s"cWHITE" (%d ICoins).", Player[playerid][uColor] == 2 ? ("Claro") : ("Oscuro"), price );
					UpdatePlayer( playerid, "uColor", Player[playerid][uColor] );
				}
				
				case 2:
				{
					Player[playerid][uNation] = GetPVarInt( playerid, "Change:Nation" );
					pformat:( ""gbSuccess"Has cambiado exitosamente tu nacionalidad a "cBLUE"%s"cWHITE" (%d ICoins).", GetNationName( Player[playerid][uNation] ), price );
					UpdatePlayer( playerid, "uNation", Player[playerid][uNation] );
				}
				
				case 3:
				{
					Player[playerid][uCountry] = GetPVarInt( playerid, "Change:Country" );
					pformat:( ""gbSuccess"Has cambiado exitosamente tu nacionalidad a "cBLUE"%s"cWHITE" (%d ICoins).", GetCountryName( Player[playerid][uCountry] ), price );
					UpdatePlayer( playerid, "uCountry", Player[playerid][uCountry] );
				}
				
				case 4:
				{
					Player[playerid][uAge] = GetPVarInt( playerid, "Change:Age" );
					pformat:( ""gbSuccess"Has cambiado exitosamente tu edad a "cBLUE"%d %s"cWHITE" (%d ICoins).", Player[playerid][uAge], AgeTextEnd( Player[playerid][uAge]%10 ), price );
					UpdatePlayer( playerid, "uAge", Player[playerid][uAge] );
				}
				
				case 5:
				{
					new
						name[ MAX_PLAYER_NAME ];
					GetPVarString( playerid, "Change:Name", name, MAX_PLAYER_NAME );
				
					format:g_small_string( "%s cambi� el apodo a %s", Player[playerid][uName], name );
					log( LOG_CHANGE_NAME, g_small_string, Player[playerid][uID] );
				
					format:g_small_string( ""ADMIN_PREFIX" %s[%d] cambi� el apodo a %s",
						Player[playerid][uName],
						playerid,
						name );
						
					SendAdmin:( C_DARKGRAY, g_small_string );
				
					SetPlayerName( playerid, name );
					
					Player[playerid][uName][0] = EOS;
					GetPlayerName( playerid, Player[playerid][uName], MAX_PLAYER_NAME );
					
					pformat:( ""gbSuccess"Has cambiado exitosamente tu apodo a "cBLUE"%s"cWHITE" (%d ICoins).", Player[playerid][uName], price );
					UpdatePlayerString( playerid, "uName", Player[playerid][uName] );
				}	
			}
			
			psend:( playerid, C_WHITE );
			
			DeletePVar( playerid, "Change:Sex" );
			DeletePVar( playerid, "Change:Color" );
			DeletePVar( playerid, "Change:Nation" );
			DeletePVar( playerid, "Change:Country" );
			DeletePVar( playerid, "Change:Age" );
			DeletePVar( playerid, "Change:Price" );
			DeletePVar( playerid, "Change:Type" );
			DeletePVar( playerid, "Change:Name" );
			
			ShowDonatAdd( playerid );
		}
		
		case d_donate + 39:
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
		
			if( !CheckDonateName( inputtext ) )
			{
				return showPlayerDialog( playerid, d_donate + 39, DIALOG_STYLE_INPUT, " ", "\
							"cBLUE"Edici�n: apodo\n\n\
								"cWHITE"Especifique un nuevo nombre y apellido:\n\
								"gbDialogError"Apodo incorrecto\n\n\
								Verifique la ortograf�a del Nombre / Apellido y vuelva a intentarlo:\n\
								"cBLUE"- "cGRAY"el apodo debe consistir en letras latinas solamente,\n\
								"cBLUE"- "cGRAY"entre el Nombre y el Apellido se debe usar el signo '_'\n\
								"cBLUE"- "cGRAY"el apodo no debe contener m�s de 24 y menos de 6 caracteres,\n\
								"cBLUE"- "cGRAY"Nombre y / o Apellido no debe contener menos de 3 caracteres,\n\
								"cBLUE"- "cGRAY"El nombre y el apellido deben comenzar con letras may�sculas.",
						"Siguiente", "Atras" );
			}
			
			SetPVarString( playerid, "Change:Name", inputtext );
			ConfirmChangePlayer( playerid, 5 );
		}
		
		case d_donate + 40:
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( !listitem )
			{
				return showPlayerDialog( playerid, d_donate + 40, DIALOG_STYLE_LIST, "Aprende el estilo de combate.", "\
							"gbDialog"Elige un estilo de batalla:\n\
							"cBLUE"- "cWHITE"Boxeo\n\
							"cBLUE"- "cWHITE"Kung Fu\n\
							"cBLUE"- "cWHITE"KneeHead\n\
							"cBLUE"- "cWHITE"Grab Kick\n\
							"cBLUE"- "cWHITE"ElBow", 
						"Seleccionar", "Atras" );
			}
			
			if( Player[playerid][uStyle][listitem - 1] )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Ya se ha aprendido un fuerte estilo de lucha." );
				return showPlayerDialog( playerid, d_donate + 40, DIALOG_STYLE_LIST, "Aprende el estilo de combate.", "\
							"gbDialog"Elige un estilo de batalla:\n\
							"cBLUE"- "cWHITE"Boxeo\n\
							"cBLUE"- "cWHITE"Kung Fu\n\
							"cBLUE"- "cWHITE"KneeHead\n\
							"cBLUE"- "cWHITE"Grab Kick\n\
							"cBLUE"- "cWHITE"ElBow", 
						"Seleccionar", "Atras" );
			}
			
			if( Player[playerid][uGMoney] < PRICE_STYLE )
			{
				SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
				return showPlayerDialog( playerid, d_donate + 40, DIALOG_STYLE_LIST, "Aprende el estilo de combate.", "\
							"gbDialog"Elige un estilo de batalla:\n\
							"cBLUE"- "cWHITE"Boxeo\n\
							"cBLUE"- "cWHITE"Kung Fu\n\
							"cBLUE"- "cWHITE"KneeHead\n\
							"cBLUE"- "cWHITE"Grab Kick\n\
							"cBLUE"- "cWHITE"ElBow", 
						"Seleccionar", "Atras" );
			}
			
			Player[playerid][uStyle][listitem - 1] = 1;
			Player[playerid][uGMoney] -= PRICE_STYLE;
			UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
			
			mysql_format:g_small_string( "UPDATE `"DB_USERS"` SET `uStyle` = '%d|%d|%d|%d|%d' WHERE `uID` = %d LIMIT 1",
				Player[playerid][uStyle][0],
				Player[playerid][uStyle][1],
				Player[playerid][uStyle][2],
				Player[playerid][uStyle][3],
				Player[playerid][uStyle][4],
				Player[playerid][uID]
				);
			mysql_tquery( mysql, g_small_string );
			
			switch( listitem )
			{
				case 1: pformat:( ""gbSuccess"Has aprendido con �xito el estilo de combate "cBLUE"Boxeo"cWHITE" (%d ICoins).", PRICE_STYLE );
				case 2: pformat:( ""gbSuccess"Has aprendido con �xito el estilo de combate "cBLUE"Kung Fu"cWHITE" (%d ICoins).", PRICE_STYLE );
				case 3: pformat:( ""gbSuccess"Has aprendido con �xito el estilo de combate "cBLUE"KneeHead"cWHITE" (%d ICoins).", PRICE_STYLE );
				case 4: pformat:( ""gbSuccess"Has aprendido con �xito el estilo de combate "cBLUE"Grab Kick"cWHITE" (%d ICoins).", PRICE_STYLE );
				case 5: pformat:( ""gbSuccess"Has aprendido con �xito el estilo de combate "cBLUE"ElBow"cWHITE" (%d ICoins).", PRICE_STYLE );
			}
			
			psend:( playerid, C_WHITE );
			ShowDonatAdd( playerid );
		}
		
		case d_donate + 41:
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( Player[playerid][uGMoney] < PRICE_STYLE )
			{
				ShowDonatAdd( playerid );
				return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
			}
			
			for( new i; i < 5; i++ )
			{
				if( !Player[playerid][uStyle][i] )
					Player[playerid][uStyle][i] = 1;
			}
			
			Player[playerid][uGMoney] -= PRICE_STYLE_ALL;
			UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
			
			mysql_format:g_small_string( "UPDATE `"DB_USERS"` SET `uStyle` = '1|1|1|1|1' WHERE `uID` = %d LIMIT 1", Player[playerid][uID] );
			mysql_tquery( mysql, g_small_string );
			
			pformat:( ""gbSuccess"Has estudiado con �xito todos los estilos de combate (%d ICoins).", PRICE_STYLE_ALL );
			psend:( playerid, C_WHITE );
			
			ShowDonatAdd( playerid );
		}
		
		case d_donate + 42: //Quitar la advertencia
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( Player[playerid][uGMoney] < PRICE_UNWARN )
			{
				ShowDonatAdd( playerid );
				return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
			}
			
			Player[playerid][uGMoney] -= PRICE_UNWARN;
			UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
			
			Player[playerid][uWarn] --;
			UpdatePlayer( playerid, "uWarn", Player[playerid][uWarn] );
			
			pformat:( ""gbSuccess"Has eliminado correctamente una advertencia (%d ICoins). Advertencias: "cBLUE"%d/3", PRICE_UNWARN, Player[playerid][uWarn] );
			psend:( playerid, C_WHITE );
			
			ShowDonatAdd( playerid );
		}
		
		case d_donate + 43: //Eliminar todas las advertencias
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( Player[playerid][uGMoney] < PRICE_UNWARN_ALL )
			{
				ShowDonatAdd( playerid );
				return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
			}
			
			Player[playerid][uGMoney] -= PRICE_UNWARN_ALL;
			UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
		
			Player[playerid][uWarn] = 0;
			UpdatePlayer( playerid, "uWarn", 0 );
			
			pformat:( ""gbSuccess"Has eliminado correctamente todas las advertencias (%d ICoins).", PRICE_UNWARN_ALL );
			psend:( playerid, C_WHITE );
			
			ShowDonatAdd( playerid );
		}
		
		case d_donate + 44: //Cancelar un contrato
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( Player[playerid][uGMoney] < PRICE_UNJOB )
			{
				ShowDonatAdd( playerid );
				return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
			}
			
			Player[playerid][uGMoney] -= PRICE_UNJOB;
			UpdatePlayer( playerid, "uGMoney", Player[playerid][uGMoney] );
			
			Player[ playerid ][uJob] = 
			Job[ playerid ][j_time] =
			job_duty{ playerid } = 0;
			
			UpdatePlayer( playerid, "uJob", 0 );
			UpdatePlayer( playerid, "uJobTime", 0 );
			
			pformat:( ""gbSuccess"Usted ha rescindido con �xito un contrato de trabajo de seis horas (%d ICoins).", PRICE_UNJOB );
			psend:( playerid, C_WHITE );
			
			ShowDonatAdd( playerid );
		}
		
		case d_donate + 45:
		{
			if( !response )
			{
				ShowDonatAdd( playerid );
				return 1;
			}
			
			if( !IsNumeric( inputtext ) || inputtext[0] == EOS || strval( inputtext ) < 100000 || strval( inputtext ) > 999999 || strlen( inputtext ) < 6 )
			{
				return showPlayerDialog( playerid, d_donate + 45, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Cambiar n�mero de tel�fono\n\n\
					"cWHITE"Especifique el n�mero de tel�fono deseado:\n\
					"gbDialog"El n�mero de tel�fono debe ser de 6 d�gitos.\n\n\
					"gbDialogError"Formato incorrecto.", "Siguiente", "Atras" );
			}
			
			SetPVarInt( playerid, "Change:Phone", strval( inputtext ) );
			
			format:g_small_string( "\
				"cBLUE"Cambiar n�mero de tel�fono\n\n\
				"cWHITE"Numero actual: "cBLUE"%d\n\
				"cWHITE"Nuevo numero: "cBLUE"%d\n\n\
				"cWHITE"El costo de cambiar el n�mero de tel�fono "cBLUE"%d"cWHITE" ICoins, �Est�s seguro?\n\
				"gbDialog"Despu�s de la confirmaci�n, seguir� la validaci�n del n�mero deseado.",
				GetPhoneNumber( playerid ),
				strval( inputtext ),
				PRICE_NUMBER_PHONE );
				
			showPlayerDialog( playerid, d_donate + 46, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Si", "No" );
		}
		
		case d_donate + 46:
		{
			if( !response )
			{
				DeletePVar( playerid, "Change:Phone" );
				return showPlayerDialog( playerid, d_donate + 45, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Cambiar n�mero de tel�fono\n\n\
					"cWHITE"Especifique el n�mero de tel�fono deseado:\n\
					"gbDialog"El n�mero de tel�fono debe ser de 6 d�gitos.", "Siguiente", "Atras" );
			}
		
			new
				number = GetPVarInt( playerid, "Change:Phone" );
		
			if( Player[playerid][uGMoney] < PRICE_NUMBER_PHONE )
			{
				DeletePVar( playerid, "Change:Phone" );
				ShowDonatAdd( playerid );
				
				return SendClient:( playerid, C_WHITE, !NO_HAVE_RCOIN );
			}
		
			mysql_format:g_small_string( "SELECT * FROM `"DB_PHONES"` WHERE `p_number` = %d", number );
			mysql_tquery( mysql, g_small_string, "ChangeNumber", "dd", playerid, number );
		}
	}
	
	return 1;
}

function LoadPremiumAccount( playerid )
{
	new 
		rows = cache_get_row_count(); 
	
	if( !rows )
		return 1;
		
	Premium[playerid][prem_id] = cache_get_field_content_int( 0, "prem_id", mysql );
	Premium[playerid][prem_type] = cache_get_field_content_int( 0, "prem_type", mysql );
	Premium[playerid][prem_time] = cache_get_field_content_int( 0, "prem_time", mysql );
	Premium[playerid][prem_color] = cache_get_field_content_int( 0, "prem_color", mysql );
	Premium[playerid][prem_gmoney] = cache_get_field_content_int( 0, "prem_gmoney", mysql );
	Premium[playerid][prem_bank] = cache_get_field_content_int( 0, "prem_bank", mysql );
	Premium[playerid][prem_salary] = cache_get_field_content_int( 0, "prem_salary", mysql );
	Premium[playerid][prem_benefit] = cache_get_field_content_int( 0, "prem_benefit", mysql );
	Premium[playerid][prem_mass] = cache_get_field_content_int( 0, "prem_mass", mysql );
	Premium[playerid][prem_admins] = cache_get_field_content_int( 0, "prem_admins", mysql );
	Premium[playerid][prem_supports] = cache_get_field_content_int( 0, "prem_supports", mysql );
	Premium[playerid][prem_h_payment] = cache_get_field_content_int( 0, "prem_h_payment", mysql );
	
	Premium[playerid][prem_house] = cache_get_field_content_int( 0, "prem_house", mysql );
	Premium[playerid][prem_car] = cache_get_field_content_int( 0, "prem_car", mysql );
	Premium[playerid][prem_business] = cache_get_field_content_int( 0, "prem_business", mysql );
	
	Premium[playerid][prem_house_property] = cache_get_field_content_int( 0, "prem_house_property", mysql );
	
	Premium[playerid][prem_drop_retreature] = cache_get_field_content_int( 0, "prem_drop_retreature", mysql );
	Premium[playerid][prem_drop_tuning] = cache_get_field_content_int( 0, "prem_drop_tuning", mysql );
	Premium[playerid][prem_drop_repair] = cache_get_field_content_int( 0, "prem_drop_repair", mysql );
	Premium[playerid][prem_drop_payment] = cache_get_field_content_int( 0, "prem_drop_payment", mysql );
	
	printf( "[load] Load Premium #%d for %s[%d]", Premium[playerid][prem_type], Player[playerid][uName], playerid );
	
	if( Premium[playerid][prem_time] )
	{
		new
			day,
			Float:interval;
			
		interval = float( Premium[playerid][prem_time] - gettime() ) / 86400.0;
		day = floatround( interval, floatround_floor );
	
		pformat:( ""gbDialog"Duraci�n de la cuenta premium %s%s"cGRAY" expira en "cBLUE"%d"cGRAY" d�as.", GetPremiumColor( Premium[playerid][prem_color] ), GetPremiumName( Premium[playerid][prem_type] ), day );
		psend:( playerid, C_WHITE );
	}
		
	return 1;
}

stock ShowPremiumInfo( playerid, premium ) //Informaci�n Para Inicio, Confort, �ptimo.
{
	clean:<g_big_string>;
	
	format:g_small_string( ""cWHITE"Cuenta premium%s %s\n\n", GetPremiumColor( premium_info[premium][ prem_color ][0] ), GetPremiumName( premium ) ), strcat( g_big_string, g_small_string );
	
	if( premium_info[premium][prem_gmoney][0] )
		format:g_small_string( ""cGRAY"ICoins a PayDay: "cBLUE"%d\n", premium_info[premium][prem_gmoney][0] ), strcat( g_big_string, g_small_string );
	
	if( premium_info[premium][prem_bank][0] )
		format:g_small_string( ""cGRAY"Un aumento de los ahorros en la cuenta del Banco en "cBLUE"0.%d %%", premium_info[premium][prem_bank][0] ), strcat( g_big_string, g_small_string );

	if( premium_info[premium][prem_salary][0] )
		format:g_small_string( "\n"cGRAY"Aumento salarial "cBLUE"%d %%", premium_info[premium][prem_salary][0] ), strcat( g_big_string, g_small_string );
		
	if( premium_info[premium][prem_benefit][0] )
		format:g_small_string( "\n"cGRAY"El aumento de las prestaciones de desempleo por "cBLUE"%d %%", premium_info[premium][prem_benefit][0] ), strcat( g_big_string, g_small_string );
		
	if( premium_info[premium][prem_mass][0] )
		format:g_small_string( "\n"cGRAY"Peso Extra: "cBLUE"%d kg.", premium_info[premium][prem_mass][0] ), strcat( g_big_string, g_small_string );
	
	if( premium_info[premium][prem_admins][0] )
		strcat( g_big_string, "\n"cGRAY"Acceso al equipo "cBLUE"/admins" );
		
	if( premium_info[premium][prem_supports][0] )
		strcat( g_big_string, "\n"cGRAY"Acceso al equipo "cBLUE"/supports" );
		
	if( premium_info[premium][prem_h_payment][0] )
		format:g_small_string( "\n"cGRAY"Posibilidad de pagar en casa "cBLUE"+%d d�as", premium_info[premium][prem_h_payment][0] ), strcat( g_big_string, g_small_string );
		
	if( premium_info[premium][prem_car][0] )
		strcat( g_big_string, "\n"cGRAY"Oportunidad de tener "cBLUE"2"cGRAY" Oportunidad de tener" );
		
	if( premium_info[premium][prem_house][0] )
		strcat( g_big_string, "\n"cGRAY"Oportunidad de tener "cBLUE"2"cGRAY" casas" );
		
	if( premium_info[premium][prem_business][0] )
		strcat( g_big_string, "\n"cGRAY"Oportunidad de tener "cBLUE"2"cGRAY" negocios" );
		
	if( premium_info[premium][prem_house_property][0] )
		format:g_small_string( "\n"cGRAY"El aumento en el valor de la venta de bienes al Estado "cBLUE"%d %%", premium_info[premium][prem_house_property][0] ), strcat( g_big_string, g_small_string );
		
	if( premium_info[premium][prem_drop_retreature][0] )
		format:g_small_string( "\n"cGRAY"Reducci�n de precio en la gesti�n inmobiliaria. "cBLUE"%d %%", premium_info[premium][prem_drop_retreature][0] ), strcat( g_big_string, g_small_string );
	
	if( premium_info[premium][prem_drop_tuning][0] )
		format:g_small_string( "\n"cGRAY"Reduciendo el precio del tuning de transporte. "cBLUE"%d %%", premium_info[premium][prem_drop_tuning][0] ), strcat( g_big_string, g_small_string );
		
	if( premium_info[premium][prem_drop_repair][0] )
		format:g_small_string( "\n"cGRAY"Precios reducidos para reparaciones de veh�culos por "cBLUE"%d %%", premium_info[premium][prem_drop_repair][0] ), strcat( g_big_string, g_small_string );
		
	if( premium_info[premium][prem_drop_payment][0] )
		format:g_small_string( "\n"cGRAY"Reducir el precio de las facturas de servicios p�blicos / alquilar una casa / apartamento en "cBLUE"%d %%", premium_info[premium][prem_drop_payment][0] ), strcat( g_big_string, g_small_string );
	
	format:g_small_string( "\n\n"cWHITE"El costo de adquisici�n para "cBLUE"30"cWHITE" dias es "cBLUE"%d"cWHITE" ICoins.", premium_info[premium][prem_price] ), strcat( g_big_string, g_small_string );
	format:g_small_string( "\nEn su cuenta tiene - "cBLUE"%d"cWHITE" ICoins", Player[playerid][uGMoney] ), strcat( g_big_string, g_small_string );
	
	return showPlayerDialog( playerid, d_donate + 3, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Comprar", "Atras" );
}

stock ShowMyPremiumInfo( playerid )
{
	clean:<g_big_string>;
	clean:<g_string>;
	
	new
		year, month, day;
	
	format:g_small_string( ""cWHITE"Mi cuenta premium%s %s\n", GetPremiumColor( Premium[playerid][ prem_color ] ), GetPremiumName( Premium[playerid][prem_type] ) ), strcat( g_big_string, g_small_string );
	
	gmtime( Premium[playerid][prem_time], year, month, day );	
	format:g_small_string( ""cWHITE"Premium expira "cBLUE"%02d.%02d.%d\n\n", day, month, year ), strcat( g_big_string, g_small_string );
	
	if( Premium[playerid][prem_gmoney] )
		format:g_small_string( ""cGRAY"ICoins a PayDay: "cBLUE"%d\n", Premium[playerid][prem_gmoney] ), strcat( g_big_string, g_small_string );
	
	if( Premium[playerid][prem_bank] )
		format:g_small_string( ""cGRAY"Un aumento de los ahorros en la cuenta del Banco en "cBLUE"0.%d %%", Premium[playerid][prem_bank] ), strcat( g_big_string, g_small_string );

	if( Premium[playerid][prem_salary] )
		format:g_small_string( "\n"cGRAY"Aumento salarial "cBLUE"%d %%", Premium[playerid][prem_salary] ), strcat( g_big_string, g_small_string );
		
	if( Premium[playerid][prem_benefit] )
		format:g_small_string( "\n"cGRAY"El aumento de las prestaciones de desempleo por "cBLUE"%d %%", Premium[playerid][prem_benefit] ), strcat( g_big_string, g_small_string );
		
	if( Premium[playerid][prem_mass] )
		format:g_small_string( "\n"cGRAY"Peso Extra: "cBLUE"%d kg.", Premium[playerid][prem_mass] ), strcat( g_big_string, g_small_string );
	
	if( Premium[playerid][prem_admins] )
		strcat( g_big_string, "\n"cGRAY"Acceso al equipo "cBLUE"/admins" );
		
	if( Premium[playerid][prem_supports] )
		strcat( g_big_string, "\n"cGRAY"Acceso al equipo "cBLUE"/supports" );
		
	if( Premium[playerid][prem_h_payment] )
		format:g_small_string( "\n"cGRAY"Posibilidad de pagar en casa. "cBLUE"+%d d�as", Premium[playerid][prem_h_payment] ), strcat( g_big_string, g_small_string );
		
	if( Premium[playerid][prem_car] )
	{
		strcat( g_big_string, "\n"cGRAY"Oportunidad de tener "cBLUE"2"cGRAY" veh�culos" );
		strcat( g_string, "\n\n"gbDialogError"Presta atenci�n al vencimiento de la cuenta premium\n\
		El segundo veh�culo se adquiere autom�ticamente al estado a valor de mercado." );
	}
		
	if( Premium[playerid][prem_house] )
	{
		strcat( g_big_string, "\n"cGRAY"Oportunidad de tener "cBLUE"2"cGRAY" casas" );
		strcat( g_string, "\n\n"gbDialogError"Presta atenci�n al vencimiento de la cuenta premium\n\
		La segunda casa / apartamento se adquiere autom�ticamente al estado al valor de mercado." );
	}
		
	if( Premium[playerid][prem_business] )
	{
		strcat( g_big_string, "\n"cGRAY"Oportunidad de tener "cBLUE"2"cGRAY" negocios" );
		strcat( g_string, "\n\n"gbDialogError"Presta atenci�n al vencimiento de la cuenta premium\n\
		El segundo negocio se proclama autom�ticamente al estado a su valor de mercado." );
	}
		
	if( Premium[playerid][prem_house_property] )
		format:g_small_string( "\n"cGRAY"El aumento en el valor de la venta de bienes al Estado "cBLUE"%d %%", Premium[playerid][prem_house_property] ), strcat( g_big_string, g_small_string );
		
	if( Premium[playerid][prem_drop_retreature] )
		format:g_small_string( "\n"cGRAY"Reducci�n de precio en la gesti�n inmobiliaria "cBLUE"%d %%", Premium[playerid][prem_drop_retreature] ), strcat( g_big_string, g_small_string );
	
	if( Premium[playerid][prem_drop_tuning] )
		format:g_small_string( "\n"cGRAY"Reduciendo el precio del tuning de transporte "cBLUE"%d %%", Premium[playerid][prem_drop_tuning] ), strcat( g_big_string, g_small_string );
		
	if( Premium[playerid][prem_drop_repair] )
		format:g_small_string( "\n"cGRAY"Precios reducidos para reparaciones de veh�culos por "cBLUE"%d %%", Premium[playerid][prem_drop_repair] ), strcat( g_big_string, g_small_string );
		
	if( Premium[playerid][prem_drop_payment] )
		format:g_small_string( "\n"cGRAY"Reducir el precio de las facturas de servicios p�blicos / alquilar una casa / apartamento en "cBLUE"%d %%", Premium[playerid][prem_drop_payment] ), strcat( g_big_string, g_small_string );
	
	if( g_string[0] != EOS )
	{
		strcat( g_big_string, g_string );
	}
	
	return showPlayerDialog( playerid, d_donate + 25, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atras", "" );
}

function AddPremium( playerid )
{
	Premium[playerid][prem_id] = cache_insert_id();
	return 1;
}

stock ShowPremiumSettings( playerid )
{
	clean:<g_big_string>;
	strcat( g_big_string, ""cWHITE"Personalizaci�n\t"cWHITE"Valor\n" );
	// -- 0
	if( TimePremium[playerid][prem_color] )
		format:g_small_string( ""cWHITE"Color premium\t%sColor\n", GetPremiumColor( TimePremium[playerid][prem_color] ) ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Color premium\t"cBLUE"No\n" );
	// -- 1	
	if( TimePremium[playerid][prem_gmoney] )
		format:g_small_string( ""cWHITE"ICoins a PayDay\t"cBLUE"%d\n", TimePremium[playerid][prem_gmoney] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"ICoins a PayDay\t"cRED"No\n" );
	// -- 2	
	if( TimePremium[playerid][prem_bank] )
		format:g_small_string( ""cWHITE"Porcentaje de ahorro en la cuenta del Banco.\t"cBLUE"0.%d %%\n", TimePremium[playerid][prem_bank] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Porcentaje de ahorro en la cuenta del Banco.\t"cRED"No\n" );
	// -- 3	
	if( TimePremium[playerid][prem_salary] )
		format:g_small_string( ""cWHITE"Porcentaje de salarios\t"cBLUE"%d %%\n", TimePremium[playerid][prem_salary] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Porcentaje de salarios\t"cRED"No\n" );
	// -- 4	
	if( TimePremium[playerid][prem_benefit] )
		format:g_small_string( ""cWHITE"Porcentaje de prestaciones por desempleo\t"cBLUE"%d %%\n", TimePremium[playerid][prem_benefit] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Porcentaje de prestaciones por desempleo\t"cRED"No\n" );
	// -- 5	
	if( TimePremium[playerid][prem_mass] )
		format:g_small_string( ""cWHITE"Peso Extra\t"cBLUE"%d kg.\n", TimePremium[playerid][prem_mass] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Peso Extra\t"cRED"No\n" );
	// -- 6 -- 7	
	format:g_small_string( ""cWHITE"Acceso al equipo /admins\t%s\n", !TimePremium[playerid][prem_admins] ? (""cRED"No") : (""cBLUE"Si") ), strcat( g_big_string, g_small_string );
	format:g_small_string( ""cWHITE"Acceso al equipo /supports\t%s\n", !TimePremium[playerid][prem_supports] ? (""cRED"No") : (""cBLUE"Si") ), strcat( g_big_string, g_small_string );
	// -- 8
	if( TimePremium[playerid][prem_h_payment] )
		format:g_small_string( ""cWHITE"Incrementa la cantidad de d�as a pagar en casa\t"cBLUE"+ %d\n", TimePremium[playerid][prem_h_payment] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Incrementa la cantidad de d�as a pagar en casa\t"cRED"No\n" );
	// -- 9 -- 10 -- 11	
	format:g_small_string( ""cWHITE"Posibilidad de tener 2 veh�culos\t%s\n", !TimePremium[playerid][prem_car] ? (""cRED"No") : (""cBLUE"Si") ), strcat( g_big_string, g_small_string );
	format:g_small_string( ""cWHITE"Posibilidad de tener 2 casas\t%s\n", !TimePremium[playerid][prem_house] ? (""cRED"No") : (""cBLUE"Si") ), strcat( g_big_string, g_small_string );
	format:g_small_string( ""cWHITE"Posibilidad de tener 2 negocios\t%s\n", !TimePremium[playerid][prem_business] ? (""cRED"No") : (""cBLUE"Si") ), strcat( g_big_string, g_small_string );
	// -- 12
	if( TimePremium[playerid][prem_house_property] )
		format:g_small_string( ""cWHITE"Incremento en el costo de venta de propiedades al estado\t"cBLUE"en %d %%\n", TimePremium[playerid][prem_house_property] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Incremento en el costo de venta de propiedades al estado\t"cRED"No\n" );
	// -- 13	
	if( TimePremium[playerid][prem_drop_retreature] )
		format:g_small_string( ""cWHITE"Descuento de los precios en la gesti�n inmobiliaria\t"cBLUE"en %d %%\n", TimePremium[playerid][prem_drop_retreature] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Descuento de los precios en la gesti�n inmobiliaria\t"cRED"No\n" );
	// -- 14	
	if( TimePremium[playerid][prem_drop_tuning] )
		format:g_small_string( ""cWHITE"Precios m�s bajos para tunear del veh�culo\t"cBLUE"en %d %%\n", TimePremium[playerid][prem_drop_tuning] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Precios m�s bajos para tunear del veh�culo\t"cRED"No\n" );
	// -- 15	
	if( TimePremium[playerid][prem_drop_repair] )
		format:g_small_string( ""cWHITE"Precios reducidos para reparaciones de veh�culos\t"cBLUE"en %d %%\n", TimePremium[playerid][prem_drop_repair] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Precios reducidos para reparaciones de veh�culos\t"cRED"No\n" );
	// -- 16	
	if( TimePremium[playerid][prem_drop_payment] )
		format:g_small_string( ""cWHITE"Menores facturas de servicios p�blicos/precios de alquiler\t"cBLUE"en %d %%\n", TimePremium[playerid][prem_drop_payment] ), strcat( g_big_string, g_small_string );
	else
		strcat( g_big_string, ""cWHITE"Menores facturas de servicios p�blicos/precios de alquiler\t"cRED"No\n" );
	// -- 17
	//format:g_small_string( ""cWHITE"Duraci�n de la prima\t"cBLUE"%d dias\n", ValuePremium[playerid][value_days] ), strcat( g_big_string, g_small_string );
	// -- 18
	format:g_small_string( ""gbDialog"Comprar cuenta premium\t"cBLUE"%d ICoins", ValuePremium[playerid][value_gmoney] ), strcat( g_big_string, g_small_string );
	
	showPlayerDialog( playerid, d_donate + 6, DIALOG_STYLE_TABLIST_HEADERS, "Comprar cuenta premium", g_big_string, "Seleccionar", "Atras" );
}

stock ShowDonatAdd( playerid )
{
	format:g_string( donatadd, 
		PRICE_CHANGE_ROLE,
		PRICE_CHANGE_SEX,
		PRICE_CHANGE_COLOR,
		PRICE_CHANGE_NATION,
		PRICE_CHANGE_COUNTRY,
		PRICE_CHANGE_AGE,
		PRICE_CHANGE_NAME,
		PRICE_STYLE,
		PRICE_STYLE_ALL,
		PRICE_UNWARN,
		PRICE_UNWARN_ALL,
		PRICE_UNJOB,
		PRICE_NUMBER_PHONE );
		
	//PRICE_NUMBER_CAR
		
	showPlayerDialog( playerid, d_donate + 26, DIALOG_STYLE_TABLIST_HEADERS, "Caracter�sticas adicionales", g_string, "Seleccionar", "Atras" );
}

stock ConfirmChangePlayer( playerid, type )
{
	SetPVarInt( playerid, "Change:Type", type );
	switch( type )
	{
		case 0: //Sexo
		{
			format:g_small_string( "\
				"cBLUE"Cambio de car�cter: g�nero\n\n\
				"cWHITE"G�nero actual - "cBLUE"%s\n\
				"cWHITE"G�nero nuevo - "cBLUE"%s\n\n\
				"cWHITE"Costo de cambio: "cBLUE"%d"cWHITE" ICoins. �Est�s seguro?",
				GetSexName( Player[playerid][uSex] ),
				GetSexName( GetPVarInt( playerid, "Change:Sex" ) ),
				PRICE_CHANGE_SEX );
				
			SetPVarInt( playerid, "Change:Price", PRICE_CHANGE_SEX );
		}
		
		case 1: //Color de piel
		{
			format:g_small_string( "\
				"cBLUE"Cambio de personaje: color de piel\n\n\
				"cWHITE"Color de piel actual - "cBLUE"%s\n\
				"cWHITE"Nueva piel de color - "cBLUE"%s\n\n\
				"cWHITE"Costo de cambio "cBLUE"%d"cWHITE" ICoins. �Est�s seguro?",
				Player[playerid][uColor] == 2 ? ("Claro") : ("Oscuro"),
				GetPVarInt( playerid, "Change:Color" ) == 2 ? ("Claro") : ("Oscuro"),
				PRICE_CHANGE_COLOR );
				
			SetPVarInt( playerid, "Change:Price", PRICE_CHANGE_COLOR );
		}
		
		case 2: //Nacionalidad
		{
			format:g_small_string( "\
				"cBLUE"Cambio de car�cter: nacionalidad\n\n\
				"cWHITE"Nacionalidad actual - "cBLUE"%s\n\
				"cWHITE"Nueva nacionalidad - "cBLUE"%s\n\n\
				"cWHITE"Costo de cambio "cBLUE"%d"cWHITE" ICoins. �Est�s seguro?",
				GetNationName( Player[playerid][uNation] ),
				GetNationName( GetPVarInt( playerid, "Change:Nation" ) ),
				PRICE_CHANGE_NATION );
				
			SetPVarInt( playerid, "Change:Price", PRICE_CHANGE_NATION );
		}
		
		case 3: //Pa�s de nacimiento
		{
			format:g_small_string( "\
				"cBLUE"Cambio de car�cter: pa�s de nacimiento\n\n\
				"cWHITE"Pa�s actual - "cBLUE"%s\n\
				"cWHITE"Nuevo pa�s - "cBLUE"%s\n\n\
				"cWHITE"Costo de cambio: "cBLUE"%d"cWHITE" ICoins. �Est�s seguro?",
				GetCountryName( Player[playerid][uCountry] ),
				GetCountryName( GetPVarInt( playerid, "Change:Country" ) ),
				PRICE_CHANGE_COUNTRY );
			
			SetPVarInt( playerid, "Change:Price", PRICE_CHANGE_COUNTRY );
		}
		
		case 4: //Edad
		{
			format:g_small_string( "\
				"cBLUE"Cambio de car�cter: edad\n\n\
				"cWHITE"Edad actual - "cBLUE"%d %s\n\
				"cWHITE"Nueva edad - "cBLUE"%d %s\n\n\
				"cWHITE"Costo de cambio: "cBLUE"%d"cWHITE" ICoins. �Est�s seguro?",
				Player[playerid][uAge], AgeTextEnd( Player[playerid][uAge]%10 ),
				GetPVarInt( playerid, "Change:Age" ), AgeTextEnd( GetPVarInt( playerid, "Change:Age" )%10 ),
				PRICE_CHANGE_AGE );
				
			SetPVarInt( playerid, "Change:Price", PRICE_CHANGE_AGE );
		}
		
		case 5: //Nick
		{
			new
				name[ MAX_PLAYER_NAME ];
			GetPVarString( playerid, "Change:Name", name, MAX_PLAYER_NAME );
		
			format:g_small_string( "\
				"cBLUE"Cambio de personaje: apodo\n\n\
				"cWHITE"Nombre actual - "cBLUE"%s\n\
				"cWHITE"Nuevo nombre - "cBLUE"%s\n\n\
				"cWHITE"Costo de cambio: "cBLUE"%d"cWHITE" ICoins. �Est�s seguro?",
				Player[playerid][uName],
				name,
				PRICE_CHANGE_NAME );
				
			SetPVarInt( playerid, "Change:Price", PRICE_CHANGE_NAME );
		}
	}
	
	showPlayerDialog( playerid, d_donate + 38, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Si", "No" );
}

stock CheckDonateName( name[] )
{
	if( strlen( name ) < 6 || strlen( name ) > MAX_PLAYER_NAME )
		return STATUS_ERROR;
	
    for( new i = 0; i < strlen( name ); i++ )
    {

		if( !( ( name[i] >= 'a' && name[i] <= 'z' ) || ( name[i] >= 'A' && name[i] <= 'Z' ) || name[i] == '_' ) )
                return STATUS_ERROR;
	}
		
	new 
		d = strfind( name, "_" );
			

    if( d == INVALID_PARAM ) 
		return STATUS_ERROR; 
		

    if( strfind( name, "_", false, d + 1 ) != INVALID_PARAM ) 
		return STATUS_ERROR; 
		
	new 
		pname[10];
    strmid( pname, name, 0, d, sizeof pname );
        
	new 
		surname[10];
    strmid( surname, name, d + 1, strlen( name ), sizeof surname);
		

    if( strlen( pname ) < 3 || strlen( surname ) < 3 ) 
		return STATUS_ERROR;
        

	if( !( pname[0] >= 'A' && pname[0] <= 'Z' ) )
		return STATUS_ERROR;
			

    if( !( surname[0] >= 'A' && surname[0]<='Z' ) ) 
		return STATUS_ERROR; 
			

    for( new i = 1; i < strlen( pname ); i++ )
    {
        if( !( pname[i] >= 'a' && pname[i] <= 'z' ) ) 
			return STATUS_ERROR; 
    }
		

    for( new i = 1; i < strlen( surname ); i++ )
    {
        if( !( surname[i] >= 'a' && surname[i] <= 'z' ) )
			return STATUS_ERROR;
	}
		
    return STATUS_OK;
}

function ChangeNumber( playerid, number )
{
	if( cache_get_row_count() )
	{
		DeletePVar( playerid, "Change:Phone" );
		return showPlayerDialog( playerid, d_donate + 45, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Cambiar n�mero de tel�fono\n\n\
					"cWHITE"Especifique el n�mero de tel�fono deseado:\n\
					"gbDialog"El n�mero de tel�fono debe ser de 6 d�gitos.\n\n\
					"gbDialogError"El n�mero de tel�fono especificado ya est� tomado, intente con otro.", "Siguiente", "Atras" );
	}
	
	new
		oldnumber,
		item;
		
	/* - - - - - - -  Aprendemos el n�mero de tel�fono actual y recordamos el �ndice. - - - - - - - */
	for( new i; i < MAX_INVENTORY_USE; i++ ) 
	{
		new
			id = getInventoryId( UseInv[playerid][i][inv_id] );
	
		if( inventory[id][i_type] == INV_PHONE )
		{
			oldnumber = UseInv[playerid][i][inv_param_2];
			item = i;
			break;
		}	
	}
	
	/* - - - - - - -  Cambiar el numero de telefono - - - - - - - */
	for( new i; i < MAX_PHONES; i++ )
	{
		if( Phone[playerid][i][p_number] == oldnumber )
		{
			Phone[playerid][i][p_number] = number;
			break;
		}
	}
	
	mysql_format:g_small_string( "UPDATE `"DB_PHONES"` SET `p_number` = %d WHERE `p_number` = %d AND `p_user_id` = %d LIMIT 1",
		number, oldnumber, Player[playerid][uID] );
	mysql_tquery( mysql, g_small_string );
	
	UseInv[playerid][item][inv_param_2] = number;
	
	mysql_format:g_small_string( "UPDATE `"DB_ITEMS"` SET `item_param_2` = %d WHERE `id` = %d LIMIT 1",
		number, UseInv[playerid][item][inv_bd] );
	mysql_tquery( mysql, g_small_string );
	
	/* - - - - - - - Informamos sobre el cambio exitoso del n�mero. - - - - - - - */
	
	pformat:( ""gbSuccess"Has cambiado con �xito el n�mero de tel�fono a "cBLUE"%d"cWHITE" (%d ICoins).", number, PRICE_NUMBER_PHONE );
	psend:( playerid, C_WHITE );
	
	DeletePVar( playerid, "Change:Phone" );
	
	ShowDonatAdd( playerid );
	
	return 1;
}