CMD:capo( playerid )
{
	if( !IsPlayerInAnyVehicle( playerid ) )
		return SendClient:( playerid, C_WHITE, !PLAYER_NEED_VEHICLE );
		
	if( GetPlayerState( playerid ) != PLAYER_STATE_DRIVER )
		return SendClient:( playerid, C_WHITE, !""gbError"Debes estar en el asiento del conductor." );
	
	new 
		vehicleid = GetPlayerVehicleID( playerid );
		
	if( !VehicleInfo[GetVehicleModel( vehicleid ) - 400][v_hood] )
		return SendClient:( playerid, C_WHITE, !""gbError"Este vehículo no tiene capo." );
	
	CheckVehicleParams( vehicleid );
	
	if( Vehicle[vehicleid][vehicle_state_hood] ) 
	{
		Vehicle[vehicleid][vehicle_state_hood] = false;
	}
	else 
	{
		Vehicle[vehicleid][vehicle_state_hood] = true;
	}
	
	SetVehicleParams( vehicleid );
	
	return 1;
}
CMD:turbinas( playerid )
{
	if( !IsPlayerInAnyVehicle( playerid ) )
		return SendClient:( playerid, C_WHITE, !PLAYER_NEED_VEHICLE );
	new vehicle = GetPlayerVehicleID( playerid );
	if( !NoApagar( vehicle ) ) 
		return SendClient:( playerid, C_WHITE, !PLAYER_NEED_VEHICLE );
	CheckEngine( playerid, vehicle );
	return 1;
}
CMD:maletero( playerid )
{
	if( !IsPlayerInAnyVehicle( playerid ) )
		return SendClient:( playerid, C_WHITE, !PLAYER_NEED_VEHICLE );
	
	if( GetPlayerState( playerid ) != PLAYER_STATE_DRIVER )
		return SendClient:( playerid, C_WHITE, !""gbError"Debes estar en el asiento del conductor." );
	
	new 
		vehicleid = GetPlayerVehicleID( playerid );
		
	if( !GetVehicleBag( GetVehicleModel( vehicleid ) ) )
		return SendClient:( playerid, C_WHITE, !""gbError"Este vehiculo no tiene maletero." );
	
	CheckVehicleParams( vehicleid );
	
	if( Vehicle[vehicleid][vehicle_state_boot] ) 
	{
		Vehicle[vehicleid][vehicle_state_boot] = false;
	}
	else 
	{
		Vehicle[vehicleid][vehicle_state_boot] = true;
	}
	
	SetVehicleParams( vehicleid );
	
	return 1;
}

CMD:ventana( playerid, params[] )
{
	if( !IsPlayerInAnyVehicle( playerid ) )
		return SendClient:( playerid, C_WHITE, !PLAYER_NEED_VEHICLE );
	
	new
		vehicleid = GetPlayerVehicleID( playerid ),
		seat = GetPlayerVehicleSeat( playerid );
		
	if( !vehicleid || seat == 128 || seat > 3 )
		return 1;
	
	if( isnull( params ) )
	{
		if( !Vehicle[vehicleid][vehicle_state_window][seat] )
		{
			Vehicle[vehicleid][vehicle_state_window][seat] = true;
		}
		else
		{
			Vehicle[vehicleid][vehicle_state_window][seat] = false;
		}
	}
	else if( !isnull( params ) && seat == 0 )
	{
		if( !strcmp( params, "i", true ) )
		{
			if( !Vehicle[vehicleid][vehicle_state_window][0] )
			{
				Vehicle[vehicleid][vehicle_state_window][0] = true;
			}
			else
			{
				Vehicle[vehicleid][vehicle_state_window][0] = false;
			}
		}
		else if( !strcmp( params, "d", true ) )
		{
			if( !Vehicle[vehicleid][vehicle_state_window][1] )
			{
				Vehicle[vehicleid][vehicle_state_window][1] = true;
			}
			else
			{
				Vehicle[vehicleid][vehicle_state_window][1] = false;
			}
		}
		else if( !strcmp( params, "ti", true ) )
		{
			if( !Vehicle[vehicleid][vehicle_state_window][2] )
			{
				Vehicle[vehicleid][vehicle_state_window][2] = true;
			}
			else
			{
				Vehicle[vehicleid][vehicle_state_window][2] = false;
			}
			
		}
		else if( !strcmp( params, "td", true ) )
		{
			if( !Vehicle[vehicleid][vehicle_state_window][3] )
			{
				Vehicle[vehicleid][vehicle_state_window][3] = true;
			}
			else
			{
				Vehicle[vehicleid][vehicle_state_window][3] = false;
			}
		}
		else
		{
			return SendClient:( playerid, C_WHITE, ""gbDefault"Sintaxis: /ventana <i - Izquierda | d - Derecha | ti - Izquierda (Trasera) | td - Derecha(Trasera)>" );
		}

	}
	
	SetVehicleParams( vehicleid );
	
	return 1;
}

CMD:pauto( playerid ) 
{
	if( IsPlayerInAnyVehicle( playerid ) && Vehicle[GetPlayerVehicleID(playerid)][vehicle_user_id] != Player[playerid][uID] )
		return ShowVehicleInformation( playerid, GetPlayerVehicleID(playerid), INVALID_DIALOG_ID, "Cerrar", "" );
	
	if( !IsOwnerVehicleCount( playerid ) )
		return SendClient:( playerid, C_WHITE, !""gbError"No tienes un vehículo personal." );
		
	g_player_interaction{playerid} = 1;
	ShowPlayerVehicleList( playerid, d_cars );
	
	return 1;
}


CMD:estacionarfc( playerid, params[] ) 
{
	if( !IsPlayerInAnyVehicle( playerid ) )
		return SendClient:( playerid, C_WHITE, !PLAYER_NEED_VEHICLE );

	if( !GetAccessCommand( playerid, "veh" ) )
	    return SendClient:( playerid, C_WHITE, !NO_ACCESS_CMD);

	new 
		vehicleid = GetPlayerVehicleID( playerid );

    SetVehiclePark( vehicleid );
	
	format:g_small_string( ""gbSuccess"Estacionaste el vehículo de facción ID: "cBLUE"%d"cWHITE".",
	    vehicleid
	);
	
	SendClient:( playerid, C_WHITE, g_small_string );
	
	clean_array();

	return 1;
}
CMD:crearfc( playerid, params[] ) 
{
	#define model   	params[0]
	#define color1  	params[1]
	#define color2  	params[2]
	
	if( !GetAccessCommand( playerid, "veh" ) )
	    return SendClient:( playerid, C_WHITE, !NO_ACCESS_CMD);

	if( !Player[playerid][uMember] || !Player[playerid][uRank] )
		return SendClient:( playerid, C_WHITE, !NO_ACCESS_CMD );

 	if( sscanf( params, "ddd", model, color1, color2 ) )
		return SendClient:( playerid, C_WHITE, !""gbDefault"Sintaxis: /crearfc [Modelo] [Color 1] [Color 2]" );

	if( model < 400 || model > 611  )
	    return SendClient:( playerid, C_WHITE, !""gbError"El modelo del vehículo creado debe ser de 400 a 600." );

	if( color1 < 0 || color1 > 255 )
	    return SendClient:( playerid, C_WHITE, !""gbError"El color 1 del vehículo creado debe ser de 0 a 255." );
	    
	if( color2 < 0 || color2 > 255 )
	    return SendClient:( playerid, C_WHITE, !""gbError"El color 2 del vehículo creado debe ser de 0 a 255." );

	if( GetPlayerInterior( playerid ) != 0 )
	    return SendClient:( playerid, C_WHITE, !""gbError"No puedes crear un vehículo en un interior." );
		
	new
	    Float:x,
	    Float:y,
	    Float:z;

	GetPlayerPos( playerid, x, y, z );

	new car = CreateVehicle( model, x + 1.5, y + 1.5, z, 0.0, color1, color2, 99999 );
			
	ClearVehicleData( car );
			
	Vehicle[car][vehicle_user_id] = -1;
	Vehicle[car][vehicle_model] = model;
	Vehicle[car][vehicle_member] = Player[playerid][uMember];
	Vehicle[car][vehicle_crime] = 0;
			
	Vehicle[car][vehicle_pos][0] = x;
	Vehicle[car][vehicle_pos][1] = y;
	Vehicle[car][vehicle_pos][2] = z;
	Vehicle[car][vehicle_pos][3] = 0.0;
			
	Vehicle[car][vehicle_color][0] = color1;
	Vehicle[car][vehicle_color][1] = color2;
	Vehicle[car][vehicle_color][2] = 0;
			
	Vehicle[car][vehicle_fuel] = 60.0;
	Vehicle[car][vehicle_engine] = 100.0;
			
	Vehicle[car][vehicle_state_window][0] = 
	Vehicle[car][vehicle_state_window][1] =
	Vehicle[car][vehicle_state_window][2] =
	Vehicle[car][vehicle_state_window][3] = 1;
			
	Vehicle[car][vehicle_engine_date] = 
	Vehicle[car][vehicle_date] = gettime();
			
	CreateCar( car );
	SetVehicleParams( car );
			
	SetVehicleNumberPlate( car, "SIN PATENTE" );
    
	
	format:g_small_string( ""ADMIN_PREFIX" %s [%d] ha creado un vehículo de facción %s[%d].",
	    GetAccountName( playerid ),
	    playerid,
	    GetVehicleModelName( model ),
	    car
	);
	
	SendAdmin:( C_GRAY, g_small_string );
	
	format:g_small_string( ""gbSuccess"Has creado con éxito un vehículo - ID: "cBLUE"%d"cWHITE".",
	    car
	);
	
	SendClient:( playerid, C_WHITE, g_small_string );
	
	clean_array();

    #undef model
    #undef color1
    #undef color2
	return 1;
}


/*car = CreateVehicle( Salon[shop][page][s_model], car_buy_pos[shop][pos][0], car_buy_pos[shop][pos][1], car_buy_pos[shop][pos][2], car_buy_pos[shop][pos][3], listitem, listitem, 99999 );
			
			ClearVehicleData( car );
			
			Vehicle[car][vehicle_user_id] = Player[playerid][uID];
			Vehicle[car][vehicle_model] = Salon[shop][page][s_model];
			Vehicle[car][vehicle_member] = 
			Vehicle[car][vehicle_crime] = 0;
			
			Vehicle[car][vehicle_pos][0] = car_buy_pos[shop][pos][0];
			Vehicle[car][vehicle_pos][1] = car_buy_pos[shop][pos][1];
			Vehicle[car][vehicle_pos][2] = car_buy_pos[shop][pos][2];
			Vehicle[car][vehicle_pos][3] = car_buy_pos[shop][pos][3];
			
			Vehicle[car][vehicle_color][0] = listitem;
			Vehicle[car][vehicle_color][1] = listitem;
			Vehicle[car][vehicle_color][2] = 0;
			
			Vehicle[car][vehicle_fuel] = VehicleInfo[index][v_fuel] / 100.0 * 20.0;
			Vehicle[car][vehicle_engine] = 100.0;
			
			Vehicle[car][vehicle_state_window][0] = 
			Vehicle[car][vehicle_state_window][1] =
			Vehicle[car][vehicle_state_window][2] =
			Vehicle[car][vehicle_state_window][3] = 1;
			
			Vehicle[car][vehicle_engine_date] = 
			Vehicle[car][vehicle_date] = gettime();
			
			CreateCar( car );
			SetVehicleParams( car );
			
			SetVehicleNumberPlate( car, "SIN PATENTE" );*/