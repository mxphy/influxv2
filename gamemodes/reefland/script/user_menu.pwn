/*
0 - Pantalla del veloc�metro
1 - Configuraci�n de Spawn
2 - Visualizaci�n de aires
3 - Pantalla de apodo
4 - Mostrar anuncios
5 - Acciones de administraci�n
6 - marcha
7 - Estilo de conversaci�n
8 - Visualizaci�n de la radio.
9 - Ocultar el logo del servidor
10 - Muestra las desconexiones del jugador
*/

stock ShowSettings( playerid ) 
{
	new 
		spawn	[ 32 ],
		walk	[ 32 ],
		talk	[ 32 ],
		style	[ 32 ];

	switch( Player[playerid][uSettings][1] )
	{
	    case 0: spawn = ""cBLUE"Hotel";
	    case 1: spawn = ""cBLUE"Faccion";
	    case 2: spawn = ""cBLUE"Casa";
		case 3: spawn = ""cBLUE"Mismo lugar";
	}
	
	switch( Player[playerid][uSettings][6] ) 
	{
		case 0: walk = ""cBLUE"Por defecto";
		case 1: walk = ""cBLUE"Ordinario";
		case 2: walk = ""cBLUE"Relajado";
		case 3: walk = ""cBLUE"Gangster";
		case 4: walk = ""cBLUE"Erguido";
		case 5: walk = ""cBLUE"Anciano";
		case 6: walk = ""cBLUE"Atl�tico";
		case 7: walk = ""cBLUE"Caminando";
		case 8: walk = ""cBLUE"Mujer";
		case 9: walk = ""cBLUE"Podio";
		case 10: walk = ""cBLUE"Ritmico";
		case 11: walk = ""cBLUE"Borracho";
		case 12: walk = ""cBLUE"Con la mano";
	}
	
	switch( Player[playerid][uSettings][7] ) 
	{
		case 0: talk = ""cBLUE"Por defecto";
		case 1: talk = ""cBLUE"Desconfianza";
		case 2: talk = ""cBLUE"Apertura";
		case 3: talk = ""cBLUE"Desaprobaci�n";
		case 4: talk = ""cBLUE"Perseverancia";
		case 5: talk = ""cBLUE"Positividad";
		case 6: talk = ""cBLUE"Incertidumbre";
	}
	
	switch( Player[playerid][uChangeStyle] ) 
	{
		case 0: style = ""cBLUE"Por defecto"cWHITE"";
	    case 1: style = ""cBLUE"Incertidumbre"cWHITE"";
	    case 2: style = ""cBLUE"Kung Fu"cWHITE"";
	    case 3: style = ""cBLUE"Kneehead"cWHITE"";
	    case 4: style = ""cBLUE"Grabkick"cWHITE"";
	    case 5: style = ""cBLUE"Elbow"cWHITE"";
	}
	
	format:g_big_string(
    "\
		"cWHITE"Personalizaci�n\t"cWHITE"Valor\n\
		"cBLUE"-"cWHITE" Interfaz del veloc�metro\t%s\n\
		"cBLUE"-"cWHITE" Apodo\t%s\n\
		"cBLUE"-"cWHITE" Acciones administrativas\t%s\n\
		"cBLUE"-"cWHITE" Lugar de aparici�n\t%s\n\
		"cBLUE"-"cWHITE" Estilo de caminar\t%s\n\
		"cBLUE"-"cWHITE" Estilo de conversaci�n\t%s\n\
		"cBLUE"-"cWHITE" Estilo de pelea\t%s\n\
		"cBLUE"-"cWHITE" Visualizaci�n /b\t%s\n\
		"cBLUE"-"cWHITE" Visualizaci�n /f\t%s\n\
		"cBLUE"-"cWHITE" Visualizaci�n /pm\t%s\n\
		"cBLUE"-"cWHITE" Interfaz de radio\t%s\n\
		"cBLUE"-"cWHITE" Interfaz de servidor\t%s\n\
		"cBLUE"-"cWHITE" Mostrar anuncios\t%s\n\
		"cBLUE"-"cWHITE" Transmisiones\t%s\n\
		"cBLUE"-"cWHITE" Desc. de jugadores\t%s\
	",
		Player[playerid][uSettings][0] ? (""cBLUE"Si"cWHITE"") : (""cGRAY"No"cWHITE""),
		Player[playerid][uSettings][3] ? (""cBLUE"Si"cWHITE"") : (""cGRAY"No"cWHITE""),
		Player[playerid][uSettings][5] ? (""cBLUE"Si"cWHITE"") : (""cGRAY"No"cWHITE""),
		spawn,
		walk,
		talk,
		style,
		Player[playerid][uOOC] ? (""cBLUE"Si"cWHITE"") : (""cGRAY"No"cWHITE""),
		Player[playerid][uRO] ? (""cBLUE"Si"cWHITE"") : (""cGRAY"No"cWHITE""),
		Player[playerid][uPM] ? (""cBLUE"Si"cWHITE"") : (""cGRAY"No"cWHITE""),
		Player[playerid][uSettings][8] ? (""cBLUE"Si"cWHITE"") : (""cGRAY"No"cWHITE""),
		Player[playerid][uSettings][9] ? (""cBLUE"Si"cWHITE"") :(""cGRAY"No"cWHITE""),
		Player[playerid][uSettings][4] ? (""cBLUE"Si"cWHITE"") : (""cGRAY"No"cWHITE""),
		Player[playerid][uSettings][2] ? (""cBLUE"Si"cWHITE"") : (""cGRAY"No"cWHITE""),
		Player[playerid][uSettings][10] ? (""cBLUE"Si"cWHITE"") : (""cGRAY"No"cWHITE"")
	);
	
    showPlayerDialog( playerid, d_menu, DIALOG_STYLE_TABLIST_HEADERS, " ", g_big_string, "Cambiar", "Cerrar" );

    return 1;
}

stock ShowMenu( playerid ) 
{
	clean_array();
	
	new 
		Float:X, 
		Float:Y, 
		Float:Z, 
		Float:R, 
		Float:X2, 
		Float:Y2,
		day, month, year;
		
	if( GetPVarInt( playerid, "UserMenu:Use" ) ) 
		return 1;
		
	if( !IsPlayerInAnyVehicle( playerid ) ) 
	{
		GetPlayerPos( playerid, X, Y, Z );
		GetPlayerFacingAngle( playerid, R );
		X2 = X - 3.5 * floatsin( R, degrees );
		Y2 = Y + 3.5 * floatcos( R, degrees );
		InterpolateCameraPos( playerid, X, Y, Z, X2, Y2, Z, 600, 1000 );
		InterpolateCameraLookAt( playerid, X, Y, Z, X, Y, Z, 600, 1000 );
	}
	
	for( new i; i != 21; i++ ) TextDrawShowForPlayer( playerid, MenuPlayer[i] );
	for( new i; i != 3; i++ ) PlayerTextDrawShow( playerid, menuPlayer[i][playerid] );
	
	format:g_small_string(  "%d", Player[playerid][uLevel] ), strcat( g_big_string, g_small_string );
	
	format:g_small_string(  "~n~%s", getPlayerRank( playerid ) ), strcat( g_big_string, g_small_string );
	
	format:g_small_string(  "~n~$%09d", Player[playerid][uMoney] ), strcat( g_big_string, g_small_string );
	
	if( Player[playerid][uMember] )
	{
		format:g_small_string(  "~n~%s", Fraction[ Player[playerid][uMember] - 1 ][f_short_name] ), strcat( g_big_string, g_small_string );
	}
	else if( Player[playerid][uJob] )
	{
		switch( Player[playerid][uJob] )
		{
			case 1, 2: strcat( g_big_string, "~n~Compa�ia de pasajeros" );
			case 3: strcat( g_big_string, "~n~Empresa de transporte" );
			case 4: strcat( g_big_string, "~n~CTO" );
		}
	}
	else
	{
		strcat( g_big_string, "~n~No"  );
	}
	
	if( Player[playerid][uRank] && Player[playerid][uMember] )
	{
		new
			rank = getRankId( playerid, Player[playerid][uMember] - 1 );
			
		format:g_small_string(  "~n~%s", FRank[ Player[playerid][uMember] - 1 ][rank][r_name] ), strcat( g_big_string, g_small_string );
	}
	else if( Player[playerid][uJob] )
	{
		switch( Player[playerid][uJob] )
		{
			case 1:	strcat( g_big_string, "~n~Conductor de taxi" );
			case 2:	strcat( g_big_string, "~n~Conductor de bus" );
			case 3:	strcat( g_big_string, "~n~Conductor" );
			case 4: strcat( g_big_string, "~n~Mec�nica" );
		}
	}
	else
	{
		strcat( g_big_string, "~n~No"  );
	}
	
	if( IsOwnerHouseCount( playerid ) )
	{
		for( new i; i < MAX_PLAYER_HOUSE; i++ )
		{
			if( Player[playerid][tHouse][i] != INVALID_PARAM )
			{
				if( !i )
				{
					format:g_small_string( "~n~%d", HouseInfo[ Player[playerid][tHouse][i] ][hID] );
					strcat( g_big_string, g_small_string );
				}
				else
				{
					format:g_small_string( ", %d", HouseInfo[ Player[playerid][tHouse][i] ][hID] );
					strcat( g_big_string, g_small_string );
				}
			}
		}
	}
	else
	{
		strcat( g_big_string, "~n~No" );
	}

	if( IsOwnerBusinessCount( playerid ) )
	{
		for( new i; i < MAX_PLAYER_BUSINESS; i++ )
		{
			if( Player[playerid][tBusiness][i] != INVALID_PARAM )
			{
				if( !i )
				{
					format:g_small_string( "~n~%d", BusinessInfo[ Player[playerid][tBusiness][i] ][b_id] );
					strcat( g_big_string, g_small_string );
				}
				else
				{
					format:g_small_string( ", %d", BusinessInfo[ Player[playerid][tBusiness][i] ][b_id] );
					strcat( g_big_string, g_small_string );
				}
			}
		}
	}
	else
	{
		strcat( g_big_string, "~n~No" );
	}
	
	if( !GetPhoneNumber( playerid ) )
	{
		strcat( g_big_string, "~n~No" );
	}
	else
	{
		format:g_small_string(  "~n~%d", GetPhoneNumber( playerid ) ), strcat( g_big_string, g_small_string );
	}
	
	if( !Player[playerid][uWarn] )
	{
		strcat( g_big_string, "~n~No" );
	}
	else
	{
		format:g_small_string(  "~n~%d", Player[playerid][uWarn] ), strcat( g_big_string, g_small_string );
	}
	
	gmtime( Player[playerid][uRegDate], year, month, day );
	
	format:g_small_string(  "~n~%02d.%02d.%d", day, month, year ), strcat( g_big_string, g_small_string );
	format:g_small_string(  "~n~%d min.", 60 - Player[playerid][uPayTime] ), strcat( g_big_string, g_small_string );
	format:g_small_string(  "~n~%d", Player[playerid][uGMoney] ), strcat( g_big_string, g_small_string );
	
	if( !Premium[playerid][prem_id] )
	{
		strcat( g_big_string, "~n~No" );
	}
	else
	{
		strcat( g_big_string, "~n~" );
		strcat( g_big_string, GetPremiumName( Premium[playerid][prem_type] ) );
		
		gmtime( Premium[playerid][prem_time], _, month, day );
		format:g_small_string(  " (Hasta %02d.%02d)", day, month ), strcat( g_big_string, g_small_string );
	}
	
	PlayerTextDrawSetString( playerid, menuPlayer[1][playerid], g_big_string );
	PlayerTextDrawSetString( playerid, menuPlayer[0][playerid], 
		"Nivel:~n~\
		Estado:~n~\
		Efectivo:~n~\
		Organizacion:~n~\
		Posicion:~n~\
		Casa:~n~\
		Negocios:~n~\
		Telefono:~n~\
		Advertencias:~n~\
		Registro:~n~\
		Payday:~n~\
		ICoins:~n~\
		Premium:~n~" );
	PlayerTextDrawSetString( playerid, menuPlayer[2][playerid], Player[playerid][uName] );
	
	SetPVarInt( playerid, "UserMenu:Use", 1 );
	SelectTextDraw( playerid, 0xd3d3d3FF );
	
	return 1;
}


CheckInventoryIncluded( playerid )
{
	new 
		i = 0,
		Float: x,
		Float: y,
		Float: z;
	
	/*if( GetPlayerVirtualWorld( playerid ) != 0 )
		goto SKIP_CHECK_VEHICHLES;*/
		
	for( i = 0; i < MAX_VEHICLES; i++ )
	{
		if( !IsVehicleStreamedIn( i, playerid ) ||
			IsPlayerInAnyVehicle( playerid ) || 
			!GetVehicleBag( GetVehicleModel( i ) ) || 
			Vehicle[i][vehicle_use_boot] || 
			!VehicleInfo[GetVehicleModel( i ) - 400][v_boot] || 
			!Vehicle[i][vehicle_state_boot] )
			continue;
		
		GetPosVehicleBoot( i, x, y, z );
		
		if( IsPlayerInRangeOfPoint( playerid, 1.5, x, y, z ) )
		{
			if( Vehicle[i][vehicle_member] )
			{
				if( !Player[playerid][uRank] || Vehicle[i][vehicle_member] != Player[playerid][uMember] ) 
					return SendClient:( playerid, C_WHITE, !""gbError"No puedes usar el maletero de este coche." );
					
				new
					rank = getRankId( playerid, Vehicle[i][vehicle_member] - 1 );
					
				if( !FRank[ Vehicle[i][vehicle_member] - 1 ][rank][r_boot] ) 
					return SendClient:( playerid, C_WHITE, !""gbError"No puedes usar el maletero de este coche." );
			}
		
			SetPVarInt( playerid, "Inv:CarId", i );
			
			format:g_small_string( "maletero%s abierto.", SexTextEnd( playerid ) );
			SendRolePlayAction( playerid, g_small_string, RP_TYPE_AME );
			
			format:g_string( "Veh�culo %s %d", GetVehicleModelName( GetVehicleModel(i) ), Vehicle[i][vehicle_id] );
			ShowInventoryAdditional( playerid, g_string, true, GetVehicleBag( GetVehicleModel(i) ) );
			
			Vehicle[i][vehicle_use_boot] = true;
			return STATUS_OK;
		}
	}
	
			
	return STATUS_OK;
}

UserMenu_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] )
{
	switch( dialogid )
	{
		case d_a_menu: 
		{
		    if( !response ) 
				return 1;
				
		    switch( listitem ) 
			{
		        case 0 : ShowMenu( playerid );
				case 1 : 
				{
					showInventory( playerid, true );
					CheckInventoryIncluded( playerid );
				}
				case 2 : 
				{
		        	if( GetPVarInt( playerid, "Player:Cuff" ) ) 
						return SendClient:( playerid, C_WHITE, ""gbError"No se puede usar el tel�fono con las esposas!" );	
						
					if( GetPVarInt( playerid, "Phone:Incoming" ) )
						return SendClient:( playerid, C_WHITE, ""gbError"Primero completa la llamada entrante!" );
						
		        	ShowPhone( playerid, true );
		        }
		        case 3 : cmd_gps( playerid );
				case 4 : cmd_favanim( playerid );
				case 5 : cmd_pauto( playerid );
		    }
		}
		
		case d_menu: 
		{
		    if( !response ) 
				return 1;
				
		    switch(listitem) {
				case 0: 
				{
					if( !Player[playerid][uSettings][0] ) 
					{
						Player[playerid][uSettings][0] = 1;
					}
					else
					{
						Player[playerid][uSettings][0] = 0;
					}
				}
				case 1: 
				{
					if( !Player[playerid][uSettings][3] ) 
					{
						Player[playerid][uSettings][3] = 1;
						
						foreach(new i : Player) 
							ShowPlayerNameTagForPlayer( playerid, i, true );
					}
					else 
					{
						Player[playerid][uSettings][3] = 0;
						
						foreach(new i : Player)
							ShowPlayerNameTagForPlayer( playerid, i, false );
					}
				}
				
				case 2: 
				{
					if( !Player[playerid][uSettings][5] ) 
					{
						Player[playerid][uSettings][5] = 1;
					}
					else
					{
						Player[playerid][uSettings][5] = 0;
					}
				}
				
				case 3: 
					return showPlayerDialog( playerid, d_menu + 1, DIALOG_STYLE_LIST, "Lugar de Spawn", "\
						"cBLUE"- "cWHITE"Hotel\n\
						"cBLUE"- "cWHITE"Facci�n\n\
						"cBLUE"- "cWHITE"Casa\n\
						"cBLUE"- "cWHITE"Lugar de desc.", "Cambiar", "Cerrar" );
				
				case 4: 
					return showPlayerDialog( playerid, d_menu + 2, DIALOG_STYLE_LIST, "Estilo de caminar", "\
						"cBLUE" - "cWHITE" Por defecto \n \
						"cBLUE" - "cWHITE" Normal \n \
						"cBLUE" - "cWHITE" Relajado \n \
						"cBLUE" - "cWHITE" Gangster \n \
						"cBLUE" - "cWHITE" Encorvado \n \
						"cBLUE" - "cWHITE" Anciano \n \
						"cBLUE" - "cWHITE" Atl�tico \n \
						"cBLUE" - "cWHITE" Caminando \n \
						"cBLUE" - "cWHITE" Mujer \n \
						"cBLUE" - "cWHITE" Podio  \n \
						"cBLUE" - "cWHITE" R�tmico \n \
						"cBLUE" - "cWHITE" Borracho \n \
						"cBLUE" - "cWHITE" Con una mano ","Seleccionar","Atras");
				
				case 5: 
					return showPlayerDialog( playerid, d_menu + 3, DIALOG_STYLE_LIST, "Estilo de conversaci�n", "\
						"cBLUE" - "cWHITE" Por defecto \n \
						"cBLUE" - "cWHITE" Desconfianza \n \
						"cBLUE" - "cWHITE" Apertura  \n \
						"cBLUE" - "cWHITE" Desaprobaci�n \n \
						"cBLUE" - "cWHITE" Perseverancia \n \
						"cBLUE" - "cWHITE" Positividad \n \
						"cBLUE" - "cWHITE" Incertidumbre ","Seleccionar","Atras");
				
				case 6: 
					return showPlayerDialog( playerid, d_menu + 5, DIALOG_STYLE_LIST, "Estilo de pelea", "\
						"cBLUE"- "cWHITE"Por defecto\n\
						"cBLUE"- "cWHITE"Boxeo\n\
						"cBLUE"- "cWHITE"Kung fu \n \
						"cBLUE"- "cWHITE"Kneehead \n \
						"cBLUE"- "cWHITE"Grabkick \n \
						"cBLUE"- "cWHITE"Elbow "," Seleccionar "," Atr�s ");
				
				case 7:
				{
					if( !Player[playerid][uOOC] )
					{
						Player[playerid][uOOC] = 1;
						UpdatePlayer( playerid, "uOOC", 1 );
					}
					else
					{
						Player[playerid][uOOC] = 0;
						UpdatePlayer( playerid, "uOOC", 0 );
					}
				}
				case 8:
				{
				    if( !Player[playerid][uRO] )
					{			   
						Player[playerid][uRO] = 1;
						UpdatePlayer( playerid, "uRO", 1 );
					}
					else
					{
						Player[playerid][uRO] = 0;
						UpdatePlayer( playerid, "uRO", 0 );
					}
				}
				case 9:
				{
				    if( !Player[playerid][uPM] )
					{
						Player[playerid][uPM] = 1;
						UpdatePlayer( playerid, "uPM", 1 );
					}
					else
					{
						Player[playerid][uPM] = 0;
						UpdatePlayer( playerid, "uPM", 0 );
					}
				}
				case 10:
				{
	    			if( !Player[playerid][uSettings][8] )
					{
					    
						Player[playerid][uSettings][8] = 1;
						ShowRadioInfo( playerid, true );
					}
					else
					{
						Player[playerid][uSettings][8] = 0;
						ShowRadioInfo( playerid, false );
					}
				}
				
				case 11 :
				{
	    			if( !Player[playerid][uSettings][9] )
					{
						Player[playerid][uSettings][9] = 1;
						ShowServerLogo( playerid, true );
					}
					else
					{
						Player[playerid][uSettings][9] = 0;
						ShowServerLogo( playerid, false );
					}
				}
				
				case 12 :
				{
					if( !Player[playerid][uSettings][4] )
					{
						Player[playerid][uSettings][4] = 1;
					}
					else
					{
						Player[playerid][uSettings][4] = 0;
					}
				}
				
				case 13 :
				{
					if( !Player[playerid][uSettings][2] )
					{
						Player[playerid][uSettings][2] = 1;
					}
					else
					{
						Player[playerid][uSettings][2] = 0;
					}
				}
				
				case 14 :
				{
					if( !Player[playerid][uSettings][10] )
					{
						Player[playerid][uSettings][10] = 1;
					}
					else
					{
						Player[playerid][uSettings][10] = 0;
					}
				}
				


			}
			
			ShowSettings( playerid );
			SavePlayerSettings( playerid );
		}
		
		case d_menu + 4:
		{
			if( !response ) return 1;
			
			if( inputtext[0] == EOS || strval( inputtext ) > 128 )
			{
				return showPlayerDialog( playerid, d_menu + 4, DIALOG_STYLE_INPUT, " ","\
					"cWHITE"Ingrese su mensaje a continuaci�n:\n\n\
					"gbDialog"El mensaje se env�a a los administradores y soporte.\n\
					"gbDialog"Si te quejas de un jugador en particular, entonces indica su ID.\n\
					"gbDialog"Si envias mensajes fuera de tema, podr�s obtener un bloqueo de chat OOC.\n\n\
					"gbDialogError"Formato incorrecto.",
				"Enviar", "Cancelar" );
			}
			
			switch( Premium[playerid][prem_color] )
			{
				case 0:
				{
					format:g_small_string( ""SUPPORT_PREFIX" %s[%d]: %s",
						GetAccountName( playerid ),
						playerid,
						inputtext
					);
					SendSupport:( C_YELLOW, g_small_string, false );
					
					format:g_small_string( ""ADMIN_PREFIX" %s[%d]: %s",
						GetAccountName(playerid),
						playerid,
						inputtext
					);
					SendAdmin:( C_YELLOW, g_small_string );
				}
				
				case 1:
				{
					format:g_small_string( ""SUPPORT_PREFIX" %s[%d]: %s",
						GetAccountName( playerid ),
						playerid,
						inputtext
					);
					SendSupport:( C_LIGHTGREEN, g_small_string, false );
					
					format:g_small_string( ""ADMIN_PREFIX" %s[%d]: %s",
						GetAccountName(playerid),
						playerid,
						inputtext
					);
					SendAdmin:( C_LIGHTGREEN, g_small_string );
				}
				
				case 2:
				{
					format:g_small_string( ""SUPPORT_PREFIX" %s[%d]: %s",
						GetAccountName( playerid ),
						playerid,
						inputtext
					);
					SendSupport:( C_LIGHTORANGE, g_small_string, false );
					
					format:g_small_string( ""ADMIN_PREFIX" %s[%d]: %s",
						GetAccountName(playerid),
						playerid,
						inputtext
					);
					SendAdmin:( C_LIGHTORANGE, g_small_string );
				}
				
				case 3:
				{
					format:g_small_string( ""SUPPORT_PREFIX" %s[%d]: %s",
						GetAccountName( playerid ),
						playerid,
						inputtext
					);
					SendSupport:( C_LIGHTRED, g_small_string, false );
					
					format:g_small_string( ""ADMIN_PREFIX" %s[%d]: %s",
						GetAccountName(playerid),
						playerid,
						inputtext
					);
					SendAdmin:( C_LIGHTRED, g_small_string );
				}
			}
			
			pformat:( ""gbDefault"Texto: "cBLUE"%s"cWHITE"", inputtext );
			psend:( playerid, C_WHITE );
			
			SendClient:( playerid, C_WHITE, !""gbDefault"Usted ha enviado con �xito un mensaje al soporte y los administradores.");
	
			SetPVarInt( playerid, "Player:ReportTime", GetTickCount() + 20000 );
		}
		
		
		//Configuraci�n de estilo de batalla
		case d_menu + 5: 
		{
		    if( !response ) 
			{
				ShowSettings( playerid );
				return 1;
			}
			
			if( listitem && !Player[playerid][uStyle][listitem - 1] )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Este estilo de lucha no ha sido estudiado." );
				return showPlayerDialog( playerid, d_menu + 5, DIALOG_STYLE_LIST, "Estilo de pelea", "\
						"cBLUE"- "cWHITE"Por defecto\n\
						"cBLUE"- "cWHITE"Boxeo\n\
						"cBLUE"- "cWHITE"Kung fu \n \
						"cBLUE"- "cWHITE"Kneehead \n \
						"cBLUE"- "cWHITE"Grabkick \n \
						"cBLUE"- "cWHITE"Elbow "," Seleccionar "," Atr�s ");
			}
			
			Player[playerid][uChangeStyle] = listitem;
			SendClient:( playerid, C_WHITE, ""gbSuccess"El estilo de lucha ha cambiado." );	
			
			UpdatePlayer( playerid, "uChangeStyle", listitem );
			
			SetPlayerFightingStyleEx( playerid );
			ShowSettings( playerid );
		}
		//Configuraci�n SPAWN
		case d_menu + 1 : 
		{
		    if( !response ) 
			{
				ShowSettings( playerid );
				return 1;
			}
			
			if( Player[playerid][uSettings][1] == listitem )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Ya has configurado este lugar de spawn." );
				return showPlayerDialog( playerid, d_menu + 1, DIALOG_STYLE_LIST, " ", "\
					"cBLUE"- "cWHITE"Hotel\n\
					"cBLUE"- "cWHITE"Facci�n\n\
					"cBLUE"- "cWHITE"Casa\n\
					"cBLUE"- "cWHITE"Lugar de desc.", "Cambiar", "Atras" );
			}
			
      		switch( listitem ) 
			{
				case 1:
				{
					if( !Player[playerid][uMember] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"Usted no es un miembro de facci�n." );
						return showPlayerDialog( playerid, d_menu + 1, DIALOG_STYLE_LIST, "Lugar de spawn", "\
							"cBLUE"- "cWHITE"Hotel\n\
							"cBLUE"- "cWHITE"Facci�n\n\
							"cBLUE"- "cWHITE"Casa\n\
							"cBLUE"- "cWHITE"Lugar de desc.", "Cambiar", "Atras" );
					}
					
					if( !Player[playerid][uRank] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"No tienes rango." );
						return showPlayerDialog( playerid, d_menu + 1, DIALOG_STYLE_LIST, "Lugar de spawn", "\
							"cBLUE"- "cWHITE"Hotel\n\
							"cBLUE"- "cWHITE"Facci�n\n\
							"cBLUE"- "cWHITE"Casa\n\
							"cBLUE"- "cWHITE"Lugar de desc.", "Cambiar", "Atras" );
					}
					
					if( FRank[ Player[playerid][uMember] - 1 ][ getRankId( playerid, Player[playerid][uMember] - 1 ) ][r_spawn][0] == 0.0 )
					{
						SendClient:( playerid, C_WHITE, !""gbError"Tu rango es insuficiente." );
						return showPlayerDialog( playerid, d_menu + 1, DIALOG_STYLE_LIST, "Lugar de spawn", "\
							"cBLUE"- "cWHITE"Hotel\n\
							"cBLUE"- "cWHITE"Facci�n\n\
							"cBLUE"- "cWHITE"Casa\n\
							"cBLUE"- "cWHITE"Lugar de desc.", "Cambiar", "Atras" );
					}
				}
				
		        case 2: 
				{
					if( Player[playerid][tHouse][0] == INVALID_PARAM && !Player[playerid][uHouseEvict] ) 
					{
						SendClient:( playerid, C_WHITE, !""gbError"Usted no tiene casa." );
						return showPlayerDialog( playerid, d_menu + 1, DIALOG_STYLE_LIST, "Lugar de spawn", "\
							"cBLUE"- "cWHITE"Hotel\n\
							"cBLUE"- "cWHITE"Facci�n\n\
							"cBLUE"- "cWHITE"Casa\n\
							"cBLUE"- "cWHITE"Lugar de desc.", "Cambiar", "Atras" );
					}
				}
			}
			
			Player[playerid][uSettings][1] = listitem;
			SendClient:( playerid, C_WHITE, !""gbDefault"Has cambiado el lugar de spawn de tu personaje.");
			
			SavePlayerSettings( playerid );
			ShowSettings(playerid);
		}
		case d_menu + 10:
		{
		    if( !response ) 
				return 1;
		}
		case d_menu + 2:
		{
		    if( !response ) 
			{
				ShowSettings(playerid);
				return 1;
			}
			
			if( Player[playerid][uSettings][6] == listitem )
			{
				SendClient:( playerid, C_WHITE, !""gbDefault"Ya tienes este estilo de caminar." );
				return showPlayerDialog( playerid, d_menu + 2, DIALOG_STYLE_LIST, "Estilos de caminar", "\
					"cBLUE"- "cWHITE"Por defecto\n\
					"cBLUE"- "cWHITE"Ordinario\n\
					"cBLUE"- "cWHITE"Relajado\n\
					"cBLUE"- "cWHITE"G�ngster\n\
					"cBLUE"- "cWHITE"Encorvado\n\
					"cBLUE"- "cWHITE"Anciano\n\
					"cBLUE"- "cWHITE"Atl�tico\n\
					"cBLUE"- "cWHITE"Caminando\n\
					"cBLUE"- "cWHITE"Mujer\n\
					"cBLUE"- "cWHITE"Podio\n\
					"cBLUE"- "cWHITE"Ritmico\n\
					"cBLUE"- "cWHITE"Borracho\n\
					"cBLUE"- "cWHITE"Con la mano", "Cambiar", "Atras" );
			}
			
		    Player[playerid][uSettings][6] = listitem;
		    SendClient:( playerid, C_WHITE, !""gbDefault"Has cambiado tu estilo de caminar." );

			SavePlayerSettings( playerid );
			ShowSettings( playerid );
		}
		
		case d_menu + 3: 
		{
			if( !response ) 
			{
				ShowSettings(playerid);
				return 1;
			}
			
			if( Player[playerid][uSettings][7] == listitem )
			{
				SendClient:( playerid, C_WHITE, !""gbDefault"Ya tienes este estilo de conversaci�n." );
				return showPlayerDialog( playerid, d_menu + 3, DIALOG_STYLE_LIST, "Estilo de conversaci�n", "\
					"cBLUE"- "cWHITE"Por defecto\n\
					"cBLUE"- "cWHITE"Desconfianza\n\
					"cBLUE"- "cWHITE"Apertura\n\
					"cBLUE"- "cWHITE"Desaprobaci�n\n\
					"cBLUE"- "cWHITE"Perseverancia\n\
					"cBLUE"- "cWHITE"Positividad\n\
					"cBLUE"- "cWHITE"Incertidumbre", "Cambiar", "Atras" );
			}
			
		    Player[playerid][uSettings][7] = listitem;
		    SendClientMessage( playerid, C_WHITE, !""gbDefault"Has cambiado el estilo de conversaci�n." );
			
			SavePlayerSettings( playerid );
			ShowSettings(playerid);
		}
	}
	
	return 1;
}

function UserMenu_OnPlayerKeyStateChange( playerid, newkeys, oldkeys )
{
	if( PRESSED( KEY_YES ) )
	{
		if( GetPVarInt( playerid, "PlayerMenuShow" ) == 5 )
		{
			return 1;
		}
		
		if( !GetPVarInt( playerid, "Phone:Call" ) ) //Si no hablas por telefono
			ShowDialogMenu( playerid );
	}
	return 1;
}

function UserMenu_OnPlayerClickTextDraw( playerid, Text: clickedid )
{
	if( _:clickedid == INVALID_TEXT_DRAW ) 
	{
		if( GetPVarInt( playerid, "UserMenu:Use" ) )
		{
			for( new i; i != 21; i++ ) 
			{
				TextDrawHideForPlayer(playerid, MenuPlayer[i]);
			}
			
			for( new i; i != 3; i++ )
			{
				PlayerTextDrawHide(playerid, menuPlayer[i][playerid]);
			}
			
			SetPVarInt( playerid, "UserMenu:Use", 0 );
			CancelSelectTextDraw( playerid );
			SetCameraBehindPlayer( playerid );
			return 1;
		}
	}
	else if( clickedid == MenuPlayer[15] )
	{
        if( IsMuted( playerid, OOC ) )
			return SendClient:( playerid, C_WHITE, !CHAT_MUTE_OOC );
			
		if( GetPVarInt( playerid, "Player:ReportTime" ) > GetTickCount() )
			return SendClient:( playerid, C_WHITE, !""gbError"Puede utilizar la conexi�n con la administraci�n una vez cada 20 segundos." );
			
		showPlayerDialog( playerid, d_menu + 4, DIALOG_STYLE_INPUT, " ","\
			"cWHITE"Ingrese su mensaje a continuaci�n:\n\n\
			"gbDialog"El mensaje se env�a a los administradores y soporte.\n\
			"gbDialog"Si te quejas de un jugador en particular, entonces indica su ID.\n\
			"gbDialog"Para publicar mensajes fuera de tema, puede obtener un bloqueo de chat OOC.",
		"Enviar", "Cerrar" );
	}
    else if( clickedid == MenuPlayer[16] ) //Ayuda en el juego
	{
		cmd_ayuda( playerid );
		return 1;
	}
	else if( clickedid == MenuPlayer[17] ) //Ajustes
	{
		ShowSettings( playerid );
		return 1;
    }
	else if( clickedid == MenuPlayer[18] ) //Equipos
	{
		showPlayerDialog( playerid, d_help, DIALOG_STYLE_LIST, "Comandos del servidor", d_help_command, "Cambiar", "Cerrar" );
		return 1;
	}
	else if( clickedid == MenuPlayer[19] ) //Estadisticas
	{
		ShowStats( playerid, playerid );
		return 1;
	}
    else if( clickedid == MenuPlayer[20] ) //La tienda
	{
		cmd_donate( playerid );
		return 1;
	}
	
	return 0;
}

ShowDialogMenu( playerid )
{
	clean:<g_big_string>;
	
	if( GetPVarInt( playerid, "UserMenu:Use" ) ) 
		return 0;
		
	if( GetPVarInt( playerid, "HTx:Use" ) ) 
		return 0;
		
	strcat( g_big_string, ""cBLUE"-"cWHITE" Men� del juego\n" );
	strcat( g_big_string, ""cBLUE"-"cWHITE" Inventario\n" );
	strcat( g_big_string, ""cBLUE"-"cWHITE" Tel�fono celular\n" );
	strcat( g_big_string, ""cBLUE"-"cWHITE" Navegador GPS\n" );
	//strcat( g_big_string, ""cBLUE"-"cWHITE" Caracteristicas\n" );
	strcat( g_big_string, ""cBLUE"-"cWHITE" Animaciones favoritas" );
	
	if( IsPlayerInAnyVehicle( playerid ) )
	{
		strcat( g_big_string, "\n"cBLUE"-"cWHITE" Veh�culo" );
	}
	
	showPlayerDialog( playerid, d_a_menu, 2, " ", g_big_string, "Cambiar", "Cerrar" );
	 
	return 1;
}

SavePlayerSettings( playerid )
{
	format:g_small_string( "%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d",
		Player[playerid][uSettings][0],
		Player[playerid][uSettings][1],
		Player[playerid][uSettings][2],
		Player[playerid][uSettings][3],
		Player[playerid][uSettings][4],
		Player[playerid][uSettings][5],
		Player[playerid][uSettings][6],
		Player[playerid][uSettings][7], 
		Player[playerid][uSettings][8],
		Player[playerid][uSettings][9],
		Player[playerid][uSettings][10]
	);
	
	UpdatePlayerString( playerid, "uSettings", g_small_string );
}

stock ShowStats( playerid, giveplayerid ) 
{
	clean:<g_big_string>;
	
	new 
		bool: idx,
		year,
		month,
		day;
	
	format:g_small_string( ""gbDefault"Estad�sticas del jugador "cBLUE"%s[%d]"cWHITE".\n\n",
		Player[giveplayerid][uName],
		giveplayerid
	);
	strcat( g_big_string, g_small_string );
	
	strcat( g_big_string, ""cBLUE" - Personaje:"cWHITE"\n");
	
	format:g_small_string( "Dinero: "cGRAY"$%d"cWHITE"\n", Player[giveplayerid][uMoney]);
	strcat( g_big_string,g_small_string );
	
	format:g_small_string( "Banco: "cGRAY"$%d"cWHITE"\n", Player[giveplayerid][uBank]);
	strcat( g_big_string, g_small_string );
	
	format:g_small_string( "Sexo: "cGRAY"%s"cWHITE"\n", GetSexName( Player[giveplayerid][uSex] ) );
	strcat( g_big_string, g_small_string );
	
	format:g_small_string( "Edad: "cGRAY"%d %s"cWHITE"\n", Player[giveplayerid][uAge], AgeTextEnd( Player[giveplayerid][uAge]%10 ) );
	strcat( g_big_string, g_small_string );
	
	format:g_small_string( "Pa�s de nacimiento: "cGRAY"%s"cWHITE"\n", GetCountryName( Player[giveplayerid][uCountry] ) );
	strcat( g_big_string, g_small_string );
	
	format:g_small_string( "Nacionalidad: "cGRAY"%s"cWHITE"\n", GetNationName( Player[giveplayerid][uNation] ) );
	strcat( g_big_string, g_small_string );
	
	format:g_small_string( "Color de piel: "cGRAY"%s"cWHITE"\n", Player[giveplayerid][uColor] == 2 ? ("Blanca") : ("Oscura") );
	strcat( g_big_string, g_small_string );

	
	if( !GetPhoneNumber( playerid ) )
	{
		format:g_small_string( "Telefono: "cGRAY"No"cWHITE"\n" );
	}
	else
	{
		format:g_small_string( "Telefono: "cGRAY"%d"cWHITE"\n", GetPhoneNumber( playerid ) );
	}
	strcat( g_big_string, g_small_string );
	

	
	format:g_small_string( "Trabajo: "cGRAY"%s"cWHITE"\n", GetJobName( Player[giveplayerid][uJob] ) );
	strcat( g_big_string, g_small_string);
	
	if( Player[giveplayerid][uMember] )
	{
		new
			fid = Player[giveplayerid][uMember] - 1,
			rank;
	
		format:g_small_string( "Facci�n: "cGRAY"%s"cWHITE"\n", Fraction[ fid ][f_name] ), 
		strcat( g_big_string, g_small_string );
		
		format:g_small_string( "Lider: "cGRAY"%s"cWHITE"\n", !Player[giveplayerid][uLeader] ? ("No") : ("Si") );
		strcat( g_big_string, g_small_string );
		
		if( Player[giveplayerid][uRank] )
		{
			rank = getRankId( giveplayerid, fid );
			format:g_small_string(  "Rango: "cGRAY"%s"cWHITE"\n", FRank[ fid ][rank][r_name] ), 
			strcat( g_big_string, g_small_string );
		}
		else
		{
			strcat( g_big_string, "Rango: "cGRAY"No"cWHITE"\n" );
		}
	}
	else if( Player[giveplayerid][uCrimeM] )
	{
		new
			crime = getIndexCrimeFraction( Player[giveplayerid][uCrimeM] ),
			rank;
	
		format:g_small_string(  "Organizaci�n: "cGRAY"%s"cWHITE"\n", CrimeFraction[ crime ][c_name] ), 
		strcat( g_big_string, g_small_string );
		
		format:g_small_string( "Lider: "cGRAY"%s"cWHITE"\n", !Player[giveplayerid][uLeader] ? ("No") : ("Si") );
		strcat( g_big_string, g_small_string );
		
		if( Player[giveplayerid][uCrimeRank] )
		{
			rank = getCrimeRankId( giveplayerid, crime );
			format:g_small_string(  "Rango: "cGRAY"%s"cWHITE"\n", CrimeRank[ crime ][rank][r_name] ), 
			strcat( g_big_string, g_small_string );
		}
		else
		{
			strcat( g_big_string, "Rango: "cGRAY"No"cWHITE"\n" );
		}
	}
	else
	{
		strcat( g_big_string, "Organizaci�n: "cGRAY"No"cWHITE"\n" );
		strcat( g_big_string, "Lider: "cGRAY"No"cWHITE"\n" );
		strcat( g_big_string, "Rango: "cGRAY"No"cWHITE"\n" );
	}
	
	strcat( g_big_string, "\nVehiculos: " );
	
	for( new i; i < MAX_PLAYER_VEHICLES; i++ )
	{
		if( Player[giveplayerid][tVehicle][i] != INVALID_VEHICLE_ID )
		{
			new
				vehicleid = Player[giveplayerid][tVehicle][i];
		
			format:g_small_string(  "\n"cGRAY"%s [%d]"cWHITE"", GetVehicleModelName( GetVehicleModel( vehicleid ) ), vehicleid ), 
			strcat( g_big_string, g_small_string );
			
			idx = true;
		}
	}
	
	if( !idx ) strcat( g_big_string, "No" );
	idx = false;
	
	strcat( g_big_string, "\nCasas: " );
	
	for( new i; i < MAX_PLAYER_HOUSE; i++ )
	{
		if( Player[giveplayerid][tHouse][i] != INVALID_PARAM )
		{
			new
				house = Player[giveplayerid][tHouse][i];
		
			format:g_small_string(  "\n"cGRAY"%s [%d]"cWHITE"", !HouseInfo[house][hType] ? ("Casa") : ("Apartamento"), HouseInfo[house][hID] ), 
			strcat( g_big_string, g_small_string );
		
			idx = true;
		}
	}
	
	if( !idx ) strcat( g_big_string, "No" );
	idx = false;
	
	strcat( g_big_string, "\nNegocios: " );
	
	for( new i; i < MAX_PLAYER_BUSINESS; i++ )
	{
		if( Player[giveplayerid][tBusiness][i] != INVALID_PARAM )
		{
			new
				business = Player[giveplayerid][tBusiness][i];
		
			format:g_small_string(  "\n"cGRAY"%s [%d]"cWHITE"", GetBusinessType( business ), BusinessInfo[ business ][b_id] ), 
			strcat( g_big_string, g_small_string );
		
			idx = true;
		}
	}
	
	if( !idx ) strcat( g_big_string, "No" );
	
	strcat( g_big_string, "\n\n"cBLUE" - Cuenta:"cWHITE"\n");
	
	format:g_small_string( "Nivel: "cBLUE"%d"cWHITE"\n", Player[giveplayerid][uLevel] );
	strcat( g_big_string,g_small_string );
	
	format:g_small_string( "Estado: "cBLUE"%s"cWHITE"\n", getPlayerRank( giveplayerid ) );
	strcat( g_big_string,g_small_string );
	
	format:g_small_string( "Siguiente nivel: "cBLUE"%d/%d"cWHITE" exp.\n", Player[giveplayerid][uHours] - getLevelHours( giveplayerid ), ( Player[giveplayerid][uLevel] + 1 ) * 6 );
	strcat( g_big_string,g_small_string );
	
	format:g_small_string( "Total de horas en el juego: "cBLUE"%d"cWHITE"\n", Player[giveplayerid][uHours] );
	strcat( g_big_string,g_small_string );
	
	format:g_small_string( "Advertencias: "cBLUE"%d/3"cWHITE"\n", Player[giveplayerid][uWarn] );
	strcat( g_big_string,g_small_string );
	
	format:g_small_string( "Tiempo de pago: "cGRAY"%d"cWHITE" min.\n", 60 - Player[giveplayerid][uPayTime] );
	strcat( g_big_string, g_small_string );
	
	format:g_small_string( "ID de cuenta: "cGRAY"%d"cWHITE"\n", Player[giveplayerid][uID] );
	strcat( g_big_string, g_small_string );
	
	format:g_small_string( "Cuenta premium: %s%s[%d]"cWHITE"\n", GetPremiumColor( Premium[giveplayerid][prem_color] ), GetPremiumName( Premium[giveplayerid][prem_type] ), Premium[playerid][prem_id] );
	strcat( g_big_string, g_small_string );
	
	format:g_small_string( "ICoins: "cBLUE"%d"cWHITE"\n", Player[giveplayerid][uGMoney] );
	strcat( g_big_string, g_small_string );
	

	
	format:g_small_string( "IP actual: "cGRAY"%s"cWHITE"\n", Player[giveplayerid][tIP] );
	strcat( g_big_string, g_small_string );
	
	gmtime( Player[giveplayerid][uRegDate], year, month, day );
	format:g_small_string( "Fecha de registro: "cGRAY"%02d.%02d.%d"cWHITE"\n", day, month, year );
	strcat( g_big_string, g_small_string );
	
	if( Player[ giveplayerid ][ uDMJail ] != 0 )
	{
		format:g_small_string( "Tiempo de carcel: "cRED"%d"cWHITE" min.\n", Player[ giveplayerid ][ uDMJail ] );
		strcat( g_big_string, g_small_string );
	}
	
	else if( Player[ giveplayerid ][ uMute ] != 0 )
	{
		format:g_small_string( "Bloqueo de chat IC: "cRED"%d"cWHITE" min.\n", Player[ giveplayerid ][ uMute ] );
		strcat( g_big_string, g_small_string );
	}
	
	else if( Player[ giveplayerid ][ uBMute ] != 0 )
	{
		format:g_small_string( "Bloqueo de chat OOC: "cRED"%d"cWHITE" min.\n", Player[ giveplayerid ][ uBMute ] );
		strcat( g_big_string, g_small_string );
	}
	
	else if( Player[ giveplayerid ][ uWarn ] != 0 )
	{
		format:g_small_string( "Advertencias: "cRED"%d"cWHITE"\n", Player[ giveplayerid ][ uWarn ] );
		strcat( g_big_string, g_small_string );
	}
	
	showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Cerrar", "" );
	
	return 1;
}