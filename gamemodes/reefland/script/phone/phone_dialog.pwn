Phone_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] )
{
	//Di�logo si el tel�fono est� apagado.
	switch( dialogid )
	{
		case d_phone:
		{
			if( !response ) return 1;
			
			new
				phone = GetPVarInt( playerid, "Phone:Select" );
			
			Phone[playerid][phone][p_settings][0] = 1;
			UpdatePhoneSettings( Phone[playerid][phone][p_number], Phone[playerid][phone][p_settings] );
			
			ShowPhone( playerid, true );
		}
		
		//Di�logo de apagado
		case d_phone + 1:
		{
			if( !response ) return 1;
			
			new
				phone = GetPVarInt( playerid, "Phone:Select" );
			
			Phone[playerid][phone][p_settings][0] = 0;
			UpdatePhoneSettings( Phone[playerid][phone][p_number], Phone[playerid][phone][p_settings] );
			
			ShowPhone( playerid, false );
		}
		
		//List�n telef�nico
		case d_phone + 2:
		{
			if( !response ) return 1;
			
			new
				bool:flag = false;
			
			switch( listitem )
			{
				case 0:
				{
					for( new i; i < MAX_CONTACTS; i++ )
					{
						if( !PhoneContacts[playerid][i][p_id] )
						{
							flag = true;
							break;
						}
					}
					
					if( !flag )
					{
						SendClient:( playerid, C_WHITE, ""gbError"El tel�fono se ha quedado sin memoria!" );
						OpenPhoneBook( playerid );
						return 1;
					}
				
					showPlayerDialog( playerid, d_phone + 3, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"A�adir contacto\n\n\
						"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
						"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.",
					"Siguiente", "Atr�s" );
				}
				
				default:
				{
					SetPVarInt( playerid, "Phone:Contact", g_dialog_select[playerid][listitem - 1] );
					g_dialog_select[playerid][listitem - 1] = INVALID_PARAM;
				
					showPlayerDialog( playerid, d_phone + 5, DIALOG_STYLE_LIST, " ", "\
						"cWHITE"Llamar\n\
						"cWHITE"Escribe un mensaje\n\
						"cWHITE"Cambiar contacto\n\
						"cWHITE"Eliminar contacto",
					"Seleccionar", "Atr�s" );
				}
			}
		}
		
		//Ingrese el n�mero para agregar un contacto
		case d_phone + 3:
		{
			if( !response ) 
			{
				OpenPhoneBook( playerid );
				return 1;
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 )
			{
				return showPlayerDialog( playerid, d_phone + 3, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"A�adir contacto\n\n\
					"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
					"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\
					"gbDialogError"Formato de valor no v�lido, por favor vuelva a ingresar un n�mero.",
				"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) == GetPhoneNumber( playerid ) )
			{
				return showPlayerDialog( playerid, d_phone + 3, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"A�adir contacto\n\n\
					"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
					"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\
					"gbDialogError"No puede agregar un contacto con su n�mero.",
				"Siguiente", "Atr�s" );
			}
			
			SetPVarInt( playerid, "Phone:Number", strval( inputtext ) );
			
			showPlayerDialog( playerid, d_phone + 4, DIALOG_STYLE_INPUT, " ", "\
				"cBLUE"A�adir contacto\n\n\
				"cWHITE"Ingresa el nombre:",
			"Siguiente", "Atr�s" );
		}
		
		case d_phone + 4:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 3, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"A�adir contacto\n\n\
					"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
					"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.",
				"Siguiente", "Atr�s" );
			}
			
			if( inputtext[0] == EOS )
			{
				return showPlayerDialog( playerid, d_phone + 4, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"A�adir contacto\n\n\
					"cWHITE"Ingrese su nombre:\n\n\
					"gbDialogError"El campo de entrada est� vac�o.",
				"Siguiente", "Atr�s" );
			}
			
			if( strlen( inputtext ) > 25 )
			{
				return showPlayerDialog( playerid, d_phone + 4, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"A�adir contacto\n\n\
					"cWHITE"Ingrese su nombre:\n\n\
					"gbDialogError"N�mero excedido de caracteres.",
				"Siguiente", "Atr�s" );
			}
			
			CreateContact( playerid, inputtext );
			
			OpenPhoneBook( playerid );
		}
		
		case d_phone + 5:
		{
			if( !response ) 
			{
				OpenPhoneBook( playerid );
				return 1;
			}
			
			new
				contact = GetPVarInt( playerid, "Phone:Contact" );
			
			switch( listitem )
			{
				case 0:
				{
					if( !IsPlayerInNetwork( playerid ) )
					{
						SendClient:( playerid, C_WHITE, !NO_NETWORK );
						showPlayerDialog( playerid, d_phone + 5, DIALOG_STYLE_LIST, " ", "\
							"cWHITE"Llamar\n\
							"cWHITE"Escribe un mensaje\n\
							"cWHITE"Cambiar contacto\n\
							"cWHITE"Eliminar contacto",
						"Seleccionar", "Atr�s" );
						return 1;
					}
				
					if( !CheckNumberNetwork( playerid, PhoneContacts[playerid][contact][p_number] ) ) 
					{
						Call[playerid][c_call_id] = 0;
						showPlayerDialog( playerid, d_phone + 5, DIALOG_STYLE_LIST, " ", "\
							"cWHITE"Llamar\n\
							"cWHITE"Escribe un mensaje\n\
							"cWHITE"Cambiar contacto\n\
							"cWHITE"Eliminar contacto",
						"Seleccionar", "Atr�s" );
						return 1;
					}
					
					new
						callid = Call[playerid][c_call_id];
						
					if( GetPVarInt( callid, "Phone:Call" ) || GetPVarInt( callid, "San:Call" ) )
					{
						SendClient:( playerid, C_WHITE, ""gbPhone"El suscriptor est� ocupado." );
						
						Call[playerid][c_call_id] = 0;
						showPlayerDialog( playerid, d_phone + 5, DIALOG_STYLE_LIST, " ", "\
							"cWHITE"Llamar\n\
							"cWHITE"Escribe un mensaje\n\
							"cWHITE"Cambiar contacto\n\
							"cWHITE"Eliminar contacto",
						"Seleccionar", "Atr�s" );
						return 1;
					}					
					
					pformat:( ""gbPhone"Llamada saliente "cBLUE"%d "cWHITE"%s"cGRAY"...( "cBLUE"H"cGRAY" )", PhoneContacts[playerid][contact][p_number], PhoneContacts[playerid][contact][p_name] );
					psend:( playerid, C_WHITE );
						
					if( !CheckIncomingNumber( callid, GetPhoneNumber( playerid ) ) )
					{
						pformat:( ""gbPhone"Llamada entrante "cBLUE"%d"cGRAY"...( "cGREEN"Y"cGRAY" | "cRED"N"cGRAY" )", GetPhoneNumber( playerid ) );
						psend:( callid, C_WHITE );
					}
					else
					{
						pformat:( ""gbPhone"Llamada entrante "cWHITE"%s"cGRAY"...( "cGREEN"Y"cGRAY" | "cRED"N"cGRAY" )", Call[callid][c_name] );
						psend:( callid, C_WHITE );
					}
					
					PlayerPlaySound( playerid, 3600, 0.0, 0.0, 0.0 );
					PlayerPlaySound( callid, call_sound[Call[playerid][c_sound]][s_id], 0.0, 0.0, 0.0 );
										
					Call[callid][c_status] = true;
					Call[callid][c_call_id] = playerid;
					
					SetPVarInt( playerid, "Phone:Call", 1 );
					SetPVarInt( callid, "Phone:Call", 1 );
					
					PhoneStatus( playerid, true );
				}
				
				case 1:
				{
					if( !IsPlayerInNetwork( playerid ) )
					{
						SendClient:( playerid, C_WHITE, !NO_NETWORK );
						return showPlayerDialog( playerid, d_phone + 5, DIALOG_STYLE_LIST, " ", "\
							"cWHITE"Llamar\n\
							"cWHITE"Escribe un mensaje\n\
							"cWHITE"Cambiar contacto\n\
							"cWHITE"Eliminar contacto",
						"Seleccionar", "Atr�s" );
					}
				
					clean:<g_string>;
					mysql_format( mysql, g_string, sizeof g_string, "SELECT * FROM " #DB_PHONES " WHERE p_number = %d", PhoneContacts[playerid][contact][p_number] );
					mysql_tquery( mysql, g_string, "CheckNumber", "d", playerid );
				
					format:g_small_string( "\
						"cBLUE"Nuevo mensaje\n\n\
						"gbDialog"Destinatario "cWHITE"%s\n\n\
						"cWHITE"Ingrese su mensaje:",
						PhoneContacts[playerid][contact][p_name]
					);
					
					showPlayerDialog( playerid, d_phone + 20, DIALOG_STYLE_INPUT, " ", g_small_string, "Enviar", "Atr�s" );
				}
				
				case 2:
				{
					showPlayerDialog( playerid, d_phone + 6, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Cambiar contacto\n\n\
						"cWHITE"Ingrese un nuevo n�mero de tel�fono:\n\n\
						"gbDialog"Introduzca 0 si el n�mero permanece sin cambios.",
					"Siguiente", "Atr�s" );
				}
				
				case 3:
				{
					showPlayerDialog( playerid, d_phone + 8, DIALOG_STYLE_MSGBOX, " ", "\
						"cWHITE"�Est�s seguro de que quieres eliminar este contacto?",
					"Si", "No" );
				}
			}
		}
		
		case d_phone + 6:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 5, DIALOG_STYLE_LIST, " ", "\
					"cWHITE"Llamar\n\
					"cWHITE"Escribe un mensaje\n\
					"cWHITE"Cambiar contacto\n\
					"cWHITE"Eliminar contacto",
				"Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 0 )
			{
				return showPlayerDialog( playerid, d_phone + 6, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Cambiar contacto\n\n\
					"cWHITE"Ingrese un nuevo n�mero de tel�fono:\n\n\
					"gbDialog"Introduzca 0 si el n�mero permanece sin cambios.\n\n\
					"gbDialogError"Formato de valor no v�lido, por favor vuelva a ingresar un n�mero.",
				"Siguiente", "Atr�s" );
			}
			
			new
				contact = GetPVarInt( playerid, "Phone:Contact" );
			
			if(  strval( inputtext ) == 0 )
			{
				SetPVarInt( playerid, "Phone:Number", PhoneContacts[playerid][contact][p_number] );
			}
			else
				SetPVarInt( playerid, "Phone:Number", strval( inputtext ) );
			
			showPlayerDialog( playerid, d_phone + 7, DIALOG_STYLE_INPUT, " ", "\
				"cBLUE"Cambiar contacto\n\n\
				"cWHITE"Ingrese un nuevo nombre:\n\n\
				"gbDialog"Deje el campo en blanco si el nombre no cambia.",
			"Siguiente", "Atr�s" );
		}
		
		case d_phone + 7:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 6, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Cambiar contacto\n\n\
					"cWHITE"Ingrese un nuevo n�mero de tel�fono:\n\n\
					"gbDialog"Introduzca 0 si el n�mero permanece sin cambios.",
				"Siguiente", "Atr�s" );
			}
			
			if( strlen( inputtext ) > 25 )
			{
				return showPlayerDialog( playerid, d_phone + 7, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Cambiar contacto\n\n\
					"cWHITE"Ingrese un nuevo nombre:\n\n\
					"gbDialog"Deje el campo en blanco si el nombre no cambia.\n\n\
					"gbDialogError"N�mero excedido de caracteres.",
				"Siguiente", "Atr�s" );
			}
			
			new
				contact = GetPVarInt( playerid, "Phone:Contact" );
			
			if( inputtext[0] == EOS )
			{
				UpdateContact( playerid, contact, PhoneContacts[playerid][contact][p_name] );
			}
			else
				UpdateContact( playerid, contact, inputtext );
			
			DeletePVar( playerid, "Phone:Contact" );
			OpenPhoneBook( playerid );
		}
		
		//Borrando un contacto
		case d_phone + 8:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 5, DIALOG_STYLE_LIST, " ", "\
					"cWHITE"Llamar\n\
					"cWHITE"Escribe un mensaje\n\
					"cWHITE"Cambiar contacto\n\
					"cWHITE"Eliminar contacto",
				"Seleccionar", "Atr�s" );
			}
			
			DeleteContact( playerid, GetPVarInt( playerid, "Phone:Contact" ) );
			
			DeletePVar( playerid, "Phone:Contact" );
			
			OpenPhoneBook( playerid );
		}
		
		//Di�logo de configuraci�n
		case d_phone + 9:
		{
			if( !response ) return 1;
			
			switch( listitem )
			{
				case 0:
				{
					clean:<g_string>;
			
					for( new i; i < sizeof color_name; i++ )
					{
						format:g_small_string( ""cWHITE"%s\n", color_name[i] );
						strcat( g_string, g_small_string );
					}
					
					showPlayerDialog( playerid, d_phone + 10, DIALOG_STYLE_LIST, "Seleccionar el panel", g_string, "Instalar", "Atr�s" );
				}
				
				case 1:
				{
					ShowListSound( playerid );
				}
				
				case 2:
				{
					if( !IsPlayerInNetwork( playerid ) )
					{
						return showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", "\
							"cBLUE"Estado de la red\n\n\
							"cWHITE"Tarjeta SIM:  SA Telecom\n\n\
							"cGRAY"Sin red",
						"Cerrar", "" );
					}
					
					new
						network = GetNetwork( playerid ),
						Float:distance,
						Float:interval,
						percent;
						
					distance = GetPlayerDistanceFromPoint( playerid, action_network[network][0], action_network[network][1], action_network[network][2] );
					interval = distance/action_network[network][3];
					percent = 100 - floatround( interval * 100 );
					
					switch( percent )
					{
						case 0..15: format:g_small_string( ""cGRAY"Se�al: "cRED"Muy mala" );
				
						case 16..44: format:g_small_string( ""cGRAY"Se�al: "cRED"Mala" );
						
						case 45..84: format:g_small_string( ""cGRAY"Se�al: "cYELLOW"Buena" );
						
						case 85..100: format:g_small_string( ""cGRAY"Se�al: "cGREEN"Muy buena" );
					}
					
					clean:<g_string>;
					
					strcat( g_string, "\
						"cBLUE"Estado de la red\n\n\
						"cWHITE"Tarjeta SIM:  SA Telecom\n\n" );
					strcat( g_string, g_small_string );
					
					return showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_string, "Cerrar", "" );
				}
			}
		}
		
		case d_phone + 10:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 9, DIALOG_STYLE_LIST, "Ajustes", "\
					"cWHITE"Cambiar el color del panel\n\
					"cWHITE"Cambiar tono de llamada\n\
					"cWHITE"Verificar el estado de la red",
				"Seleccionar", "Cerrar" );
			}
			
			new
				phone = GetPVarInt( playerid, "Phone:Select" );
			
			switch( GetUsePhone( playerid ) )
			{
				case 18874:
				{
					TextDrawHideForPlayer( playerid, phoneFonOne[Phone[playerid][phone][p_settings][1]] );
					
					Phone[playerid][phone][p_settings][1] = listitem;
					UpdatePhoneSettings( Phone[playerid][phone][p_number], Phone[playerid][phone][p_settings] );
			
					TextDrawShowForPlayer( playerid, phoneFonOne[Phone[playerid][phone][p_settings][1]] );
				}
				
				case 18872:
				{
					TextDrawHideForPlayer( playerid, phoneFonThree[Phone[playerid][phone][p_settings][1]] );
					
					Phone[playerid][phone][p_settings][1] = listitem;
					UpdatePhoneSettings( Phone[playerid][phone][p_number], Phone[playerid][phone][p_settings] );
			
					TextDrawShowForPlayer( playerid, phoneFonThree[Phone[playerid][phone][p_settings][1]] );
				}
			}
			
			SendClient:( playerid, C_WHITE, ""gbSuccess"Se ha cambiado el color del panel de su tel�fono." );
			
			clean:<g_string>;
			
			for( new i; i < sizeof color_name; i++ )
			{
				format:g_small_string( ""cWHITE"%s\n", color_name[i] );
				strcat( g_string, g_small_string );
			}
			
			showPlayerDialog( playerid, d_phone + 10, DIALOG_STYLE_LIST, "Seleccionar el panel", g_string, "Instalar", "Atr�s" );
		}
		
		case d_phone + 11:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 9, DIALOG_STYLE_LIST, "Ajustes", "\
					"cWHITE"Cambiar el color del panel\n\
					"cWHITE"Cambiar tono de llamada\n\
					"cWHITE"Verificar el estado de la red",
				"Seleccionar", "Cerrar" );
			}
			
			SetPVarInt( playerid, "Phone:Sound", listitem );
			
			if( !listitem )
			{
				showPlayerDialog( playerid, d_phone + 24, DIALOG_STYLE_LIST, " ", "\
					"cWHITE"Instalar",
				"Seleccionar", "Atr�s" );
				return 1;
			}
			
			showPlayerDialog( playerid, d_phone + 12, DIALOG_STYLE_LIST, " ", "\
				"cWHITE"Escuchar\n\
				"cWHITE"Instalar",
			"Seleccionar", "Atr�s" );
		}
		
		case d_phone + 12:
		{
			if( !response )
			{	
				PlayerPlaySound( playerid, 0, 0.0, 0.0, 0.0 );
				ShowListSound( playerid );
				return 1;
			}
			
			new
				sound = GetPVarInt( playerid, "Phone:Sound" ),
				phone = GetPVarInt( playerid, "Phone:Select" );
			
			switch( listitem )
			{
				case 0:
				{
					PlayerPlaySound( playerid, call_sound[sound][s_id], 0.0, 0.0, 0.0 );
					
					return showPlayerDialog( playerid, d_phone + 12, DIALOG_STYLE_LIST, " ", "\
						"cWHITE"Escuchar\n\
						"cWHITE"Instalar",
					"Seleccionar", "Atr�s" );
				}
				
				case 1:
				{
					Phone[playerid][phone][p_settings][2] = sound;
					UpdatePhoneSettings( Phone[playerid][phone][p_number], Phone[playerid][phone][p_settings] );
				}
			}
			
			PlayerPlaySound( playerid, 0, 0.0, 0.0, 0.0 );
			SendClient:( playerid, C_WHITE, ""gbSuccess"Tono de llamada modificado" );
			DeletePVar( playerid, "Phone:Sound" ); 
			ShowListSound( playerid );
		}
		
		//Mensaje de di�logo
		case d_phone + 13:
		{
			if( !response ) return 1;
			
			switch( listitem )
			{
				case 0:
				{
					if( !IsPlayerInNetwork( playerid ) )
					{
						SendClient:( playerid, C_WHITE, !NO_NETWORK );
						
						return showPlayerDialog( playerid, d_phone + 13, DIALOG_STYLE_LIST, "Mensajes", "\
							"gbDialog"Escribe un mensaje\n\
							"cWHITE"Mensajes entrantes\n\
							"cWHITE"Mensajes salientes",
						"Seleccionar", "Cerrar" );
					}
					
					showPlayerDialog( playerid, d_phone + 14, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Nuevo mensaje\n\n\
						"cWHITE"Ingrese el n�mero de tel�fono del destinatario:\n\n\
						"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.",
					"Siguiente", "Atr�s" );
				}
				
				case 1:
				{
					ShowComingList( playerid );
				}
				
				case 2:
				{
					ShowIncomingList( playerid );
				}
			}
		}
		
		case d_phone + 14:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 13, DIALOG_STYLE_LIST, "Mensajes", "\
					"gbDialog"Escribe un mensaje\n\
					"cWHITE"Mensajes entrantes\n\
					"cWHITE"Mensajes salientes",
				"Seleccionar", "Cerrar" ); 
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 0 )
			{
				return showPlayerDialog( playerid, d_phone + 14, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Nuevo mensaje\n\n\
					"cWHITE"Ingrese el n�mero de tel�fono del destinatario:\n\n\
					"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\n\
					"gbDialogError"Formato de valor no v�lido, por favor vuelva a ingresar un n�mero.",
				"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) == GetPhoneNumber( playerid ) )
			{
				return showPlayerDialog( playerid, d_phone + 14, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Nuevo mensaje\n\n\
					"cWHITE"Ingrese el n�mero de tel�fono del destinatario:\n\n\
					"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\n\
					"gbDialogError"No puedes enviar un mensaje a ti mismo.",
				"Siguiente", "Atr�s" );
			}
			
			new
				bool:flag = false;
				
			for( new i; i < MAX_MESSAGES; i++ )
			{
				if( !Messages[playerid][i][m_id] )
				{
					flag = true;
					break;
				}
			}

			if( !flag )
			{
				SendClient:( playerid, C_WHITE, ""gbError"La memoria del tel�fono est� llena." );
				
				return showPlayerDialog( playerid, d_phone + 14, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Nuevo mensaje\n\n\
					"cWHITE"Ingrese el n�mero de tel�fono del destinatario:\n\n\
					"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\n\
					"gbDialogError"Formato de valor no v�lido, por favor vuelva a ingresar un n�mero.",
				"Siguiente", "Atr�s" );
			}
			
			clean:<g_string>;
			mysql_format( mysql, g_string, sizeof g_string, "SELECT * FROM " #DB_PHONES " WHERE p_number = %d", strval( inputtext ) );
			mysql_tquery( mysql, g_string, "CheckNumber", "d", playerid );
			
			format:g_small_string( "\
				"cBLUE"Nuevo mensaje\n\n\
				"gbDialog"Destinatario: "cWHITE"%d\n\n\
				"cWHITE"Ingrese su mensaje:",
				strval( inputtext ) 
			);
			
			SetPVarInt( playerid, "Phone:Number", strval( inputtext ) );
			
			showPlayerDialog( playerid, d_phone + 15, DIALOG_STYLE_INPUT, " ", g_small_string, "Enviar", "Atr�s" );
		}
		
		case d_phone + 15:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 14, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Nuevo mensaje\n\n\
					"cWHITE"Ingrese el n�mero de tel�fono del destinatario:\n\n\
					"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.",
				"Siguiente", "Atr�s" );
			}
			
			new
				number = GetPVarInt( playerid, "Phone:Number" ),
				phone = GetPVarInt( playerid, "Phone:Select" );
			
			if( strlen( inputtext ) > 120 )
			{
				format:g_small_string( "\
					"cBLUE"Nuevo mensaje\n\n\
					"gbDialog"Destinatario: %d\n\n\
					"cWHITE"Ingrese su mensaje:\n\n\
					"gbDialogError"El mensaje de texto es demasiado largo.",
					number 
				);
				
				return showPlayerDialog( playerid, d_phone + 15, DIALOG_STYLE_INPUT, " ", g_small_string, "Enviar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS )
			{
				format:g_small_string( "\
					"cBLUE"Nuevo mensaje\n\n\
					"gbDialog"Destinatario: %d\n\n\
					"cWHITE"Ingrese su mensaje:\n\n\
					"gbDialogError"No se puede enviar un mensaje vac�o.",
					number 
				);
				
				return showPlayerDialog( playerid, d_phone + 15, DIALOG_STYLE_INPUT, " ", g_small_string, "Enviar", "Atr�s" );
			}
			
			if( number == NEWS_NUMBER )
			{
				if( !ETHER_SMS || ETHER_STATUS == INVALID_PARAM )
				{
					SendClient:( playerid, C_WHITE, !""gbError"Error al enviar el mensaje." );
				}
				else
				{
					pformat:( "[�ter %s] SMS de %s[%d], tel. %d: %s", 
							Fraction[ FRACTION_NEWS - 1 ][f_short_name], Player[playerid][uName], playerid, GetPhoneNumber( playerid ), inputtext );
					psend:( ETHER_STATUS, C_LIGHTGREEN );
				
					pformat:( ""gbSuccess"Mensaje enviado al numero "cBLUE"%d"cWHITE".", number );
					psend:( playerid, C_WHITE );
				
					CreateMessage( playerid, INVALID_PARAM, Phone[playerid][phone][p_number], number, inputtext );
				}
			
				IsValidPhone{playerid} = 0;
				DeletePVar( playerid, "Phone:Number" );
				
				return showPlayerDialog( playerid, d_phone + 13, DIALOG_STYLE_LIST, "Mensajes", "\
					"gbDialog"Escribe un mensaje\n\
					"cWHITE"Mensajes entrantes\n\
					"cWHITE"Mensajes salientes",
				"Seleccionar", "Cerrar" );
			}
			
			if( !IsValidPhone{playerid} )
			{
				SendClient:( playerid, C_WHITE, ""gbError"Mensaje no enviado. El n�mero especificado no existe." );
				DeletePVar( playerid, "Phone:Number" );
				
				return showPlayerDialog( playerid, d_phone + 13, DIALOG_STYLE_LIST, "Mensajes", "\
					"gbDialog"Escribe un mensaje\n\
					"cWHITE"Mensajes entrantes\n\
					"cWHITE"Mensajes salientes",
				"Seleccionar", "Cerrar" );
			}
			
			if( SendMessage( playerid, number ) )
			{
				if( GetPVarInt( playerid, "Phone:Playerid" ) )
				{
					CreateMessage( playerid, GetPVarInt( playerid, "Phone:Playerid" ) - 1, Phone[playerid][phone][p_number], number, inputtext );
					DeletePVar( playerid, "Phone:Playerid" );
				}
				else
					CreateMessage( playerid, INVALID_PARAM, Phone[playerid][phone][p_number], number, inputtext );
				
				pformat:( ""gbSuccess"Mensaje enviado al numero"cBLUE"%d"cWHITE".", number );
				psend:( playerid, C_WHITE );
			}
			
			IsValidPhone{playerid} = 0;
			DeletePVar( playerid, "Phone:Number" );
				
			showPlayerDialog( playerid, d_phone + 13, DIALOG_STYLE_LIST, "Mensajes", "\
				"gbDialog"Escribe un mensaje\n\
				"cWHITE"Mensajes entrantes\n\
				"cWHITE"Mensajes salientes",
			"Seleccionar", "Cerrar" );
		}
		
		case d_phone + 16:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 13, DIALOG_STYLE_LIST, "Mensajes", "\
					"gbDialog"Escribe un mensaje\n\
					"cWHITE"Mensajes entrantes\n\
					"cWHITE"Mensajes salientes",
				"Seleccionar", "Cerrar" );
			}
					
			ShowIncomingMessage( playerid, g_dialog_select[playerid][listitem] );
			SetPVarInt( playerid, "Phone:Message", g_dialog_select[playerid][listitem] );
			
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
		}
		
		case d_phone + 17:
		{
			if( !response )
			{
				DeletePVar( playerid, "Phone:Message" );
				ShowIncomingList( playerid );
				return 1;
			}
			
			new
				message = GetPVarInt( playerid, "Phone:Message" );
				
			DeleteMessage( playerid, message );
			
			SendClient:( playerid, C_WHITE, ""gbSuccess"Mensaje eliminado." );
			
			showPlayerDialog( playerid, d_phone + 13, DIALOG_STYLE_LIST, "Mensajes", "\
				"gbDialog"Escribe un mensaje\n\
				"cWHITE"Mensajes entrantes\n\
				"cWHITE"Mensajes salientes",
			"Seleccionar", "Cerrar" );
		}
		
		case d_phone + 18:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 13, DIALOG_STYLE_LIST, "Mensajes", "\
					"gbDialog"Escribe un mensaje\n\
					"cWHITE"Mensajes entrantes\n\
					"cWHITE"Mensajes salientes",
				"Seleccionar", "Cerrar" );
			}
			
			ShowComingMessage( playerid, g_dialog_select[playerid][listitem] );
			SetPVarInt( playerid, "Phone:Message", g_dialog_select[playerid][listitem] );
			
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
		}
		
		case d_phone + 19:
		{
			if( !response )
			{
				DeletePVar( playerid, "Phone:Message" );
				ShowComingList( playerid );
				return 1;
			}
			
			new
				message = GetPVarInt( playerid, "Phone:Message" );
				
			DeleteMessage( playerid, message );
			SendClient:( playerid, C_WHITE, ""gbSuccess"Mensaje eliminado." );
			
			showPlayerDialog( playerid, d_phone + 13, DIALOG_STYLE_LIST, "Mensajes", "\
				"gbDialog"Escribe un mensaje\n\
				"cWHITE"Mensajes entrantes\n\
				"cWHITE"Mensajes salientes",
			"Seleccionar", "Cerrar" );
		}
		
		case d_phone + 20:
		{
			if( !response )
			{
				OpenPhoneBook( playerid );
				return 1;
			}
			
			new
				contact = GetPVarInt( playerid, "Phone:Contact" );
			
			if( strlen( inputtext ) > 120 )
			{
				format:g_small_string( "\
					"cBLUE"Nuevo mensaje\n\n\
					"gbDialog"Destinatario:"cWHITE"%s\n\n\
					"cWHITE"Ingrese su mensaje:\n\n\
					"gbDialogError"El mensaje de texto es demasiado largo.",
					PhoneContacts[playerid][contact][p_name]
				);
						
				return showPlayerDialog( playerid, d_phone + 20, DIALOG_STYLE_INPUT, " ", g_small_string, "Enviar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS )
			{
				format:g_small_string( "\
					"cBLUE"Nuevo mensaje\n\n\
					"gbDialog"Destinatario:"cWHITE"%s\n\n\
					"cWHITE"Ingrese su mensaje:\n\n\
					"gbDialogError"No se puede enviar un mensaje vac�o.",
					PhoneContacts[playerid][contact][p_name]
				);
						
				return showPlayerDialog( playerid, d_phone + 20, DIALOG_STYLE_INPUT, " ", g_small_string, "Enviar", "Atr�s" );
			}
	
			
			
			if( !IsValidPhone{playerid} )
			{
				SendClient:( playerid, C_WHITE, ""gbError"Mensaje no enviado. El n�mero especificado no existe." );
				
				DeletePVar( playerid, "Phone:Contact" );
				
				OpenPhoneBook( playerid );
				return 1;
			}
			
			if( SendMessage( playerid, PhoneContacts[playerid][contact][p_number] ) )
			{
				if( GetPVarInt( playerid, "Phone:Playerid" ) )
				{
					CreateMessage( playerid, GetPVarInt( playerid, "Phone:Playerid" ) - 1, GetPhoneNumber( playerid ), PhoneContacts[playerid][contact][p_number], inputtext );
					DeletePVar( playerid, "Phone:Playerid" );
				}
				else
					CreateMessage( playerid, INVALID_PARAM, GetPhoneNumber( playerid ), PhoneContacts[playerid][contact][p_number], inputtext );
				
				pformat:( ""gbSuccess"Mensaje enviado a"cBLUE"%s"cWHITE".", PhoneContacts[playerid][contact][p_name] );
				psend:( playerid, C_WHITE );
			}
			
			IsValidPhone{playerid} = 0;
			DeletePVar( playerid, "Phone:Number" );
			DeletePVar( playerid, "Phone:Contact" );
			OpenPhoneBook( playerid );
		}
		
		case d_phone + 21:
		{
			if( !response ) return 1;
			
			switch( listitem )
			{
				case 0:
				{
					showPlayerDialog( playerid, d_phone + 22, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Llamada\n\n\
						"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
						"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.",
					"Llamar", "Atr�s" );
				}
				
				case 1:
				{
					showPlayerDialog( playerid, d_phone + 23, DIALOG_STYLE_LIST, " ", "\
						"cWHITE"Emergencias (911)\n\
						"cWHITE"Taxi\n\
						"cWHITE"Mec�nico",
					"Llamar", "Atr�s" );
				}
				
				case 2:
				{
					showPlayerDialog( playerid, d_phone + 29, DIALOG_STYLE_LIST, " ", "\
						"cWHITE"CNN",
					"Llamar", "Atr�s" );
				}
				
				case 3:
				{
					format:g_small_string( "\
						"cBLUE"SA:Telecom\n\n\
						"gbSuccess"Tu numero: %d",
						GetPhoneNumber( playerid )
					);
					
					showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Cerrar", "" );
				}
			}
		}
		
		case d_phone + 22:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 21, DIALOG_STYLE_LIST, " ", "\
					"gbDialog"Marque el numero\n\
					"cWHITE"N�meros de servicio\n\
					Otras habitaciones\n\
					Mi n�mero",
				"Seleccionar", "Cerrar" );
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 || strval( inputtext ) > 10000000 )
			{
				return showPlayerDialog( playerid, d_phone + 22, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Llamada\n\n\
					"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
					"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\n\
					"gbDialogError"Formato de valor no v�lido, por favor vuelva a ingresar un n�mero.",
				"Llamar", "Atr�s" );
			}
			
			if( strval( inputtext ) == GetPhoneNumber( playerid ) )
			{
				return showPlayerDialog( playerid, d_phone + 22, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Llamada\n\n\
					"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
					"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\n\
					"gbDialogError"No puedes llamar a tu tel�fono.",
				"Llamar", "Atr�s" );
			}
			
			if( strval( inputtext ) == NUMBER_GUNDEALER )
			{
				if( !Player[playerid][uCrimeM] )
				{
					return showPlayerDialog( playerid, d_phone + 22, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Llamada\n\n\
						"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
						"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\n\
						"gbDialogError"El tel�fono con este n�mero no est� registrado en la red.",
					"Llamar", "Atr�s" );
				}
				
				new
					crime = getIndexCrimeFraction( Player[playerid][uCrimeM] ),
					rank,
					day, hour, minute,
					Float:interval, Float:interval_2, Float:interval_3;
					
				if( PlayerLeaderCrime( playerid, crime ) ) goto next;
				
				rank = getCrimeRankId( playerid, crime );
				
				if( !CrimeRank[crime][rank][r_call_weapon] )
				{
					return showPlayerDialog( playerid, d_phone + 22, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Llamada\n\n\
						"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
						"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\n\
						"gbDialogError"No puede hacer una llamada a este n�mero.",
					"Llamar", "Atr�s" );
				}
				
				next:
				
				if( !CrimeFraction[crime][c_type_weapon] )
				{
					return showPlayerDialog( playerid, d_phone + 22, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Llamada\n\n\
						"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
						"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\n\
						"gbDialogError"No puede hacer una llamada a este n�mero.",
					"Llamar", "Atr�s" );
				}
				
				if( CrimeFraction[crime][c_time] )
				{
					interval = float( CrimeFraction[crime][c_time] - gettime() ) / 86400.0;
					day = floatround( interval, floatround_floor );
					
					interval_2 = ( interval - float( day ) ) * 86400.0 / 3600.0;
					hour = floatround( interval_2, floatround_floor );
					
					interval_3 = ( interval_2 - float( hour ) ) * 3600.0 / 60.0;
					minute = floatround( interval_3 );
				
					format:g_small_string( "\
						"cBLUE"Llamada\n\n\
						"cWHITE"Introduzca el n�mero de tel�fono:\n\n\
						"gbDialog"Un n�mero de tel�fono puede contener s�lo n�meros.\n\n\
						"gbDialogError"Puedes llamar de nuevo el d�a %d, %d h. %d min.",
						day, hour, minute );
					return showPlayerDialog( playerid, d_phone + 22, DIALOG_STYLE_INPUT, " ", g_small_string, "Llamar", "Atr�s" );
				}
				
				format:g_string( dialog_gundealer, CrimeFraction[crime][c_type_weapon], DAYS_TO_GUNDEALER );
				return showPlayerDialog( playerid, d_crime + 33, DIALOG_STYLE_MSGBOX, " ", g_string, "Siguiente", "Cancelar" );
			}
			
			if( !CheckNumberNetwork( playerid, strval( inputtext ) ) )
			{
				Call[playerid][c_call_id] = 0;
				
				return showPlayerDialog( playerid, d_phone + 21, DIALOG_STYLE_LIST, " ", "\
					"gbDialog"Marque el numero\n\
					"cWHITE"Servicios\n\
					Otras habitaciones\n\
					Mi n�mero",
				"Seleccionar", "Cerrar" );
			}
			
			new
				callid = Call[playerid][c_call_id];
				
			if( GetPVarInt( callid, "Phone:Call" ) )
			{
				SendClient:( playerid, C_WHITE, ""gbPhone"El tel�fono da ocupado." );
						
				Call[playerid][c_call_id] = 0;
				
				return showPlayerDialog( playerid, d_phone + 21, DIALOG_STYLE_LIST, " ", "\
					"gbDialog"Marque el numero\n\
					"cWHITE"Servicios\n\
					Otras habitaciones\n\
					Mi n�mero",
				"Seleccionar", "Cerrar" );
			}					
					
			pformat:( ""gbPhone"Llamada saliente "cBLUE"%d "cGRAY"...( "cBLUE"H"cGRAY" )",  strval( inputtext ) );
			psend:( playerid, C_WHITE );
						
			if( !CheckIncomingNumber( callid, GetPhoneNumber( playerid ) ) )
			{
				pformat:( ""gbPhone"Llamada entrante "cBLUE"%d"cGRAY"...( "cGREEN"Y"cGRAY" | "cRED"N"cGRAY" )", GetPhoneNumber( playerid ) );
				psend:( callid, C_WHITE );
			}
			else
			{
				pformat:( ""gbPhone"Llamada entrante "cWHITE"%s"cGRAY"...( "cGREEN"Y"cGRAY" | "cRED"N"cGRAY" )", Call[callid][c_name] );
				psend:( callid, C_WHITE );
			}
					
			PlayerPlaySound( playerid, 3600, 0.0, 0.0, 0.0 );
			PlayerPlaySound( callid, call_sound[Call[playerid][c_sound]][s_id], 0.0, 0.0, 0.0 );
										
			Call[callid][c_status] = true;
			Call[callid][c_call_id] = playerid;
					
			SetPVarInt( playerid, "Phone:Call", 1 );
			SetPVarInt( callid, "Phone:Call", 1 );
					
			PhoneStatus( playerid, true );
		}
		
		case d_phone + 23:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 21, DIALOG_STYLE_LIST, " ", "\
					"gbDialog"Marque el numero\n\
					"cWHITE"Servicios\n\
					Otras habitaciones\n\
					Mi n�mero",
				"Seleccionar", "Cerrar" );
			}
			
			switch( listitem )
			{
				//Llamada de emergencia
				case 0:
				{
					if( Player[playerid][jPolice] )
						return SendClient:( playerid, C_WHITE, ""gbError"Ya has llamado al 911." );
				
					showPlayerDialog( playerid, d_phone + 27, DIALOG_STYLE_TABLIST, ""cBLUE"Servicio 911", ""cWHITE"\
						Polic�a\n\
						Bomberos & M�dicos\n\
						Polic�a, Bomberos y M�dicos", 
					"Llamar", "Atr�s" );
				}
				
				//Servicio de taxi
				case 1:
				{
					if( !IsPlayerInNetwork( playerid ) )
					{
						SendClient:( playerid, C_WHITE, !NO_NETWORK );
						
						return showPlayerDialog( playerid, d_phone + 23, DIALOG_STYLE_LIST, " ", "\
							"cWHITE"Emergencias (911)\n\
							"cWHITE"Taxi\n\
							"cWHITE"Mec�nico",
						"Llamar", "Atr�s" );
					}
				
					showPlayerDialog( playerid, d_phone + 25, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Servicio de taxi\n\n\
						"cWHITE"Por favor, informe brevemente su ubicaci�n al despachador:\n\n\
						"gbDialog"Su ubicaci�n ser� enviada al servicio de taxi.",
					"Llamar", "Atr�s" );
				}
				
				case 2:
				{
					showPlayerDialog( playerid, d_phone + 26, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Servicio de mec�nicos\n\n\
						"cWHITE"Por favor, informe brevemente su ubicaci�n al despachador:\n\n\
						"gbDialog"Su ubicaci�n ser� enviada al servicio al cliente.",
					"Llamar", "Atr�s" );
				}
			}
			
		}
		
		//Modo silencioso
		case d_phone + 24:
		{
			if( !response )
			{	
				ShowListSound( playerid );
				return 1;
			}
			
			new
				sound = GetPVarInt( playerid, "Phone:Sound" ),
				phone = GetPVarInt( playerid, "Phone:Select" );
			
			switch( listitem )
			{
				case 0:
				{
					Phone[playerid][phone][p_settings][2] = sound;
					UpdatePhoneSettings( Phone[playerid][phone][p_number], Phone[playerid][phone][p_settings] );
				}
			}
			
			SendClient:( playerid, C_WHITE, ""gbSuccess"El modo silencioso est� activado." );
			DeletePVar( playerid, "Phone:Sound" ); 
			ShowListSound( playerid );
		}
	
		//Llamar en un taxi
		case d_phone + 25:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 23, DIALOG_STYLE_LIST, " ", "\
					"cWHITE"Emergencias (911)\n\
					"cWHITE"Taxi\n\
					"cWHITE"Mec�nico",
				"Llamar", "Atr�s" );
			}
			
			if( Player[playerid][jTaxi] )
				return SendClient:( playerid, C_WHITE, ""gbError"Ya has solicitado un servicio de taxi." );
			
			if( inputtext[0] == EOS || strlen( inputtext ) > 30 )
			{
				return showPlayerDialog( playerid, d_phone + 25, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Taxi\n\n\
					"cWHITE"Por favor, informe brevemente su ubicaci�n al despachador:\n\n\
					"gbDialog"Su ubicaci�n ser� enviada al servicio de taxi.\n\n\
					"gbDialogError"Formato de entrada no v�lido.",
				"Llamar", "Atr�s" );
			}
			
			new
				bool:flag = false,
				call;
			
			for( new i; i < MAX_TAXICALLS; i++ )
			{
				if( Taxi[i][t_playerid] == INVALID_PARAM )
				{
					new
						Float:pos[3];
						
					GetPlayerPos( playerid, pos[0], pos[1], pos[2] );
				
					Taxi[i][t_playerid] = playerid;
					Taxi[i][t_time] = gettime() + 300;
					
					Taxi[i][t_place][0] = 
					Taxi[i][t_zone][0] = EOS;
					
					strcat( Taxi[i][t_place], inputtext, 30 );
					GetPlayer2DZone( playerid, Taxi[i][t_zone], 28 );
					
					Taxi[i][t_pos][0] = pos[0];
					Taxi[i][t_pos][1] = pos[1];
					Taxi[i][t_pos][2] = pos[2];
					
					call = i;
					flag = true;
					break;
				}
			}
			
			if( !flag ) return SendClient:( playerid, C_WHITE, ""gbError"Desafortunadamente, en este momento todos los autos est�n ocupados." );
			
			Player[playerid][jTaxi] = true;		
			SendClient:( playerid, C_WHITE, ""gbSuccess"Datos transferidos al servicio de taxi. Espera, tu llamada est� siendo procesada." );
			
			format:g_small_string( "[HQ: %d] Despachador: Recibimos una nueva llamada en %s.", 
				CHANNEL_TAXI,
				Taxi[call][t_zone]
			);
			SendRadioMessage( CHANNEL_TAXI, g_small_string );
		}
		
		case d_phone + 26:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 23, DIALOG_STYLE_LIST, " ", "\
					"cWHITE"Emergencias (911)\n\
					"cWHITE"Taxi\n\
					"cWHITE"Mec�nico",
				"Llamar", "Atr�s" );
			}
			
			if( Player[playerid][jMech] )
				return SendClient:( playerid, C_WHITE, ""gbError"Ya ha enviado una solicitud al servicio de atenci�n al cliente." );
			
			if( inputtext[0] == EOS || strlen( inputtext ) > 30 )
			{
				return showPlayerDialog( playerid, d_phone + 25, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Mec�nico\n\n\
					"cWHITE"Por favor, informe brevemente su ubicaci�n al despachador:\n\n\
					"gbDialog"Su ubicaci�n ser� enviada al servicio al cliente.\n\n\
					"gbDialogError"Formato de entrada no v�lido.",
				"Llamar", "Atr�s" );
			}
			
			new
				bool:flag = false,
				call;
			
			for( new i; i < MAX_MECHCALLS; i++ )
			{
				if( Mechanic[i][m_playerid] == INVALID_PARAM )
				{
					new
						Float:pos[3];
						
					GetPlayerPos( playerid, pos[0], pos[1], pos[2] );
				
					Mechanic[i][m_playerid] = playerid;
					Mechanic[i][m_time] = gettime() + 300;
					
					Mechanic[i][m_place][0] = 
					Mechanic[i][m_zone][0] = EOS;
					
					strcat( Mechanic[i][m_place], inputtext, 30 );
					GetPlayer2DZone( playerid, Mechanic[i][m_zone], 28 );
					
					Mechanic[i][m_pos][0] = pos[0];
					Mechanic[i][m_pos][1] = pos[1];
					Mechanic[i][m_pos][2] = pos[2];
					
					call = i;
					flag = true;
					break;
				}
			}
			
			if( !flag ) return SendClient:( playerid, C_WHITE, ""gbError"Desafortunadamente, en este momento todos los autos est�n ocupados." );
			
			Player[playerid][jMech] = true;		
			SendClient:( playerid, C_WHITE, ""gbSuccess"Datos transferidos al departamento de servicio. Espera, tu llamada est� siendo procesada." );
			
			format:g_small_string( "[HQ: %d] Despachador: Recibimos una llamada en %s.", 
				CHANNEL_MECHANIC,
				Mechanic[call][m_zone]
			);
			SendRadioMessage( CHANNEL_MECHANIC, g_small_string );
		}
		
		case d_phone + 27:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 23, DIALOG_STYLE_LIST, " ", "\
					"cWHITE"Emergencias (911)\n\
					"cWHITE"Taxi\n\
					"cWHITE"Mec�nico",
				"Llamar", "Atr�s" );
			}
			
			SetPVarInt( playerid, "Phone:Fraction", listitem );
			
			showPlayerDialog( playerid, d_phone + 28, DIALOG_STYLE_INPUT, " ", "\
				"cBLUE"Servicio 911\n\n\
				"cWHITE"Por favor describa brevemente su situaci�n:\n\
				"gbDialog"Su ubicaci�n ser� enviada al 911.",
			"Llamar", "Atr�s" );
		}
		
		case d_phone + 28:
		{
			if( !response )
			{
				DeletePVar( playerid, "Phone:Fraction" );
				return showPlayerDialog( playerid, d_phone + 27, DIALOG_STYLE_TABLIST, ""cBLUE"Servicio 911", ""cWHITE"\
					Polic�a\n\
					Bomberos & M�dicos\n\
					Polic�a, Bomberos y M�dicos",
				"Llamar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS || strlen( inputtext ) > 32 )
			{
				return showPlayerDialog( playerid, d_phone + 28, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Servicio 911\n\n\
					"cWHITE"Por favor describa brevemente su situaci�n:\n\
					"gbDialog"Su ubicaci�n ser� enviada al 911.\n\n\
					"gbDialogError"Formato de entrada no v�lido.",
				"Llamar", "Atr�s" );
			}
			
			new
				index = INVALID_PARAM,
				type = GetPVarInt( playerid, "Phone:Fraction" ),
				abc[10] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
			
			for( new i; i < MAX_POLICECALLS; i++ )
			{
				if( !CPolice[i][p_time] )
				{
					index = i;
					break;
				}
			}
			
			if( index == INVALID_PARAM )
			{
				new
					cmin;
			
				for( new i; i < MAX_POLICECALLS; i++ )
				{
					if( CPolice[cmin][p_time] > CPolice[i][p_time] )
						cmin = i;
				}
				
				if( IsLogged( CPolice[cmin][p_playerid] ) ) 
				{
					SendClient:( CPolice[cmin][p_playerid], C_WHITE, ""gbDialog"Su llamada al 911 ha sido cancelada." );
					Player[ CPolice[cmin][p_playerid] ][jPolice] = false;
				}
				
				index = cmin;
			}
			
			GetPlayerPos( playerid, CPolice[index][p_pos][0], CPolice[index][p_pos][1], CPolice[index][p_pos][2] );
				
			CPolice[index][p_playerid] = playerid;
			CPolice[index][p_time] = gettime() + 300; 
			CPolice[index][p_type] = type;
				
			CPolice[index][p_number][0] =
			CPolice[index][p_descript][0] = 
			CPolice[index][p_zone][0] = EOS;
					
			strcat( CPolice[index][p_descript], inputtext, 32 );
			GetPlayer2DZone( playerid, CPolice[index][p_zone], 28 );
			
			for( new i; i < 4; i++ ) 
			{
				CPolice[index][p_number][i] = 0;
				CPolice[index][p_number][i] = abc[ random( 10 ) ];
			}
			
			CPolice[index][p_status] = 0;
			Player[playerid][jPolice] = true;
			
			pformat:( ""gbSuccess"Datos transferidos al servicio 911. Espere, su llamada "cBLUE"#%s"cWHITE" est� siendo procesada.", CPolice[index][p_number] );
			psend:( playerid, C_WHITE );
			
			DeletePVar( playerid, "Phone:Fraction" );
			SendEmergencyCall( playerid, type, index );
		}
		
		case d_phone + 29:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_phone + 21, DIALOG_STYLE_LIST, " ", "\
					"gbDialog"Marque el numero\n\
					"cWHITE"Servicios\n\
					Otras habitaciones\n\
					Mi n�mero",
				"Seleccionar", "Cerrar" );
			}
			
			switch( listitem )
			{
				case 0:
				{
					if( ETHER_STATUS == INVALID_PARAM || ETHER_CALL == false )
					{
						SendClient:( playerid, C_WHITE, !""gbError"Las llamadas en vivo est�n deshabilitadas." );
						return showPlayerDialog( playerid, d_phone + 29, DIALOG_STYLE_LIST, " ", "\
							"cWHITE"CNN",
						"Llamar", "Atr�s" );
					}
					
					if( Player[playerid][tEther] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"Ya est�s en vivo." );
						return showPlayerDialog( playerid, d_phone + 29, DIALOG_STYLE_LIST, " ", "\
							"cWHITE"CNN",
						"Llamar", "Atr�s" );
					}
					
					if( ETHER_CALLID != INVALID_PARAM )
					{
						SendClient:( playerid, C_WHITE, !""gbError"La llamada ya ha llegado al aire, int�ntalo de nuevo m�s tarde." );
						return showPlayerDialog( playerid, d_phone + 29, DIALOG_STYLE_LIST, " ", "\
							"cWHITE"CNN",
						"Llamar", "Atr�s" );
					}
					
					pformat:( ""gbPhone"Llamada saliente "cBLUE"%s "cGRAY"...( "cBLUE"H"cGRAY" )", Fraction[ FRACTION_NEWS - 1 ][f_name] );
					psend:( playerid, C_WHITE );
					
					PlayerPlaySound( playerid, 3600, 0.0, 0.0, 0.0 );
					
					pformat:( "[�ter %s] Nueva llamada de %s[%d], tel. %d. Use /ether para aceptar una llamada.", 
						Fraction[ FRACTION_NEWS - 1 ][f_short_name], Player[playerid][uName], playerid, GetPhoneNumber( playerid ) );
					psend:( ETHER_STATUS, C_LIGHTGREEN );
					
					ETHER_CALLID = playerid;
					SetPVarInt( playerid, "San:Call", 1 );
							
					PhoneStatus( playerid, true );
				}
			}
		}
	}
	
	return 1;
}