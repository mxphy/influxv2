function Target_OnPlayerKeyStateChange( playerid, newkeys, oldkeys )
{	
	/*if( HOLDING( KEY_CTRL_BACK | KEY_HANDBRAKE ) ) 
	{
        new 
			target = GetPlayerTargetPlayer( playerid );
			
        if( target != INVALID_PLAYER_ID ) 
		{
			if( GetDistanceBetweenPlayers( playerid, target ) > 3.0 )
				return 1;
			
			showPlayerDialog( playerid, d_target + 1, DIALOG_STYLE_LIST, Player[target][uName], 
				""cBLUE"-"cWHITE" Mostrar tarjeta de identificación\n\
				"cBLUE"-"cWHITE" Transferir dinero\n\
				"cBLUE"-"cWHITE" Di hola\n\
				"cBLUE"-"cWHITE" Para buscar", 
				"Seleccionar", "Atras"
			);
			
			SetPVarInt( playerid, "Target:PlayerId", target );
        }
    }*/
	
	return 1;
}

Target_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] )
{
	#pragma unused inputtext
	#pragma unused listitem
	
	switch( dialogid )
	{
		case d_target :
		{
			new
				targetid = GetPVarInt( playerid, "Target:PlayerId" );
				
			if( response )
			{
				format:g_small_string( ""gbSuccess"El jugador "cBLUE"%s[%d]"cWHITE" mira su tarjeta de identificación.",
					GetAccountName( playerid ),
					playerid
				);
				
				SendClient:( targetid, C_WHITE, g_small_string );
					
				format:g_small_string( ""gbSuccess"Has aceptado una oferta del jugador "cBLUE"%s[%d]"cWHITE".",
					GetAccountName( targetid ),
					targetid
				);
				
				SendClient:( playerid, C_WHITE, g_small_string );
				
				showIdCard( targetid, playerid );
				
				format:g_small_string( "mostro la tarjeta de identificación de %s %s",
					SexTextEnd( targetid ),
					GetAccountName( playerid )
				);
				
				SendRolePlayAction( targetid, g_small_string, RP_TYPE_AME );
				
				format:g_small_string( "buscando tarjeta de identificación de %s",
					GetAccountName( targetid )
				);
				
				SendRolePlayAction( playerid, g_small_string, RP_TYPE_AME );
			}
			else
			{
				format:g_small_string( ""gbError"El jugador "cBLUE"%s[%d]"cWHITE" rechazó su oferta para ver la tarjeta de identificación.",
					GetAccountName( playerid ),
					playerid
				);
				
				SendClient:( targetid, C_WHITE, g_small_string );
					
				format:g_small_string( ""gbError"Has rechazado la oferta del jugador. "cBLUE"%s[%d]"cWHITE".",
					GetAccountName( targetid ),
					targetid
				);
				
				SendClient:( playerid, C_WHITE, g_small_string );
			}
			
			DeletePVar( playerid, "Target:PlayerId" );
		}
	}
	
	return 1;
}