enum pa_attach_info 
{
	pa_name[64],
	pa_object,
	pa_bone,
	
}

new const
	pa_attach[][pa_attach_info] = {
	{"Biker rosa", 19112, 2},
	{"Black biker bowler", 19115, 2},
	{"Casco azul y blanco", 18976, 2},
	{"Casco rojo", 18977, 2},
	{"Sombrero de bruja", 19528, 2},
	{"Guitarra el�ctrica [atr�s]", 19319, 1},
	{"Guitarra El�ctrica [Mano Derecha]", 19319, 6},
	{"Guitarra el�ctrica [mano izquierda]", 19319, 5},
	{"Chatarra [mano derecha]", 18634, 6},
	{"Chatarra [mano izquierda]", 18634, 5},
	{"Chatarra [cintur�n]", 18634, 7},
	{"Martillo [mano derecha]", 18635, 6},
	{"Martillo [mano izquierda]", 18635, 5},
	{"Martillo [cintur�n]", 18635, 7},
	{"Rake [mano derecha]", 18890, 6},
	{"Rastrillo [mano izquierda]", 18890, 5},
	{"Stun gun [mano derecha]", 18642, 6},
	{"Pistola de aturdimiento [mano izquierda]", 18642, 5},
	{"Pistola de aturdimiento [cintur�n]", 18642, 7},
	{"Destornillador [mano derecha]", 18644, 6},
	{"Destornillador [mano izquierda]", 18644, 5},
	{"Destornillador [cintur�n]", 18644, 7},
	{"Sombrero del jud�o", 19136, 2},
	{"Ca�a [mano derecha]", 19348, 6},
	{"Ca�a [mano izquierda]", 19348, 5},
	{"Respirador", 19472, 2},
	{"Bigote 1", 19350, 2},
	{"Bigote 2", 19351, 2},
	{"Sombrero de paja", 19553, 2},
	{"Mochila de camping", 19559, 1},
	{"Cesta del supermercado [mano derecha]", 19592, 6},
	{"Cesta del supermercado [mano izquierda]", 19592, 5},
	{"Micr�fono [mano derecha]", 19610, 6},
	{"Micr�fono [mano izquierda]", 19610, 5},
	{"Butterdish [mano derecha]", 19621, 6},
	{"Butterdish [mano izquierda]", 19621, 5},
	{"Llave [mano derecha]", 19627, 6},
	{"Llave [mano izquierda]", 19627, 5},
	{"Llave [correa]", 19627, 7},
	{"Maleta [mano derecha]", 19624, 6},
	{"Maleta [mano izquierda]", 19624, 5},
	{"Pasamonta�as", 19801, 2},
	{"Vestirse en un ojo", 19085, 2}
};


/*CMD:pattach( playerid, params[] ) 
{
	if( Player[playerid][uPremium] ) 
	{
		new slot;
		for( new i; i < sizeof pa_attach; i++ ) 
		{
			format:g_big_string( "%s%d. %s\n", g_big_string, slot + 1, pa_attach[i][pa_name] );
			slot++;
		}
		format:g_small_string(  "%d. Objetos claros", slot + 1 ), strcat( g_big_string, g_small_string );
		showPlayerDialog( playerid, 3001, DIALOG_STYLE_LIST, "Lista de objetos", g_big_string, "Seleccionar", "Cancelar");
	}
	return 1;
}*/

Pattach_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{
	#pragma unused inputtext
	
	switch( dialogid ) {
		case 3001: {
			if(!response) return 1;
			if( listitem == sizeof( pa_attach ) ) {
				RemovePlayerAttachedObject( playerid, 5 ), RemovePlayerAttachedObject( playerid, 6 );
	            return SendClient:( playerid, -1, ""gbDefault"�Has borrado tus objetos!" );
			}
			if( IsPlayerAttachedObjectSlotUsed( playerid, 5 ) 
				&& IsPlayerAttachedObjectSlotUsed( playerid, 6 ) ) return SendClient:(playerid, -1, ""gbError"Todas las m�quinas tragamonedas est�n ocupadas, elim�nelas en el men�!");
	        if( !IsPlayerAttachedObjectSlotUsed( playerid, 5 )) {
	            SetPlayerAttachedObject( playerid, 5, pa_attach[listitem][pa_object], pa_attach[listitem][pa_bone] );
    			return EditAttachedObject( playerid, 5 );
	        }
	        else if( IsPlayerAttachedObjectSlotUsed( playerid, 5 )) {
	            SetPlayerAttachedObject( playerid, 6, pa_attach[listitem][pa_object], pa_attach[listitem][pa_bone] );
    			return EditAttachedObject( playerid, 6 );
	        }
	    }
	}
	return 1;
}