Admin_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{
	switch( dialogid ) 
	{
		case d_admin : 
		{
		    if( response ) 
			{
				if( inputtext[0] == EOS ) 
				    return showPlayerDialog( playerid, d_admin, DIALOG_STYLE_INPUT, " ", acontent_login, "Siguiente", "Cerrar" );
				 
				OnAdminLogin( playerid, inputtext );
			}
			
			return 1;
		}
		case d_admin + 1 : 
		{
		    if( response ) 
			{
				if( !GetAccessCommand( playerid, "acp" ) )
					return SendClientMessage( playerid, C_WHITE, NO_ACCESS_CMD );
				
				switch( listitem ) 
				{
					case 0 : 
					{

						new
						    fmt_player[ 32 + MAX_PLAYER_NAME + 2 ],
							fmt_spectate[ 64 ],
							fmt_afk [ 32 ],
							admin_count = 0,
							support_count = 0;
							
						clean_array();
						
						strcat( g_big_string, ""gbDefault"Administradores online:\n\n" );
						foreach(new i : Player) 
						{
							if( !IsLogged( i ) || !GetAccessAdmin( i, 1, false ) ) 
								continue;

							format( fmt_player, sizeof fmt_player, "(%d) "cBLUE"%s[%d]"cWHITE"",
								Admin[i][aLevel],
								Player[i][uName],
								i
							);
							
							if( GetPVarInt( i, "Admin:SpectateId" ) != INVALID_PLAYER_ID ) 
							{
								format( fmt_spectate, sizeof fmt_spectate, " - "cGREEN"/sp %d"cWHITE"",
								    GetPVarInt( i, "Admin:SpectateId" )
								);
							}
							
							if( IsAfk( i ) ) 
							{
                                format( fmt_afk, sizeof fmt_afk, " - "cGRAY"[AFK: %d]"cWHITE"",
								    GetPVarInt( i, "Player:Afk" )
								);
							}
							
							
							format:g_small_string(  "%s%s%s%s\n",
								fmt_player,
								GetPVarInt( i, "Admin:SpectateId" ) != INVALID_PLAYER_ID ? fmt_spectate : (""),
								IsAfk( i ) ? fmt_afk : (""),
								!GetPVarInt( i, "Admin:Duty" ) ? (" - "cRED"[No autorizado]"cGRAY"") : ("")
							);
							
							++admin_count;
							strcat( g_big_string, g_small_string );
						}
						
						strcat( g_big_string, "\n\n\n"gbDefault" Soportes online:\n\n" );

						foreach(new i : Player) 
						{
							if( !IsLogged( i ) || !GetAccessSupport( i ) ) 
								continue;
							
							if( IsAfk( i ) ) 
							{
                                format( fmt_afk, sizeof fmt_afk, " - "cGRAY"[AFK: %d]",
								    GetPVarInt( i, "Player:Afk" )
								);
							}
							
							format( fmt_player, sizeof fmt_player, ""cBLUE"%s[%d]%s%s"cWHITE"\n",
								Player[i][uName],
								i,
								IsAfk( i ) ? fmt_afk : (""),
								!GetPVarInt( i, "Support:Duty" ) ? (" - "cGRAY"[Fuera de servicio]"cWHITE"") : ("")
							);
							
							++support_count;
							
							strcat( g_big_string, fmt_player );
						}
						
						if( support_count == 0 )
						    strcat( g_big_string, "No hay soportes online\n" );
						
						format:g_small_string(  "\n\nTotal de administradores conectados: "cBLUE"%d\n"cWHITE"Total de soportes conectados: "cBLUE"%d",
							admin_count,
							support_count
						);
						
						strcat( g_big_string, g_small_string );
						
						showPlayerDialog( playerid, d_admin + 1, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Cerrar", "");
						clean_array();
					}
					
					case 1 : 
					{
						clean_array();
						if( GetAccessAdmin( playerid, 1 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"1"cWHITE"\n" );
						if( GetAccessAdmin( playerid, 2 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"2"cWHITE"\n" );
						if( GetAccessAdmin( playerid, 3 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"3"cWHITE"\n" );
						if( GetAccessAdmin( playerid, 4 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"4"cWHITE"\n" );
						if( GetAccessAdmin( playerid, 5 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"5"cWHITE"\n" );
						if( GetAccessAdmin( playerid, 6 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"6"cWHITE"\n" );
						if( GetAccessAdmin( playerid, 7 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"7"cWHITE"\n" );
    					showPlayerDialog( playerid, d_admin + 2, DIALOG_STYLE_TABLIST, " ", g_big_string, "Siguiente", "Atr�s");
					}
					case 2 : 
					{
						clean:<g_big_string>;
						strcat( g_big_string, ""gbSuccess"Estadisticas\n\n" );
						format:g_small_string( " - Identificador: "cBLUE"%d"cWHITE"\n", Admin[playerid][aID] ), strcat( g_big_string, g_small_string );
						format:g_small_string( " - Nombre: "cBLUE"%s"cWHITE"\n", Admin[playerid][aName] ), strcat( g_big_string, g_small_string );
						format:g_small_string( " - Nivel de acceso: "cBLUE"%d"cWHITE"\n\n", Admin[playerid][aLevel] ), strcat( g_big_string, g_small_string );
						format:g_small_string( " - Horas jugadas: "cBLUE"-"cWHITE"\n"), strcat( g_big_string, g_small_string );
						format:g_small_string( "     Por dia:"cBLUE"-"cWHITE"\n" ), strcat( g_big_string, g_small_string );
						format:g_small_string( "     Por semana: "cBLUE"-"cWHITE"\n\n" ), strcat( g_big_string, g_small_string );
						format:g_small_string( " - Acciones: "cBLUE"-"cWHITE"\n" ), strcat( g_big_string, g_small_string );
						format:g_small_string( "     Desconectado: "cBLUE"-"cWHITE"\n" ), strcat( g_big_string, g_small_string );
						format:g_small_string( "     Bloqueados: "cBLUE"-"cWHITE"\n" ), strcat( g_big_string, g_small_string );
						format:g_small_string( "     Chat bloqueados: "cBLUE"-"cWHITE"\n" ), strcat( g_big_string, g_small_string );
						format:g_small_string( "     Advertencias: "cBLUE"-"cWHITE"\n\n" ), strcat( g_big_string, g_small_string );
						format:g_small_string( " - Total de cuentas comprobadas: "cBLUE"-"cWHITE"\n" ), strcat( g_big_string, g_small_string );
						format:g_small_string( "     Solicitudes aprobadas: "cBLUE"-"cWHITE"\n" ), strcat( g_big_string, g_small_string );
						format:g_small_string( "     Solicitudes rechazadas: "cBLUE"-"cWHITE"" ), strcat( g_big_string, g_small_string );
						showPlayerDialog( playerid, d_admin + 7, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atr�s", "Cerrar");
					}
					case 3 :
					{
						return 1;
					}
					case 4 :
					{
						if( !COUNT_KILL_LOG )
							return SendClient:( playerid, C_WHITE, ""gbError"Actualmente el registro de muertes est� vac�o.");
					
						new 
							weapon_name[32];
									
						clean:<g_big_string>;
						strcat( g_big_string, ""cBLUE"Nombre\t"cBLUE"Arma\t"cBLUE"Asesinado\t"cBLUE"Tiempo"cWHITE"\n" );
					
						for( new i; i != COUNT_KILL_LOG; i++ )
						{
							GetWeaponName( KillLog[i][killerGun], weapon_name, sizeof weapon_name );
							
							if( weapon_name[0] == EOS )	
								strcat( weapon_name, "Fist", 32 );
							
							format:g_small_string( "%s[%d]\t%s\t%s[%d]\t%s\n",
								KillLog[i][killerName],
								KillLog[i][killerID],
								weapon_name,
								KillLog[i][killedName],
								KillLog[i][killedID],
								date( "%hh:%ii:%ss", KillLog[i][killedTime] )
							);
							
							strcat( g_big_string, g_small_string );
						}
						
						showPlayerDialog( playerid, d_admin + 7, DIALOG_STYLE_TABLIST_HEADERS, " ", g_big_string, "Atr�s", "Cerrar" );
					}
					case 5 :
					{	
						if( !COUNT_DISCONNECT_LOG )
							return SendClient:( playerid, C_WHITE, ""gbError"El registro de desconexiones est� actualmente vac�o.");
					
						clean:<g_big_string>;
						strcat( g_big_string, ""cBLUE"Nombre\t"cBLUE"Tiempo\t"cBLUE"Raz�n"cWHITE"\n" );
					
						for( new i; i != COUNT_DISCONNECT_LOG; i++ )
						{
							format:g_small_string( "%s\t%s\t%s\n",
								DisconnectLog[i][disconnectName],
								date( "%hh:%ii:%ss", DisconnectLog[i][disconnectTime] ),
								DisconnectLog[i][disconnectReason]
							);
							
							strcat( g_big_string, g_small_string );
						}
						
						showPlayerDialog( playerid, d_admin + 7, DIALOG_STYLE_TABLIST_HEADERS, " ", g_big_string, "Atr�s", "Cerrar" );
					}
					case 6 :
					{
						new
							server_tick = GetTickCount();
							
						if( GetPVarInt( playerid, "Admin:UpdateTime" ) > server_tick )
							return SendClientMessage(playerid, C_WHITE, !""gbError"No puede utilizar este elemento tan a menudo.");
						
						SetPVarInt( playerid, "Admin:Level", Admin[playerid][aLevel] );
						
						clean:<g_string>;
						mysql_format( mysql, g_string, sizeof g_string, "SELECT * FROM "DB_ADMINS" WHERE aUserID = %d", Player[playerid][uID] );
						mysql_tquery( mysql, g_string, "ReloadAdminAccount", "iii", playerid, 2, server_tick );
					}
				}
			}
			return 1;
		}
		
		case d_admin + 2 : 
		{
		    if( !response )
		        return showPlayerDialog( playerid, d_admin + 1, DIALOG_STYLE_LIST, " ", acontent_acp, "Siguiente", "Cerrar" );
			new
			    level = listitem + 1;
			
			format:g_small_string( ""gbSuccess"Lista de comandos\n - Nivel de acceso: "cBLUE"%d"cWHITE"\n\n", level );
			strcat( g_big_string, g_small_string);

			for( new i; i != COUNT_PERMISSIONS; i++ ) 
			{
				if( Permission[i][pLevel] != level )
					continue;
					
				format:g_small_string( ""cBLUE"/%s"cWHITE" - %s\n", 
					Permission[i][pName],
					Permission[i][pDescription] 
				);
				
				strcat( g_big_string, g_small_string);
			}
			
			showPlayerDialog( playerid, d_admin + 3, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atr�s", "Cerrar" );

		    return 1;
		}
		
		case d_admin + 3 : 
		{
			if( !response )
			    return 1;
				
        	clean_array();
			if( GetAccessAdmin( playerid, 1 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"1"cWHITE"\n" );
			if( GetAccessAdmin( playerid, 2 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"2"cWHITE"\n" );
			if( GetAccessAdmin( playerid, 3 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"3"cWHITE"\n" );
			if( GetAccessAdmin( playerid, 4 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"4"cWHITE"\n" );
			if( GetAccessAdmin( playerid, 5 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"5"cWHITE"\n" );
			if( GetAccessAdmin( playerid, 6 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"6"cWHITE"\n" );
			if( GetAccessAdmin( playerid, 7 ) ) strcat( g_big_string, " - Nivel de acceso\t"cBLUE"7"cWHITE"\n" );
			showPlayerDialog( playerid, d_admin + 2, DIALOG_STYLE_TABLIST, " ", g_big_string, "Siguiente", "Atr�s");
		}
		case d_admin + 4 : 
		{
			if( !response )
			    return 1;

			SetPVarInt( playerid, "Admin:vehicleSelectServerID", g_dialog_select[playerid][listitem] );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;

			showPlayerDialog( playerid, d_admin + 5, DIALOG_STYLE_TABLIST, " ", "\
				- Teletransportarse\n\
				- Eliminar", "Siguiente", "Atr�s");
		}
		case d_admin + 5 : 
		{
			if( !response )
			{
				DeletePVar( playerid, "Admin:VehicleSelectServerID" );
				return cmd_vehlist( playerid );
			}
			
			new
				vehicleid = GetPVarInt( playerid, "Admin:VehicleSelectServerID" );
		
			switch( listitem ) 
			{
				case 0 : 
				{
				    new
				        Float:vehicle_x,
				        Float:vehicle_y,
				        Float:vehicle_z;
				        
					GetVehiclePos( vehicleid, vehicle_x, vehicle_y, vehicle_z );
					setPlayerPos( playerid, vehicle_x + 1.0, vehicle_y + 1.0, vehicle_z );
					
					SetPlayerVirtualWorld( playerid, GetVehicleVirtualWorld( vehicleid ) );
					if( GetVehicleVirtualWorld( vehicleid ) ) SetPlayerInterior( playerid, 1 );
					
                    format:g_small_string( ""gbSuccess"Te has teletransportado con �xito al coche ID "cBLUE"%d"cWHITE".",
                        GetPVarInt( playerid, "Admin:VehicleSelectServerID" )
					);
					SendClient:( playerid, C_WHITE, g_small_string );
				}
				
				case 1 : 
				{					
					format:g_small_string( "%d", GetPVarInt( playerid, "Admin:VehicleSelectServerID" ) );
					cmd_dveh( playerid, g_small_string );
				}
			}
			
			DeletePVar( playerid, "Admin:VehicleSelectServerID" );
		}
		case d_admin + 6 : 
		{
			if( response ) 
			{
				if( inputtext[0] == EOS ) 
						return showPlayerDialog( playerid, d_admin + 6, DIALOG_STYLE_INPUT, " ", acontent_reg, "Siguiente", "Cerrar" );
				
				OnAdminRegister( playerid, inputtext );
			}
		}
		case d_admin + 7 :
		{
			if( response )
				return showPlayerDialog( playerid, d_admin + 1, DIALOG_STYLE_LIST, " ", acontent_acp, "Siguiente", "Cerrar" );
			else
				return 1;
		}
		
		case d_admin + 8 :
		{
			if( !response )
			{
				SetPVarInt( playerid, "Admin:ClickedPlayerid", INVALID_PLAYER_ID );
				return 1;
			}
			
			new 
				clickedplayerid = GetPVarInt( playerid, "Admin:ClickedPlayerid" );
			
			switch( listitem )
			{
				case 0 :
				{
					format:g_small_string( "%d", clickedplayerid );
					cmd_sp( playerid, g_small_string );
				}
				case 1 :
				{
					format:g_small_string( "%d", clickedplayerid );
					cmd_goto( playerid, g_small_string );
				}
				case 2 :
				{
					SendClient:( playerid, C_WHITE, ""gbError"En construcci�n" );
				}
				case 3 :
				{
					format:g_small_string( "%d", clickedplayerid );
					cmd_slap( playerid, g_small_string );
				}
			}

			SetPVarInt( playerid, "Admin:ClickedPlayerid", INVALID_PLAYER_ID );
		}
		
		// Facci�n de cambio de di�logo
		case d_admin + 9:
		{
			if( !response ) return 1;
			
			SetPVarInt( playerid, "Admin:SelectFrac", g_dialog_select[playerid][listitem] );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
			
			showPlayerDialog( playerid, d_admin + 10, DIALOG_STYLE_LIST, " ", ""cWHITE"\
				Informaci�n\n\
				Cambio", "siguiente", "Atr�s" );
		}
		
		case d_admin + 10:
		{
			if( !response )
			{
				DeletePVar( playerid, "Admin:SelectFrac" );
				return cmd_frac( playerid );
			}	
			
			new
				fid = GetPVarInt( playerid, "Admin:SelectFrac" );
			
			switch( listitem )
			{
				case 0:
				{
					clean:<g_big_string>;
					
					strcat( g_big_string, ""cWHITE"Informaci�n de la organizaci�n\n\n" );
					
					format:g_small_string( ""cWHITE"Nombre:\n"cBLUE"%s"cWHITE"", Fraction[fid][f_name] );
					strcat( g_big_string, g_small_string );
					
					format:g_small_string( "\n\n"cWHITE"N�mero de rangos:"cBLUE"%d"cWHITE"", Fraction[fid][f_ranks] );
					strcat( g_big_string, g_small_string );
					
					strcat( g_big_string, "\n\nLideres:" );
					
					for( new i; i < 3; i++ )
					{
						if( Fraction[fid][f_leader][i] )
						{
							format:g_small_string( "\n"cBLUE"%s", FNameLeader[fid][i] );
							strcat( g_big_string, g_small_string );
						}
					}
					
					format:g_small_string( "\n\n"cWHITE"Coches: "cBLUE"%d/%d"cWHITE".", Fraction[fid][f_amountveh], Fraction[fid][f_vehicles] );
					strcat( g_big_string, g_small_string );
					
					format:g_small_string( "\n\n"cWHITE"Salario: "cBLUE"$%d"cWHITE"", Fraction[fid][f_salary] );
					strcat( g_big_string, g_small_string );
					
					strcat( g_big_string, "\n\nSkins de organizaci�n:\n"cBLUE"" );
					
					for( new i; i < 10; i++ )
					{
						if( Fraction[fid][f_skin][i] )
						{
							format:g_small_string( " %d", Fraction[fid][f_skin][i] );
							strcat( g_big_string, g_small_string );
						}
					}
					
					strcat( g_big_string, "\n" );
					
					for( new i = 10; i < 20; i++ )
					{
						if( Fraction[fid][f_skin][i] )
						{
							format:g_small_string( " %d", Fraction[fid][f_skin][i] );
							strcat( g_big_string, g_small_string );
						}
					}
					
					strcat( g_big_string, "\n\n"cWHITE"Armas disponibles:\n"cBLUE"" );
					
					for( new i; i < 10; i++ )
					{
						if( Fraction[fid][f_gun][i] )
						{
							format:g_small_string( " %d", Fraction[fid][f_gun][i] );
							strcat( g_big_string, g_small_string );
						}
					}
					
					strcat( g_big_string, "\n\n"cWHITE"Almac�n:\n"cBLUE"" );
					for( new i; i < 10; i++ )
					{
						if( Fraction[fid][f_stock][i] )
						{
							format:g_small_string( " %d", Fraction[fid][f_stock][i] );
							strcat( g_big_string, g_small_string );
						}
					}
					
					showPlayerDialog( playerid, d_admin + 11, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atr�s", "" );
				}
				
				case 1:
				{
					showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
				}
			}
		}
		
		case d_admin + 11:
		{
			showPlayerDialog( playerid, d_admin + 10, DIALOG_STYLE_LIST, " ", ""cWHITE"\
				Informacion\n\
				Cambio", "Siguiente", "Atr�s" );
		}
		
		
		case d_admin + 12:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_admin + 10, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Informacion\n\
					Cambio", "Siguiente", "Atr�s" );
			}
			
			new
				fid = GetPVarInt( playerid, "Admin:SelectFrac" );
			
			switch( listitem )
			{
				case 0: // cambiar el nombre
				{
					format:g_small_string( "\
						"cBLUE"Cambiar el nombre de la organizaci�n.\n\n\
						"cWHITE"Nombre actual: "cBLUE"%s\n\
						"cWHITE"Ingrese un nuevo nombre:\n\n\
						"gbDialog"Recuerda informar del cambio de nombre.", Fraction[fid][f_name] );
				
					showPlayerDialog( playerid, d_admin + 13, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
				
				case 1:	// Cambiar nombre abreviado
				{
					format:g_small_string( "\
						"cWHITE"Abreviaci�n\n\
						"cBLUE"%s\n\n\
						"cWHITE"Introduzca el nombre abreviado:\n\n\
						"gbDialog"Recuerda informar del cambio de abreviaci�n.", Fraction[fid][f_name] );
				
					showPlayerDialog( playerid, d_admin + 19, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
				
				case 2:	// Cambiar el l�mite de transporte
				{
					format:g_small_string( "\
						"cBLUE"Cambiar el l�mite de coches para\n\
						%s\n\n\
						"cWHITE"Antiguo limite: "cBLUE"%d"cWHITE".\n\
						Introduzca un nuevo l�mite:", Fraction[fid][f_name], Fraction[fid][f_vehicles] );
				
					showPlayerDialog( playerid, d_admin + 14, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
				
				case 3:// cambios a la lista de m�scaras
				{
					format:g_small_string( "\
						"cBLUE"Cambiar los skins disponibles para\n\
						%s\n\n\
						"cWHITE"Entrar en la nueva lista de skins:\n\n\
						"gbDialog"Introduzca 20 valores separados por comas sin espacios.\n\
						"gbDialog"El valor predeterminado es 0.", Fraction[fid][f_name] );
				
					showPlayerDialog( playerid, d_admin + 15, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
				
				case 4:// Cambiar la lista de armas
				{
					format:g_small_string( "\
						"cBLUE"Cambiar las armas disponibles para\n\
						%s\n\n\
						Introduce una nueva lista de armas:\n\n\
						"gbDialog"Introduzca 10 valores separados por comas sin espacios.\n\
						"gbDialog"El valor predeterminado es 0.", Fraction[fid][f_name] );
				
					showPlayerDialog( playerid, d_admin + 16, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
				
				case 5:
				{
					format:g_small_string( "\
						"cBLUE"Cambiar almac�n para\n\
						%s\n\n\
						Introduzca una nueva lista de almacenes:\n\n\
						"gbDialog"Introduzca 10 valores separados por comas sin espacios.\n\
						"gbDialog"El valor predeterminado es 0.", Fraction[fid][f_name] );
				
					showPlayerDialog( playerid, d_admin + 17, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
				
				case 6:
				{
					format:g_small_string( "\
						"cWHITE"Salario total para\n\
						"cBLUE"%s\n\n\
						"cWHITE"Establecer nuevo valor:\n\
						"gbDialog"Se restablecer�n los salarios para todos los rangos de la facci�n.", Fraction[fid][f_name] );
				
					showPlayerDialog( playerid, d_admin + 18, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
			}
		}
		
		case d_admin + 13 :
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
			}
			
			new
				fid = GetPVarInt( playerid, "Admin:SelectFrac" );
				
			if( inputtext[0] == EOS )
			{
				format:g_small_string( "\
						"cBLUE"Cambiar el nombre de la organizaci�n.\n\n\
						"cWHITE"Nombre actual: "cBLUE"%s\n\
						"cWHITE"Ingrese un nuevo nombre:\n\n\
						"gbDialog"Recuerda informar el cambio de nombre.\n\n\
						"gbDialogError"Los campos de entrada est�n en blanco.", Fraction[fid][f_name] );
				
				return showPlayerDialog( playerid, d_admin + 13, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			if( strlen( inputtext ) > 64 )
			{
				format:g_small_string( "\
						"cBLUE"Cambiar el nombre de la organizaci�n.\n\n\
						"cWHITE"Nombre actual:"cBLUE"%s\n\
						"cWHITE"Ingrese un nuevo nombre:\n\n\
						"gbDialog"Recuerda informar el cambio de nombre.\n\n\
						"gbDialogError"L�mite de caracteres superado.", Fraction[fid][f_name] );
				
				return showPlayerDialog( playerid, d_admin + 13, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			clean:<Fraction[fid][f_name]>;
			
			strcat( Fraction[fid][f_name], inputtext, 64 );
			
			mysql_format:g_string( "UPDATE `"DB_FRACTIONS"` SET `f_name` = '%e' WHERE `f_id` = %d",
				Fraction[fid][f_name],
				Fraction[fid][f_id]
			);
			mysql_tquery( mysql, g_string );
			
			pformat:(""gbSuccess"Cambiaste el nombre de la organizaci�n a "cBLUE"%s"cWHITE".", Fraction[fid][f_name] );
			psend:( playerid, C_WHITE );

			showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
		}
		
		case d_admin + 14:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
			}
			
			new
				fid = GetPVarInt( playerid, "Admin:SelectFrac" );
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) == Fraction[fid][f_vehicles] || strval( inputtext ) < 0 || strval( inputtext ) > MAX_FRACVEHICLES )
			{
				format:g_small_string( "\
					"cBLUE"Cambiar el l�mite de coches para\n\
					%s\n\n\
					"cWHITE"Antiguo limite: "cBLUE"%d"cWHITE".\n\
					Introduzca un nuevo l�mite:\n\n\
					"gbDialogError"Formato de entrada no v�lido.", Fraction[fid][f_name], Fraction[fid][f_vehicles] );
				
				return showPlayerDialog( playerid, d_admin + 14, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			Fraction[fid][f_vehicles] = strval( inputtext );
			
			mysql_format:g_string( "UPDATE `"DB_FRACTIONS"` SET `f_vehicles` = %d WHERE `f_id` = %d",
				Fraction[fid][f_vehicles],
				Fraction[fid][f_id]
			);
			mysql_tquery( mysql, g_string );
			
			pformat:(""gbSuccess"Has cambiado el l�mite de coches a "cBLUE"%d"cWHITE".", Fraction[fid][f_vehicles] );
			psend:( playerid, C_WHITE );

			showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
		}
		
		case d_admin + 15:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
			}
		
			new
				fid = GetPVarInt( playerid, "Admin:SelectFrac" );
				
			if( sscanf( inputtext, "p<,>a<d>[20]", inputtext[0], inputtext[1], inputtext[2], inputtext[3], inputtext[4], inputtext[5], inputtext[6], inputtext[7], inputtext[8],
				inputtext[9], inputtext[10], inputtext[11], inputtext[12], inputtext[13], inputtext[14], inputtext[15], inputtext[16], inputtext[17], inputtext[18], inputtext[19] ) ) 
			{
				format:g_small_string( "\
					"cBLUE"Cambiar los skins disponibles para\n\
					%s\n\n\
					"cWHITE"Entrar en la nueva lista de skins:\n\n\
					"gbDialog"Introduzca 20 valores separados por comas sin espacios.\n\
					"gbDialog"El valor predeterminado es 0.\n\n\
					"gbDialogError"Formato de entrada no v�lido.", Fraction[fid][f_name] );
				
				return	showPlayerDialog( playerid, d_admin + 15, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			for( new i; i < 20; i++ )
				Fraction[fid][f_skin][i] = inputtext[i];
			
			mysql_format:g_string( "UPDATE `"DB_FRACTIONS"` SET `f_skin` = '%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d' WHERE `f_id` = %d",
				Fraction[fid][f_skin][0], Fraction[fid][f_skin][1], Fraction[fid][f_skin][2], Fraction[fid][f_skin][3],
				Fraction[fid][f_skin][4], Fraction[fid][f_skin][5], Fraction[fid][f_skin][6], Fraction[fid][f_skin][7],
				Fraction[fid][f_skin][8], Fraction[fid][f_skin][9], Fraction[fid][f_skin][10], Fraction[fid][f_skin][11],
				Fraction[fid][f_skin][12], Fraction[fid][f_skin][13], Fraction[fid][f_skin][14], Fraction[fid][f_skin][15],
				Fraction[fid][f_skin][16], Fraction[fid][f_skin][17], Fraction[fid][f_skin][18], Fraction[fid][f_skin][19],
				Fraction[fid][f_id]
			);
			mysql_tquery( mysql, g_string );

			SendClient:( playerid, C_WHITE, ""gbSuccess"Lista de skins cambiadas." );
		
			showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
		}
		
		case d_admin + 16:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
			}
	
			new
				fid = GetPVarInt( playerid, "Admin:SelectFrac" );
				
			if( sscanf( inputtext, "p<,>a<d>[10]", inputtext[0], inputtext[1], inputtext[2], inputtext[3], inputtext[4], inputtext[5], inputtext[6], inputtext[7], inputtext[8], inputtext[9] ) ) 
			{
				format:g_small_string( "\
					"cBLUE"Cambiar las armas disponibles par\n\
					%s\n\n\
					Introduce una nueva lista de armas:\n\n\
					"gbDialog"Introduzca 10 valores separados por comas sin espacios.\n\
					"gbDialog"El valor predeterminado es 0.\n\n\
					"gbDialogError"Formato de entrada no v�lido.", Fraction[fid][f_name] );
				
				return showPlayerDialog( playerid, d_admin + 16, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
		
			for( new i; i < 10; i++ )
				Fraction[fid][f_gun][i] = inputtext[i];
			
			mysql_format:g_string( "UPDATE `"DB_FRACTIONS"` SET `f_gun` = '%d|%d|%d|%d|%d|%d|%d|%d|%d|%d' WHERE `f_id` = %d",
				Fraction[fid][f_gun][0], Fraction[fid][f_gun][1], Fraction[fid][f_gun][2], Fraction[fid][f_gun][3],
				Fraction[fid][f_gun][4], Fraction[fid][f_gun][5], Fraction[fid][f_gun][6], Fraction[fid][f_gun][7],
				Fraction[fid][f_gun][8], Fraction[fid][f_gun][9],
				Fraction[fid][f_id]
			);
			mysql_tquery( mysql, g_string );

			SendClient:( playerid, C_WHITE, ""gbSuccess"Lista de armas disponibles cambiadas." );
		
			showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
		}
		
		case d_admin + 17:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
			}
	
			new
				fid = GetPVarInt( playerid, "Admin:SelectFrac" );
				
			if( sscanf( inputtext, "p<,>a<d>[10]", inputtext[0], inputtext[1], inputtext[2], inputtext[3], inputtext[4], inputtext[5], inputtext[6], inputtext[7], inputtext[8], inputtext[9] ) ) 
			{
				format:g_small_string( "\
					"cBLUE"Cambiar almac�n para\n\
					%s\n\n\
					Introduzca una nueva lista de almacenes:\n\n\
					"gbDialog"Introduzca 10 valores separados por comas sin espacios.\n\
					"gbDialog"El valor predeterminado es 0.\n\n\
					"gbDialogError"Formato de entrada no v�lido.", Fraction[fid][f_name] );
				
				showPlayerDialog( playerid, d_admin + 17, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
		
			for( new i; i < 10; i++ )
				Fraction[fid][f_stock][i] = inputtext[i];
			
			mysql_format:g_string( "UPDATE `"DB_FRACTIONS"` SET `f_stock` = '%d|%d|%d|%d|%d|%d|%d|%d|%d|%d' WHERE `f_id` = %d",
				Fraction[fid][f_stock][0], Fraction[fid][f_stock][1], Fraction[fid][f_stock][2], Fraction[fid][f_stock][3],
				Fraction[fid][f_stock][4], Fraction[fid][f_stock][5], Fraction[fid][f_stock][6], Fraction[fid][f_stock][7],
				Fraction[fid][f_stock][8], Fraction[fid][f_stock][9],
				Fraction[fid][f_id]
			);
			mysql_tquery( mysql, g_string );

			SendClient:( playerid, C_WHITE, ""gbSuccess"Almac�n cambiado." );
		
			showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
		}
		
		case d_admin + 18:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
			}
			
			new
				fid = GetPVarInt( playerid, "Admin:SelectFrac" );
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 0 || strval( inputtext ) > 1000000 )
			{
				format:g_small_string( "\
						"cWHITE"Salario total para\n\
						"cBLUE"%s\n\n\
						"cWHITE"Establecer nuevo valor:\n\
						"gbDialog"Se restablecer�n los salarios para todos los rangos de la facci�n.\n\n\
						"gbDialogError"Formato de entrada no v�lido.", Fraction[fid][f_name] );
				
				return showPlayerDialog( playerid, d_admin + 18, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			Fraction[fid][f_salary] = strval( inputtext );
			
			mysql_format:g_string( "UPDATE `"DB_FRACTIONS"` SET `f_salary` = %d WHERE `f_id` = %d",
				Fraction[fid][f_salary],
				Fraction[fid][f_id]
			);
			mysql_tquery( mysql, g_string );
			
			pformat:(""gbSuccess"Cambiaste el l�mite de dinero a "cBLUE"$%d"cWHITE". El salario de todos los rangos de la facci�n es cero.", Fraction[fid][f_salary] );
			psend:( playerid, C_WHITE );
			
			for( new i; i < MAX_RANKS; i++ )
			{
				if( FRank[fid][i][r_id] )
					FRank[fid][i][r_salary] = 0;
			}
			
			mysql_format:g_string( "UPDATE `"DB_RANKS"` SET `r_salary` = 0 WHERE `r_fracid` = %d", fid );
			mysql_tquery( mysql, g_string );

			showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
		}
		
		case d_admin + 19:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
			}
			
			new
				fid = GetPVarInt( playerid, "Admin:SelectFrac" );
				
			if( inputtext[0] == EOS )
			{
				format:g_small_string( "\
					"cWHITE"Abreviaci�n para la organizaci�n\n\
					"cBLUE"%s\n\n\
					"cWHITE"Introduzca el nombre abreviado:\n\n\
					"gbDialog"Recuerda avisar del cambio de abreviaci�n.\n\
					"gbDialogError"Los campos de entrada est�n en blanco.", Fraction[fid][f_name] );
				
				return showPlayerDialog( playerid, d_admin + 19, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			if( strlen( inputtext ) > 10 )
			{
				format:g_small_string( "\
					"cWHITE"Abreviaci�n para la organizaci�n\n\
					"cBLUE"%s\n\n\
					"cWHITE"Introduzca el nombre abreviado:\n\n\
					"gbDialog"Recuerda avisar del cambio de abreviaci�n.\n\
					"gbDialogError"L�mite de caracteres superado.", Fraction[fid][f_name] );
				
				return showPlayerDialog( playerid, d_admin + 19, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			clean:<Fraction[fid][f_short_name]>;
			
			strcat( Fraction[fid][f_short_name], inputtext, 10 );
			
			mysql_format:g_string( "UPDATE `"DB_FRACTIONS"` SET `f_short_name` = '%e' WHERE `f_id` = %d",
				Fraction[fid][f_short_name],
				Fraction[fid][f_id]
			);
			mysql_tquery( mysql, g_string );
			
			pformat:(""gbSuccess"Has cambiado la abreviaci�n de la organizaci�n a "cBLUE"%s"cWHITE".", Fraction[fid][f_short_name] );
			psend:( playerid, C_WHITE );

			showPlayerDialog( playerid, d_admin + 12, DIALOG_STYLE_LIST, "Cambiando....", fraction_change, "Siguiente", "Atr�s" );
		}
		
		// Panel de la facci�n criminal.
		case d_admin + 20:
		{
			if( !response ) return 1;
			
			if( !listitem )
			{
				showPlayerDialog( playerid, d_admin + 32, DIALOG_STYLE_LIST, "Seleccione tipo", "\
					Pandilla\n\
					Mafia\n\
					Motoclub\n\
					Otros", "Seleccione", "Atr�s" );
			}
			else
			{
				SetPVarInt( playerid, "Crime:ID", g_dialog_select[playerid][listitem - 1] );
				g_dialog_select[playerid][listitem - 1] = INVALID_PARAM;
			
				showPlayerDialog( playerid, d_admin + 21, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Informaci�n\n\
					Cambiar\n\
					Eliminar", "Siguiente", "Atr�s" );
			}
		}
		
		case d_admin + 21:
		{
			if( !response )
			{
				DeletePVar( playerid, "Crime:ID" );
				return cmd_cfrac( playerid );
			}
			
			new
				crime = GetPVarInt( playerid, "Crime:ID" );
				
			switch( listitem )
			{
				case 0:
				{
					clean:<g_string>;
				
					format:g_small_string( "\
						"cWHITE"Informaci�n sobre la estructura criminal. #%d\n\
						"cBLUE"%s"cWHITE"",
						CrimeFraction[crime][c_id],
						CrimeFraction[crime][c_name] );
					strcat( g_string, g_small_string );
					
					strcat( g_string, "\n\nLideres:" );
					
					for( new i; i < 3; i++ )
					{
						if( CrimeFraction[ crime ][c_leader][i] )
						{
							format:g_small_string( "\n"cBLUE"%s", CNameLeader[crime][i] );
							strcat( g_string, g_small_string );
						}
					}
					
					format:g_small_string( "\n\n"cWHITE"Veh�culos: "cBLUE"%d/%d"cWHITE".", CrimeFraction[crime][c_amountveh], CrimeFraction[crime][c_vehicles] );
					strcat( g_string, g_small_string );
					
					strcat( g_string, "\n\nPosibilidad de adquirir veh�culos:" );
					
					if( CrimeFraction[crime][c_type_vehicles] )
					{
						strcat( g_string, "\n"cBLUE"S�" );
					}
					else
					{
						strcat( g_string, "\n"cBLUE"No" );
					}
					
					strcat( g_string, "\n\n"cWHITE"Armas en traficante:" );
					
					switch( CrimeFraction[crime][c_type_weapon] )
					{
						case 0: strcat( g_string, "\n"cBLUE"No" );
						case 1: strcat( g_string, "\n"cBLUE"1 grupo" );
						case 2: strcat( g_string, "\n"cBLUE"2 grupo" );
						case 3: strcat( g_string, "\n"cBLUE"3 grupo" );
					}
					
					showPlayerDialog( playerid, d_admin + 22, DIALOG_STYLE_MSGBOX, " ", g_string, "Atr�s", "" );
				}
				
				case 1:
				{
					showPlayerDialog( playerid, d_admin + 23, DIALOG_STYLE_LIST, "Cambiando....", crime_change, "Seleccione", "Atr�s" );
				}
				
				case 2:
				{
					format:g_small_string( "\
						"cBLUE"Eliminaci�n de la estructura criminal #%d\n\n\
						"cWHITE"�Realmente quieres borrar la EC "cBLUE"%s"cWHITE"?",
						CrimeFraction[crime][c_id],
						CrimeFraction[crime][c_name]						
					);
					
					showPlayerDialog( playerid, d_admin + 29, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
				}
			}
		}
		
		case d_admin + 22:
		{
			return showPlayerDialog( playerid, d_admin + 21, DIALOG_STYLE_LIST, " ", ""cWHITE"\
				Informaci�n\n\
				Cambiar\n\
				Eliminar", "Siguiente", "Atr�s" );
		}
		
		case d_admin + 23:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_admin + 21, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Informaci�n\n\
					Cambiar\n\
					Eliminar", "Siguiente", "Atr�s" );
			}
			
			new
				crime = GetPVarInt( playerid, "Crime:ID" );
			
			switch( listitem )
			{	// Cambiando. el nombre
				case 0:
				{
					format:g_small_string( "\
						"cBLUE"Cambiar el nombre de la estructura criminal.\n\n\
						"cWHITE"Nombre actual: "cBLUE"%s\n\
						"cWHITE"Ingrese un nombre nuevo:\n\n\
						"gbDialog"Recuerda avisar del cambio de nombre.", CrimeFraction[crime][c_name] );
				
					showPlayerDialog( playerid, d_admin + 24, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
				// Cambiando. l�mite de transporte
				case 1:
				{
					format:g_small_string( "\
						"cBLUE"Cambiar el l�mite de coches para\n\
						%s\n\n\
						"cWHITE"Antiguo l�mite: "cBLUE"%d"cWHITE".\n\
						Ingrese un nuevo l�mite:", CrimeFraction[crime][c_name], CrimeFraction[crime][c_vehicles] );
				
					showPlayerDialog( playerid, d_admin + 25, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
				}
				// Armas Traficante
				case 2:
				{
					showPlayerDialog( playerid, d_admin + 26, DIALOG_STYLE_LIST, "Armas", weapon_change, "Seleccionar", "Atr�s" );
				}
				// Accesibilidad al veh�culo.
				case 3:
				{
					format:g_small_string( crime_vehicle, 
						CrimeFraction[crime][c_type_vehicles] ? ("S�") : ("No")
					);
					showPlayerDialog( playerid, d_admin + 28, DIALOG_STYLE_TABLIST_HEADERS, "Veh�culos", g_small_string, "Cambiar", "Atr�s" );
				}
			}
		}
		
		case d_admin + 24:
		{
			if( !response )
				return showPlayerDialog( playerid, d_admin + 23, DIALOG_STYLE_LIST, "Cambiando....", crime_change, "Seleccionar", "Atr�s" );
		
			new
				crime = GetPVarInt( playerid, "Crime:ID" );
		
			if( inputtext[0] == EOS )
			{
				format:g_small_string( "\
						"cBLUE"Cambiar el nombre de la estructura criminal\n\n\
						"cWHITE"Nombre actual: "cBLUE"%s\n\
						"cWHITE"Ingrese un nuevo nombre:\n\n\
						"gbDialog"Recuerda avisar del cambio de nombre.\n\n\
						"gbDialogError"Los campos de entrada est�n en blanco.", CrimeFraction[crime][c_name] );
				
				return showPlayerDialog( playerid, d_admin + 24, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			if( strlen( inputtext ) > 64 )
			{
				format:g_small_string( "\
						"cBLUE"Cambiar el nombre de la estructura criminal\n\n\
						"cWHITE"Nombre actual: "cBLUE"%s\n\
						"cWHITE"Ingrese un nuevo nombre:\n\n\
						"gbDialog"Recuerda avisar el cambio de nombre.\n\n\
						"gbDialogError"L�mite de caracteres superado.", CrimeFraction[crime][c_name] );
				
				return showPlayerDialog( playerid, d_admin + 24, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			clean:<CrimeFraction[crime][c_name]>;
			strcat( CrimeFraction[crime][c_name], inputtext, 64 );
			
			mysql_format:g_string( "UPDATE `"DB_CRIME"` SET `c_name` = '%e' WHERE `c_id` = %d",
				CrimeFraction[crime][c_name],
				CrimeFraction[crime][c_id]
			);
			mysql_tquery( mysql, g_string );
			
			pformat:(""gbSuccess"Cambiaste el nombre de la estructura criminal a "cBLUE"%s"cWHITE".", CrimeFraction[crime][c_name] );
			psend:( playerid, C_WHITE );

			showPlayerDialog( playerid, d_admin + 23, DIALOG_STYLE_LIST, "Cambiando....", crime_change, "Seleccionar", "Atr�s" );
		}
		
		case d_admin + 25:
		{
			if( !response )
				return showPlayerDialog( playerid, d_admin + 23, DIALOG_STYLE_LIST, "Cambiando....", crime_change, "Seleccionar", "Atr�s" );
			
			new
				crime = GetPVarInt( playerid, "Crime:ID" );
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) == CrimeFraction[crime][c_vehicles] || strval( inputtext ) < 0 || strval( inputtext ) > MAX_FRACVEHICLES )
			{
				format:g_small_string( "\
					"cBLUE"Cambiar el l�mite de veh�culos para\n\
					%s\n\n\
					"cWHITE"Antiguo l�mite: "cBLUE"%d"cWHITE".\n\
					Ingrese un nuevo l�mite:\n\n\
					"gbError"Formato de entrada no v�lido.", CrimeFraction[crime][c_name], CrimeFraction[crime][c_vehicles] );
				
				return showPlayerDialog( playerid, d_admin + 25, DIALOG_STYLE_INPUT, " ", g_small_string, "Siguiente", "Atr�s" );
			}
			
			CrimeFraction[crime][c_vehicles] = strval( inputtext );
			
			mysql_format:g_string( "UPDATE `"DB_CRIME"` SET `c_vehicles` = %d WHERE `c_id` = %d",
				CrimeFraction[crime][c_vehicles],
				CrimeFraction[crime][c_id]
			);
			mysql_tquery( mysql, g_string );
			
			pformat:(""gbSuccess"Has cambiado el l�mite de veh�culos a "cBLUE"%d"cWHITE".", CrimeFraction[crime][c_vehicles] );
			psend:( playerid, C_WHITE );

			showPlayerDialog( playerid, d_admin + 23, DIALOG_STYLE_LIST, "Cambiando....", crime_change, "Seleccionar", "Atr�s" );
		}
		
		case d_admin + 26:
		{
			if( !response )
				return showPlayerDialog( playerid, d_admin + 23, DIALOG_STYLE_LIST, "Cambiando....", crime_change, "Seleccionar", "Atr�s" );
				
			if( listitem == 4 )
			{
				return showPlayerDialog( playerid, d_admin + 27, DIALOG_STYLE_MSGBOX, " ", weapon_info, "Atr�s", "" );
			}
			
			new
				crime = GetPVarInt( playerid, "Crime:ID" );
				
			if( CrimeFraction[crime][c_type_weapon] == listitem )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Esta estructura criminal ya tiene este grupo de armas." );
				return showPlayerDialog( playerid, d_admin + 26, DIALOG_STYLE_LIST, "Armas", weapon_change, "Seleccionar", "Atr�s" );
			}
			
			CrimeFraction[crime][c_type_weapon] = listitem;
			
			mysql_format:g_string( "UPDATE `"DB_CRIME"` SET `c_type_weapon` = %d WHERE `c_id` = %d",
				CrimeFraction[crime][c_type_weapon],
				CrimeFraction[crime][c_id]
			);
			mysql_tquery( mysql, g_string );
			
			SendClient:( playerid, C_WHITE, !""gbSuccess"Has cambiado el grupo de armas para la estructura criminal." );
			
			showPlayerDialog( playerid, d_admin + 23, DIALOG_STYLE_LIST, "Cambiando....", crime_change, "Seleccionar", "Atr�s" );
		}
		
		case d_admin + 27:
		{
			showPlayerDialog( playerid, d_admin + 26, DIALOG_STYLE_LIST, "Armas", weapon_change, "Seleccionar", "Atr�s" );
		}
		
		case d_admin + 28:
		{
			if( !response )
				return showPlayerDialog( playerid, d_admin + 23, DIALOG_STYLE_LIST, "Cambiando....", crime_change, "Seleccionar", "Atr�s" );
				
			new
				crime = GetPVarInt( playerid, "Crime:ID" );
				
			if( CrimeFraction[crime][c_type_vehicles] )
				CrimeFraction[crime][c_type_vehicles] = 0;
			else
				CrimeFraction[crime][c_type_vehicles] = 1;
				
			mysql_format:g_string( "UPDATE `"DB_CRIME"` SET `c_type_vehicles` = '%d' WHERE `c_id` = %d",
				CrimeFraction[crime][c_type_vehicles],
				CrimeFraction[crime][c_id]
			);
			mysql_tquery( mysql, g_string );
			
			format:g_small_string( crime_vehicle, 
				CrimeFraction[crime][c_type_vehicles] ? ("S�") : ("No")
			);
			showPlayerDialog( playerid, d_admin + 28, DIALOG_STYLE_TABLIST_HEADERS, "Veh�culos", g_small_string, "Cambiar", "Atr�s" );
		}
		
		case d_admin + 29:
		{
			if( !response )
				return showPlayerDialog( playerid, d_admin + 23, DIALOG_STYLE_LIST, "Cambiando....", crime_change, "Seleccionar", "Atr�s" );
				
			new
				crime = GetPVarInt( playerid, "Crime:ID" );
				
			if( !CrimeFraction[crime][c_id] )
			{
				SendClient:( playerid, C_WHITE, !""gbDefault"Esta estructura criminal ha sido eliminada anteriormente." );
			
				DeletePVar( playerid, "Crime:ID" );
				return cmd_cfrac( playerid );
			}
				
			mysql_format:g_small_string( "DELETE FROM `"DB_CRIME"` WHERE `c_id` = %d LIMIT 1", CrimeFraction[crime][c_id] );
			mysql_tquery( mysql, g_small_string );
			
			mysql_format:g_small_string( "DELETE FROM `"DB_CRIME_RANKS"` WHERE `r_fracid` = %d", CrimeFraction[crime][c_id] );
			mysql_tquery( mysql, g_small_string );
			
			mysql_format:g_small_string( "DELETE FROM `"DB_VEHICLES"` WHERE `vehicle_crime` = %d", CrimeFraction[crime][c_id] );
			mysql_tquery( mysql, g_small_string );
			
			mysql_format:g_small_string( "UPDATE `"DB_USERS"` SET `uCrimeL` = 0, `uCrimeM` = 0, `uCrimeRank` = 0 WHERE `uCrimeM` = %d", CrimeFraction[crime][c_id] );
			mysql_tquery( mysql, g_small_string );
			
			foreach(new i: Player)
			{
				if( !IsLogged(i) || Player[i][uCrimeM] != CrimeFraction[crime][c_id] ) continue;
				
				Player[i][uCrimeM] = 
				Player[i][uCrimeL] = 
				Player[i][uCrimeRank] = 0;
				
				pformat:( !""gbDialog"La organizaci�n criminal ha sido disuelta por el administrador %s [%d].", Player[playerid][uName], playerid );
				psend:( i, C_WHITE );
			}
			
			for( new i; i < CrimeFraction[crime][c_amountveh]; i++ )
			{
				DestroyVehicleEx( CVehicle[ crime ][i][v_id] );
				
				mysql_format:g_string( "DELETE FROM `"DB_ITEMS"` WHERE `item_type_id` = %d AND `item_type` = 2", Vehicle[ CVehicle[ crime ][i][v_id] ][vehicle_id] );	
				mysql_tquery( mysql, g_string );
				
				CVehicle[ crime ][i][v_id] = 0;
				CVehicle[ crime ][i][v_number][0] = EOS;
			}
			
			for( new i; i < MAX_RANKS; i++ )
			{
				if( CrimeRank[crime][i][r_id] )
				{
					CrimeRank[crime][i][r_id] =
					CrimeRank[crime][i][r_fracid] =
					CrimeRank[crime][i][r_invite] =
					CrimeRank[crime][i][r_uninvite] =
					CrimeRank[crime][i][r_attach] =
					CrimeRank[crime][i][r_spawnveh] =
					CrimeRank[crime][i][r_call_weapon] = 0;
					
					CrimeRank[crime][i][r_name][0] =
					CrimeRank[crime][i][r_vehicles][0] =
					CrimeRank[crime][i][r_world][0] = EOS;
					
					for( new j; j < 4; j++ ) CrimeRank[crime][i][r_spawn][j] = 0.0;
				}
			}
			
			for( new i; i < 3; i++ )
			{
				CNameLeader[crime][i][0] = EOS;
			}
			
			pformat:( ""gbSuccess"Estructura criminal "cBLUE"%s"cWHITE" eliminado con �xito.", CrimeFraction[crime][c_name] );
			psend:( playerid, C_WHITE );
			
			CrimeFraction[crime][c_id] =
			CrimeFraction[crime][c_vehicles] =
			CrimeFraction[crime][c_ranks] =
			CrimeFraction[crime][c_amountveh] =
			CrimeFraction[crime][c_members] =
			CrimeFraction[crime][c_type_vehicles] =
			CrimeFraction[crime][c_type_weapon] = 0;
			
			CrimeFraction[crime][c_name][0] = 
			CrimeFraction[crime][c_leader][0] = EOS;
			
			CrimeFraction[crime][c_time] = 
			CrimeFraction[crime][c_time_dealer] = 0;
			
			CrimeFraction[crime][c_index_dealer] = INVALID_PARAM;
			
			COUNT_CRIMES --;
			
			DeletePVar( playerid, "Crime:ID" );
			cmd_cfrac( playerid );
		}
		
		case d_admin + 30:
		{
			if( !response ) return 1;
			
			SetPVarInt( playerid, "Admin:CrimePoint", listitem );
			
			showPlayerDialog( playerid, d_admin + 31, DIALOG_STYLE_LIST, "Acciones", "\
				- Teletransportarse\n\
				- Eliminar", "Seleccionar", "Atr�s" );
		}
		
		case d_admin + 31:
		{
			if( !response ) return cmd_showpoint( playerid );
			
			new
				index = GetPVarInt( playerid, "Admin:CrimePoint" );
			
			switch( listitem )
			{
				case 0:
				{
					setPlayerPos( playerid, GunDealer[index][g_actor_pos][0], GunDealer[index][g_actor_pos][1], GunDealer[index][g_actor_pos][2] );
					SetPlayerVirtualWorld( playerid, 0 );
					SetPlayerInterior( playerid, 0 );
					
					pformat:( ""gbSuccess"Has sido teletransportado con �xito al punto de el traficante ID %d.", GunDealer[index][g_id] );
					psend:( playerid, C_WHITE );
				}
				
				case 1:
				{
					if( GunDealer[index][g_fracid] )
					{
						SendClient:( playerid, C_WHITE, !""gbDefault"No puedes borrar el checkpoint." );
						return showPlayerDialog( playerid, d_admin + 31, DIALOG_STYLE_LIST, "Acciones", "\
							- Teletransportarse\n\
							- Eliminar", "Seleccionar", "Atr�s" );
					}
				
					mysql_format:g_small_string( "DELETE FROM `"DB_CRIME_GUNDEALER"` WHERE `g_id` = %d LIMIT 1", GunDealer[index][g_id] );
					mysql_tquery( mysql, g_small_string );
					
					format:g_small_string( ""ADMIN_PREFIX" %s [%d] punto de traficante eliminado, ID %d en el area %s",
						Player[playerid][uName],
						playerid,
						GunDealer[index][g_id], GunDealer[index][g_zone] );
						
					SendAdmin:( C_DARKGRAY, g_small_string );
					
					GunDealer[index][g_id] = 
					GunDealer[index][g_actor] = 
					GunDealer[index][g_actor_id] = 
					GunDealer[index][g_car] = 
					GunDealer[index][g_car_id] = 
					GunDealer[index][g_fracid] = 
					GunDealer[index][g_time] = 0;
					
					for( new i; i < 4; i++ )
					{
						GunDealer[index][g_actor_pos][i] =
						GunDealer[index][g_car_pos][i] = 0;
					}
				}
			}
			
			DeletePVar( playerid, "Admin:CrimePoint" );
		}
		
		case d_admin + 32:
		{
			if( !response )
			{
				return cmd_cfrac( playerid );
			}
		
			for( new i; i < MAX_CRIMINAL; i++ )
			{
				if( !CrimeFraction[i][c_id] )
				{
					CrimeFraction[i][c_type] = listitem;
				
					mysql_format:g_string( "INSERT INTO `"DB_CRIME"` ( `c_name`, `c_type`, `c_leader` ) VALUES ( 'Crime Fraction', %d, '0|0|0' )", CrimeFraction[i][c_type] );
					mysql_tquery( mysql, g_string, "InsertCrimeFraction", "di", playerid, i );
				
					clean:<CrimeFraction[i][c_name]>;
					strcat( CrimeFraction[i][c_name], "New Crime Fraction", 64 );
					
					CrimeFraction[i][c_index_dealer] = INVALID_PARAM;
					
					return 1;
				}
			}
				
			SendClient:( playerid, C_WHITE, !""gbError"Error. L�mite de las estructuras criminales superado." );
			cmd_cfrac( playerid );
		}

		case d_makeleader :
		{
			if( !response )
			{
				DeletePVar( playerid, "UnLeader:Fraction" );
				return 1;
			}
				
			new 
				fid = GetPVarInt( playerid, "UnLeader:Fraction" );
				
			if( !Fraction[fid][f_leader][listitem] )
			{
				SendClient:( playerid, C_WHITE, ""gbError"No hay l�der en esta facci�n." );
				format:g_small_string( "%d", fid + 1 );
				cmd_unleader( playerid, g_small_string );
				return 1;
			}
			
			SetPVarInt( playerid, "UnLeader:Select", listitem );
		
			format:g_small_string( "\
				"cBLUE"Eliminar al l�der \n\n\
				"cWHITE"Realmente quieres eliminar a "cBLUE"%s"cWHITE" como l�der de la organizaci�n "cBLUE"%s"cWHITE".",
				FNameLeader[fid][listitem], Fraction[fid][f_name] );
			
			showPlayerDialog( playerid, d_makeleader + 1, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
		}
		
		case d_makeleader + 1 : 
		{
			new 
				fid = GetPVarInt( playerid, "UnLeader:Fraction" ),
				index = GetPVarInt( playerid, "UnLeader:Select" );
		
			if( !response )
			{
				format:g_small_string( "%d", fid + 1 );
				return cmd_unleader( playerid, g_small_string );
			}
				
			if( !Fraction[fid][f_leader][index] )
			{
				SendClient:( playerid, C_WHITE, ""gbError"Este l�der ya ha sido eliminado." );
				format:g_small_string( "%d", fid + 1 );
				return cmd_unleader( playerid, g_small_string );
			}
			
			foreach(new i: Player)
			{
				if( IsLogged( i ) && Fraction[fid][f_leader][index] == Player[i][uID] )
				{
					Player[i][uLeader] = 
					Player[i][uMember] = 
					Player[i][uRank] = 0;
					
					break;
				}
			}

			clean:<g_string>;
			mysql_format( mysql, g_string, sizeof g_string, 
				"\
					UPDATE "DB_USERS" \
					SET \
						uLeader = 0, \
						uMember = 0, \
						uRank = 0 \
					WHERE uID = %d\
				",
				Fraction[fid][f_leader][index]
			);
			mysql_tquery( mysql, g_string );
			
			format:g_small_string("" #ADMIN_PREFIX " %s [%d] expulso a %s como l�der de la organizaci�n '%s'.",
				GetAccountName( playerid ),
				playerid,
				FNameLeader[fid][index],
				Fraction[fid][f_name]
			);
				
			SendAdmin:( C_GRAY, g_small_string );
			
			Fraction[fid][f_leader][index] = 0;
			clean:<FNameLeader[fid][index]>;
			
			clean:<g_string>;
			mysql_format( mysql, g_string, sizeof g_string, 
				"\
					UPDATE `"DB_FRACTIONS"` \
					SET \
						`f_leader` = '%d|%d|%d', \
						`f_leadername_1` = '%e', \
						`f_leadername_2` = '%e', \
						`f_leadername_3` = '%se' \
					WHERE `f_id` = %d\
				",
				Fraction[fid][f_leader][0],
				Fraction[fid][f_leader][1],
				Fraction[fid][f_leader][2],
				FNameLeader[fid][0],
				FNameLeader[fid][1],
				FNameLeader[fid][2],
				Fraction[fid][f_id]
			);		
			mysql_tquery( mysql, g_string );
			
			DeletePVar( playerid, "UnLeader:Fraction" );
			DeletePVar( playerid, "UnLeader:Select" );
			
			return 1;
		}
		
		case d_makeleader + 2:
		{
			if( !response )
			{
				DeletePVar( playerid, "UnLeader:Crime" );
				return 1;
			}
			
			new 
				crime = GetPVarInt( playerid, "UnLeader:Crime" );
			
			SetPVarInt( playerid, "UnLeader:Select", listitem );
		
			format:g_small_string( "\
				"cBLUE"Eliminar l�der\n\n\
				"cWHITE"Realmente quieres expulsar "cBLUE"%s"cWHITE" de el puesto de l�der de la estructura criminal "cBLUE"%s"cWHITE".",
				CNameLeader[crime][listitem], CrimeFraction[crime][c_name] );
			
			showPlayerDialog( playerid, d_makeleader + 3, DIALOG_STYLE_MSGBOX, " ", g_small_string, "S�", "No" );
		}
		
		case d_makeleader + 3:
		{
			new 
				crime = GetPVarInt( playerid, "UnLeader:Crime" ),
				index = GetPVarInt( playerid, "UnLeader:Select" );
		
			if( !response )
			{
				format:g_small_string( "%d", CrimeFraction[crime][c_id] );
				return cmd_uncleader( playerid, g_small_string );
			}
				
			if( !CrimeFraction[crime][c_leader][index] )
			{
				SendClient:( playerid, C_WHITE, ""gbError"Este l�der ya ha sido eliminado." );
				format:g_small_string( "%d", CrimeFraction[crime][c_id] );
				return cmd_uncleader( playerid, g_small_string );
			}
			
			foreach(new i: Player)
			{
				if( IsLogged( i ) && CrimeFraction[crime][c_leader][index] == Player[i][uID] )
				{
					Player[i][uCrimeL] = 
					Player[i][uCrimeM] = 
					Player[i][uCrimeRank] = 0;
					
					break;
				}
			}

			mysql_format:g_string(
				"\
					UPDATE "DB_USERS" \
					SET \
						uCrimeL = 0, \
						uCrimeM = 0, \
						uCrimeRank = 0 \
					WHERE uID = %d\
				",
				CrimeFraction[crime][c_leader][index]
			);
			mysql_tquery( mysql, g_string );
			
			format:g_small_string("" #ADMIN_PREFIX " %s [%d] expulso a %s del puesto de l�der de la estructura criminal '%s'.",
				GetAccountName( playerid ),
				playerid,
				CNameLeader[crime][index],
				CrimeFraction[crime][c_name]
			);
				
			SendAdmin:( C_GRAY, g_small_string );
			
			CrimeFraction[crime][c_leader][index] = 0;
			clean:<CNameLeader[crime][index]>;
			
			mysql_format:g_string(
				"\
					UPDATE `"DB_CRIME"` \
					SET \
						`c_leader` = '%d|%d|%d', \
						`c_leadername_1` = '%e', \
						`c_leadername_2` = '%e', \
						`c_leadername_3` = '%e' \
					WHERE `c_id` = %d\
				",
				CrimeFraction[crime][c_leader][0],
				CrimeFraction[crime][c_leader][1],
				CrimeFraction[crime][c_leader][2],
				CNameLeader[crime][0],
				CNameLeader[crime][1],
				CNameLeader[crime][2],
				CrimeFraction[crime][c_id]
			);		
			mysql_tquery( mysql, g_string );
			
			DeletePVar( playerid, "UnLeader:Crime" );
			DeletePVar( playerid, "UnLeader:Select" );
		}
		
		case d_support :
		{
			if( !response )
				return 1;
				
			switch( listitem )
			{
				case 0 :
				{
					new
					    fmt_player[ 32 + MAX_PLAYER_NAME + 2 ],
						fmt_spectate[ 64 ],
						fmt_afk [ 32 ],
						admin_count = 0,
						support_count = 0;
						
					clean:<g_big_string>;
					
					strcat( g_big_string, ""gbDefault"Administradores online:\n\n" );
					
					foreach(new i : Player) 
					{
						if( !IsLogged( i ) || !GetAccessAdmin( i, 1, false ) ) 
							continue;
						
						format( fmt_player, sizeof fmt_player, "(%d) "cBLUE"%s [%d]"cWHITE"",
							Admin[i][aLevel],
							Player[i][uName],
							i
						);
						
						if( GetPVarInt( i, "Admin:SpectateId" ) != INVALID_PLAYER_ID ) 
						{
							format( fmt_spectate, sizeof fmt_spectate, " - "cGREEN"/sp %d"cWHITE"",
							    GetPVarInt( i, "Admin:SpectateId" )
							);
						}
						
						if( IsAfk( i ) ) 
						{
                               format( fmt_afk, sizeof fmt_afk, " - "cGRAY"[AFK: %d]"cWHITE"",
							    GetPVarInt( i, "Player:Afk" )
							);
						}
						
						
						format:g_small_string(  "%s%s%s%s\n",
							fmt_player,
							GetPVarInt( i, "Admin:SpectateId" ) != INVALID_PLAYER_ID ? fmt_spectate : (""),
							IsAfk( i ) ? fmt_afk : (""),
							!GetPVarInt( i, "Admin:Duty" ) ? (" - "cRED"[No autorizado]"cGRAY"") : ("")
						);
						
						++admin_count;
						strcat( g_big_string, g_small_string );
					}
					
					strcat( g_big_string, "\n\n\n"gbDefault"Soporte en linea:\n\n" );
					
					foreach(new i : Player) 
					{
						if( !IsLogged( i ) || !GetAccessSupport( i ) ) 
							continue;
						
						if( IsAfk( i ) ) 
						{
                               format( fmt_afk, sizeof fmt_afk, " - "cGRAY"[AFK: %d]",
							    GetPVarInt( i, "Player:Afk" )
							);
						}
						
						format( fmt_player, sizeof fmt_player, ""cBLUE"%s[%d]%s%s"cWHITE"\n",
							Player[i][uName],
							i,
							IsAfk( i ) ? fmt_afk : (""),
							!GetPVarInt( i, "Support:Duty" ) ? (" - "cGRAY"[Fuera de servicio]"cWHITE"") : ("")
						);
						
						++support_count;
						
						strcat( g_big_string, fmt_player );
					}
					
					if( support_count == 0 )
					    strcat( g_big_string, "No hay soporte en linea.\n" );
					
					format:g_small_string(  "\n\nTotal de administradores en linea: "cBLUE"%d\n"cWHITE"Total de soporte en linea: "cBLUE"%d",
						admin_count,
						support_count
					);
					
					strcat( g_big_string, g_small_string );
				
					showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Cerrar", "");
				}
				
				case 1 :
				{
					clean:<g_big_string>;
					strcat( g_big_string, "" #gbSuccess "Lista de comandos\n\n" );
					strcat( g_big_string, "\
						"cBLUE"/h"cWHITE" - Soporte en chat\n\
						"cBLUE"/scp"cWHITE" - Panel de soporte\n\
						"cBLUE"/sduty"cWHITE" - Entrar / salir de servicio\n\
						"cBLUE"/ans"cWHITE" - Responder al jugador" );
					
					showPlayerDialog( playerid, d_support + 1, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atr�s", "Cerrar" );
				}
				
				case 2 :
				{
					clean:<g_big_string>;
					strcat( g_big_string, ""gbSuccess"Estadisticas\n\n" );
					format:g_small_string( " - Identificador: "cBLUE"%d"cWHITE"\n", Support[playerid][sID] ), strcat( g_big_string, g_small_string );
					format:g_small_string( " - Nombre: "cBLUE"%s"cWHITE"\n", Support[playerid][sName] ), strcat( g_big_string, g_small_string );

					format:g_small_string( " - Horas jugadas: "cBLUE"-"cWHITE"\n"), strcat( g_big_string, g_small_string );
					format:g_small_string( "     Por dia:"cBLUE"-"cWHITE"\n" ), strcat( g_big_string, g_small_string );
					format:g_small_string( "     Por semana: "cBLUE"-"cWHITE"\n\n" ), strcat( g_big_string, g_small_string );

					format:g_small_string( " - Respuestas totales: "cBLUE"-"cWHITE"\n" ), strcat( g_big_string, g_small_string );
					showPlayerDialog( playerid, d_support + 1, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Atr�s", "Cerrar");
				}
			}
		}
		
		case d_support + 1 :
		{
			if( response )
				return showPlayerDialog( playerid, d_support, DIALOG_STYLE_LIST, " ", hcontent_hcp, "Siguiente", "Cerrar" );
		}
	
	}
	
	return 1;
}