function House_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{
	switch( dialogid ) 
	{
		case d_house :
		{
			if( !response )	
			{
				DeletePVar( playerid, "House:EnterId" );
				return 1;
			}
			
			new
				h = GetPVarInt( playerid, "House:EnterId" );
			
			if( !IsPlayerInRangeOfPoint( playerid, 2.0, HouseInfo[h][hEnterPos][0], HouseInfo[h][hEnterPos][1], HouseInfo[h][hEnterPos][2] ) )
			{
				DeletePVar( playerid, "House:EnterId" );
				return SendClient:( playerid, C_WHITE, !""gbError"Debes estar cerca de la puerta de entrada." );
			}
				
			SetPlayerInterior( playerid, 1 );
			SetPlayerVirtualWorld( playerid, HouseInfo[h][hID] );
			
			Player[playerid][tgpsPos][0] = HouseInfo[h][hEnterPos][0];
			Player[playerid][tgpsPos][1] = HouseInfo[h][hEnterPos][1];
			Player[playerid][tgpsPos][2] = HouseInfo[h][hEnterPos][2];
	
			setPlayerPos( playerid, HouseInfo[h][hExitPos][0], HouseInfo[h][hExitPos][1], HouseInfo[h][hExitPos][2] );
			SetPlayerFacingAngle( playerid, HouseInfo[h][hExitPos][3] );
			
			setHouseWeather( playerid );
		}
	
		case d_buy_menu: 
		{
			if( !response )
			{
				g_player_interaction{playerid} = 0;
				return 1;
			}
			
			switch( listitem ) 
			{
				case 0: 
				{ // Cat�logo de la casa
					showPlayerDialog( playerid, d_buy_menu + 1, DIALOG_STYLE_LIST, " ", dialog_house_buy, "Seleccionar", "Atr�s" );
				}
				
				case 1: 
				{ // Directorio de negocios
					showPlayerDialog( playerid, d_buy_business + 1, DIALOG_STYLE_LIST, " ", 
						"1. Lista de negocios\n2. Ordenar por costo\
						\n3. Ordenar por tipo de negocio\n4. Comprar un negocio por n�mero\
						\n5. Vender un negocio", "Seleccionar", "Atr�s" );
				}
			}
		
		}
		
		case d_buy_menu + 1: 
		{
			if( !response ) 
			{
				return showPlayerDialog( playerid, d_buy_menu, DIALOG_STYLE_LIST, 
					"Agencia inmobiliaria", "\
					1. "cGRAY"Lista de casas en venta\n\
					2. "cGRAY"Lista de negocios en venta", 
					"Seleccionar", "Cerrar" );
			}
			
			switch( listitem ) 
			{
				case 0: 
				{	// Lista de todas las casas
					showHouseList( playerid, 1, 0 );
					SetPVarInt( playerid, "HBuy:case", 1 );
				}
				
				case 1: 
				{	// Ordenar por costo
					showPlayerDialog( playerid, d_buy_menu + 3, DIALOG_STYLE_INPUT, " ", 
						""cBLUE"Ordenar por costo\n\n\
						"cWHITE"Especifique un rango de precios para las casas que desea ver:\
						\nEjemplo: "cBLUE"60000-200000", "Aceptar", "Atr�s" );
					SetPVarInt( playerid, "HBuy:case", 2 );
				}
					// Ordenar por tipo
				case 2: 
				{
					showPlayerDialog( playerid, d_buy_menu + 14, DIALOG_STYLE_LIST, " ", ""cWHITE"\
						Casa\n\
						Apartamento", "Seleccionar", "Atr�s" );
					SetPVarInt( playerid, "HBuy:case", 3 );
				}
					// Encuentra Casa por n�mero
				case 3: 
				{
					showPlayerDialog( playerid, d_buy_menu + 9, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Comprando una propiedad\n\n\
						"cWHITE"Indique el n�mero de Casa (apartamento):", "Aceptar", "Atr�s" );
					SetPVarInt( playerid, "HBuy:case", 4 );
				}
				
				case 4:
				{
					showHouseList( playerid, 4, 0 );
				}
			}
		}
		
		case d_buy_menu + 2: 
		{
			if( !response ) 
			{
				DeletePVar( playerid, "HBuy:List" );
				
				switch( GetPVarInt( playerid, "HBuy:case" ) )
				{
					case 2:
					{
						return showPlayerDialog( playerid, d_buy_menu + 3, DIALOG_STYLE_INPUT, " ", 
							""cBLUE"Ordenar por costo\n\n\
							"cWHITE"Especifique un rango de precios para las casas que desea ver:\
							\nEjemplo: "cBLUE"60000-200000", "Aceptar", "Atr�s" );
					}
					
					case 3:
					{
						DeletePVar( playerid, "HBuy:TypeTwo" );
						return showPlayerDialog( playerid, d_buy_menu + 15, DIALOG_STYLE_LIST, " ", dialog_house_type, "Seleccionar", "Atr�s" );
					}
					
					case 4:
					{
						return showPlayerDialog( playerid, d_buy_menu + 9, DIALOG_STYLE_INPUT, " ", "\
							"cBLUE"Comprando una propiedad\n\n\
							"cWHITE"Indique el n�mero de Casa (apartamento):", "Aceptar", "Atr�s" );
					}
					
					default:
					{
						DeletePVar( playerid, "HBuy:case" );
						return showPlayerDialog( playerid, d_buy_menu + 1, DIALOG_STYLE_LIST, " ", dialog_house_buy, "Seleccionar", "Atr�s" );
					}
				}
			}
			
			if( listitem == HBUY_LIST ) 
			{
				return showHouseList( playerid, GetPVarInt( playerid, "HBuy:Type" ), GetPVarInt( playerid, "HBuy:List" ) + 1 );
			}
			else if( listitem == HBUY_LIST + 1 || 
				listitem == GetPVarInt( playerid, "HBuy:Last" ) && GetPVarInt( playerid, "HBuy:Last" ) < BBUY_LIST ) 
			{
				return showHouseList( playerid, GetPVarInt( playerid, "HBuy:Type" ), GetPVarInt( playerid, "HBuy:List" ) - 1 );
			}
			
			showHouseBuyMenu( playerid, g_dialog_select[playerid][listitem] );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
		}
		
		case d_buy_menu + 3: 
		{
			if( !response ) 
			{
				DeletePVar( playerid, "HBuy:case" );
				return showPlayerDialog( playerid, d_buy_menu + 1, DIALOG_STYLE_LIST, " ", dialog_house_buy, "Seleccionar", "Atr�s" );
			}
			
			if( sscanf( inputtext, "p<->a<d>[2]", inputtext[0], inputtext[1] ) || inputtext[1] <= inputtext[0] )
			{
				return showPlayerDialog( playerid, d_buy_menu + 3, DIALOG_STYLE_INPUT, " ", 
					""cBLUE"Ordenar por costo\n\n\
					"cWHITE"Especifique un rango de precios para las casas que desea ver:\
					\nEjemplo: "cBLUE"60000-200000\n\n\
					"gbDialogError"Has ingresado un rango incorrecto.", "Aceptar", "Atr�s" );
			}
			
			SetPVarInt( playerid, "HBuy:PriceM", inputtext[0] ), 
			SetPVarInt( playerid, "HBuy:PriceH", inputtext[1] );
			DeletePVar( playerid, "HBuy:House" );
			showHouseList( playerid, 2, 0 );
		}
		
		//Di�logo al ver Casaa
		case d_buy_menu + 4: 
		{
			if( !response )
			{
				setPlayerPos( playerid,
					GetPVarFloat( playerid, "HBuy:PX" ),
					GetPVarFloat( playerid, "HBuy:PY" ),
					GetPVarFloat( playerid, "HBuy:PZ") 
				);
					
				SetCameraBehindPlayer( playerid );
				
				SetPlayerVirtualWorld( playerid, 13 ), 
				SetPlayerInterior( playerid, 1 );
				
				DeletePVar( playerid, "HBuy:PX" ), 
				DeletePVar( playerid, "HBuy:PY" ),
				DeletePVar( playerid, "HBuy:PZ" ),
				DeletePVar( playerid, "HBuy:Camera" );			
				DeletePVar( playerid, "HBuy:House" );
				
				showHouseList( playerid, GetPVarInt( playerid, "HBuy:Type" ), GetPVarInt( playerid, "HBuy:List" ) );
				return 1;
			}
		
			if( listitem == 2 )
			{
				return showPlayerDialog( playerid, d_buy_menu + 13, DIALOG_STYLE_MSGBOX, " ", "\
					"cBLUE"Informaci�n\n\n\
					"cWHITE"Si no tienes suficiente dinero para comprar una casa (apartamento),\n\
					Puedes alquilar una habitaci�n, pero algunas caracter�sticas ser�n limitadas:\n\
					- No podr�s realizar refacciones en la casa (retexturizaci�n),\n\
					- No podr�s alojar a otras personas en la casa,\n\
					- No podr�s vender la casa a otra persona.\n\n\
					Podr�s pagar el alquiler en el Banco Central, el precio del mismo depender� del interior,\n\
					En caso de alquilar, tendr�s que pagar un costo adicional por expensas.", "Atr�s", "" );
			}
		
			new
				h = GetPVarInt( playerid, "HBuy:House" );
					
			if( Player[playerid][uHouseEvict] )
			{
				showPlayerDialog( playerid, d_buy_menu + 4, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Comprar\n\
					Alquilar\n\
					Informaci�n", "Seleccionar", "Atr�s" );
				return SendClient:( playerid, C_WHITE, !""gbError"No puedes comprar ni alquilar esta propiedad por que ya est�s alojado en otra casa. Usa "cBLUE"/desalojar"cWHITE", para desalojar tu actual alquiler." );
			}
			
			if( IsOwnerHouseCount( playerid ) >= 1 + Premium[playerid][prem_house] )
			{
				showPlayerDialog( playerid, d_buy_menu + 4, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Comprar\n\
					Alquilar\n\
					Informaci�n", "Seleccionar", "Atr�s" );
				return SendClient:( playerid, C_WHITE, !""gbError"Ya tienes la cantidad m�xima permitida de propiedades." );
			}			
				
			if( HouseInfo[h][huID] != INVALID_PARAM )
			{
				setPlayerPos( playerid,
					GetPVarFloat( playerid, "HBuy:PX" ),
					GetPVarFloat( playerid, "HBuy:PY" ),
					GetPVarFloat( playerid, "HBuy:PZ") 
				);
					
				SetCameraBehindPlayer( playerid );
				
				SetPlayerVirtualWorld( playerid, 13 ), 
				SetPlayerInterior( playerid, 1 );
				
				DeletePVar( playerid, "HBuy:PX" ), 
				DeletePVar( playerid, "HBuy:PY" ),
				DeletePVar( playerid, "HBuy:PZ" ),
				DeletePVar( playerid, "HBuy:Camera" );			
				DeletePVar( playerid, "HBuy:House" );
			
				showHouseList( playerid, GetPVarInt( playerid, "HBuy:Type" ), GetPVarInt( playerid, "HBuy:List" ) );
				return SendClient:( playerid, C_GRAY, !""gbError"Esta casa ya ha sido adquirida por alguien." );
			}
			
			if( listitem == 1 )
			{
				if( Player[playerid][uMoney] < GetPriceRentHouse( h ) )
				{
					showPlayerDialog( playerid, d_buy_menu + 4, DIALOG_STYLE_LIST, " ", ""cWHITE"\
						Comprar\n\
						Alquilar\n\
						Informaci�n", "Seleccionar", "Atr�s" );
					return SendClient:( playerid, C_GRAY, !""gbError"No tienes dinero para pagar el alquiler ni por 1 d�a." );
				}
			
				HouseInfo[h][hSellDate] = gettime() + 1 * 86400;
			
				MeAction( playerid, "Firmo un contrato de alquiler", 1 );
				
				SetPlayerCash( playerid, "-", GetPriceRentHouse( h ) );
			
				HouseInfo[h][hRent] = 1;
				InsertPlayerHouse( playerid, h );
				
				clean:<HouseInfo[h][hOwner]>;
				strcat( HouseInfo[h][hOwner], Player[playerid][uName], MAX_PLAYER_NAME );
					
				HouseInfo[h][huID] = Player[playerid][uID];
				
				pformat:( ""gbSuccess"Alquilaste "cBLUE"%s #%d"cWHITE". El alquiler se generar� en "cBLUE"1"cWHITE" dia.", 
					!HouseInfo[h][hType] ? ("una casa") : ("un departamento"), 
					HouseInfo[h][hID] );
				psend:( playerid, C_WHITE );
				
				log( LOG_RENT_HOUSE, "alquil� una casa", Player[playerid][uID], HouseInfo[h][hID] );
				
				DestroyDynamicPickup( HouseInfo[h][hPickup] );
				
				HouseInfo[h][hPickup] = CreateDynamicPickup( 19524, 23, 
					HouseInfo[h][hEnterPos][0], 
					HouseInfo[h][hEnterPos][1], 
					HouseInfo[h][hEnterPos][2], 
					HouseInfo[h][hType], -1, -1, 30.0 );
			}
			else
			{
				if( HouseInfo[h][hPrice] > Player[playerid][uMoney] )
				{
					showPlayerDialog( playerid, d_buy_menu + 4, DIALOG_STYLE_LIST, " ", ""cWHITE"\
						Comprar\n\
						Alquilar\n\
						Informaci�n", "Seleccionar", "Atr�s" );
					return SendClient:( playerid, C_GRAY, !""gbError"No tienes suficiente dinero para comprar esta casa." );
				}
			
				HouseInfo[h][hSellDate] = gettime() + 2 * 86400;
				MeAction( playerid, "Firm� los papeles para adquirir una casa", 1 );
					
				SetPlayerCash( playerid, "-", HouseInfo[h][hPrice] );
				InsertPlayerHouse( playerid, h );
				
				clean:<HouseInfo[h][hOwner]>;
				strcat( HouseInfo[h][hOwner], Player[playerid][uName], MAX_PLAYER_NAME );
					
				HouseInfo[h][huID] = Player[playerid][uID];
			
				pformat:( ""gbSuccess"Compraste "cBLUE"%s #%d"cWHITE". El espacio habitable se paga autom�ticamente por "cBLUE"2"cWHITE" d�as.", 
					!HouseInfo[h][hType] ? ("una casa") : ("un departamento"), 
					HouseInfo[h][hID] );
				psend:( playerid, C_WHITE );
				
				log( LOG_BUY_HOUSE, "compro una Casa", Player[playerid][uID], HouseInfo[h][hID] );
				
				DestroyDynamicPickup( HouseInfo[h][hPickup] );
				
				HouseInfo[h][hPickup] = CreateDynamicPickup( 19522, 23, 
					HouseInfo[h][hEnterPos][0], 
					HouseInfo[h][hEnterPos][1], 
					HouseInfo[h][hEnterPos][2], 
					HouseInfo[h][hType], -1, -1, 30.0 );
			}
			
			mysql_format:g_string( "UPDATE `"DB_HOUSE"` SET `huID` = %d, `hRent` = %d, `hOwner` = '%s', `hSellDate` = %d WHERE `hID` = %d LIMIT 1",
				HouseInfo[h][huID], HouseInfo[h][hRent], HouseInfo[h][hOwner], HouseInfo[h][hSellDate], HouseInfo[h][hID] );
			mysql_tquery( mysql, g_string );
		
			setPlayerPos( playerid,
				GetPVarFloat( playerid, "HBuy:PX" ),
				GetPVarFloat( playerid, "HBuy:PY" ),
				GetPVarFloat( playerid, "HBuy:PZ") 
			);
				
			SetCameraBehindPlayer( playerid );
			
			SetPlayerVirtualWorld( playerid, 13 ), 
			SetPlayerInterior( playerid, 1 );
				
			DeletePVar( playerid, "HBuy:PX" ), 
			DeletePVar( playerid, "HBuy:PY" ),
			DeletePVar( playerid, "HBuy:PZ" ),
			DeletePVar( playerid, "HBuy:Camera" );			
			DeletePVar( playerid, "HBuy:House" );
			DeletePVar( playerid, "HBuy:TypeOne" );
			DeletePVar( playerid, "HBuy:TypeTwo" );
			DeletePVar( playerid, "HBuy:Last" );
			DeletePVar( playerid, "HBuy:PriceM" );
			DeletePVar( playerid, "HBuy:PriceH" );
			DeletePVar( playerid, "HBuy:List" );
			DeletePVar( playerid, "HBuy:Type" );
			DeletePVar( playerid, "HBuy:case" );
			
			g_player_interaction{playerid} = 0;
		}
		
		case d_buy_menu + 5: 
		{
			if( !response ) 
			{
				DeletePVar( playerid, "HBuy:HId" );
				showHouseList( playerid, 4, 0 );
				return 1;
			}
			
			new
				h = GetPVarInt( playerid, "HBuy:HId" );
			
			switch( listitem ) 
			{
				case 0: 
				{
					if( HouseInfo[h][hRent] )
					{
						format:g_small_string( "\
							"cBLUE"Confirmaci�n\n\n\
							"cWHITE"�Quieres cancelar el alquiler de "cBLUE"%s #%d"cWHITE"?",
							!HouseInfo[h][hType] ? ("una casa") : ("un departamento"),
							HouseInfo[h][hID] );
						return showPlayerDialog( playerid, d_buy_menu + 6, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Siguiente", "Atr�s" );
					}
				
					format:g_string( "\
						"cBLUE"Vender %s\n\n\
						"cWHITE"Su "cBLUE"%s #%d"cWHITE" ser� vendida a la agencia inmoviliaria por "cBLUE"$%d"cWHITE".",
						!HouseInfo[h][hType] ? ("una casa") : ("un departamento"),
						!HouseInfo[h][hType] ? ("Casa") : ("Apartamento"),
						HouseInfo[h][hID],
						floatround( HouseInfo[h][hPrice] * ( 0.6 + ( Premium[playerid][prem_house_property] / 100 ) ) ) 
					);
					
					showPlayerDialog( playerid, d_buy_menu + 6, DIALOG_STYLE_MSGBOX, " ", g_string, "Vender", "Cancelar" );
				}
				
				case 1: 
				{
					if( HouseInfo[h][hRent] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"No puedes vender una Casa (apartamento) alquilada por un jugador." );
						return showPlayerDialog( playerid, d_buy_menu + 5, DIALOG_STYLE_LIST,
							"Venta de propiedades", "\
							"cWHITE"1. "cGRAY"Vender una casa a la agencia\n\
							"cWHITE"2. "cGRAY"Vender una casa a un jugador",
							"Seleccionar", "Atr�s" );
					}
				
					showPlayerDialog( playerid, d_buy_menu + 10, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Venta de propiedades\n\n\
						"cWHITE"Para vender una casa a un jugador, ingrese la ID del comprador:", "Aceptar", "Atr�s" );
				}
			}	
		}
		
		case d_buy_menu + 6: 
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_buy_menu + 5, DIALOG_STYLE_LIST,
					"Venta de propiedades", "\
					"cWHITE"1. "cGRAY"Vender una casa a la agencia\n\
					"cWHITE"2. "cGRAY"Vender una casa a un jugador",
				"Seleccionar", "Atr�s" );
			}
			
			new 
				h = GetPVarInt( playerid, "HBuy:HId" );
				
			if( !HouseInfo[h][hRent] )
			{
				SetPlayerCash( playerid, "+", floatround( HouseInfo[h][hPrice] * ( 0.6 + ( Premium[playerid][prem_house_property] / 100 ) ) ) );
				
				mysql_format:g_small_string( "UPDATE `"DB_USERS"` SET `uHouseEvict` = 0 WHERE `uHouseEvict` = %d", HouseInfo[h][hID] );
				mysql_tquery( mysql, g_small_string );
			
				foreach( new i : Player) 
				{
					if( !IsLogged(i) ) continue;
				
					if( Player[i][uHouseEvict] == HouseInfo[h][hID] ) 
					{
						SendClient:( i, C_GRAY, !""gbDefault"La casa en la que viv�as, fue vendida por el due�o. Fuiste desalojado." );
						Player[i][uHouseEvict] = 0;
					}
				}
				
				pformat:( ""gbSuccess"�Felicidades! Vendiste tu %s a la agencia por $%d.", !HouseInfo[h][hType] ? ("casa") : ("departamento"), 
					floatround( HouseInfo[h][hPrice] * ( 0.6 + ( Premium[playerid][prem_house_property] / 100 ) ) ) );
				psend:( playerid, C_WHITE );
			}
			else
			{
				SendClient:( playerid, C_WHITE, !""gbSuccess"Usted ha terminado con �xito el contrato de alquiler." );
			}
			
			mysql_format:g_small_string( "UPDATE `"DB_HOUSE"` SET `huID` = -1, `hRent` = 0, `hOwner` = '', `hSellDate` = 0 WHERE `hID` = %d LIMIT 1",
				HouseInfo[h][hID] );
			mysql_tquery( mysql, g_small_string );
			
			RemovePlayerHouse( playerid, h );
			
			clean:<HouseInfo[h][hOwner]>;
			HouseInfo[h][huID] = INVALID_PARAM;
			HouseInfo[h][hRent] =
			HouseInfo[h][hSellDate] = 0;
				
			for( new i; i < MAX_EVICT; i++ )
			{
				HEvict[h][i][hEvictUID] = 0;
				HEvict[h][i][hEvictName][0] = EOS;
			}
			
			DestroyDynamicPickup( HouseInfo[h][hPickup] );
			
			HouseInfo[h][hPickup] = CreateDynamicPickup( 1272, 23, 
				HouseInfo[h][hEnterPos][0], 
				HouseInfo[h][hEnterPos][1],
				HouseInfo[h][hEnterPos][2], HouseInfo[h][hType], -1, -1, 30.0 );
			
			g_player_interaction{playerid} = 0;
			
			log( LOG_SELL_HOUSE, "desaloj� una Casa", Player[playerid][uID], HouseInfo[h][hID] );
		}
		
		case d_buy_menu + 8:
		{
			new 
				sellid = GetPVarInt( playerid, "HBuy:SellID" ), //Vendedor
				price = GetPVarInt( playerid, "HBuy:Price" ),	//Comprador
				house = GetPVarInt( playerid, "HBuy:HId" );
		
			if( !IsLogged( sellid ) ) 
			{
				SendClient:( playerid, C_GRAY, !""gbError"El vendedor no est� en l�nea, la operaci�n se interrumpe!" );
					
				DeletePVar( playerid, "HBuy:HId" ), 
				DeletePVar( playerid, "HBuy:SellID" ),
				DeletePVar( playerid, "HBuy:Price" );
				
				g_player_interaction{playerid} = 0;
				return 1;
			}
		
			if( !response )
			{
				pformat:(""gbError"Te negaste a comprar la casa de "cBLUE"%s[%d]"cWHITE".",
					GetAccountName( sellid ),
					sellid
				);
				psend:( playerid, C_WHITE );
				
				pformat:(""gbError"El jugador "cBLUE"%s[%d]"cWHITE" se neg� a comprar tu casa.",
					GetAccountName( playerid ),
					playerid
				);
				psend: ( sellid, C_WHITE );

				DeletePVar( playerid, "HBuy:SellID" ),
				DeletePVar( playerid, "HBuy:Price" );
				DeletePVar( playerid, "HBuy:HId" );
				
				g_player_interaction{playerid} = 0;
				g_player_interaction{sellid} = 0;
				return 1;
			}
			
			if( Player[playerid][uMoney] < price )
			{
				SendClient:( playerid, C_GRAY, !NO_MONEY );
				
				DeletePVar( playerid, "HBuy:HId" ), 
				DeletePVar( playerid, "HBuy:SellID" ),
				DeletePVar( playerid, "HBuy:Price" );
				
				pformat:(""gbError"El jugador "cBLUE"%s[%d]"cWHITE" no tiene suficiente dinero para comprar tu casa.",
					GetAccountName( playerid ),
					playerid
				);
				psend:( sellid, C_WHITE );
				
				g_player_interaction{playerid} = 0;
				g_player_interaction{sellid} = 0;
				
				return 1;
			}
			
			if( GetDistanceBetweenPlayers( playerid, sellid ) > 3.0 )
			{
				SendClient:( playerid, C_WHITE, !""gbError"El due�o de la casa est� muy lejos de ti." );
				
				DeletePVar( playerid, "BBuy:BId" ), 
				DeletePVar( playerid, "BBuy:SellID" ),
				DeletePVar( playerid, "BBuy:Price" );
				
				pformat:(""gbError"El jugador "cBLUE"%s[%d]"cWHITE" est� muy lejos de ti.",
					GetAccountName( playerid ),
					playerid
				);
				psend:( sellid, C_WHITE );
				
				g_player_interaction{playerid} = 0;
				g_player_interaction{sellid} = 0;
				
				return 1;
			}
			
			if( Player[playerid][uHouseEvict] )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Est�s alquilando una casa, usa /desalojar para cancelar el alquiler." );
				
				DeletePVar( playerid, "BBuy:BId" ), 
				DeletePVar( playerid, "BBuy:SellID" ),
				DeletePVar( playerid, "BBuy:Price" );
				
				pformat:(""gbError"El jugador "cBLUE"%s[%d]"cWHITE" no puede comprar tu casa por tiene con contrato de alquiler en curso.",
					GetAccountName( playerid ),
					playerid
				);
				psend:( sellid, C_WHITE );
				
				g_player_interaction{playerid} = 0;
				g_player_interaction{sellid} = 0;
			}
			
			if( IsOwnerHouseCount( playerid ) >= 1 + Premium[ playerid ][prem_house] )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Ya tienes una casa." );
				
				DeletePVar( playerid, "BBuy:BId" ), 
				DeletePVar( playerid, "BBuy:SellID" ),
				DeletePVar( playerid, "BBuy:Price" );
				
				pformat:(""gbError"El jugador "cBLUE"%s[%d]"cWHITE" alcanz� el limite de casas permitidas.",
					GetAccountName( playerid ),
					playerid
				);
				psend:( sellid, C_WHITE );
				
				g_player_interaction{playerid} = 0;
				g_player_interaction{sellid} = 0;
			}
			
			SetPlayerCash( sellid, "+", price );
			SetPlayerCash( playerid, "-", price );
				
			OfferSalePlayerHouse( sellid, playerid, house );
				
			pformat:( ""gbSuccess"�Felicidades! Compraste "cBLUE"%s #%d"cWHITE" a "cBLUE"%s[%d]"cWHITE".",
				!HouseInfo[house][hType] ? ("una casa") : ("un departamento"),
				HouseInfo[house][hID],
				GetAccountName( sellid ),
				sellid
			);
			psend:( playerid, C_WHITE );
				
			pformat:( ""gbSuccess"�Felicidades! Le vendiste tu "cBLUE"%s #%d"cWHITE" al usuario "cBLUE"%s[%d]"cWHITE".",
				!HouseInfo[house][hType] ? ("casa") : ("departamento"),
				HouseInfo[house][hID],
				GetAccountName( playerid ),
				playerid 
			);
			psend:( sellid, C_WHITE );
				
			g_player_interaction{playerid} = 0;
			g_player_interaction{sellid} = 0;
				
			log( LOG_BUY_HOUSE_FROM_PLAYER, "vendi� una casa", Player[playerid][uID], Player[sellid][uID], price );
				
			DeletePVar( playerid, "HBuy:HId" ), 
			DeletePVar( playerid, "HBuy:SellID" ),
			DeletePVar( playerid, "HBuy:Price" );
		}
		
		case d_buy_menu + 9: 
		{
			if( !response ) 
			{
				DeletePVar( playerid, "HBuy:case" );
				return showPlayerDialog( playerid, d_buy_menu + 1, DIALOG_STYLE_LIST, " ", dialog_house_buy, "Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 )
			{
				return showPlayerDialog( playerid, d_buy_menu + 9, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Comprando una propiedad\n\n\
					"cWHITE"Indique el n�mero de Casa (apartamento):\n\
					"gbDialogError"Valor inv�lido.", "Aceptar", "Atr�s" );
			}
			
			for( new h; h < MAX_HOUSE; h++ ) 
			{
				if( HouseInfo[h][hID] == strval( inputtext ) )
				{
					if( HouseInfo[h][huID] != INVALID_PARAM ) 
					{
						return showPlayerDialog( playerid, d_buy_menu + 9, DIALOG_STYLE_INPUT, " ", "\
							"cBLUE"Comprando una propiedad\n\n\
							"cWHITE"Indique el n�mero de Casa (apartamento):\n\
							"gbDialogError"Esta casa ya est� vendida.", "Aceptar", "Atr�s" );
					}
					
					showHouseBuyMenu( playerid, h );
					return 1;
				}
			}
			
			showPlayerDialog( playerid, d_buy_menu + 9, DIALOG_STYLE_INPUT, " ", "\
				"cBLUE"Comprando una propiedad\n\n\
				"cWHITE"Indique el n�mero de Casa (apartamento):\n\
				"gbDialogError"La casa con este n�mero no existe.", "Aceptar", "Atr�s" );
		}
		
		case d_buy_menu + 10: 
		{
			if( !response ) 
			{
				return showPlayerDialog( playerid, d_buy_menu + 5, DIALOG_STYLE_LIST,
					"Venta de propiedades", "\
					"cWHITE"1. "cGRAY"Vender una casa a la agencia\n\
					"cWHITE"2. "cGRAY"Vender una casa a un jugador",
				"Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS ) 
			{
				return showPlayerDialog( playerid, d_buy_menu + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Venta de propiedades\n\n\
					"cWHITE"Para vender una casa a un jugador, ingrese el ID del comprador:\n\n\
					"gbDialog"El jugador debe estar en la casa contigo.\n\
					"gbDialogError"Ingrese la ID del jugador.", 
				"Siguiente", "Atr�s" );
			}	
					
			if( !IsLogged( strval( inputtext ) ) || playerid == strval( inputtext ) ) 
			{
				return showPlayerDialog( playerid, d_buy_menu + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Venta de propiedades\n\n\
					"cWHITE"Para vender una casa a un jugador, ingrese el ID del comprador:\n\n\
					"gbDialog"El jugador debe estar en la casa contigo.\n\
					"gbDialogError"Has introducido un ID de jugador inv�lido.", 
				"Siguiente", "Atr�s" );
			}
					
			if( Player[playerid][uHouseEvict] ) 
			{
				return showPlayerDialog( playerid, d_buy_menu + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Venta de propiedades\n\n\
					"cWHITE"Para vender una casa a un jugador, ingrese el ID del comprador:\n\n\
					"gbDialog"El jugador debe estar en la casa contigo.\n\
					"gbDialogError"Este jugador tiene alquiler, pidele que use /desalojar.", 
				"Siguiente", "Atr�s" );
			}
			
			if( IsOwnerHouseCount( strval( inputtext ) ) >= 1 + Premium[ strval( inputtext ) ][prem_house] )
			{
				return showPlayerDialog( playerid, d_buy_menu + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Venta de propiedades\n\n\
					"cWHITE"Para vender una casa a un jugador, ingrese el ID del comprador:\n\n\
					"gbDialog"El jugador debe estar en la casa contigo.\n\
					"gbDialogError"Este jugador ya tiene una casa", 
				"Siguiente", "Atr�s" );
			}
			
			if( GetDistanceBetweenPlayers( playerid, strval( inputtext ) ) > 3.0 )
			{
				return showPlayerDialog( playerid, d_buy_menu + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Venta de propiedades\n\n\
					"cWHITE"Para vender una casa a un jugador, ingrese el ID del comprador:\n\n\
					"gbDialogError"El jugador debe estar en la casa contigo.", 
				"Siguiente", "Atr�s" );
			}
			
			if( g_player_interaction{ strval( inputtext ) } )
			{
				return showPlayerDialog( playerid, d_buy_menu + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Venta de propiedades\n\n\
					"cWHITE"Para vender una casa a un jugador, ingrese el ID del comprador:\n\n\
					"gbDialog"El jugador debe estar en la casa contigo.\n\
					"gbDialogError"No puedes interactuar con este jugador.", 
				"Siguiente", "Atr�s" );
			}		
			
			g_player_interaction{strval( inputtext )} = 1;
			SetPVarInt( playerid, "HBuy:PlayerID", strval( inputtext ) );
			ShowDialogHouseSell( playerid, GetPVarInt( playerid, "HBuy:HId" ), " " );
		}
		
		case d_buy_menu + 11: 
		{
			if( !response ) 
			{
				DeletePVar( playerid, "HBuy:PlayerID" );
				return showPlayerDialog( playerid, d_buy_menu + 10, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Venta de propiedades\n\n\
					"cWHITE"Para vender una casa a un jugador, ingrese el ID del comprador:", "Aceptar", "Atr�s" );
			}
		
			new
				h = GetPVarInt( playerid, "HBuy:HId" ),
				sellid = GetPVarInt( playerid, "HBuy:PlayerID" );
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) ) 
			{
				return ShowDialogHouseSell( playerid, h, "\n\n"gbDialogError"Formato incorrecto." );
			}
			
			if( GetDistanceBetweenPlayers( playerid, sellid ) > 3.0 )
			{
				return ShowDialogHouseSell( playerid, h, "\n\n"gbDialogError"El jugador debe estar en la casa contigo." );
			}
			
			if( strval( inputtext ) < floatround( HouseInfo[h][hPrice] * 0.5 ) ||  strval( inputtext ) > ( HouseInfo[h][hPrice] * 2 ) )
			{
				return ShowDialogHouseSell( playerid, h, "\n\n"gbDialogError"La cantidad especificada est� fuera de rango." );
			}
		
			if( Player[sellid][uMoney] < strval( inputtext ) ) 
			{
				return ShowDialogHouseSell( playerid, h, "\n\n"gbDialogError"El jugador no tiene la cantidad requerida de dinero en efectivo." );
			}
			
			pformat:( ""gbSuccess"Le ofreciste al jugador "cBLUE"%s[%d]"cWHITE" comprar tu "cBLUE"%s #%d"cWHITE".",
				GetAccountName( sellid ),
				sellid,
				!HouseInfo[h][hType] ? ("casa") : ("departamento"),
				HouseInfo[h][hID]
			);
			
			psend:( playerid, C_WHITE ); 
			
			format:g_small_string( 
				""cWHITE"El jugador "cBLUE"%s[%d]"cWHITE" te ofrece\n\
				comprar su "cBLUE"%s #%d"cWHITE" por "cBLUE"$%d"cWHITE".",
				GetAccountName( playerid ),
				playerid,
				!HouseInfo[h][hType] ? ("casa") : ("departamento"),
				HouseInfo[h][hID],
				strval( inputtext )
			);
			
			showPlayerDialog( sellid, d_buy_menu + 8, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Aceptar", "Rechazar" );
			
			SetPVarInt( sellid, "HBuy:SellID", playerid );
			SetPVarInt( sellid, "HBuy:Price", strval( inputtext ) );
			SetPVarInt( sellid, "HBuy:HId", h );
			
			DeletePVar( playerid, "HBuy:PlayerID" );
			DeletePVar( playerid, "HBuy:HId" );
		}

		case d_buy_menu + 12:
		{
			if( !response )
			{
				setPlayerPos( playerid,
					GetPVarFloat( playerid, "HBuy:PX" ),
					GetPVarFloat( playerid, "HBuy:PY" ),
					GetPVarFloat( playerid, "HBuy:PZ") 
				);
					
				SetCameraBehindPlayer( playerid );
				
				SetPlayerVirtualWorld( playerid, 13 ), 
				SetPlayerInterior( playerid, 1 );
					
				DeletePVar( playerid, "HBuy:PX" ), 
				DeletePVar( playerid, "HBuy:PY" ),
				DeletePVar( playerid, "HBuy:PZ" ),
				DeletePVar( playerid, "HBuy:Camera" );
				
				if( GetPVarInt( playerid, "HBuy:case" ) == 4 )
				{
					showPlayerDialog( playerid, d_buy_menu + 9, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Comprando una propiedad\n\n\
						"cWHITE"Indique el n�mero de Casa (apartamento):", "Aceptar", "Atr�s" );
				}
				else
				{
					showHouseList( playerid, GetPVarInt( playerid, "HBuy:Type" ), GetPVarInt( playerid, "HBuy:List" ) );
				}
				
				DeletePVar( playerid, "HBuy:House" );
				
				return 1;
			}
		
			showPlayerDialog( playerid, d_buy_menu + 4, DIALOG_STYLE_LIST, " ", ""cWHITE"\
				Comprar\n\
				Alquilar\n\
				Informaci�n", "Seleccionar", "Atr�s" );
		}
		
		case d_buy_menu + 13:
		{
			showPlayerDialog( playerid, d_buy_menu + 4, DIALOG_STYLE_LIST, " ", ""cWHITE"\
				Comprar\n\
				Alquilar\n\
				Informaci�n", "Seleccionar", "Atr�s" );
		}
		
		case d_buy_menu + 14:
		{
			if( !response )
			{
				DeletePVar( playerid, "HBuy:case" );
				return showPlayerDialog( playerid, d_buy_menu + 1, DIALOG_STYLE_LIST, " ", dialog_house_buy, "Seleccionar", "Atr�s" );
			}
			
			if( listitem == 1 )
			{
				SendClient:( playerid, C_WHITE, !""gbError"No hay apartamentos disponibles para comprar." );
				
				return showPlayerDialog( playerid, d_buy_menu + 14, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Casa\n\
					Apartamento", "Seleccionar", "Atr�s" );
			}
			
			SetPVarInt( playerid, "HBuy:TypeOne", listitem );
			showPlayerDialog( playerid, d_buy_menu + 15, DIALOG_STYLE_LIST, " ", dialog_house_type, "Seleccionar", "Atr�s" );
		}
		
		case d_buy_menu + 15:
		{
			if( !response )
			{
				DeletePVar( playerid, "HBuy:TypeOne" );
				return showPlayerDialog( playerid, d_buy_menu + 14, DIALOG_STYLE_LIST, " ", ""cWHITE"\
					Casa\n\
					Apartamento", "Seleccionar", "Atr�s" );
			}
			
			SetPVarInt( playerid, "HBuy:TypeTwo", listitem );
			showHouseList( playerid, 3, 0 );
		}
		
		case d_buy_menu + 16:
		{
			if( !response ) return showPlayerDialog( playerid, d_buy_menu + 1, DIALOG_STYLE_LIST, " ", dialog_house_buy, "Seleccionar", "Atr�s" );
			
			SetPVarInt( playerid, "HBuy:HId", g_dialog_select[playerid][listitem] );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
			
			showPlayerDialog( playerid, d_buy_menu + 5, DIALOG_STYLE_LIST,
				"Venta de propiedades", "\
				"cWHITE"1. "cGRAY"Vender una casa a la agencia\n\
				"cWHITE"2. "cGRAY"Vender una casa a un jugador",
				"Seleccionar", "Atr�s" );
		}
		
		
		/*- - - - - - - - - - - - Di�logos con el panel de la casa. - - - - - - - - - - - - -*/
		case d_house_panel: 
		{
			if( !response )
			{
				DeletePVar( playerid, "Hpanel:HId" );
				return 1;
			}
			
			new
				h = GetPVarInt( playerid, "Hpanel:HId" ),
				year, month, day;
			
			switch( listitem ) 
			{
				//Informaci�n
				case 0: 
				{
					if( !Player[playerid][uHouseEvict] && HouseInfo[h][huID] != Player[playerid][uID] ) return 1;
				
					gmtime( HouseInfo[h][hSellDate], year, month, day );
				
					format:g_string(  ""cBLUE"Informaci�n de %s\n\n\
						"cWHITE"Estado: "cRED"%s\n\
						"cWHITE"Numero de %s: "cBLUE"%d\n\
						"cWHITE"Valor: "cBLUE"$%d\n\
						"cWHITE"Tipo de interior: "cBLUE"%s\n\n\
						"cWHITE"Due�o: "cBLUE"%s\n\
						"cWHITE"Puertas: %s\n\
						"cWHITE"Muebles: "cBLUE"%d/%d\n\n\
						"cWHITE"Pagado el: %02d.%02d.%d", 
						!HouseInfo[h][hType] ? ("la casa") : ("el departamento"),
						!HouseInfo[h][hRent] ? ("Comprado") : ("Alquilado"),
						!HouseInfo[h][hType] ? ("casa") : ("departamento"),
						HouseInfo[h][hID],
						HouseInfo[h][hPrice],
						GetNameInteriorHouse(h),
						HouseInfo[h][hOwner],
						HouseInfo[h][hLock] ? (""cBLUE"Abiertas"cWHITE"") : 
											  (""cRED"Cerradas"cWHITE""),
						HouseInfo[h][hCountFurn],
						GetMaxFurnHouse( h ),
						day, month, year
					);
					
					showPlayerDialog( playerid, d_house_panel + 1, DIALOG_STYLE_MSGBOX, " ", g_string, "Atr�s", "" );
				}
				//Sub-establecimiento
				case 1:
				{ 
					if( !Player[playerid][uHouseEvict] && HouseInfo[h][huID] != Player[playerid][uID] ) return 1;
				
					if( HouseInfo[h][hRent] || Player[playerid][uHouseEvict] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"No se puede enga�ar a los jugadores." );
						return showPlayerDialog( playerid, d_house_panel, DIALOG_STYLE_TABLIST, " ", h_panel, "Seleccionar", "Cerrar" );
					}
					
					if( !hinterior_info[ HouseInfo[h][hInterior] - 1 ][h_evict] )
					{
						pformat:( ""gbError"No puedes enga�ar a los jugadores en %s.", !HouseInfo[h][hType] ? ("esta casa") : ("este departamento") );
						psend:( playerid, C_WHITE );
					
						return showPlayerDialog( playerid, d_house_panel, DIALOG_STYLE_TABLIST, " ", h_panel, "Seleccionar", "Cerrar" );
					}
					
					format:g_string( "SELECT * FROM `"DB_USERS"` WHERE `uHouseEvict` = %d", HouseInfo[h][hID] );
					mysql_tquery( mysql, g_string, "checkHouseEvict", "dd", playerid, h );
				}
				//Transacciones en efectivo
				case 2: 
				{
					if( !Player[playerid][uHouseEvict] && HouseInfo[h][huID] != Player[playerid][uID] ) return 1;
				
					if( Player[playerid][uHouseEvict] && !HouseInfo[h][hSettings][0] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"No pudes usar la caja fuerte de esta casa." );
						return showPlayerDialog( playerid, d_house_panel, DIALOG_STYLE_TABLIST, " ", h_panel, "Seleccionar", "Cerrar" );
					}
				
					format:g_string( "Caja Fuerte - "cBLUE"$%d", HouseInfo[h][hMoney] );
					showPlayerDialog( playerid, d_house_panel + 6, DIALOG_STYLE_LIST, g_string, h_panel_p3, "Seleccionar", "Atr�s" );
				}
				//Ajustes
				case 3: 
				{
					if( !Player[playerid][uHouseEvict] && HouseInfo[h][huID] != Player[playerid][uID] ) return 1;
				
					showPlayerDialog( playerid, d_house_panel + 9, DIALOG_STYLE_LIST, "Ajustes", h_panel_plan, "Seleccionar", "Atr�s" );
				}
			}
		}
		
		case d_house_panel + 1: 
		{
			showPlayerDialog( playerid, d_house_panel, DIALOG_STYLE_TABLIST, " ", h_panel, "Seleccionar", "Cerrar" );
		}
		
		case d_house_panel + 2:
		{
			if( !response ) return showPlayerDialog( playerid, d_house_panel, DIALOG_STYLE_TABLIST, " ", h_panel, "Seleccionar", "Cerrar" );
		
			new
				h = GetPVarInt( playerid, "Hpanel:HId" );
				
			if( !HEvict[h][listitem][hEvictUID] )
			{
				showPlayerDialog( playerid, d_house_panel + 3, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Alojamiento\n\n\
					"cWHITE"Ingrese la ID del jugador al que desea alojar:", "Siguiente", "Atr�s" );
			}
			else
			{
				format:g_small_string("\
					"cBLUE"Desalojo\n\n\
					"cWHITE"�Quieres desalojar a "cBLUE"%s"cWHITE"?", HEvict[h][listitem][hEvictName] );
				showPlayerDialog( playerid, d_house_panel + 5, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Si", "No" );
			}
			
			SetPVarInt( playerid, "Hpanel:EvictSlot", listitem );
		}
		
		case d_house_panel + 3: 
		{
			if( !response ) return showPlayerDialog( playerid, d_house_panel, DIALOG_STYLE_TABLIST, " ", h_panel, "Seleccionar", "Cerrar" );
			
			new
				h = GetPVarInt( playerid, "Hpanel:HId" );
			
			if( !IsNumeric( inputtext ) || inputtext[0] == EOS || 
				strval( inputtext ) < 0 || strval( inputtext ) > MAX_PLAYERS || 
				!IsLogged( strval( inputtext ) ) || playerid == strval( inputtext ) )
			{
				return showPlayerDialog( playerid, d_house_panel + 3, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Alojamiento\n\n\
					"cWHITE"Ingrese la ID del jugador al que desea alojar:\n\
					"gbDialogError"ID de jugador inv�lido.", "Siguiente", "Atr�s" );
			}
			
			if( g_player_interaction{ strval( inputtext ) } )
			{
				return showPlayerDialog( playerid, d_house_panel + 3, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Alojamiento\n\n\
					"cWHITE"Ingrese la ID del jugador al que desea alojar:\n\
					"gbDialogError"No puedes interactuar con este jugador.", "Siguiente", "Atr�s" );
			}
			
			if( GetDistanceBetweenPlayers( playerid, strval( inputtext ) ) > 3.0 || GetPlayerVirtualWorld( playerid ) != GetPlayerVirtualWorld( strval( inputtext ) ) )
			{
				return showPlayerDialog( playerid, d_house_panel + 3, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Alojamiento\n\n\
					"cWHITE"Ingrese la ID del jugador al que desea alojar:\n\
					"gbDialogError"El jugador debe estar en la casa contigo.", "Siguiente", "Atr�s" );
			}
			
			if( Player[ strval( inputtext ) ][uHouseEvict] || IsOwnerHouseCount( strval( inputtext ) ) )
			{
				return showPlayerDialog( playerid, d_house_panel + 3, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Alojamiento\n\n\
					"cWHITE"Ingrese la ID del jugador al que desea alojar:\n\
					"gbDialogError"Este jugador ya tiene un lugar donde alojarse.", "Siguiente", "Atr�s" );
			}
			
			pformat:( ""gbDefault"Le ofreciste a "cBLUE"%s[%d]"cWHITE" alojarse en tu casa.", Player[ strval( inputtext ) ][uName], strval( inputtext ) );
			psend:( playerid, C_WHITE );
			
			format:g_string( "\
				"cBLUE"%s[%d]"cWHITE" te ha ofrecido alojarte en %s de %s  "cBLUE"#%d"cWHITE". �Aceptas?",
				Player[playerid][uName], 
				playerid, 
				!HouseInfo[h][hType] ? ("la casa") : ("el departamento"), 
				Player[playerid][uSex] == 2 ? ("ella") : ("�l"),
				HouseInfo[h][hID] 
			);
			showPlayerDialog( strval( inputtext ), d_house_panel + 4, DIALOG_STYLE_MSGBOX, " ", g_string, "Si", "No" );
			
			g_player_interaction{ strval( inputtext ) } = 1;
			g_player_interaction{ playerid } = 1;
			
			SetPVarInt( strval( inputtext ), "Hpanel:Playerid", playerid );
		}
		
		case d_house_panel + 4: 
		{
			new
				id = GetPVarInt( playerid, "Hpanel:Playerid" ),
				slot = GetPVarInt( id, "Hpanel:EvictSlot" ),
				h = GetPVarInt( id, "Hpanel:HId" );
				
			if( !IsLogged( id ) || !g_player_interaction{id} || GetDistanceBetweenPlayers( playerid, id ) > 3.0 || GetPlayerVirtualWorld( playerid ) != GetPlayerVirtualWorld( id ) )
			{
				g_player_interaction{ id } = 
				g_player_interaction{ playerid } = 0;
				
				DeletePVar( id, "Hpanel:HId" );
				DeletePVar( id, "Hpanel:EvictSlot" );
				DeletePVar( playerid, "Hpanel:Playerid" );
			
				return SendClient:( playerid, C_WHITE, !""gbError"Ha ocurrido un error durante el check in, int�ntalo de nuevo." );
			}
				
			if( !response )
			{
				SendClient:( playerid, C_WHITE, !""gbDefault"Te negaste a alojarte en una casa." );
				
				pformat:( ""gbDefault"El jugador "cBLUE"%s[%d]"cWHITE" se neg� a alojarse en tu casa", Player[playerid][uName], playerid );
				psend:( id, C_WHITE );
					
				DeletePVar( id, "Hpanel:HId" );
				DeletePVar( id, "Hpanel:EvictSlot" );
				DeletePVar( playerid, "Hpanel:Playerid" );
			
				g_player_interaction{ id } = 
				g_player_interaction{ playerid } = 0;
			
				return 1;
			}
			
			HEvict[h][slot][hEvictUID] = Player[playerid][uID];
			strcat( HEvict[h][slot][hEvictName], Player[playerid][uName], MAX_PLAYER_NAME );
			
			pformat:( ""gbDefault"Aceptaste alojarte en %s #%d de "cBLUE"%s"cWHITE".", !HouseInfo[h][hType] ? ("la casa") : ("el departamento"), HouseInfo[h][hID], Player[id][uName] );
			psend:( playerid, C_WHITE );
			
			pformat:( ""gbSuccess""cBLUE"%s[%d]"cWHITE" acept� alojarse en tu casa.", Player[playerid][uName], playerid );
			psend:( id, C_WHITE );
			
			Player[playerid][uHouseEvict] = HouseInfo[h][hID];
			UpdatePlayer( playerid, "uHouseEvict", Player[playerid][uHouseEvict] );
			
			DeletePVar( id, "Hpanel:HId" );
			DeletePVar( id, "Hpanel:EvictSlot" );
			DeletePVar( playerid, "Hpanel:Playerid" );
			
			g_player_interaction{ id } = 
			g_player_interaction{ playerid } = 0;
		}
		
		case d_house_panel + 5: 
		{
			if( !response ) 
			{
				DeletePVar( playerid, "Hpanel:EvictSlot" );
				return showPlayerDialog( playerid, d_house_panel, DIALOG_STYLE_TABLIST, " ", h_panel, "Seleccionar", "Cerrar" );
			}
			
			new
				slot = GetPVarInt( playerid, "Hpanel:EvictSlot" ),
				h = GetPVarInt( playerid, "Hpanel:HId" );
			
			pformat:( ""gbDefault"Desalojaste de %s a "cBLUE"%s"cWHITE".", !HouseInfo[h][hType] ? ("la casa") : ("el departamento"),
				HEvict[h][slot][hEvictName] );
			psend:( playerid, C_WHITE );
			
			foreach(new i: Player)
			{
				if( !IsLogged( i ) ) continue;
				
				if( HEvict[h][slot][hEvictUID] == Player[i][uID] )
				{
					Player[i][uHouseEvict] = 0;
					
					pformat:( ""gbDefault"Fuiste desalojado de %s #%d por "cBLUE"%s[%d]"cWHITE".", 
						!HouseInfo[h][hType] ? ("la casa") : ("el departamento" ), HouseInfo[h][hID],
						Player[playerid][uName], playerid );
					psend:( i, C_WHITE );
					
					break;
				}
			}
			
			mysql_format:g_small_string( "UPDATE `"DB_USERS"` SET `uHouseEvict` = 0 WHERE `uID` = %d LIMIT 1", HEvict[h][slot][hEvictUID] );
			mysql_tquery( mysql, g_small_string );
			
			HEvict[h][slot][hEvictName][0] = EOS;
			HEvict[h][slot][hEvictUID] = 0;
			
			DeletePVar( playerid, "Hpanel:EvictSlot" );
			showPlayerDialog( playerid, d_house_panel, DIALOG_STYLE_TABLIST, " ", h_panel, "Seleccionar", "Cerrar" );
		}
		
		case d_house_panel + 6: 
		{
			if( !response ) return showPlayerDialog( playerid, d_house_panel, DIALOG_STYLE_TABLIST, " ", h_panel, "Seleccionar", "Cerrar" );
			
			new
				h = GetPVarInt( playerid, "Hpanel:HId" );
			
			switch( listitem )
			{
				case 0:
				{
					if( !Player[playerid][uHouseEvict] && HouseInfo[h][huID] != Player[playerid][uID] ) return 1;
				
					showPlayerDialog( playerid, d_house_panel + 7, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Depositar en la caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea depositar:", 
					"Siguiente", "Atr�s" );
				}
				
				case 1:
				{
					if( !Player[playerid][uHouseEvict] && HouseInfo[h][huID] != Player[playerid][uID] ) return 1;
				
					showPlayerDialog( playerid, d_house_panel + 8, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Retirar de la caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea retirar:", 
					"Siguiente", "Atr�s" );
				}
			}
		}
		
		case d_house_panel + 7:
		{
			new
				h = GetPVarInt( playerid, "Hpanel:HId" );
		
			if( !response )
			{
				format:g_string( "Caja fuerte - "cBLUE"$%d", HouseInfo[h][hMoney] );
				return showPlayerDialog( playerid, d_house_panel + 6, DIALOG_STYLE_LIST, g_string, h_panel_p3, "Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 )
			{
				return showPlayerDialog( playerid, d_house_panel + 7, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Depositar en la caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea depositar:\n\n\
						"gbDialogError"Valor inv�lido, utilice solo numeros.", 
					"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) > Player[playerid][uMoney] )
			{
				return showPlayerDialog( playerid, d_house_panel + 7, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Depositar en la caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea depositar:\n\n\
						"gbDialogError"No tienes suficiente efectivo, vuelve a intentarlo.", 
					"Siguiente", "Atr�s" );
			}
			
			pformat:( ""gbDefault"Depositaste en la caja fuerte "cBLUE"$%d", strval( inputtext ) );
			psend:( playerid, C_WHITE );
			
			SetPlayerCash( playerid, "-", strval( inputtext ) );
			
			HouseInfo[h][hMoney] += strval( inputtext );
			HouseUpdate( h, "hMoney", HouseInfo[h][hMoney] );
			
			format:g_string( "Caja fuerte - "cBLUE"$%d", HouseInfo[h][hMoney] );
			showPlayerDialog( playerid, d_house_panel + 6, DIALOG_STYLE_LIST, g_string, h_panel_p3, "Seleccionar", "Atr�s" );
		}
		
		case d_house_panel + 8: 
		{
			new
				h = GetPVarInt( playerid, "Hpanel:HId" );
		
			if( !response )
			{
				format:g_string( "Caja fuerte - "cBLUE"$%d", HouseInfo[h][hMoney] );
				return showPlayerDialog( playerid, d_house_panel + 6, DIALOG_STYLE_LIST, g_string, h_panel_p3, "Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 0 )
			{
				return showPlayerDialog( playerid, d_house_panel + 8, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Retirar dinero de la caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea retirar:\n\n\
						"gbDialogError"Valor inv�lido, utilice solo numeros.", 
					"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) > HouseInfo[h][hMoney] )
			{
				return showPlayerDialog( playerid, d_house_panel + 8, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Retirar dinero de la caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea retirar:\n\n\
						"gbDialogError"No tienes tanto dinero en la caja fuerte.", 
					"Siguiente", "Atr�s" );
			}
			
			pformat:( ""gbDefault"Retiraste "cBLUE"$%d"cWHITE" de la caja fuerte.", strval( inputtext ) );
			psend:( playerid, C_WHITE );
			
			SetPlayerCash( playerid, "+", strval( inputtext ) );
			
			HouseInfo[h][hMoney] -= strval( inputtext );
			HouseUpdate( h, "hMoney", HouseInfo[h][hMoney] );
			
			format:g_string( "Caja fuerte - "cBLUE"$%d", HouseInfo[h][hMoney] );
			showPlayerDialog( playerid, d_house_panel + 6, DIALOG_STYLE_LIST, g_string, h_panel_p3, "Seleccionar", "Atr�s" );
		}
		
		case d_house_panel + 9: 
		{
			if( !response ) return showPlayerDialog( playerid, d_house_panel, DIALOG_STYLE_TABLIST, " ", h_panel, "Seleccionar", "Cerrar" );
			
			new
				h = GetPVarInt( playerid, "Hpanel:HId" );
			
			switch( listitem )
			{
				case 0:
				{
					if( !Player[playerid][uHouseEvict] && HouseInfo[h][huID] != Player[playerid][uID] ) return 1;
				
					if( Player[playerid][uHouseEvict] || HouseInfo[h][hRent] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"No puedes cambiar texturas en esta casa." );
						return showPlayerDialog( playerid, d_house_panel + 9, DIALOG_STYLE_LIST, "Ajustes", h_panel_plan, "Seleccionar", "Atr�s" );
					}
					
					showPlayerDialog( playerid, d_house_panel + 13, DIALOG_STYLE_LIST, "Retexturizaci�n", h_panel_texture, "Seleccionar", "Atr�s" );
				}
				
				case 1:
				{
					if( !Player[playerid][uHouseEvict] && HouseInfo[h][huID] != Player[playerid][uID] ) return 1;
				
					if( Player[playerid][uHouseEvict] && !HouseInfo[h][hSettings][2] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"No se puede disponer de muebles en esta casa." );
						return showPlayerDialog( playerid, d_house_panel + 9, DIALOG_STYLE_LIST, "Ajustes", h_panel_plan, "Seleccionar", "Atr�s" );
					}
					
					ShowHouseFurnList( playerid, h, 0 );
				}
			
				case 2:
				{
					if( !Player[playerid][uHouseEvict] && HouseInfo[h][huID] != Player[playerid][uID] ) return 1;
				
					if( Player[playerid][uHouseEvict] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"No puedes comprar muebles adicionales en esta casa." );
						return showPlayerDialog( playerid, d_house_panel + 9, DIALOG_STYLE_LIST, "Ajustes", h_panel_plan, "Seleccionar", "Atr�s" );
					}
				
					showPlayerDialog( playerid, d_mebelbuy + 8, DIALOG_STYLE_LIST, " ", furniture_other, "Seleccionar", "Atr�s" );
				}
				
				case 3:
				{
					if( !Player[playerid][uHouseEvict] && HouseInfo[h][huID] != Player[playerid][uID] ) return 1;
				
					if( Player[playerid][uHouseEvict] )
					{
						SendClient:( playerid, C_WHITE, !""gbError"No se pueden cambiar ajustes si no eres el due�o." );
						return showPlayerDialog( playerid, d_house_panel + 9, DIALOG_STYLE_LIST, "Ajustes", h_panel_plan, "Seleccionar", "Atr�s" );
					}
				
					format:g_small_string( h_panel_evict,
						!HouseInfo[h][hSettings][0] ? ("No") : ("Si"),
						!HouseInfo[h][hSettings][1] ? ("No") : ("Si"),
						!HouseInfo[h][hSettings][2] ? ("No") : ("Si") );
					showPlayerDialog( playerid, d_house_panel + 10, DIALOG_STYLE_TABLIST_HEADERS, "Ajustes de la casa", g_small_string, "Cambiar", "Atr�s" );
				}
			}
		}
		
		case d_house_panel + 10:
		{
			if( !response )
				return showPlayerDialog( playerid, d_house_panel + 9, DIALOG_STYLE_LIST, "Ajustes", h_panel_plan, "Seleccionar", "Atr�s" );
		
			new
				h = GetPVarInt( playerid, "Hpanel:HId" );
		
			if( HouseInfo[h][hSettings][listitem] )
			{
				HouseInfo[h][hSettings][listitem] = 0;
			}
			else
			{
				HouseInfo[h][hSettings][listitem] = 1;
			}
			
			mysql_format:g_small_string( "UPDATE `"DB_HOUSE"` SET `hSettings` = '%d|%d|%d' WHERE `hID` = %d LIMIT 1", 
				HouseInfo[h][hSettings][0], 
				HouseInfo[h][hSettings][1], 
				HouseInfo[h][hSettings][2],
				HouseInfo[h][hID]
			);
			mysql_tquery( mysql, g_small_string );
			
			format:g_small_string( h_panel_evict,
				!HouseInfo[h][hSettings][0] ? ("No") : ("Si"),
				!HouseInfo[h][hSettings][1] ? ("No") : ("Si"),
				!HouseInfo[h][hSettings][2] ? ("No") : ("Si") );
			showPlayerDialog( playerid, d_house_panel + 10, DIALOG_STYLE_TABLIST_HEADERS, "Ajustes de la casa", g_small_string, "Cambiar", "Atr�s" );
		}
		
		case d_house_panel + 11: 
		{
			if( !response ) 
			{
				DeletePVar( playerid, "Hpanel:FPage" );
				DeletePVar( playerid, "Hpanel:FPageMax" );
				DeletePVar( playerid, "Hpanel:FAll" );
			
				return showPlayerDialog( playerid, d_house_panel + 9, DIALOG_STYLE_LIST, "Ajustes", h_panel_plan, "Seleccionar", "Atr�s" );
			}
			
			new
				h = GetPVarInt( playerid, "Hpanel:HId" ),
				page = GetPVarInt( playerid, "Hpanel:FPage" ),
				
				fid;
			
			if( listitem == FURN_PAGE ) 
			{
				if( page == GetPVarInt( playerid, "Hpanel:FPageMax" ) )
					return ShowHouseFurnList( playerid, h, GetPVarInt( playerid, "Hpanel:FPage" ) );
					
				return ShowHouseFurnList( playerid, h, GetPVarInt( playerid, "Hpanel:FPage" ) + 1 );
			}
			else if( listitem == FURN_PAGE + 1 || 
				listitem == GetPVarInt( playerid, "Hpanel:FAll" ) && GetPVarInt( playerid, "Hpanel:FAll" ) < FURN_PAGE ) 
			{
				return ShowHouseFurnList( playerid, h, GetPVarInt( playerid, "Hpanel:FPage" ) - 1 );
			}
			
			SetPVarInt( playerid, "Hpanel:FId", g_dialog_select[playerid][listitem] );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
			
			fid = GetPVarInt( playerid, "Hpanel:FId" );
			 
			format:g_small_string(  ""cBLUE"1. "cGRAY"%s", 
				!HFurn[h][fid][f_state] ? ( "Colocar muebles\n"cBLUE"2. "cGRAY"Destruir los muebles"):("Mover muebles\n"cBLUE"2. "cGRAY"Poner los muebles en el almac�n\n"cBLUE"3. "cGRAY"Destruir los muebles") );
		    
			showPlayerDialog(playerid, d_house_panel + 12, DIALOG_STYLE_LIST, "Gesti�n de los muebles", g_small_string, "Seleccionar", "Atr�s");
		}
		
		case d_house_panel + 12:
		{
			new
				h = GetPVarInt( playerid, "Hpanel:HId" ),
				fid = GetPVarInt( playerid, "Hpanel:FId" );
					
			if( !response ) return ShowHouseFurnList( playerid, h, GetPVarInt( playerid, "Hpanel:FPage" ) );
					
			switch( listitem )
			{
				case 0:
				{
					if( !HFurn[h][fid][f_state] )
					{
						new 
							Float:dist = 2.0,
							Float:angle,
							Float:pos[3],
							Float:rot[3];
								
						SendClient:( playerid, C_WHITE, !HELP_EDITOR );
							
						GetPlayerPos( playerid, HFurn[h][fid][f_pos][0], HFurn[h][fid][f_pos][1], HFurn[h][fid][f_pos][2] );
							
						GetPlayerFacingAngle( playerid, angle );

						HFurn[h][fid][f_pos][0] = HFurn[h][fid][f_pos][0] + dist * - floatsin( angle, degrees );
						HFurn[h][fid][f_pos][1] = HFurn[h][fid][f_pos][1] + dist * floatcos( angle, degrees );
							
						for( new i = 0; i != 3; i++ )
						{
							pos[i] = HFurn[h][fid][f_pos][i];
							rot[i] = HFurn[h][fid][f_rot][i];
						}
							
						HFurn[h][fid][f_object] = CreateDynamicObject(
							HFurn[h][fid][f_model], 
							pos[0], pos[1], pos[2], rot[0], rot[1], rot[2],
							HouseInfo[h][hID] 
						);
							
						EditDynamicObject( playerid, HFurn[h][fid][f_object] );
							
						HFurn[h][fid][f_state] = 1;
							
						SetPVarInt( playerid, "Furn:Edit", 2 );
							
						//SendClient:( playerid, C_GRAY, ""gbSuccess"Conjunto de temas." );
							
						return 1;
					}
						
					new 
						Float:fpos[3];
						
					GetDynamicObjectPos( HFurn[h][fid][f_object], fpos[0], fpos[1], fpos[2] );
						
					if( IsPlayerInRangeOfPoint( playerid, 10.0, fpos[0], fpos[1], fpos[2] ) ) 
					{ 
						EditDynamicObject( playerid, HFurn[h][fid][f_object] );
						SetPVarInt( playerid, "Furn:Edit", 2 );
						return 1;
					}
					else 
					{
						return 
							ShowHouseFurnList( playerid, h, GetPVarInt( playerid, "Hpanel:FPage" ) ),
							SendClient:( playerid, C_GRAY, ""gbError"Est�s muy lejos del mueble" );
					}
				}
					
				case 1:
				{
					if( HFurn[h][fid][f_state] ) 
					{
						if( IsValidDynamicObject( HFurn[h][fid][f_object] ) )
							DestroyDynamicObject( HFurn[h][fid][f_object] );
						
						for( new i = 0; i != 3; i++ )
						{
							HFurn[h][fid][f_pos][i] = 
							HFurn[h][fid][f_rot][i] = 0.0;
						}
							
						HFurn[h][fid][f_state] = 0;
							
						UpdateFurnitureHouse( h, fid );
							
						SendClient:( playerid, C_GRAY, ""gbSuccess"Este mueble fue destruido." );
							
						return ShowHouseFurnList( playerid, h, GetPVarInt( playerid, "Hpanel:FPage" ) );
					}
				}
			}
				
			pformat:( ""gbSuccess"El mueble "cBLUE"%s"cWHITE" fue destruido.", HFurn[h][fid][f_name] );
			psend:( playerid, C_WHITE );
				
			if( IsValidDynamicObject( HFurn[h][fid][f_object] ) )
				DestroyDynamicObject( HFurn[h][fid][f_object] );
							
			DeleteFurnitureHouse( h, fid );
			HouseInfo[h][hCountFurn]--;
							
			ShowHouseFurnList( playerid, h, GetPVarInt( playerid, "Bpanel:FPage" ) );
		}
		//Retexter
		case d_house_panel + 13:
		{
			if( !response ) return showPlayerDialog( playerid, d_house_panel + 9, DIALOG_STYLE_LIST, "Ajustes", h_panel_plan, "Seleccionar", "Atr�s" );
		
			SetPVarInt( playerid, "Hpanel:Type", listitem );
			ShowHousePartInterior( playerid, GetPVarInt( playerid, "Hpanel:HId" ), listitem );
		}
		//Lista de paredes
		case d_house_panel + 14:
		{
			if( !response )
			{
				DeletePVar( playerid, "Hpanel:Type" );
				return showPlayerDialog( playerid, d_house_panel + 13, DIALOG_STYLE_LIST, "Retexturizaci�n", h_panel_texture, "Seleccionar", "Atr�s" );
			}
			
			ShowTexViewer( playerid, GetPVarInt( playerid, "Hpanel:Type" ), listitem, 0 );
		}
		
		case d_house_panel + 15:
		{
			if( !response ) 
			{
				DeletePVar( playerid, "Hpanel:PriceTexture" );
				return 1;
			}
			
			switch( listitem )
			{
				case 0:
				{
					format:g_small_string( "\
						"cWHITE"�Quieres comprar esta textura?\n\
						"gbDialog"Precio: "cBLUE"$%d", GetPVarInt( playerid, "Hpanel:PriceTexture" ) );
						
					showPlayerDialog( playerid, d_house_panel + 16, DIALOG_STYLE_MSGBOX, "Retexturizaci�n", g_small_string, "Si", "No" );
				}
				
				case 1:
				{
					new
						h = GetPVarInt( playerid, "Hpanel:HId" );
				
					switch( Menu3DData[playerid][CurrTextureType] )
					{
						case 0: SetHouseTexture( h, HouseInfo[h][hWall][ Menu3DData[playerid][CurrPartNumber] ], 0, Menu3DData[playerid][CurrPartNumber] );
						case 1: SetHouseTexture( h, HouseInfo[h][hFloor][ Menu3DData[playerid][CurrPartNumber] ], 1, Menu3DData[playerid][CurrPartNumber] );
						case 2: SetHouseTexture( h, HouseInfo[h][hRoof][ Menu3DData[playerid][CurrPartNumber] ], 2, Menu3DData[playerid][CurrPartNumber] );
						case 3: SetHouseTexture( h, HouseInfo[h][hStairs], 3, INVALID_PARAM );
					}
				
					DestroyTexViewer( playerid );
					
					showPlayerDialog( playerid, d_house_panel + 13, DIALOG_STYLE_LIST, "Retexturizaci�n", h_panel_texture, "Seleccionar", "Atr�s" );
				}
			}
		}
		
		case d_house_panel + 16:
		{
			new
				price = GetPVarInt( playerid, "Hpanel:PriceTexture" ),
				h = GetPVarInt( playerid, "Hpanel:HId" );
		
			if( !response )
			{
				format:g_small_string( "\
					"cWHITE"Acci�n\t"cWHITE"Precio\n\
					"cWHITE"Comprar textura\t"cBLUE"$%d\n\
					"cWHITE"Salir", price );
		
				return showPlayerDialog( playerid, d_house_panel + 15, DIALOG_STYLE_TABLIST_HEADERS, "Retexturizaci�n", g_small_string, "Seleccionar", "Cerrar" );
			}
			
			if( Player[playerid][uMoney] < price )
			{
				SendClient:( playerid, C_WHITE, !NO_MONEY );
			
				format:g_small_string( "\
					"cWHITE"Acci�n\t"cWHITE"Precio\n\
					"cWHITE"Comprar textura\t"cBLUE"$%d\n\
					"cWHITE"Salir", price );
		
				return showPlayerDialog( playerid, d_house_panel + 15, DIALOG_STYLE_TABLIST_HEADERS, "Retexturizaci�n", g_small_string, "Seleccionar", "Cerrar" );
			}
			
			switch( Menu3DData[playerid][CurrTextureType] )
			{
				case 0:
				{
					HouseInfo[h][hWall][ Menu3DData[playerid][CurrPartNumber] ] = SelectedBox[playerid] + Menu3DData[playerid][CurrTextureIndex];
				
					mysql_format:g_small_string( "UPDATE `"DB_HOUSE"` SET `hWall` = '%d|%d|%d|%d|%d|%d|%d|%d|%d|%d' WHERE `hID` = %d LIMIT 1", 
						HouseInfo[h][hWall][0],
						HouseInfo[h][hWall][1],
						HouseInfo[h][hWall][2],
						HouseInfo[h][hWall][3],
						HouseInfo[h][hWall][4],
						HouseInfo[h][hWall][5],
						HouseInfo[h][hWall][6],
						HouseInfo[h][hWall][7],
						HouseInfo[h][hWall][8],
						HouseInfo[h][hWall][9],
						HouseInfo[h][hID] );
					mysql_tquery( mysql, g_small_string );
				}
				
				case 1:
				{
					HouseInfo[h][hFloor][ Menu3DData[playerid][CurrPartNumber] ] = SelectedBox[playerid] + Menu3DData[playerid][CurrTextureIndex];
				
					mysql_format:g_small_string( "UPDATE `"DB_HOUSE"` SET `hFloor` = '%d|%d|%d|%d|%d|%d|%d|%d|%d|%d' WHERE `hID` = %d LIMIT 1", 
						HouseInfo[h][hFloor][0],
						HouseInfo[h][hFloor][1],
						HouseInfo[h][hFloor][2],
						HouseInfo[h][hFloor][3],
						HouseInfo[h][hFloor][4],
						HouseInfo[h][hFloor][5],
						HouseInfo[h][hFloor][6],
						HouseInfo[h][hFloor][7],
						HouseInfo[h][hFloor][8],
						HouseInfo[h][hFloor][9],
						HouseInfo[h][hID] );
					mysql_tquery( mysql, g_small_string );
				}
				
				case 2:
				{
					HouseInfo[h][hRoof][ Menu3DData[playerid][CurrPartNumber] ] = SelectedBox[playerid] + Menu3DData[playerid][CurrTextureIndex];
				
					mysql_format:g_small_string( "UPDATE `"DB_HOUSE"` SET `hRoof` = '%d|%d|%d|%d|%d|%d|%d' WHERE `hID` = %d LIMIT 1", 
						HouseInfo[h][hRoof][0],
						HouseInfo[h][hRoof][1],
						HouseInfo[h][hRoof][2],
						HouseInfo[h][hRoof][3],
						HouseInfo[h][hRoof][4],
						HouseInfo[h][hRoof][5],
						HouseInfo[h][hRoof][6],
						HouseInfo[h][hID] );
					mysql_tquery( mysql, g_small_string );
				}
				
				case 3:
				{
					HouseInfo[h][hStairs] = SelectedBox[playerid] + Menu3DData[playerid][CurrTextureIndex];
					HouseUpdate( h, "hStairs", HouseInfo[h][hStairs] );
				}
			}
			
			SetPlayerCash( playerid, "-", price );
			
			DeletePVar( playerid, "Hpanel:PriceTexture" );
			DestroyTexViewer( playerid );
			
			SendClient:( playerid, C_WHITE, ""gbSuccess"La textura fue aplicada con �xito." );
			
			showPlayerDialog( playerid, d_house_panel + 13, DIALOG_STYLE_LIST, "Retexturizaci�n", h_panel_texture, "Seleccionar", "Atr�s" );
		}
	}
	
	return 1;
}