
function Business_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{
	switch( dialogid ) 
	{
		case d_buy_business: 
		{
			if( !response ) 
			{	
				DeletePVar( playerid, "Enter:BId" ); 
				return 1;
			}
			
			new
				b = GetPVarInt( playerid, "Enter:BId" );
			
			if( !IsPlayerInRangeOfPoint( playerid, 2.0, BusinessInfo[b][b_enter_pos][0], BusinessInfo[b][b_enter_pos][1], BusinessInfo[b][b_enter_pos][2] ) )
			{
				DeletePVar( playerid, "Enter:BId" ); 
				return SendClient:( playerid, C_WHITE, !""gbError"Debes estar cerca de la puerta de entrada." );
			}
			
			setPlayerPos( playerid, BusinessInfo[b][b_exit_pos][0],
									BusinessInfo[b][b_exit_pos][1],
									BusinessInfo[b][b_exit_pos][2] );
			SetPlayerFacingAngle( playerid, BusinessInfo[b][b_exit_pos][3] );
			
			Player[playerid][tgpsPos][0] = BusinessInfo[b][b_enter_pos][0];
			Player[playerid][tgpsPos][1] = BusinessInfo[b][b_enter_pos][1];
			Player[playerid][tgpsPos][2] = BusinessInfo[b][b_enter_pos][2];
	
			SetPlayerVirtualWorld( playerid, BusinessInfo[b][b_id] );						
			setHouseWeather( playerid );
			//stopPlayer( playerid, 1 );
		}
		
		case d_buy_business + 1:
		{
			if( !response ) 
			{
				return showPlayerDialog( playerid, d_buy_menu, DIALOG_STYLE_LIST, 
					"Agencia inmobiliaria", 
					"1. "cGRAY"Directorio de vivienda"cWHITE"\n\
					2. "cGRAY"Directorio de negocios", 
				"Seleccionar", "Cerrar" );
			}
			
			switch( listitem ) 
			{
				case 0: 
				{// Lista de todas las empresas
					ShowBusinessList( playerid, 1, 0 );
					SetPVarInt( playerid, "BBuy:case", 1 );
				}
				case 1:
				{
					// Ordenar por categor�a de precio
					showPlayerDialog( playerid, d_buy_business + 4, DIALOG_STYLE_INPUT, " ", 
					""cBLUE"Ordenar por precio\n\n\
					"cWHITE"Especifique el rango de precios para ver los negocios que le interesan\
					\n Ejemplo: "cBLUE"60000-200000", "Buscar", "Atr�s" );
					SetPVarInt( playerid, "BBuy:case", 2 );
				}
				case 2: 
				{	// Ordenar por tipo de negocio
					showPlayerDialog( playerid, d_buy_business + 5, DIALOG_STYLE_LIST, "Ordenar por tipo de negocio",
						"1. "cGRAY"Barras"cWHITE"\n\ 
						 2. "cGRAY"Restaurantes"cWHITE"\n\
						 3. "cGRAY"Bares"cWHITE"\n\
						 4. "cGRAY"Tiendas"cWHITE"", 
					"Seleccionar", "Cerrar" );
					SetPVarInt( playerid, "BBuy:case", 3 );
				}
				case 3: 
				{
					showPlayerDialog( playerid, d_buy_business + 6, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Comprar propiedad\n\n\
						"cWHITE"Ingrese el n�mero de negocio:", "Ingresar", "Atr�s" );
				}
				case 4: 
				{
					// Listado de empresas del jugador.
					ShowBusinessList( playerid, 4, 0 );
				}
				
			}
		}
		
		case d_buy_business + 2:
		{
			if( !response )
			{
				SetPVarInt( playerid, "BBuy:List", -1 );
				
				switch( GetPVarInt( playerid, "BBuy:case" ) )
				{
					case 1:
					{
						DeletePVar( playerid, "BBuy:case" );
						return showPlayerDialog( playerid, d_buy_business + 1, DIALOG_STYLE_LIST, " ", 
								"1. Lista de todas las empresas\n2. Ordenar por costo\
								\n3. Ordenar por tipo de negocio\n4. Comprar un negocio por n�mero\
								\n5. Vender negocio", "Seleccionar", "Atr�s" 
								);
					}
					
					case 2:
					{
						return showPlayerDialog( playerid, d_buy_business + 4, DIALOG_STYLE_INPUT, " ", 
							""cBLUE"Ordenar por precio\n\n\
							"cWHITE"Especifique el rango de precios para ver los negocios que le interesan\
							\nEjemplo: "cBLUE"60000-200000", "Ingresar", "Atr�s" );
					}
					
					case 3:
					{
					
						return showPlayerDialog( playerid, d_buy_business + 5, DIALOG_STYLE_LIST, "Ordenar por tipo de negocio",
							"1. "cGRAY"Barras"cWHITE"\n\ 
							 2. "cGRAY"Restaurantes"cWHITE"\n\
							 3. "cGRAY"Bares"cWHITE"\n\
							 4. "cGRAY"Tiendas"cWHITE"", 
						"Seleccionar", "Cerrar" );
					}
					
					case 4:
					{
						return showPlayerDialog( playerid, d_buy_business + 6, DIALOG_STYLE_INPUT, " ", "\
							"cBLUE"Comprar propiedad\n\n\
							"cWHITE"Ingrese el n�mero de negocio:",
							"Ingresar", "Atr�s" );
					}
				}
			}
			
			if( listitem == BBUY_LIST ) 
			{
				return ShowBusinessList( playerid, GetPVarInt( playerid, "BBuy:Type" ), GetPVarInt( playerid, "BBuy:List" ) + 1 );
			}
			else if( listitem == BBUY_LIST + 1 || 
				listitem == GetPVarInt( playerid, "BBuy:Last" ) && GetPVarInt( playerid, "BBuy:Last" ) < BBUY_LIST ) 
			{
				return ShowBusinessList( playerid, GetPVarInt( playerid, "BBuy:Type" ), GetPVarInt( playerid, "BBuy:List" ) - 1 );
			}
			
			ShowBusinessBuyMenu( playerid, g_dialog_select[playerid][listitem] );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
		}
		
		case d_buy_business + 3:
		{
			setPlayerPos( playerid,
				GetPVarFloat( playerid, "BBuy:pos_buy_x" ),
				GetPVarFloat( playerid, "BBuy:pos_buy_y" ),
				GetPVarFloat( playerid, "BBuy:pos_buy_z") 
			);
				
			SetCameraBehindPlayer( playerid );
			
			SetPlayerVirtualWorld( playerid, 13 ), 
			SetPlayerInterior( playerid, 1 );
				
			DeletePVar( playerid, "BBuy:pos_buy_x" ), 
			DeletePVar( playerid, "BBuy:pos_buy_y" ),
			DeletePVar( playerid, "BBuy:pos_buy_z" ),
			DeletePVar( playerid, "BBuy:Camera" );
			
			if( response ) 
			{
				new 
					b = GetPVarInt( playerid, "BBuy:Business" );
				
				if( IsOwnerBusinessCount( playerid ) >= 1 + Premium[playerid][prem_business] ) 
				{
					ShowBusinessList( playerid, GetPVarInt( playerid, "BBuy:Type" ), GetPVarInt( playerid, "BBuy:List" ) );
					return SendClient:( playerid, C_WHITE, ""gbError"Ya tienes un negocio" );
				}
				
				if( BusinessInfo[b][b_price] > Player[playerid][uMoney] ) 
				{
					ShowBusinessList( playerid, GetPVarInt( playerid, "BBuy:Type" ), GetPVarInt( playerid, "BBuy:List" ) );
					return SendClient:( playerid, C_GRAY, ""gbError"No tienes suficiente dinero para comprar este negocio" );
				}
				
				if( BusinessInfo[b][b_user_id] != INVALID_PARAM )
				{
					ShowBusinessList( playerid, GetPVarInt( playerid, "BBuy:Type" ), GetPVarInt( playerid, "BBuy:List" ) );
					return SendClient:( playerid, C_WHITE, ""gbError"Este negocio fue adquirido por alguien antes que tu" );
				}

				BusinessInfo[b][b_user_id] = Player[playerid][uID];
				UpdateBusiness( b, "b_user_id", BusinessInfo[b][b_user_id] );
				
				BusinessInfo[b][b_products] = 1000;
				UpdateBusiness( b, "b_products", BusinessInfo[b][b_products] );
				
				SetPlayerCash( playerid, "-", BusinessInfo[b][b_price] );
				
				format:g_small_string( "compr� propio negocio");
				MeAction( playerid, g_small_string, 1 );
			
				InsertPlayerBusiness( playerid, b );
				
				log( LOG_BUY_BUSINESS, "compr� un negocio", Player[playerid][uID], BusinessInfo[b][b_id] );
				
				pformat:(""gbSuccess"Te convertiste en el due�o del negocio '"cBLUE"%s #%d"cWHITE"'. Para administrar tu nuevo negocio usa "cBLUE"/npanel"cWHITE".", GetBusinessType( b ), BusinessInfo[b][b_id] );
				psend:( playerid, C_WHITE );
				
				g_player_interaction{playerid} = 0;
			}
			else
			{
				if( GetPVarInt( playerid, "BBuy:case" ) == 4 )
				{
					SetPVarInt( playerid, "BBuy:case", INVALID_PLAYER_ID );
					return showPlayerDialog( playerid, d_buy_business + 1, DIALOG_STYLE_LIST, " ", 
					"1. Lista de todos los negocios\n2. Ordenar por costo\
					\n3. Ordenar por tipo de negocio\n4. Comprar negocio por n�mero\
					\n5. Vender negocio", "Seleccionar", "Atr�s" );
					
				}
				
				ShowBusinessList( playerid, GetPVarInt( playerid, "BBuy:Type" ), GetPVarInt( playerid, "BBuy:List" ) );
			}
						
			DeletePVar( playerid, "BBuy:Business" );
		}
		
		
		case d_buy_business + 4:
		{
			if( !response ) 
			{
				showPlayerDialog( playerid, d_buy_business + 1, DIALOG_STYLE_LIST, " ", 
					"1. Lista de todos los negocios\n2. Ordenar por costo\
					\n3. Ordenar por tipo de negocio\n4. Comprar negocio por n�mero\
					\n5. Vender negocio", "Seleccionar", "Atr�s" );
				return 1;
			}
			
			if( sscanf( inputtext, "p<->a<d>[2]", inputtext[0], inputtext[1] ) || inputtext[1] <= inputtext[0] ) 
			{
				showPlayerDialog( playerid, d_buy_business + 4, DIALOG_STYLE_INPUT, " ", 
					""cBLUE"Ordenar por precio\n\n\
					"cWHITE"Especifique el rango de precios para ver los negocios que le interesan\
					\nEjemplo: "cBLUE"60000-200000\n\n\
					"gbDialogError"El rango especificado no es valido",
				"Ingresar", "Atr�s" );
				return 1;
			}
			
			SetPVarInt( playerid, "BBuy:PriceM", inputtext[0] ), 
			SetPVarInt( playerid, "BBuy:PriceH", inputtext[1] );
			DeletePVar( playerid, "BBuy:Business" );
			ShowBusinessList( playerid, 2, 0 );
		}
		
		case d_buy_business + 5:
		{
			if( !response ) 
			{
				showPlayerDialog( playerid, d_buy_business + 1, DIALOG_STYLE_LIST, " ", 
					"1. Lista de todos los negocios\n2. Ordenar por costo\
					\n3. Ordenar por tipo de negocio\n4. Comprar negocio por n�mero\
					\n5. Vender negocio", "Seleccionar", "Atr�s" );
				return 1;
			}
			
			SetPVarInt( playerid, "BBuy:btype", listitem );
			ShowBusinessList( playerid, 3, 0 );
			
		}
		
		
		case d_buy_business + 6:
		{
			if( !response ) 
			{
				return showPlayerDialog( playerid, d_buy_business + 1, DIALOG_STYLE_LIST, " ", 
					"1. Lista de todos los negocios\n2. Ordenar por costo\
					\n3. Ordenar por tipo de negocio\n4. Comprar negocio por n�mero\
					\n5. Vender negocio", "Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 )
			{
				return showPlayerDialog( playerid, d_buy_business + 6, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Comprar propiedad\n\n\
					"cWHITE"Ingrese el n�mero de negocio:\n\
					"gbDialogError"El n�mero de negocio introducido no es valido", "Ingresar", "Atr�s" );
			}
			
			for( new b; b < MAX_BUSINESS; b++ )
			{
				if( BusinessInfo[b][b_id] == strval( inputtext ) )
				{
					if( BusinessInfo[b][b_user_id] != INVALID_PARAM )
					{
						return showPlayerDialog( playerid, d_buy_business + 6, DIALOG_STYLE_INPUT, " ", "\
							"cBLUE"Comprar propiedad\n\n\
							"cWHITE"Ingrese el n�mero de negocio:\n\
							"gbDialogError"Este negocio ya tiene due�o",
						"Ingresar", "Atr�s" );
					}
					
					ShowBusinessBuyMenu( playerid, b );
					SetPVarInt( playerid, "BBuy:case", 4 );
					return 1;
				}
			}
			
			showPlayerDialog( playerid, d_buy_business + 6, DIALOG_STYLE_INPUT, " ", "\
				"cBLUE"Comprar propiedad\n\n\
				"cWHITE"Ingrese el n�mero de negocio:\n\
				"gbDialogError"El negocio no existe",
				"Ingresar", "Atr�s" );
		}
		
		
		case d_buy_business + 7:
		{
			if( !response )
			{
				showPlayerDialog( playerid, d_buy_business + 1, DIALOG_STYLE_LIST, " ", 
					"1. Lista de todos los negocios\n2. Ordenar por costo\
					\n3. Ordenar por tipo de negocio\n4. Comprar negocio por n�mero\
					\n5. Vender negocio", "Seleccionar", "Atr�s" ); 
				return 1;
			}
			
			SetPVarInt( playerid, "BBuy:BId", g_dialog_select[playerid][listitem] );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
			
			showPlayerDialog( playerid, d_buy_business + 8, DIALOG_STYLE_LIST,
				"Venta de propiedades", 
				""cWHITE"1. "cGRAY"Vender a una agencia de negocios"cWHITE" \
				\n2. "cGRAY"Vender negocio a un jugador",
				"Seleccionar", "Atr�s" 
			);	
		}

		
		case d_buy_business + 8:
		{
				
			if( !response )
			{
				ShowBusinessList( playerid, 4, 0 );
				return 1;
			}
			
			switch( listitem )
			{
				case 0:
				{
					new
						businessid = GetPVarInt( playerid, "BBuy:BId" );
						
					format:g_string( "\
						"cBLUE"Vender negocio\n\
						"cWHITE"Tu %s #%d se vender� a la agencia por $%d",
						GetBusinessType( businessid ),
						BusinessInfo[businessid][b_id],
						floatround( BusinessInfo[businessid][b_price] * ( 0.6 + ( Premium[playerid][prem_house_property] / 100 ) ) ) 
					);
						
						
					showPlayerDialog( playerid, d_buy_business + 11, DIALOG_STYLE_MSGBOX, " ", g_string, 
						"Vender", "Cancelar" );
				}
				
				case 1:
				{
					showPlayerDialog( playerid, d_buy_business + 9, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Vender negocio a un jugador\n\n\
						"cWHITE"Ingresa el ID del jugador", "Ingresar", "Atr�s" );
				}
			}
		}
		
		
		case d_buy_business + 9:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_buy_business + 8, DIALOG_STYLE_LIST,
					"Venta de propiedades", 
					"1. "cGRAY"Vender a una agencia de negocios"cWHITE" \
					\n2. "cGRAY"Vender negocio a un jugador",
				"Seleccionar", "Atr�s" );
			}
		
			if( inputtext[0] == EOS ) 
			{
				return showPlayerDialog( playerid, d_buy_business + 9, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Vender negocio\n\n\
					"cWHITE"Ingresa la ID del jugador:\n\n\
					"gbDialog"El jugador debe estar cerca de ti.\n\
					"gbDialogError"Ingrese el ID del jugador.", 
				"Siguiente", "Atr�s" );
			}	
					
			if( !IsLogged( strval( inputtext ) ) || strval( inputtext ) == playerid ) 
			{
				return showPlayerDialog( playerid, d_buy_business + 9, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Vender negocio\n\n\
					"cWHITE"Ingresa la ID del jugador:\n\n\
					"gbDialog"El jugador debe estar cerca de ti.\n\
					"gbDialogError"Ingrese el ID del jugador.", 
				"Siguiente", "Atr�s" );
			}
					
			if( IsOwnerBusinessCount( strval( inputtext ) ) >= 1 + Premium[ strval( inputtext ) ][prem_business] ) 
			{
				return showPlayerDialog( playerid, d_buy_business + 9, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Vender negocio\n\n\
					"cWHITE"Ingresa la ID del jugador:\n\n\
					"gbDialog"El jugador debe estar cerca de ti.\n\
					"gbDialogError"Este jugador ya tiene un negocio.", 
				"Siguiente", "Atr�s" );
			}
			
			if( GetDistanceBetweenPlayers( playerid, strval( inputtext ) ) > 3.0 )
			{
				return showPlayerDialog( playerid, d_buy_business + 9, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Vender negocio\n\n\
					"cWHITE"Ingresa la ID del jugador:\n\n\
					"gbDialogError"El jugador debe estar cerca de ti.", 
				"Siguiente", "Atr�s" );
			}
			
			if( g_player_interaction{ strval( inputtext ) } )
			{
				return showPlayerDialog( playerid, d_buy_business + 9, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Vender negocio\n\n\
					"cWHITE"Ingresa la ID del jugador:\n\n\
					"gbDialog"El jugador debe estar cerca de ti.\n\
					"gbDialogError"No puedes interactuar con este jugador.", 
				"Siguiente", "Atr�s" );
			}
			
			SetPVarInt( playerid, "BBuy:PlayerID", strval( inputtext ) );
			ShowDialogBusinessSell( playerid, GetPVarInt( playerid, "BBuy:BId" ), " " );
		}
		
		
		case d_buy_business + 10:
		{
			//new 
			//	sellid = GetPVarInt( playerid, "BBuy:PlayerID" );
			
			if( !response )
			{
				DeletePVar( playerid, "BBuy:PlayerID" );
				return showPlayerDialog( playerid, d_buy_business + 9, DIALOG_STYLE_INPUT, " ", "\
					"cBLUE"Vender negocio a un jugador\n\n\
					"cWHITE"Ingresa el ID del jugador", "Ingresar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS ) 
			{
				return ShowDialogBusinessSell( playerid, GetPVarInt( playerid, "BBuy:BId" ), "\n\n"gbDialogError"Ingresa el monto a continuaci�n:" );
			}
			
			if( !IsNumeric( inputtext ) )
			{
				return ShowDialogBusinessSell( playerid, GetPVarInt( playerid, "BBuy:BId" ), "\n\n"gbDialogError"Ingresa el monto a continuaci�n:" );
			}
			
			if( GetDistanceBetweenPlayers( playerid, GetPVarInt( playerid, "BBuy:PlayerID" ) ) > 3.0 )
			{
				return ShowDialogBusinessSell( playerid, GetPVarInt( playerid, "BBuy:BId" ), "\n\n"gbDialogError"El jugador debe estar cerca de ti." );
			}
			
			if( strval( inputtext ) < floatround( BusinessInfo[GetPVarInt( playerid, "BBuy:BId" )][b_price] * 0.5 ) ||  strval( inputtext ) > ( BusinessInfo[GetPVarInt( playerid, "BBuy:BId" )][b_price] * 2 ) )
			{
				return ShowDialogBusinessSell( playerid, GetPVarInt( playerid, "BBuy:BId" ), "\n\n"gbDialogError"La cantidad especificada et� fuera de los limites." );
			}
		
			if( Player[GetPVarInt( playerid, "BBuy:PlayerID" )][uMoney] < strval( inputtext ) ) 
			{
				return ShowDialogBusinessSell( playerid, GetPVarInt( playerid, "BBuy:BId" ), "\n\n"gbDialogError"El jugador no tiene la cantidad especificada de dinero en efectivo." );
			}
			
			pformat:( ""gbSuccess"Has enviado una oferta a "cBLUE"%s [%d]"cWHITE" sobre la venta de '"cBLUE"%s #%d"cWHITE"'.",
				GetAccountName( GetPVarInt( playerid, "BBuy:PlayerID" ) ),
				GetPVarInt( playerid, "BBuy:PlayerID" ),
				GetBusinessType( GetPVarInt( playerid, "BBuy:BId" ) ),
				BusinessInfo[GetPVarInt( playerid, "BBuy:BId" )][b_id]
			);
			
			psend:( playerid, C_WHITE ); 
			
			format:g_big_string( 
				""gbSuccess""cBLUE"%s [%d]"cWHITE" te ha enviado una oferta\n\ 
				de venta de "cBLUE"%s #%d"cWHITE" por "cBLUE"$%d"cWHITE".",
				GetAccountName( playerid ),
				playerid,
				GetBusinessType( GetPVarInt( playerid, "BBuy:BId" ) ),
				BusinessInfo[GetPVarInt( playerid, "BBuy:BId" )][b_id],
				strval( inputtext )
			);
			
			showPlayerDialog( GetPVarInt( playerid, "BBuy:PlayerID" ), d_buy_business + 12, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Aceptar", "Rechazar" );
			
			g_player_interaction{GetPVarInt( playerid, "BBuy:PlayerID" )} = 1;
			
			SetPVarInt( GetPVarInt( playerid, "BBuy:PlayerID" ), "BBuy:SellID", playerid );
			SetPVarInt( GetPVarInt( playerid, "BBuy:PlayerID" ), "BBuy:Price", strval( inputtext ) );
			SetPVarInt( GetPVarInt( playerid, "BBuy:PlayerID" ), "BBuy:BId", GetPVarInt( playerid, "BBuy:BId" ) );
			
			DeletePVar( playerid, "BBuy:PlayerID" );
			DeletePVar( playerid, "BBuy:BId" );
		}
		
		
		case d_buy_business + 11:
		{
			new
				businessid = GetPVarInt( playerid, "BBuy:BId" );
			if( !response )
			{
				return showPlayerDialog( playerid, d_buy_business + 8, DIALOG_STYLE_LIST,
					"Venta de propiedades", 
					"1. "cGRAY"Vender a una agencia de negocios"cWHITE" \
					\n2. "cGRAY"Vender negocio a un jugador",
				"Seleccionar", "Atr�s" );
			}
			
			SellBusiness( businessid );
			
			Player[playerid][uMoney] += floatround( BusinessInfo[businessid][b_price] * ( 0.6 + ( Premium[playerid][prem_house_property] / 100 ) ) );
				
			UpdatePlayer( playerid, "uMoney", Player[playerid][uMoney] );
			
			pformat:( ""gbSuccess"Vendiste tu negocio a una agencia por "cBLUE"$%d"cWHITE".", floatround( BusinessInfo[businessid][b_price] * ( 0.6 + ( Premium[playerid][prem_house_property] / 100 ) ) ) );
			psend:( playerid, C_WHITE ); 
			
			RemovePlayerBusiness( playerid, businessid );
			g_player_interaction{playerid} = 0;
			
			SetPVarInt( playerid, "BBuy:BId", INVALID_PLAYER_ID );
		}
		
		case d_buy_business + 12:
		{
			new 
				sellid = GetPVarInt( playerid, "BBuy:SellID" ),
				price = GetPVarInt( playerid, "BBuy:Price" ),
				businessid = GetPVarInt( playerid, "BBuy:BId" );
			
			if( !IsLogged( sellid ) ) 
			{
				SendClient:( playerid, C_GRAY, !""gbError"Operaci�n interrumpida. El jugador se ha desconectado." );
					
				DeletePVar( playerid, "BBuy:BId" ), 
				DeletePVar( playerid, "BBuy:SellID" ),
				DeletePVar( playerid, "BBuy:Price" );
				
				g_player_interaction{playerid} = 0;
				return 1;
			}
			
			if( !response )
			{
				pformat:(""gbError"Rechazas la oferta de compra de "cBLUE"%s [%d]"cWHITE".",
					GetAccountName( sellid ),
					sellid
				);
				
				psend:( playerid, C_WHITE );
				
				pformat:(""gbError""cBLUE"%s [%d]"cWHITE" rechaza la oferta de venta.",
					GetAccountName( playerid ),
					playerid
				);
				
				psend: ( sellid, C_WHITE );

				DeletePVar( playerid, "BBuy:SellID" ),
				DeletePVar( playerid, "BBuy:Price" );
				DeletePVar( playerid, "BBuy:BId" );
				
				g_player_interaction{playerid} = 0;
				g_player_interaction{sellid} = 0;
				return 1;
			}
				
			if( Player[playerid][uMoney] < price )
			{
				SendClient:( playerid, C_GRAY, !NO_MONEY );
				
				DeletePVar( playerid, "BBuy:BId" ), 
				DeletePVar( playerid, "BBuy:SellID" ),
				DeletePVar( playerid, "BBuy:Price" );
				
				pformat:(""gbError""cBLUE"%s [%d]"cWHITE" no tiene dinero suficiente para la compra. Operaci�n interrumpida",
					GetAccountName( playerid ),
					playerid
				);
				psend:( sellid, C_WHITE );
				
				g_player_interaction{playerid} = 0;
				g_player_interaction{sellid} = 0;
				
				return 1;
			}
			
			if( GetDistanceBetweenPlayers( playerid, sellid ) > 3.0 )
			{
				SendClient:( playerid, C_WHITE, !""gbError"El due�o del negocio est� demasiado lejos de ti." );
				
				DeletePVar( playerid, "BBuy:BId" ), 
				DeletePVar( playerid, "BBuy:SellID" ),
				DeletePVar( playerid, "BBuy:Price" );
				
				pformat:(""gbError""cBLUE"%s [%d]"cWHITE" est� demasiado lejos de ti.",
					GetAccountName( playerid ),
					playerid
				);
				psend:( sellid, C_WHITE );
				
				g_player_interaction{playerid} = 0;
				g_player_interaction{sellid} = 0;
				
				return 1;
			}
			
			if( IsOwnerBusinessCount( playerid ) >= 1 + Premium[ playerid ][prem_business] )
			{
				SendClient:( playerid, C_WHITE, !""gbError"Operaci�n interrumpida. Ya tienes un negocio." );
				
				DeletePVar( playerid, "BBuy:BId" ), 
				DeletePVar( playerid, "BBuy:SellID" ),
				DeletePVar( playerid, "BBuy:Price" );
				
				pformat:(""gbError"Operaci�n interrumpida. "cBLUE"%s [%d]"cWHITE" ya tiene un negocio.",
					GetAccountName( playerid ),
					playerid
				);
				psend:( sellid, C_WHITE );
				
				g_player_interaction{playerid} = 0;
				g_player_interaction{sellid} = 0;
				
				return 1;
			}			
				
			SetPlayerCash( sellid, "+", price );
			SetPlayerCash( playerid, "-", price );
				
			OfferSalePlayerBusiness( sellid, playerid, businessid );
				
			pformat:( ""gbSuccess"Compraste '"cBLUE"%s #%d"cWHITE"' a "cBLUE"%s [%d]"cWHITE" por $%d.",
				GetBusinessType( businessid ),
				BusinessInfo[businessid][b_id],
				GetAccountName( sellid ),
				sellid,
				price
			);
			psend:( playerid, C_WHITE );
				
			pformat:( ""gbSuccess"Vendiste '"cBLUE"%s #%d"cWHITE"' a "cBLUE"%s [%d]"cWHITE" por $%d.",
				GetBusinessType( businessid ),
				BusinessInfo[businessid][b_id],
				GetAccountName( playerid ),
				playerid,
				price
			);
			psend:( sellid, C_WHITE );
				
			g_player_interaction{playerid} = 0;
			g_player_interaction{sellid} = 0;
				
			log( LOG_BUY_BUSINESS_FROM_PLAYER, "compr� un negocio de", Player[playerid][uID], Player[sellid][uID], price );
				
			DeletePVar( playerid, "BBuy:BId" ), 
			DeletePVar( playerid, "BBuy:SellID" ),
			DeletePVar( playerid, "BBuy:Price" );		
		}
		
//----------------------------------------Conversaciones con / bpanel--------------------------
//-----------------------------------------------------------------------------------
		
		case d_business_panel:
		{
			if( !response )
			{
				SetPVarInt( playerid, "Bpanel:BId", INVALID_PLAYER_ID );
				SetPVarInt( playerid, "Bpanel:Interior", INVALID_PLAYER_ID );
			
				CancelSelectTextDraw( playerid );
				return 1;
			}
			
			new 
				businessid = GetPVarInt( playerid, "Bpanel:BId" ),
				hour;
				
			if( BusinessInfo[businessid][b_products_time] >= gettime() && !BusinessInfo[businessid][b_products] )
			{
				hour = floatround ( ( 259200 - ( BusinessInfo[businessid][b_products_time] - gettime() ) ) / 1200 );
			}
			else
			{
				hour = 0;
			}
			
			switch( listitem )
			{
				case 0:
				{
					format:g_big_string( "\
						"cWHITE"Informaci�n\n\n\
						"cBLUE"%s"cWHITE"\n\
						ID: "cBLUE"%d"cWHITE"\n\n\
						Puertas: %s\n\n\
						Due�o: "cBLUE"%s"cWHITE"\n\
						Tipo de negocio: "cBLUE"%s"cWHITE"\n\
						Interior: "cBLUE"#%d"cWHITE"\n\
						Valor de mercado: "cBLUE"$%d"cWHITE"\n\n\
						Stock de almac�n: "cBLUE"%d/%d"cWHITE"\n\n\
						Muebles: "cBLUE"%d/%d"cWHITE"\n\n\
						Expansi�n de almac�n: %s\n\
						Expansi�n de contador: %s\n\n\
						Horas: "cBLUE"%d"cWHITE" hrs",
						BusinessInfo[businessid][b_name],
						BusinessInfo[businessid][b_id],
						BusinessInfo[businessid][b_lock] ? (""cBLUE"Abiertas"cWHITE"") : 
														   (""cRED"Cerradas"cWHITE""),
						Player[playerid][uName],
						GetBusinessType( businessid ),
						BusinessInfo[businessid][b_interior],
						BusinessInfo[businessid][b_price],
						BusinessInfo[businessid][b_products], BusinessInfo[businessid][b_improve][2],
						BusinessInfo[businessid][b_count_furn], GetMaxFurnBusiness( businessid ),
						BusinessInfo[businessid][b_improve][0] ? (""cBLUE"S�"cWHITE"") : 
																 (""cRED"No"cWHITE""),
						BusinessInfo[businessid][b_improve][1] ? (""cBLUE"S�"cWHITE"") : 
																 (""cRED"No"cWHITE""),
						hour
					);
					
					showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Cerrar","" );
				}
					
				case 1:
				{
					format:g_string( b_panel_p2, 
						BusinessInfo[businessid][b_improve][0] ? (""cBLUE"s�") : (""cGRAY"no"),
						BusinessInfo[businessid][b_improve][1] ? (""cBLUE"s�") : (""cGRAY"no")
					);
					showPlayerDialog( playerid, d_business_panel + 1, DIALOG_STYLE_TABLIST_HEADERS, " ", g_string, "Comprar", "Atr�s" );
				}
				
				case 2:
				{
					format:g_string( "Caja fuerte "cBLUE"$%d", BusinessInfo[businessid][b_safe] );
					showPlayerDialog( playerid, d_business_panel + 4, DIALOG_STYLE_LIST, g_string, b_panel_p3, "Seleccionar", "Atr�s" );
				}
				
				case 3:
				{
					format:g_string( b_panel_p4, BusinessInfo[businessid][b_product_price] );
					showPlayerDialog( playerid, d_business_panel + 7, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Atr�s" );
				}
			}
		}
		
		case d_business_panel + 1:
		{
			new 
				businessid = GetPVarInt( playerid, "Bpanel:BId" );
				
			if( !response )
			{
				return showPlayerDialog( playerid, d_business_panel, DIALOG_STYLE_TABLIST, " ", b_panel, "Seleccionar", "Cerrar" );
			}
			
			switch( listitem )
			{
				case 0:
				{
					if( BusinessInfo[businessid][b_improve][0] )
					{
						format:g_string( b_panel_p2, 
							BusinessInfo[businessid][b_improve][0] ? (""cBLUE"S�") : (""cGRAY"No"),
							BusinessInfo[businessid][b_improve][1] ? (""cBLUE"S�") : (""cGRAY"No")
						);
						
						return SendClient:( playerid, C_GRAY, ""gbError"El almac�n de este negocio ya se ha ampliado." ),
							   showPlayerDialog( playerid, d_business_panel + 1, DIALOG_STYLE_TABLIST_HEADERS, " ", g_string, "Comprar", "Atr�s" );
					}
					
					format:g_small_string( "\
						"cBLUE"Expansi�n del almac�n\n\n\
						"cWHITE"El almac�n del negocio se ampliar� al doble.\n\
						El almac�n podr� albergar el doble de mercanc�as.\n\n\
						"gbDialog"Costo de expansi�n "cBLUE"$%d"cWHITE".", GetPriceImprove( businessid ) );
					
					showPlayerDialog( playerid, d_business_panel + 2, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Comprar", "Atr�s" );	
				}
				
				case 1:
				{
					if( BusinessInfo[businessid][b_improve][1] )
					{
						format:g_string( b_panel_p2, 
							BusinessInfo[businessid][b_improve][0] ? (""cBLUE"S�") : (""cGRAY"No"),
							BusinessInfo[businessid][b_improve][1] ? (""cBLUE"S�") : (""cGRAY"No")
						);
						
						return SendClient:( playerid, C_GRAY, ""gbError"El contador de este negocio ya est� ampliado." ),
							   showPlayerDialog( playerid, d_business_panel + 1, DIALOG_STYLE_TABLIST_HEADERS, " ", g_string, "Comprar", "Atr�s" );
					}
					
					format:g_small_string( "\
						"cBLUE"Expansi�n del contador\n\n\
						"cWHITE"El contador del negocio se ampliar� al doble\n\
						Podr�s vender m�s bienes.\n\n\
						"gbDialog"Costo de expansi�n "cBLUE"$%d"cWHITE".", GetPriceImprove2( businessid ) );
					
					showPlayerDialog( playerid, d_business_panel + 3, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Comprar", "Atr�s" );
				}
				
				case 2:
				{
					showPlayerDialog( playerid, d_mebelbuy + 5, DIALOG_STYLE_LIST, " ", furniture_other, "Seleccionar", "Atr�s" );
				}
			}
		}
		
		case d_business_panel + 2:
		{
			new 
				businessid = GetPVarInt( playerid, "Bpanel:BId" );
				
			if( !response )
			{
				format:g_string( b_panel_p2, 
					BusinessInfo[businessid][b_improve][0] ? (""cBLUE"S�") : (""cGRAY"No"),
					BusinessInfo[businessid][b_improve][1] ? (""cBLUE"S�") : (""cGRAY"No")
				);
						
				return showPlayerDialog( playerid, d_business_panel + 1, DIALOG_STYLE_TABLIST_HEADERS, " ", g_string, "Comprar", "Atr�s" ); 
			}
			
			if( BusinessInfo[businessid][b_safe] < GetPriceImprove( businessid ) )
			{
				SendClient:( playerid, C_WHITE, ""gbError"En la caja fuerte no hay suficiente dinero para ampliar el almac�n." );
			
				format:g_string( b_panel_p2, 
					BusinessInfo[businessid][b_improve][0] ? (""cBLUE"S�") : (""cGRAY"No"),
					BusinessInfo[businessid][b_improve][1] ? (""cBLUE"S�") : (""cGRAY"No")
				);
						
				return showPlayerDialog( playerid, d_business_panel + 1, DIALOG_STYLE_TABLIST_HEADERS, " ", g_string, "Comprar", "Atr�s" ); 
			}
			
			BusinessInfo[businessid][b_safe] -= GetPriceImprove( businessid );
			UpdateBusiness( businessid, "b_safe", BusinessInfo[businessid][b_safe] );
			
			BusinessInfo[businessid][b_improve][0] = 1;
			BusinessInfo[businessid][b_improve][2] = 100000;
			
			UpdateBusinessSlash( businessid, "b_improve", BusinessInfo[businessid][b_improve] );
		
			showPlayerDialog( playerid, d_business_panel, DIALOG_STYLE_TABLIST, " ", b_panel, "Seleccionar", "Cerrar" );
			
			SendClient:( playerid, C_WHITE, ""gbSuccess"El almac�n se amplio con �xito. Ahora podr�s albergar "cBLUE"100000"cWHITE" bienes" );
		}
		
		case d_business_panel + 3:
		{
			new 
				businessid = GetPVarInt( playerid, "Bpanel:BId" );
				
			if( !response )
			{
				format:g_string( b_panel_p2, 
					BusinessInfo[businessid][b_improve][0] ? (""cBLUE"S�") : (""cGRAY"No"),
					BusinessInfo[businessid][b_improve][1] ? (""cBLUE"S�") : (""cGRAY"No")
				);
						
				return showPlayerDialog( playerid, d_business_panel + 1, DIALOG_STYLE_LIST, " ", g_string, "Comprar", "Atr�s" ); 
			}
			
			if( BusinessInfo[businessid][b_safe] < GetPriceImprove2( businessid ) )
			{
				SendClient:( playerid, C_WHITE, ""gbError"No hay suficiente dinero en la caja fuerte para ampliar el contador." );
			
				format:g_string( b_panel_p2, 
					BusinessInfo[businessid][b_improve][0] ? (""cBLUE"S�") : (""cGRAY"No"),
					BusinessInfo[businessid][b_improve][1] ? (""cBLUE"S�") : (""cGRAY"No")
				);
						
				return showPlayerDialog( playerid, d_business_panel + 1, DIALOG_STYLE_TABLIST_HEADERS, " ", g_string, "Comprar", "Atr�s" );
			}
			
			BusinessInfo[businessid][b_safe] -= GetPriceImprove2( businessid );
			UpdateBusiness( businessid, "b_safe", BusinessInfo[businessid][b_safe] );
			
			BusinessInfo[businessid][b_improve][1] = 1;
			
			UpdateBusinessSlash( businessid, "b_improve", BusinessInfo[businessid][b_improve] );
			
			showPlayerDialog( playerid, d_business_panel, DIALOG_STYLE_TABLIST, " ", b_panel, "Seleccionar", "Cerrar" );
		
			SendClient:( playerid, C_WHITE, ""gbSuccess"Las estanter�as se ampliaron con �xito. Ahora puedes vender m�s productos." );
		}
		
		// cuadro de di�logo
		case d_business_panel + 4:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_business_panel, DIALOG_STYLE_TABLIST, " ", b_panel, "Seleccionar", "Cerrar" );
			}
			
			switch( listitem )
			{
				case 0:
				{
					showPlayerDialog( playerid, d_business_panel + 5, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea depositar:", 
					"Siguiente", "Atr�s" );
				}
				
				case 1:
				{
					showPlayerDialog( playerid, d_business_panel + 6, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea retirar:", 
					"Siguiente", "Atr�s" );
				}
			}
		}
		
		case d_business_panel + 5:
		{
			new 
				bid = GetPVarInt( playerid, "Bpanel:BId" );
				
			if( !response )
			{
				format:g_string( "Caja fuerte "cBLUE"$%d", BusinessInfo[bid][b_safe] );
				return showPlayerDialog( playerid, d_business_panel + 4, DIALOG_STYLE_LIST, g_string, b_panel_p3, "Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 1 )
			{
				return showPlayerDialog( playerid, d_business_panel + 5, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea depositar:\n\n\
						"gbDialogError"Monto no valido. Intentelo denuevo", 
					"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) > Player[playerid][uMoney] )
			{
				return showPlayerDialog( playerid, d_business_panel + 5, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea depositar:\n\n\
						"gbDialogError"No tienes suficiente dinero. Primero ve al banco", 
					"Siguiente", "Atr�s" );
			}
			
			pformat:( ""gbDefault"Guardas "cBLUE"$%d"cWHITE" en la caja fuerte", strval( inputtext ) );
			psend:( playerid, C_WHITE );
			
			SetPlayerCash( playerid, "-", strval( inputtext ) );
			
			BusinessInfo[bid][b_safe] += strval( inputtext );
			UpdateBusiness( bid, "b_safe", BusinessInfo[bid][b_safe] );
			
			format:g_string( "Caja fuerte "cBLUE"$%d", BusinessInfo[bid][b_safe] );
			showPlayerDialog( playerid, d_business_panel + 4, DIALOG_STYLE_LIST, g_string, b_panel_p3, "Seleccionar", "Atr�s" );
		}
		
		case d_business_panel + 6:
		{
			new 
				bid = GetPVarInt( playerid, "Bpanel:BId" );
				
			if( !response )
			{
				format:g_string( "Caja fuerte "cBLUE"$%d", BusinessInfo[bid][b_safe] );
				return showPlayerDialog( playerid, d_business_panel + 4, DIALOG_STYLE_LIST, g_string, b_panel_p3, "Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) || strval( inputtext ) < 0 )
			{
				return showPlayerDialog( playerid, d_business_panel + 6, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea retirar:\n\n\
						"gbDialogError"El monto ingresado no es valido. Intentelo denuevo", 
					"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) > BusinessInfo[bid][b_safe] )
			{
				return showPlayerDialog( playerid, d_business_panel + 6, DIALOG_STYLE_INPUT, " ", "\
						"cBLUE"Caja fuerte\n\n\
						"cWHITE"Especifique la cantidad que desea retirar:\n\n\
						"gbDialogError"La caja fuerte no tiene esa cantidad de dinero.", 
					"Siguiente", "Atr�s" );
			}
			
			pformat:( ""gbDefault"Retiraste "cBLUE"$%d"cWHITE" de la caja fuerte", strval( inputtext ) );
			psend:( playerid, C_WHITE );
			
			SetPlayerCash( playerid, "+", strval( inputtext ) );
			
			BusinessInfo[bid][b_safe] -= strval( inputtext );
			UpdateBusiness( bid, "b_safe", BusinessInfo[bid][b_safe] );
			
			format:g_string( "Caja fuerte "cBLUE"$%d", BusinessInfo[bid][b_safe] );
			showPlayerDialog( playerid, d_business_panel + 4, DIALOG_STYLE_LIST, g_string, b_panel_p3, "Seleccionar", "Atr�s" );
		}
		
		case d_business_panel + 7:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_business_panel, DIALOG_STYLE_TABLIST, " ", b_panel, "Seleccionar", "Atr�s" );
			}
		
			switch( listitem )
			{
				case 0:
				{
					showPlayerDialog( playerid, d_business_panel + 8, DIALOG_STYLE_INPUT, " ", "\
						"gbDefault"Cambiar el nombre del negocio\n\n\
						Ingrese el nuevo nombre para el negocio:\n\n\
						"gbDialog"El cambio es inmediato.", 
					"Siguiente", "Atr�s" );
				}
			
				case 1:
				{
					showPlayerDialog( playerid, d_business_panel + 9, DIALOG_STYLE_INPUT, " ", "\
						"gbDefault"Precio de productos\n\n\
						Introduce el precio:\n\n\
						"gbDialog"El m�nimo es $3, el m�ximo es $10.", 
					"Siguiente", "Atr�s" );
				}
			
				case 2:
				{
					showPlayerDialog( playerid, d_business_panel + 22, DIALOG_STYLE_INPUT, " ", "\
						"cWHITE"| Ordenar productos.\n\n\
						Ingrese el n�mero de productos que desea ordenar:\n\n\
						"gbDialog"El precio mayorista del producto es $1.\n\
						"gbDialog"M�nimo 1000 productos, m�ximo 10000 productos.",
					"Siguiente", "Atr�s" );
				}
			
				case 3:
				{
					showPlayerDialog( playerid, d_business_panel + 11, DIALOG_STYLE_LIST, " ", b_panel_plan, "Seleccionar", "Atr�s" );
				}
			}
		}
		
		// Di�logo con pedidos de productos.
		case d_business_panel + 22:
		{
			new 
				bid = GetPVarInt( playerid, "Bpanel:BId" );
				
			if( !response )
			{
				format:g_string( b_panel_p4, BusinessInfo[bid][b_product_price] );
				return showPlayerDialog( playerid, d_business_panel + 7, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS || !IsNumeric( inputtext ) )
			{
				return showPlayerDialog( playerid, d_business_panel + 22, DIALOG_STYLE_INPUT, " ", "\
						"cWHITE"| Ordenar productos.\n\n\
						Ingrese el n�mero de productos que desea ordenar:\n\n\
						"gbDialog"El precio mayorista del producto es $1.\n\
						"gbDialog"M�nimo 1000 productos, m�ximo 10000 productos.\n\n\
						"gbDialogError"Valor no v�lido, por favor vuelva a ingresar.",
					"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) > 10000 || strval( inputtext ) < 1000  )
			{
				return showPlayerDialog( playerid, d_business_panel + 22, DIALOG_STYLE_INPUT, " ", "\
						"cWHITE"| Ordenar productos.\n\n\
						Ingrese el n�mero de productos que desea ordenar:\n\n\
						"gbDialog"El precio mayorista del producto es $1.\n\
						"gbDialog"M�nimo 1000 productos, m�ximo 10000 productos.\n\n\
						"gbDialogError"La cantidad especificada no est� dentro del rango permitido.", 
					"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) + BusinessInfo[bid][b_products] > BusinessInfo[bid][b_improve][2] )
			{
				return showPlayerDialog( playerid, d_business_panel + 22, DIALOG_STYLE_INPUT, " ", "\
						"cWHITE"| Ordenar productos.\n\n\
						Ingrese el n�mero de productos que desea ordenar:\n\n\
						"gbDialog"El precio mayorista del producto es $1.\n\
						"gbDialog"M�nimo 1000 productos, m�ximo 10000 productos.\n\n\
						"gbDialogError"Tu almac�n no puede albergar esa cantidad de productos.", 
					"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) > BusinessInfo[bid][b_safe] )
			{
				return showPlayerDialog( playerid, d_business_panel + 22, DIALOG_STYLE_INPUT, " ", "\
						"cWHITE"| Ordenar productos.\n\n\
						Ingrese el n�mero de productos que desea ordenar:\n\n\
						"gbDialog"El precio mayorista del producto es $1.\n\
						"gbDialog"M�nimo 1000 productos, m�ximo 10000 productos.\n\n\
						"gbDialogError"No hay suficiente dinero en la caja fuerte para pagar los productos.", 
					"Siguiente", "Atr�s" );
			}
			
			if( BusinessInfo[bid][b_status_prod] == true )
			{
				format:g_string( b_panel_p4, BusinessInfo[bid][b_product_price] );
				
				return SendClient:( playerid, C_WHITE, ""gbError"Tu pedido anterior aun no se ha completado. Prueba m�s tarde"),
					showPlayerDialog( playerid, d_business_panel + 7, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Atr�s" );
			}
			
			new 
				bool:count = false;
				
			for( new i; i < MAX_PRODUCT_INFO; i++ )
			{
				if( !ProductsInfo[i][b_id] && !ProductsInfo[i][b_count] )
				{
					ProductsInfo[i][b_id] = bid;
					ProductsInfo[i][b_business_id] = BusinessInfo[bid][b_id];
					ProductsInfo[i][b_count] = strval( inputtext );
					ProductsInfo[i][b_count_time] = strval( inputtext );
					
					count = true;
					
					pformat:( ""gbSuccess"La orden de "cBLUE"%d"cWHITE" productos ha sido aceptada.", strval( inputtext ) );
					psend:( playerid, C_WHITE );
					
					BusinessInfo[bid][b_safe] -= strval( inputtext );
					UpdateBusiness( bid, "b_safe", BusinessInfo[bid][b_safe] );
					
					BusinessInfo[bid][b_status_prod] = true;
					
					CreateOrder( i );
					
					break;
				}
			}
			
			if( count == false ) 
			{
				SendClient:( playerid, C_WHITE, ""gbError"La sala de ventas llego al l�mite de pedidos. Intentalo m�s tarde.");
				
				format:g_string( b_panel_p4, BusinessInfo[bid][b_product_price] );
				return showPlayerDialog( playerid, d_business_panel + 7, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Atr�s" );
			}
		}
		
		case d_business_panel + 8:
		{
			new 
				businessid = GetPVarInt( playerid, "Bpanel:BId" );
			
			if( !response )
			{
				return 
					format:g_string( b_panel_p4, BusinessInfo[businessid][b_product_price] ),
					showPlayerDialog( playerid, d_business_panel + 7, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS )
			{
				return
					showPlayerDialog( playerid, d_business_panel + 8, DIALOG_STYLE_INPUT, " ", "\
						"gbDefault"Cambiar el nombre del negocio\n\n\
						Ingrese el nuevo nombre para el negocio:\n\n\
						"gbDialog"El cambio es inmediato.\n\n\
						"gbDialogError"Debes escribir un nombre.", 
					"Siguiente", "Atr�s" );
			}
			
			if( strlen( inputtext ) > 32 )
			{
				return
					showPlayerDialog( playerid, d_business_panel + 8, DIALOG_STYLE_INPUT, " ", "\
						"gbDefault"Cambiar el nombre del negocio\n\n\
						Ingrese el nuevo nombre para el negocio:\n\n\
						"gbDialog"El cambio es inmediato.\n\n\
						"gbDialogError"M�ximo de caracteres excedido.", 
					"Siguiente", "Atr�s" );
			}
			
			
			clean:<BusinessInfo[businessid][b_name]>;
			
			strcat( BusinessInfo[businessid][b_name], inputtext, 32 );
			
			UpdateBusinessStr( businessid, "b_name", BusinessInfo[businessid][b_name] );
			UpdateBusiness3DText( businessid );
			
			pformat:(""gbSuccess"Cambiaste el nombre del negocio a "cBLUE"%s"cWHITE".", BusinessInfo[businessid][b_name] );
			psend:( playerid, C_WHITE );

			showPlayerDialog( playerid, d_business_panel, DIALOG_STYLE_TABLIST, " ", b_panel, "Seleccionar", "Cerrar" );
		}
		
		// Precio por producto
		case d_business_panel + 9:
		{
			new 
				businessid = GetPVarInt( playerid, "Bpanel:BId" );
				
			if( !response )
			{
				return 
					format:g_string( b_panel_p4, BusinessInfo[businessid][b_product_price] ),
					showPlayerDialog( playerid, d_business_panel + 7, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Atr�s" );
			}
			
			if( inputtext[0] == EOS )
			{
				return
					showPlayerDialog( playerid, d_business_panel + 9, DIALOG_STYLE_INPUT, " ", "\
						"gbDefault"Precio de productos\n\n\
						Introduce el precio:\n\n\
						"gbDialog"El m�nimo es $3, el m�ximo es $10.\n\
						"gbDialogError"Debes introducir un precio.", 
					"Siguiente", "Atr�s" );
			}
			
			if( !IsNumeric( inputtext ) )
			{
				return
					showPlayerDialog( playerid, d_business_panel + 9, DIALOG_STYLE_INPUT, " ", "\
						"gbDefault"Precio de productos\n\n\
						Introduce el precio:\n\n\
						"gbDialog"El m�nimo es $3, el m�ximo es $10.\n\
						"gbDialogError"Solo puedes introducir numeros.", 
					"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) == BusinessInfo[businessid][b_product_price] )
			{
				return
					showPlayerDialog( playerid, d_business_panel + 9, DIALOG_STYLE_INPUT, " ", "\
						"gbDefault"Precio de productos\n\n\
						Introduce el precio:\n\n\
						"gbDialog"El m�nimo es $3, el m�ximo es $10.\n\
						"gbDialogError"Debes introducir un precio diferente al actual.", 
					"Siguiente", "Atr�s" );
			}
			
			if( strval( inputtext ) < 3 || strval( inputtext ) > 10 )
			{
				return
					showPlayerDialog( playerid, d_business_panel + 9, DIALOG_STYLE_INPUT, " ", "\
						"gbDefault"Precio de productos\n\n\
						Introduce el precio:\n\n\
						"gbDialog"El m�nimo es $3, el m�ximo es $10.\n\
						"gbDialogError"El precio no debe ser menos de $3 ni m�s de $10", 
					"Siguiente", "Atr�s" );
			}
			
			BusinessInfo[businessid][b_product_price] = strval( inputtext );
			
			UpdateBusiness( businessid, "b_product_price", BusinessInfo[businessid][b_product_price] );
			
			pformat:( ""gbSuccess"Estableces el precio a "cBLUE"$%d"cWHITE" para el producto", BusinessInfo[businessid][b_product_price] );
			psend:( playerid, C_WHITE );
			
			format:g_string( b_panel_p4, BusinessInfo[businessid][b_product_price] );
			showPlayerDialog( playerid, d_business_panel + 7, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Atr�s" );
		}
		
		case d_business_panel + 11:
		{
			new 
				businessid = GetPVarInt( playerid, "Bpanel:BId" );
				
			if( !response )
			{
				return 
					format:g_string( b_panel_p4, BusinessInfo[businessid][b_product_price] ),
					showPlayerDialog( playerid, d_business_panel + 7, DIALOG_STYLE_LIST, " ", g_string, "Seleccionar", "Atr�s" );
			}
			
			switch( listitem )
			{
				/*case 0:
				{
					ShowBusinessInterior( playerid, businessid );
				}*/
				
				case 0:
				{
					showPlayerDialog( playerid, d_business_panel + 13, DIALOG_STYLE_LIST, " ", b_panel_texture, "Seleccionar", "Atr�s" );
				}
				
				case 1:
				{
					ShowBusinessFurnList( playerid, businessid, 0 );
				}
				
				case 2:
				{
					format:g_small_string( ""cWHITE"%s", 
						BusinessInfo[businessid][b_state_cashbox] == 0 ? ("Colocar una caja"):("Recoger dinero" ) 
					);
					
					showPlayerDialog( playerid, d_business_panel + 18, DIALOG_STYLE_LIST, " ", g_small_string, "Seleccionar", "Atr�s" );
				}
			}
	
		}
		
		/*case d_business_panel + 12:
		{
			new
				interior,
				Float:pos[3],
				businessid = GetPVarInt( playerid, "Bpanel:BId" );
				
			if( !response )
			{
				return showPlayerDialog( playerid, d_business_panel + 11, DIALOG_STYLE_LIST, " ", b_panel_plan, "Seleccionar", "Atr�s" );
			}
			
			if( GetCountAroundPlayer( playerid, 40.0 ) )
			{
				ShowBusinessInterior( playerid, businessid );
				return SendClient:( playerid, C_WHITE, ""gbError"Hay otros jugadores en tu negocio, int�ntalo de nuevo m�s tarde." );			
			}
			
			BusinessInfo[businessid][b_lock] = 0; // Cerrar el negocio para que otros no puedan entrar.
			UpdateBusiness( businessid, "b_lock", BusinessInfo[businessid][b_lock] );
			UpdateBusiness3DText( businessid );
			
			GameTextForPlayer( playerid, "~r~NEGOCIO CERRADO", 3000, 3 );
			
			GetPlayerPos( playerid, pos[0], pos[1], pos[2] );
			
			SetPVarFloat( playerid, "Bpanel:pos[0]", pos[0] ),
			SetPVarFloat( playerid, "Bpanel:pos[1]", pos[1] ),
			SetPVarFloat( playerid, "Bpanel:pos[2]", pos[2] );
			
			SetPVarInt( playerid, "Bpanel:Interior", g_dialog_select[playerid][listitem] );
			interior = GetPVarInt( playerid, "Bpanel:Interior" );
			
			setPlayerPos( playerid, business_int[interior][bt_p][0], business_int[interior][bt_p][1], business_int[interior][bt_p][2] );
			stopPlayer( playerid, 2 );
			
			SetPlayerVirtualWorld( playerid, playerid + 100 );
			
			format:g_small_string( "interior %d: $%d", business_int[interior][bt_id], GetPriceInterior( businessid, interior ) );
			PlayerTextDrawSetString( playerid, interior_info[playerid], g_small_string );
			
			showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", "\
				"gbDefault"Para cambiar el interior o salir de la visualizaci�n, pulse"cBLUE"ALT"cWHITE".", "Cerrar", "" );
		}*/
		// Retexturizaci�n - selecci�n de pieza
		case d_business_panel + 13:
		{
			if( !response )
			{
				return showPlayerDialog( playerid, d_business_panel + 11, DIALOG_STYLE_LIST, " ", b_panel_plan, "Seleccionar", "Atr�s" );
			}
			
			SetPVarInt( playerid, "Bpanel:Type", listitem );
			ShowBusinessPartInterior( playerid, GetPVarInt( playerid, "Bpanel:BId" ), listitem );
		}
		
		case d_business_panel + 14:
		{			
			if( !response )
			{
				DeletePVar( playerid, "Bpanel:Type" );
				return showPlayerDialog( playerid, d_business_panel + 13, DIALOG_STYLE_LIST, "Retexturizaci�n", b_panel_texture, "Seleccionar", "Atr�s" );
			}
			
			SelectedType{playerid} = 1;
			ShowTexViewer( playerid, GetPVarInt( playerid, "Bpanel:Type" ), listitem, 0 );
		}
		
		case d_business_panel + 15:
		{
			if( !response ) 
			{
				DeletePVar( playerid, "Bpanel:PriceTexture" );
				return 1;
			}
			
			switch( listitem )
			{
				case 0:
				{
					format:g_small_string( "\
						"cWHITE"Est�s seguro que quieres comprar esta textura?\n\
						"gbDialog"Precio: "cBLUE"$%d", GetPVarInt( playerid, "Bpanel:PriceTexture" ) );
						
					showPlayerDialog( playerid, d_business_panel + 23, DIALOG_STYLE_MSGBOX, "Retexturizaci�n", g_small_string, "S�", "No" );
				}
				
				case 1:
				{
					new
						bid = GetPVarInt( playerid, "Bpanel:BId" );
				
					switch( Menu3DData[playerid][CurrTextureType] )
					{
						case 0: SetBusinessTexture( bid, BusinessInfo[bid][b_wall][ Menu3DData[playerid][CurrPartNumber] ], 0, Menu3DData[playerid][CurrPartNumber] );
						case 1: SetBusinessTexture( bid, BusinessInfo[bid][b_floor][ Menu3DData[playerid][CurrPartNumber] ], 1, Menu3DData[playerid][CurrPartNumber] );
						case 2: SetBusinessTexture( bid, BusinessInfo[bid][b_roof][ Menu3DData[playerid][CurrPartNumber] ], 2, Menu3DData[playerid][CurrPartNumber] );
						case 3: SetBusinessTexture( bid, BusinessInfo[bid][b_stair], 3, INVALID_PARAM );
					}
				
					DeletePVar( playerid, "Bpanel:Type" );
					DeletePVar( playerid, "Bpanel:PriceTexture" );
					DestroyTexViewer( playerid );
					
					showPlayerDialog( playerid, d_business_panel + 13, DIALOG_STYLE_LIST, "Retexturizaci�n", h_panel_texture, "Seleccionar", "Atr�s" );
				}
			}
		}
		
		case d_business_panel + 23:
		{
			new
				price = GetPVarInt( playerid, "Bpanel:PriceTexture" ),
				bid = GetPVarInt( playerid, "Bpanel:BId" );
		
			if( !response )
			{
				format:g_small_string( "\
					"cWHITE"Acci�n\t"cWHITE"Costo\n\
					"cWHITE"Comprar textura\t"cBLUE"$%d\n\
					"cWHITE"Editar textura", price );
		
				return showPlayerDialog( playerid, d_business_panel + 15, DIALOG_STYLE_TABLIST_HEADERS, "Retexturizaci�n", g_small_string, "Seleccionar", "Cerrar" );
			}
			
			if( Player[playerid][uMoney] < price )
			{
				SendClient:( playerid, C_WHITE, !NO_MONEY );
			
				format:g_small_string( "\
					"cWHITE"Acci�n\t"cWHITE"Costo\n\
					"cWHITE"Comprar textura\t"cBLUE"$%d\n\
					"cWHITE"Editar textura", price );
		
				return showPlayerDialog( playerid, d_business_panel + 15, DIALOG_STYLE_TABLIST_HEADERS, "Retexturizaci�n", g_small_string, "Seleccionar", "Cerrar" );
			}
			
			UpdateBusinessTexture( playerid, bid, Menu3DData[playerid][CurrTextureType], Menu3DData[playerid][CurrPartNumber] );
		
			SetPlayerCash( playerid, "-", price );
			
			DeletePVar( playerid, "Bpanel:Type" );
			DeletePVar( playerid, "Bpanel:PriceTexture" );
			DestroyTexViewer( playerid );
			
			SendClient:( playerid, C_WHITE, ""gbSuccess"Textura aplicada con �xito." );
			
			showPlayerDialog( playerid, d_business_panel + 13, DIALOG_STYLE_LIST, "Retexturizaci�n", b_panel_texture, "Seleccionar", "Atr�s" );
		}
		
		case d_business_panel + 16:
		{
			if( !response ) 
			{
				DeletePVar( playerid, "Bpanel:FPage" );
				DeletePVar( playerid, "Bpanel:FPageMax" );
				DeletePVar( playerid, "Bpanel:FAll" );
			
				return showPlayerDialog( playerid, d_business_panel + 11, DIALOG_STYLE_LIST, " ", b_panel_plan, "Seleccionar", "Atr�s" );
			}
			
			new
				businessid = GetPVarInt( playerid, "Bpanel:BId" ),
				fid;
			
			if( listitem == FURN_PAGE ) 
			{
				if( GetPVarInt( playerid, "Bpanel:FPage" ) == GetPVarInt( playerid, "Bpanel:FPageMax" ) )
					return ShowBusinessFurnList( playerid, businessid, GetPVarInt( playerid, "Bpanel:FPage" ) );
					
				return ShowBusinessFurnList( playerid, businessid, GetPVarInt( playerid, "Bpanel:FPage" ) + 1 );
			}
			else if( listitem == FURN_PAGE + 1 || 
				listitem == GetPVarInt( playerid, "Bpanel:FAll" ) && GetPVarInt( playerid, "Bpanel:FAll" ) < FURN_PAGE ) 
			{
				return ShowBusinessFurnList( playerid, businessid, GetPVarInt( playerid, "Bpanel:FPage" ) - 1 );
			}
			
			SetPVarInt( playerid, "Bpanel:FId", g_dialog_select[playerid][listitem] );
			g_dialog_select[playerid][listitem] = INVALID_PARAM;
			
			fid = GetPVarInt( playerid, "Bpanel:FId" );
			 
			format:g_small_string(  ""cBLUE"1. "cGRAY"%s", 
				!BFurn[businessid][fid][f_state] ? ( "Colocar muebles\n"cBLUE"2. "cGRAY"Quitar muebles"):("Mover muebles\n"cBLUE"2. "cGRAY"Mover muebles al almac�n del negocio\n"cBLUE"3. "cGRAY"Quitar todos los muebles") );
		    
			showPlayerDialog(playerid, d_business_panel + 17, DIALOG_STYLE_LIST, "Gestionar muebles", g_small_string, "Seleccionar", "Atr�s");
		}
		
		case d_business_panel + 17:
		{
			new
				bid = GetPVarInt( playerid, "Bpanel:BId" ),
				fid = GetPVarInt( playerid, "Bpanel:FId" );
				
			if( !response ) return ShowBusinessFurnList( playerid, bid, GetPVarInt( playerid, "Bpanel:FPage" ) );
				
			switch( listitem )
			{
				case 0:
				{
					if( !BFurn[bid][fid][f_state] )
					{
						new 
							Float:dist = 2.0,
							Float:angle,
							Float:pos[3],
							Float:rot[3];
							
						SendClient:( playerid, C_WHITE, !HELP_EDITOR );
						
						GetPlayerPos( playerid, BFurn[bid][fid][f_pos][0], BFurn[bid][fid][f_pos][1], BFurn[bid][fid][f_pos][2] );
						
						GetPlayerFacingAngle( playerid, angle );

						BFurn[bid][fid][f_pos][0] = BFurn[bid][fid][f_pos][0] + dist * - floatsin( angle, degrees );
	     				BFurn[bid][fid][f_pos][1] = BFurn[bid][fid][f_pos][1] + dist * floatcos( angle, degrees );
						
						for( new i = 0; i != 3; i++ )
						{
							pos[i] = BFurn[bid][fid][f_pos][i];
							rot[i] = BFurn[bid][fid][f_rot][i];
						}
						
						BFurn[bid][fid][f_object] = CreateDynamicObject(
							BFurn[bid][fid][f_model], 
							pos[0], pos[1], pos[2], rot[0], rot[1], rot[2],
							BusinessInfo[bid][b_id] 
						);
						
						EditDynamicObject( playerid, BFurn[bid][fid][f_object] );
						
						BFurn[bid][fid][f_state] = 1;
						
						//UpdateFurnitureBusiness( bid, fid );
						
						SetPVarInt( playerid, "Furn:Edit", 1 );
						
						SendClient:( playerid, C_GRAY, ""gbSuccess"Muebles actualizados" );
						
						return 1;
					}
					
					new 
						Float:fpos[3];
					
					GetDynamicObjectPos( BFurn[bid][fid][f_object], fpos[0], fpos[1], fpos[2] );
					
					if( IsPlayerInRangeOfPoint( playerid, 10.0, fpos[0], fpos[1], fpos[2] ) ) 
					{ 
						EditDynamicObject( playerid, BFurn[bid][fid][f_object] );
						SetPVarInt( playerid, "Furn:Edit", 1 );
						return 1;
					}
					else 
					{
						return 
							ShowBusinessFurnList( playerid, bid, GetPVarInt( playerid, "Bpanel:FPage" ) ),
							SendClient:( playerid, C_GRAY, ""gbError"Este mueble est� muy lejos de ti." );
					}
				}
				
				case 1:
				{
					if( BFurn[bid][fid][f_state] ) 
					{
						if( IsValidDynamicObject( BFurn[bid][fid][f_object] ) )
							DestroyDynamicObject( BFurn[bid][fid][f_object] );
						
						for( new i = 0; i != 3; i++ )
						{
							BFurn[bid][fid][f_pos][i] = 0.0;
							BFurn[bid][fid][f_rot][i] = 0.0;
						}
						
						BFurn[bid][fid][f_state] = 0;
						
						UpdateFurnitureBusiness( bid, fid );
						
						SendClient:( playerid, C_GRAY, ""gbSuccess"Mueble quitado." );
						
						return ShowBusinessFurnList( playerid, bid, GetPVarInt( playerid, "Bpanel:FPage" ) );
					}
				}
			}
			
			pformat:( ""gbSuccess"Mueble "cBLUE"%s"cWHITE" quitado.", BFurn[bid][fid][f_name] );
			psend:( playerid, C_WHITE );
			
			if( IsValidDynamicObject( BFurn[bid][fid][f_object] ) )
				DestroyDynamicObject( BFurn[bid][fid][f_object] );
						
			DeleteFurnitureBusiness( bid, fid );
			BusinessInfo[bid][b_count_furn]--;
						
			ShowBusinessFurnList( playerid, bid, GetPVarInt( playerid, "Bpanel:FPage" ) );
		}
		
		// Di�logo con una caja registradora.
		case d_business_panel + 18:
		{
			if( !response ) return showPlayerDialog( playerid, d_business_panel + 11, DIALOG_STYLE_LIST, " ", b_panel_plan, "Seleccionar", "Atr�s" );
			
			new
				bid = GetPVarInt( playerid, "Bpanel:BId" );
			
			if( !BusinessInfo[bid][b_state_cashbox] )
			{
				showPlayerDialog( playerid, d_business_panel + 19, DIALOG_STYLE_MSGBOX, " ",
					""cBLUE"Colocaci�n de caja\n\n\
					"cWHITE"�Seguro que quieres instalar una caja en este lugar?\n\n\
					"gbDialog"La ubicaci�n de la caja est� determinada por la posici�n del jugador.",
				"S�","No" );
			}
			else
			{
				showPlayerDialog( playerid, d_business_panel + 20, DIALOG_STYLE_MSGBOX, " ",
					""cBLUE"Quitar caja\n\n\
					"cWHITE"�Seguro que quieres quitar la caja?",
				"S�","No" );
			}
		}
		// Di�logo con cajero 2
		case d_business_panel + 19:
		{
			if( !response ) return showPlayerDialog( playerid, d_business_panel + 11, DIALOG_STYLE_LIST, " ", b_panel_plan, "Seleccionar", "Atr�s" );
			
			new
				bid = GetPVarInt( playerid, "Bpanel:BId" ),
				Float:pos[3];
			
			BusinessInfo[bid][b_state_cashbox] = 1;
			
			GetPlayerPos( playerid, pos[0], pos[1], pos[2] );
			
			for( new i = 0; i != 3; i++ )
			{
				BusinessInfo[bid][b_pos_cashbox][i] = pos[i];
			}
			
			UpdateBusinessCashBox( bid );
			
			BusinessInfo[bid][b_cashbox] = CreateDynamicPickup( 1239, 23, 
				BusinessInfo[bid][b_pos_cashbox][0],
				BusinessInfo[bid][b_pos_cashbox][1],
				BusinessInfo[bid][b_pos_cashbox][2], BusinessInfo[bid][b_id] 
			);
			
			SendClient:( playerid, C_BLUE, ""gbSuccess"Has colocado la caja." );
		}
		
		// Di�logo con cajero 3
		case d_business_panel + 20:
		{
			if( !response ) return showPlayerDialog( playerid, d_business_panel + 11, DIALOG_STYLE_LIST, " ", b_panel_plan, "Seleccionar", "Atr�s" );
			
			new
				bid = GetPVarInt( playerid, "Bpanel:BId" );
			
			BusinessInfo[bid][b_state_cashbox] = 0;
			
			for( new i = 0; i != 3; i++ )
			{
				BusinessInfo[bid][b_pos_cashbox][i] = 0.0;
			}
			
			UpdateBusinessCashBox( bid );
			
			DestroyDynamicPickup( BusinessInfo[bid][b_cashbox] );
			
			SendClient:( playerid, C_GRAY, ""gbSuccess"Has quitado la caja." );
		}
		
		// Di�logo al cambiar de interior.
		case d_business_panel + 21:
		{
			if( !response ) 
			{
				SelectTextDraw( playerid, 0x797C7CFF );
				SetPVarInt( playerid, "Bpanel:IntShow", 1 );
				return 1;
			}
			
			new
				bid = GetPVarInt( playerid, "Bpanel:BId" ),
				interior = GetPVarInt( playerid, "Bpanel:Interior" ),
				price = GetPriceInterior( bid, interior );
			
			if( BusinessInfo[bid][b_safe] < price )
			{
				SendClient:( playerid, C_WHITE, ""gbError"No hay suficiente dinero en la caja fuerte para cambiar el interior." );
				
				SelectTextDraw( playerid, 0x797C7CFF );
				SetPVarInt( playerid, "Bpanel:IntShow", 1 );
				return 1;
			}
			
			BusinessInfo[bid][b_interior] = business_int[interior][bt_id];
			
			for( new i = 0; i < 4; i++ )
			{
				BusinessInfo[bid][b_exit_pos][i] = business_int[interior][bt_p][i];
			}
			
			BusinessInfo[bid][b_state_cashbox] = 0;
			for( new i = 0; i != 3; i++ )
			{
				BusinessInfo[bid][b_pos_cashbox][i] = 0.0;
			}
			
			BusinessInfo[bid][b_wall][0] = 
			BusinessInfo[bid][b_wall][1] = 
			BusinessInfo[bid][b_wall][2] = 
			BusinessInfo[bid][b_floor][0] = 
			BusinessInfo[bid][b_floor][1] = 
			BusinessInfo[bid][b_floor][2] = 
			BusinessInfo[bid][b_floor][3] = 
			BusinessInfo[bid][b_roof] = 0;
			
			mysql_format:g_string( "UPDATE `"DB_BUSINESS"` \
			SET `b_int` = '%d', \
				`b_exit_pos` = '%f|%f|%f|%f', \
				`b_wall` = '0|0|0', \
				`b_floor` = '0|0|0|0', \
				`b_roof` = 0 \
			WHERE `b_id` = %d LIMIT 1",
				BusinessInfo[bid][b_interior],
				BusinessInfo[bid][b_exit_pos][0],
				BusinessInfo[bid][b_exit_pos][1],
				BusinessInfo[bid][b_exit_pos][2],
				BusinessInfo[bid][b_exit_pos][3],
				BusinessInfo[bid][b_id]
			);
			mysql_tquery( mysql, g_string );
			
			setPlayerPos( playerid, BusinessInfo[bid][b_exit_pos][0], BusinessInfo[bid][b_exit_pos][1], BusinessInfo[bid][b_exit_pos][2] );
			
			SetPlayerVirtualWorld( playerid, BusinessInfo[bid][b_id] );
			
			DestroyDynamic3DTextLabel( BusinessInfo[bid][b_alt_text] );
			BusinessInfo[bid][b_alt_text] = CreateDynamic3DTextLabel( 
				"Salida", 
				C_BLUE, 
				BusinessInfo[bid][b_exit_pos][0], 
				BusinessInfo[bid][b_exit_pos][1], 
				BusinessInfo[bid][b_exit_pos][2] - 1.0, 
				3.0, 
				INVALID_PLAYER_ID, 
				INVALID_VEHICLE_ID, 
				0,
				BusinessInfo[bid][b_id] 
			);
			
			BusinessInfo[bid][b_safe] -= price;
			UpdateBusiness( bid, "b_safe", BusinessInfo[bid][b_safe] );
			
			pformat:( ""gbSuccess"Cambiaste el interior a "cBLUE"#%d"cWHITE" por "cBLUE"$%d"cWHITE".", BusinessInfo[bid][b_interior], price );
			psend:( playerid, C_WHITE );
			
			LoadBusinessInterior( bid );
			UpdateBusinessCashBox( bid );
			
			DestroyDynamicPickup( BusinessInfo[bid][b_cashbox] );
			
			for( new f; f < GetMaxFurnBusiness( bid ); f++ ) 
			{
				if( BFurn[bid][f][f_id] ) 
				{
					if( BFurn[bid][f][f_state] ) 
					{
						if( IsValidDynamicObject( BFurn[bid][f][f_object] ) )
							DestroyDynamicObject( BFurn[bid][f][f_object] );
						
						for( new j = 0; j != 3; j++ )
						{
							BFurn[bid][f][f_pos][j] = 0.0;
							BFurn[bid][f][f_rot][j] = 0.0;
						}
						
						BFurn[bid][f][f_state] = 0;
					}
				}
			}
			
			mysql_format:g_string( "UPDATE `"DB_BUSINESS_FURN"` \
			SET `f_position` = '0.0|0.0|0.0', \
				`f_rotation` = '0.0|0.0|0.0', \
				`f_state` = 0 \
			WHERE `f_bid` = %d",
				BusinessInfo[bid][b_id]
			);
			mysql_tquery( mysql, g_string );
			
			SetPVarInt( playerid, "Bpanel:Interior", INVALID_PLAYER_ID );
			SetPVarInt( playerid, "Bpanel:BId", INVALID_PLAYER_ID );
			SetPVarFloat( playerid, "Bpanel:pos[0]", INVALID_PLAYER_ID ),
			SetPVarFloat( playerid, "Bpanel:pos[1]", INVALID_PLAYER_ID ),
			SetPVarFloat( playerid, "Bpanel:pos[2]", INVALID_PLAYER_ID );
				
			PlayerTextDrawHide( playerid, interior_info[playerid] );
		}
	}
	return 1;
}