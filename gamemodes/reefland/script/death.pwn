function Death_OnPlayerConnect( playerid )
{
	ClearDeathData( playerid );
	
	return 1;
}

function Death_OnPlayerDisconnect( playerid, reason )
{
	if( IsValidDynamic3DTextLabel( death_text[ playerid ] ) )
	{
		DestroyDynamic3DTextLabel( death_text[ playerid ] );
		death_text[ playerid ] = Text3D: INVALID_3DTEXT_ID;
	}
	
	return 1;
}


function Death_OnPlayerSpawn( playerid )
{
	if( !GetPlayerSkin( playerid ) )
	{
		SetPlayerSkin( playerid, setUseSkin( playerid, true ) );
	}
	
	
	
	if( Player[playerid][uDeath] )
	{
		setPlayerHealth( playerid, 3.0 );
	
		stopPlayer( playerid, 5 );
		ClearAnimations( playerid );
		
		SetPlayerInterior( playerid, 1 );
		SetPlayerVirtualWorld( playerid, 63 );
		
		SetPVarInt( playerid, "User:inInt", 1 );
		UpdateWeather( playerid );
		
		SetCameraBehindPlayer( playerid );
		
		format:g_string( "\
			"cBLUE"Hospital Central\n\n\
			"cWHITE"Su personaje result� gravemente herido y fue trasladado a un hospital para su rehabilitaci�n.\n\n\
			"gbDialog"El curso de rehabilitaci�n dura 5 minutos, tiempo durante el cual no podr� abandonar el hospital.\n\
			"gbDialog"Puede comunicarse con uno de los empleados del hospital para acelerar el proceso de rehabilitaci�n." );
			
		death_timer[ playerid ] = 300;

		showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_string, "Cerrar", "" );
	}
	
	return 1;
}

function Death_OnPlayerDeath( playerid, killerid, reason )
{	
	
	
	new 
		rand = random( sizeof spawnHospital );
	
	SetSpawnInfo( 
		playerid, 
		264, 
		setUseSkin( playerid, true), 
		spawnHospital[rand][0], 
		spawnHospital[rand][1], 
		spawnHospital[rand][2], 
		spawnHospital[rand][3], 
		0, 0, 0, 0, 0, 0 
	);
	
	Player[ playerid ][ uDeath ] = 1;
	Player[ playerid ][ uHP ] = 0.0;
	
	UpdatePlayer( playerid, "uDeath", 1 );
	
	checkPlayerUseTexViewer( playerid );
	return 1;
}

function Death_OnDialogResponse( playerid, dialogid, response, listitem, inputtext[] ) 
{
	
	
	return 1;
}

function Death_OnPlayerGiveDamage( playerid, damagedid, Float: amount, weaponid, bodypart ) 
{
	
	
	if( weaponid == 22 && g_player_taser{playerid} || weaponid == 24 && g_player_taser{playerid} || weaponid == 25 && g_player_taser{playerid} )
	{
		if( GetPlayerState( damagedid ) != PLAYER_STATE_ONFOOT || GetDistanceBetweenPlayers( playerid, damagedid ) > 10.0 )
			return 1;
				
		SetPVarInt( damagedid, "Player:Stunned", 30 );
		togglePlayerControllable( damagedid, false );
		
		SendClient:( damagedid, C_WHITE, !""gbDefault"Fuiste aturdido por la pistola de aturdimiento durante 30 segundos." );
		
		format:g_small_string( "aturdio a %s", SexTextEnd( playerid ), Player[damagedid][uName] );
		MeAction( playerid, g_small_string, 1 );
		
		ApplyAnimation( damagedid, "CRACK", "crckdeth4", 4.0, 0, 0, 0, 1, 0, 1 );
	}
	
	return 1;
}


function Death_OnPlayerTakeDamage( playerid, issuerid, Float: amount, weaponid, bodypart ) 
{
	if( Player[playerid][uDeath] == PLAYER_INJURED || IsAfk( playerid ) ) 
		return 1;
		
	if( weaponid > 21 && weaponid < 35 )
	{
		GivePlayerDamage( playerid, damage_bodypart[ bodypart - 3 ][ weaponid - 22 ], bodypart, weaponid );
	}
	else
	{
		GivePlayerDamage( playerid, amount, bodypart, weaponid );
	}

    
	
    return 1;
}

DeathTimer( playerid )
{
	
	if( death_timer[ playerid ] )
	{
		death_timer[ playerid ]--;
			

		if( !(death_timer[ playerid ] % 6) )
		{

			if( Player[ playerid ][ uHP ] < 100.0 )
			{
				GameTextForPlayer( playerid, "~g~+ 2 HP", 1000, 4 );
				setPlayerHealth( playerid, Player[ playerid ][ uHP ] + 2.0 );
			}
		}
		
		if( !death_timer[ playerid ] )
		{
			Player[playerid][uDeath] = 0;
			UpdatePlayer( playerid, "uDeath", 0 );
			
			SendClient:( playerid, C_WHITE, ""gbSuccess"El tiempo de rehabilitaci�n ha terminado, est�s curado." );

			
			for( new i; i < MAX_DAMAGES; i++ )
			{	
				Damage[ playerid ][ i ][ dm_amount ] = 0.0;
				
				Damage[ playerid ][ i ][ dm_body ] = 
				Damage[ playerid ][ i ][ dm_weapon ] =
				Damage[ playerid ][ i ][ dm_unix ] = 0;
			}
		}
	}
	
	return 1;
}

stock GivePlayerDamage( playerid, Float: amount, body, weaponid )
{
	if( floatround( amount ) )
	{
		if( Player[ playerid ][ uArmor ] )
		{
			if( floatsub( Player[ playerid ][ uArmor ], amount ) <= 0 )
			{
				DamageForPlayer( 0, playerid, floatsub( Player[ playerid ][ uArmor ], amount ) );
				setPlayerArmour( playerid, 0.0 );
			}
			else 
			{
				DamageForPlayer( 1, playerid, amount );
			}
		}
		else 
		{
			DamageForPlayer( 0, playerid, amount );
		}
	}
	
	for( new i; i < MAX_DAMAGES; i++ )
	{
		if( Damage[ playerid ][ i ][ dm_unix ] )
			continue;
			
		Damage[ playerid ][ i ][ dm_amount ] = amount;
		Damage[ playerid ][ i ][ dm_unix ] = gettime();
		Damage[ playerid ][ i ][ dm_body ] = body;
		Damage[ playerid ][ i ][ dm_weapon ] = weaponid;
		
		break;
	}
	
	return 1;
}

DamageForPlayer( type, playerid, Float: amount )
{
	if( type == 0 ) // Health
	{
		new
			Float:health;
	
		if( Player[ playerid ][ uHP ] <= 0 )
			return 0;
			
		if( floatsub( Player[ playerid ][ uHP ], amount ) > 0 )
		{
			setPlayerHealth( playerid, floatsub( Player[ playerid ][ uHP ], amount ) );
		}
		
		GetPlayerHealth( playerid, health );
		
		if( floatround( health ) <= 2 )
		{		

			setPlayerHealth( playerid, 0 );
		}		

	}
	else if( type == 1 ) // Armor
	{
		if( Player[ playerid ][ uArmor ] <= 0 )
			return 0;
		
		if( floatsub( Player[ playerid ][ uArmor ], amount ) <= 0 )
		{
			setPlayerArmour( playerid, 0.0 );
		}
		else 
		{
			setPlayerArmour( playerid, floatsub( Player[ playerid ][ uArmor ], amount ) );
		}
	}
	
	return 1;
}



stock SetPlayerDeathStage( playerid )
{

		
	for( new i; i < MAX_DAMAGES; i++ )
	{	
		Damage[ playerid ][ i ][ dm_amount ] = 0.0;
			
		Damage[ playerid ][ i ][ dm_body ] = 
		Damage[ playerid ][ i ][ dm_weapon ] =
		Damage[ playerid ][ i ][ dm_unix ] = 0;
	}
		
	death_timer[ playerid ] = 0;
		
	return 1;
}


GetPlayerDamagesCount( playerid )
{
	new 
		count = 0;

	for( new i; i < MAX_DAMAGES; i++ )
	{
		if( !Damage[playerid][i][dm_unix] )
			continue;
			
		count++;
	}
	
	return count;
}

ClearDeathData( playerid )
{
	death_text[ playerid ] = Text3D: INVALID_3DTEXT_ID;
	death_timer[ playerid ] = 0;
	
	death_pos[ playerid ][ d_pos_x ] =
	death_pos[ playerid ][ d_pos_y ] =
	death_pos[ playerid ][ d_pos_z ] = 
	death_pos[ playerid ][ d_angle ] = 0.0;
	death_pos[ playerid ][ d_world ] =
	death_pos[ playerid ][ d_int ] = 0;
	
	for( new i; i < MAX_DAMAGES; i++ )
	{	
		Damage[ playerid ][ i ][ dm_amount ] = 0.0;
		
		Damage[ playerid ][ i ][ dm_body ] = 
		Damage[ playerid ][ i ][ dm_weapon ] =
		Damage[ playerid ][ i ][ dm_unix ] = 0;
	}
	
	SetPVarInt( playerid, "Death:Save", 0 );
}

stock GetDeathBodyPart( part, dest[] )
{
	dest[ 0 ] = EOS;
	switch( part )
	{
		case 3 : strcat( dest, "Torso", 32 );
		case 4 : strcat( dest, "Ingle", 32 );
		case 5 : strcat( dest, "Mano izquierda", 32 );
		case 6 : strcat( dest, "Mano derecha", 32 );
		case 7 : strcat( dest, "Pierna izquierda", 32 );
		case 8 : strcat( dest, "Pierna derecha", 32 );
		case 9 : strcat( dest, "Cabeza", 32 );
		default : strcat( dest, "Desconocido", 32 );
	}
}

stock GetDeathDuration( time, dest[] )
{
	if( time < 0 || time == gettime( ) || time > 2592000 ) 
	{
	    strcat( dest, "Nunca", 32 );
	    return 1;
	}
	else if( time < 60 )
	{
		format( dest, 256, "%d segundos", time );
	}
	else if( time >= 0 && time < 60 )
	{
		format( dest, 256, "%d segundo", time );
	}
	else if( time >= 60 && time < 3600 )
	{
		format( dest, 256, ( time >= 120 ) ? ( "%d minutos" ) : ( "%d minuto" ), time / 60 );
	}
	else if( time >= 3600 && time < 86400)
	{
		format( dest, 256, ( time >= 7200 ) ? ("%d horas" ) : ( "%d hora" ), time / 3600 );
	}
	else if( time >= 86400 && time < 2592000 )
	{	
		format( dest, 256, ( time >= 172800 ) ? ( "%d dias" ) : ( "%d dia" ), time / 86400 );
	}

	return 1;
}

GetPlayerDamagesInfo( playerid, damageid )
{
	clean_array();
	
	if( !GetPlayerDamagesCount( damageid ) )
		return 0;
		
	new 
		weapon_name[ 32 ],
		body_part[ 32 ],
		duration[ 32 ];
		
	for( new i; i < MAX_DAMAGES; i++ ) 
	{
		clean:<weapon_name>;
		clean:<body_part>;
		clean:<duration>;
	
  	    if( !Damage[ damageid ][ i ][ dm_unix ] ) 
			continue;
			
		switch( Damage[ damageid ][ i ][ dm_weapon ] )
		{
			case FIRE_BURN:
				strcat( weapon_name, "Quemado" );
			
			case 1..42, 49, 53, 54 :
				GetWeaponName( Damage[ damageid ][ i ][ dm_weapon ], weapon_name, sizeof weapon_name );
			
			default:
				strcat( weapon_name, "Pu�o" );
		}
		
		GetDeathDuration( gettime( ) - Damage[ damageid ][ i ][ dm_unix ], duration );
		GetDeathBodyPart( Damage[ damageid ][ i ][ dm_body ], body_part );
		
		format:g_string( ""cBLUE"%d."cWHITE" %s, %s, %s\n", 
			i + 1, 
			duration, 
			weapon_name, 
			body_part
		);
		
		strcat( g_big_string, g_string );
  	}
	
    return showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, Player[ damageid ][uName], g_big_string, "Cerrar", "" );
}

HealthPlayer( playerid )
{
	if( Player[ playerid ][uJailSettings][2] != 0 ) 
	{
		Player[ playerid ][uJailSettings][2] = 0;
	}
	
	else if( Player[ playerid ][uJailSettings][3] != 0 ) 
	{
		Player[ playerid ][uJailSettings][3] = 2;
	}
	
	else if( Player[ playerid ][uJailSettings][4] != 0 ) 
	{
		Player[ playerid ][uJailSettings][4] = 0;
	}
	
	format:g_string( "%d|%d|%d|%d|%d",
		Player[ playerid ][uJailSettings][0],
		Player[ playerid ][uJailSettings][1],
		Player[ playerid ][uJailSettings][2],
		Player[ playerid ][uJailSettings][3],
		Player[ playerid ][uJailSettings][4]
	);
	
	Player[ playerid ][uDeath] = 0; 
	
	UpdatePlayerString( playerid, "uJailSettings", g_string );
	UpdatePlayer( playerid, "uDeath", 0 );
	
	ClearAnimations( playerid );
	SetPlayerDeathStage( playerid );
	
	setPlayerHealth( playerid, 100.0 );
	
	return 1;
}

CMD:heridas( playerid, params[] ) return cmd_dm( playerid, params );
	
CMD:dm( playerid, params[] ) 
{
	if( sscanf( params, "u", params[0] ) ) 
		return SendClient:( playerid, C_WHITE, ""gbDefault"Sintaxis: /dm <ID/Jugador>" );
		
	if( GetDistanceBetweenPlayers( playerid, params[0] ) > 10.0 
		|| GetPlayerVirtualWorld( playerid ) != GetPlayerVirtualWorld( params[0] ) 
		&& !GetAccessAdmin( playerid, 1 ) 
	) 
		return SendClient:( playerid, C_WHITE,""gbError"Este jugador no est� cerca de ti.");
	
	if( !GetPlayerDamagesInfo( playerid, params[0] ) )
	{
		SendClient:( playerid, C_WHITE, ""gbDefault"Este jugador no tiene heridas." );
	}
	
	return 1;
}