Dobject_OnGameModeInit() 
{
	//Objeto De Morgan
	CreateObject(18759, 3959.89966, -942.35260, 2.84273,   0.00000, 0.00000, 0.00000);
	
    CreateDynamicPickup(1239,23,1072.4669,-342.6463,2797.7004,-1); // Centro de compras
    //CreateDynamicPickup(1239,23,1065.0851,-339.9360,2797.7010,-1); // Centro de compras - Recogida2
	
    //warehouse
    CreateDynamicPickup(1239,23,2596.7729,-833.8965,2879.5870,-1); // Centro de compras'
    CreateDynamicPickup(1239,23,2776.6360,-716.4243,2883.0959,-1); // Centro de compras
    CreateDynamicPickup(1239,23,-2842.7400,-55.5776,2999.0289,-1); // Centro de compras
    CreateDynamicPickup(1239,23,-2737.1760,841.4919,2996.6509,-1); // Centro de compras
    CreateDynamicPickup(1239,23,2623.4070,-183.9477,2879.5859,-1); // Centro de compras

    CreateDynamic3DTextLabel("Agencia inmobiliaria",0xFFFFFFFF,1072.4669,-342.6463,797.7004,5.0,INVALID_PLAYER_ID,INVALID_VEHICLE_ID,1);
	
	//Oficinas bancarias
	for( new i; i < sizeof cashbox_info; i++ )
	{
		format:g_string( "%s", cashbox_info[i][c_name] );
		CreateDynamic3DTextLabel( g_string,0xFFFFFFFF,cashbox_info[i][c_cashbox_pos][0],cashbox_info[i][c_cashbox_pos][1],cashbox_info[i][c_cashbox_pos][2],5.0,INVALID_PLAYER_ID,INVALID_VEHICLE_ID,1,-1);
	}
	
	//Compra de coche de recogida
	CreateDynamicPickup( 1239, 23, PICKUP_SALON, -1 );
	CreateDynamic3DTextLabel( "Comprar un vehiculo", 0xFFFFFFFF, PICKUP_SALON, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1 );
	
	//Reciclaje de recogida
	CreateDynamicPickup( 1239, 23, PICKUP_UTILIZATION, -1 );
	CreateDynamic3DTextLabel( "Utilizaci�n del transporte", 0xFFFFFFFF, PICKUP_UTILIZATION, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1 );
	
	//Centro de licencias de recogida
	CreateDynamicPickup( 1239, 23, PICKUP_LICENSES, -1 );
	CreateDynamic3DTextLabel( "Obtenci�n de licencias", 0xFFFFFFFF, PICKUP_LICENSES, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1 );
	
	//Camioneta
	CreateDynamicPickup( 1239, 23, PICKUP_PARKING, -1 );
	CreateDynamic3DTextLabel( "Informaci�n", 0xFFFFFFFF, PICKUP_PARKING, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1 );
	
	//Recogida en el Ayuntamiento de Recepci�n
	CreateDynamicPickup( 1239, 23, PICKUP_RECEPTION, -1 );
	CreateDynamic3DTextLabel( "Recepci�n del alcalde", 0xFFFFFFFF, PICKUP_RECEPTION, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1 );
	
	//Recogida en la tienda de armas
	CreateDynamicPickup( 1239, 23, PICKUP_WEAPON, -1 );
	
	//Recogida en minimercado de gasolinera.
	CreateDynamicPickup( 1239, 23, PICKUP_REFILL, -1 );
	CreateDynamicPickup( 1239, 23, PICKUP_REFILL_2, -1 );

	//Recogidas en el departamento de polic�a.
	CreateDynamicPickup( 1239, 23, PICKUP_POLICE, -1 );
	CreateDynamic3DTextLabel( "Informaci�n", 0xFFFFFFFF, PICKUP_POLICE, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1 );
	
	CreateDynamicPickup( 1239, 23, PICKUP_POLICE_2, -1 );
	CreateDynamic3DTextLabel( "Informaci�n", 0xFFFFFFFF, PICKUP_POLICE_2, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1 );
	
	//Bots en spawn
	new
		actor;
		
	actor =  CreateActor( 236, 1517.8912, -1172.0548, 24.0781, 139.4900 );
	ApplyActorAnimation( actor, "CAMERA","camstnd_cmon",4.0,1,0,0,0,0 );
	
	actor =  CreateActor( 235, 1465.6993, -1179.5114, 23.8256, 306.8917 );
	ApplyActorAnimation( actor, "CAMERA","camstnd_cmon",4.0,1,0,0,0,0 );
	
	CreateDynamic3DTextLabel( "Ayuda '"cBLUE"H"cWHITE"'", 0xFFFFFFFF, 1517.8912, -1172.0548, 24.5781, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1 );
	CreateDynamic3DTextLabel( "Ayuda '"cBLUE"H"cWHITE"'", 0xFFFFFFFF, 1465.6993, -1179.5114, 24.3256, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1 );
	
	actor = CreateActor(76,1451.0986,-1004.1474,2725.8760,182.5154); // Banco
	SetActorVirtualWorld( actor, 75 );
	ApplyActorAnimation( actor,"PED","SEAT_down",4.0,0,0,1,1,0 );
	
	actor = CreateActor(98,1455.7592,-1004.2281,2725.8760,165.4490); // Banco
	SetActorVirtualWorld( actor, 75 );
	ApplyActorAnimation( actor,"PED","SEAT_down",4.0,0,0,1,1,0 );
	
	actor = CreateActor(150,1463.7074,-1004.3875,2725.8760,191.6439); // Banco
	SetActorVirtualWorld( actor, 75 );
	ApplyActorAnimation( actor,"PED","SEAT_down",4.0,0,0,1,1,0 );
	
	actor = CreateActor(71,1466.1984,-1011.6840,2725.8760,84.0860); // Banco
	SetActorVirtualWorld( actor, 75 );
	
	actor = CreateActor(71,1445.5970,-1035.7144,2725.8760,264.2937); // Banco
	SetActorVirtualWorld( actor, 75 );
	ApplyActorAnimation( actor,"COP_AMBIENT", "Coplook_loop", 4.0, 0, 1, 1, 1, -1 );

	for( new i = 1004; i < 1008; i++ )
	{
		actor = CreateActor(119,2510.2446,1228.4462,1801.0859,178.5761); // Gasolinera interior
		SetActorVirtualWorld( actor, i );
	}
	
	actor = CreateActor(128,664.1229,-566.7401,16.3363,179.1579); // Gasolinera en la calle.

	for( new i = 1; i < 6; i++ )
	{
		actor = CreateActor(72,1037.1707,-304.2475,2076.6460,182.9827); // /sit...Almac�n
		SetActorVirtualWorld( actor, i );
		ApplyActorAnimation( actor,"PED","SEAT_down",4.0,0,0,1,1,0 );
		
		actor = CreateActor(67,1028.1823,-304.7317,2076.6460,204.4148); // /sit...Almac�n
		SetActorVirtualWorld( actor, i );
		ApplyActorAnimation( actor,"PED","SEAT_down",4.0,0,0,1,1,0 );
		
		actor = CreateActor(98,1032.0647,-303.2000,2076.6460,358.0306); // Almac�n
		SetActorVirtualWorld( actor, i );
	}
	
	actor = CreateActor(71,-502.8904,-75.3671,61.7376,72.9423); // aserradero calle
	
	actor = CreateActor(162,-465.1520,-81.2150,60.0411,171.3716); // aserradero calle
	ApplyActorAnimation( actor,"BEACH", "ParkSit_M_loop", 4.0, 1, 0, 0, 0, 0 );
	
	actor = CreateActor(27,-467.5204,-87.1297,60.0965,9.8363); // aserradero calle
	ApplyActorAnimation( actor,"COP_AMBIENT","Copbrowse_in",4.1,0,1,1,1,0 );
	
	actor = CreateActor(27,-452.8084,-79.5894,61.5749,179.0169); // aserradero calle
	ApplyActorAnimation( actor,"GANGS","leanIDLE",4.0,0,0,1,1,0 );
	
	actor = CreateActor(258,-509.2945,-67.5010,1805.4299,84.9350); // /crossarms...Aserradero int
	SetActorVirtualWorld( actor, 50 );
	ApplyActorAnimation( actor,"COP_AMBIENT", "Coplook_loop", 4.0, 0, 1, 1, 1, -1 );
	
	actor = CreateActor(44,-504.6394,-66.8343,1805.4299,116.6679); // /sit...Aserradero int
	ApplyActorAnimation( actor,"PED","SEAT_down",4.0,0,0,1,1,0 );
	SetActorVirtualWorld( actor, 50 );
	
	actor = CreateActor(27,-519.7987,-75.3597,1801.0778,30.5607); // /copa...Aserradero int
	SetActorVirtualWorld( actor, 50 );
	ApplyActorAnimation( actor,"COP_AMBIENT","Copbrowse_in",4.1,0,1,1,1,0 );
	
	actor = CreateActor(27,-522.7399,-79.4216,1801.0778,173.0635); // Aserradero int
	SetActorVirtualWorld( actor, 50 );

	actor = CreateActor(222,1335.2472,395.8866,19.7529,68.1990); // Reciclaje
	
	for( new i = 10; i < 13; i++ )
	{
		actor = CreateActor(150,1488.9301,764.4249,2001.0859,270.5229); // Concesionario de coches int
		SetActorVirtualWorld( actor, i );
	}
	
	for( new i = 113; i < 117; i++ )
	{
		actor = CreateActor(179,-1503.7958,93.5207,3201.9858,355.8047); // Tienda de armas
		SetActorVirtualWorld( actor, i );
	}
	
	actor = CreateActor(9,1145.9255,-1341.0416,2201.0859,271.6440); // Compa�ia de pasajeros
	SetActorVirtualWorld( actor, 3 );
	
	actor = CreateActor(93,1145.8997,-1338.7997,2201.0859,271.2471); // /sit...Compa�ia de pasajeros
	SetActorVirtualWorld( actor, 3 );
	ApplyActorAnimation( actor,"PED","SEAT_down",4.0,0,0,1,1,0 );
	
	actor = CreateActor(177,588.0640,-1539.4636,2001.0859,89.4863); // STO int
	SetActorVirtualWorld( actor, 10 );

	actor = CreateActor(170,1073.8457,-342.3788,2797.7000,79.2354); // /sit...Agencia inmobiliaria
	SetActorVirtualWorld( actor, 13 );
	ApplyActorAnimation( actor,"PED","SEAT_down",4.0,0,0,1,1,0 );
	
	actor = CreateActor(216,1063.5441,-340.5923,2797.7000,272.5405); // Agencia inmobiliaria
	SetActorVirtualWorld( actor, 13 );
	
	actor = CreateActor(186,1077.1406,-348.4432,2797.7000,207.0534); // Agencia inmobiliaria
	SetActorVirtualWorld( actor, 13 );
	
	actor = CreateActor(185,1078.9106,-348.3918,2797.7000,154.2666); // /crossarms...Agencia inmobiliaria
	SetActorVirtualWorld( actor, 13 );
	ApplyActorAnimation( actor,"COP_AMBIENT", "Coplook_loop", 4.0, 0, 1, 1, 1, -1 );
	
	actor = CreateActor(240,1079.4662,-339.0892,2797.7000,9.3640); // /sit...Agencia inmobiliaria
	SetActorVirtualWorld( actor, 13 );
	ApplyActorAnimation( actor,"PED","SEAT_down",4.0,0,0,1,1,0 );
	
	actor = CreateActor(250,1082.1580,-346.2502,2797.7000,129.2255); // /sit...Agencia inmobiliaria
	SetActorVirtualWorld( actor, 13 );
	ApplyActorAnimation( actor,"PED","SEAT_down",4.0,0,0,1,1,0 );

	for( new i = 31; i < 34; i++ )
	{
		actor = CreateActor(194,-488.1833,-261.5951,3095.8960,90.3300); // Tienda de muebles
		SetActorVirtualWorld( actor, i );
		actor = CreateActor(240,-488.1824,-263.1257,3095.8960,89.9331); // Tienda de muebles
		SetActorVirtualWorld( actor, i );
	}
	
	actor = CreateActor(190,1687.2175,-1457.9667,1401.2169,358.7936); // Servicio de entrega
	SetActorVirtualWorld( actor, 51 );
	
	actor = CreateActor(192,1685.1285,-1457.9640,1401.2169,358.7936); // Servicio de entrega
	SetActorVirtualWorld( actor, 51 );
}