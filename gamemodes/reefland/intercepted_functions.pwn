stock Desbug(string[])
    {
        new
            szFixed[1024],
            iPos,
            iLen;

        for (iLen = strlen(string); iPos < iLen; iPos ++)
            switch (string[iPos])
            {
                case 'а':   szFixed[iPos] = 151;
                case 'б':   szFixed[iPos] = 152;
                case 'в':   szFixed[iPos] = 153;
                case 'д':   szFixed[iPos] = 154;
                case 'А':   szFixed[iPos] = 128;
                case 'Б':   szFixed[iPos] = 129;
                case 'В':   szFixed[iPos] = 130;
                case 'Д':   szFixed[iPos] = 131;
                case 'и':   szFixed[iPos] = 157;
                case 'й':   szFixed[iPos] = 158;
                case 'к':   szFixed[iPos] = 159;
                case 'л':   szFixed[iPos] = 160;
                case 'И':   szFixed[iPos] = 134;
                case 'Й':   szFixed[iPos] = 135;
                case 'К':   szFixed[iPos] = 136;
                case 'Л':   szFixed[iPos] = 137;
                case 'м':   szFixed[iPos] = 161;
                case 'н':   szFixed[iPos] = 162;
                case 'о':   szFixed[iPos] = 163;
                case 'п':   szFixed[iPos] = 164;
                case 'М':   szFixed[iPos] = 138;
                case 'Н':   szFixed[iPos] = 139;
                case 'О':   szFixed[iPos] = 140;
                case 'П':   szFixed[iPos] = 141;
                case 'т':   szFixed[iPos] = 165;
                case 'у':   szFixed[iPos] = 166;
                case 'ф':   szFixed[iPos] = 167;
                case 'ц':   szFixed[iPos] = 168;
                case 'Т':   szFixed[iPos] = 142;
                case 'У':   szFixed[iPos] = 143;
                case 'Ф':   szFixed[iPos] = 144;
                case 'Ц':   szFixed[iPos] = 145;
                case 'щ':   szFixed[iPos] = 169;
                case 'ъ':   szFixed[iPos] = 170;
                case 'ы':   szFixed[iPos] = 171;
                case 'ь':   szFixed[iPos] = 172;
                case 'Щ':   szFixed[iPos] = 146;
                case 'Ъ':   szFixed[iPos] = 147;
                case 'Ы':   szFixed[iPos] = 148;
                case 'Ь':   szFixed[iPos] = 149;
                case 'с':   szFixed[iPos] = 174;
                case 'С':   szFixed[iPos] = 173;
                case 'Ў':   szFixed[iPos] = 64;
                case 'ї':   szFixed[iPos] = 175;
                case '`':   szFixed[iPos] = 177;
                case '&':   szFixed[iPos] = 38;
                default:    szFixed[iPos] = string[iPos];
            }

        return szFixed;
    }
stock SetPlayerCash( playerid, type[] = "+", _: cash )
{
	clean:<g_string>;
	
	switch( type[0] )
	{
		case '+' :
		{
			Player[playerid][uMoney] += cash;
			
			mysql_format( mysql, g_string, sizeof g_string, "UPDATE " #DB_USERS " SET uMoney = uMoney + %d WHERE uID = %d LIMIT 1",
				cash,
				Player[playerid][uID]
			);
		}
		
		case '-' :
		{
			Player[playerid][uMoney] -= cash;
			
			mysql_format( mysql, g_string, sizeof g_string, "UPDATE " #DB_USERS " SET uMoney = uMoney - %d WHERE uID = %d LIMIT 1",
				cash,
				Player[playerid][uID]
			);
		}
		
		case '=' :
		{
			Player[playerid][uMoney] = cash;
			
			mysql_format( mysql, g_string, sizeof g_string, "UPDATE " #DB_USERS " SET uMoney = %d WHERE uID = %d LIMIT 1",
				cash,
				Player[playerid][uID]
			);
		}
	}
	
	
	return mysql_tquery( mysql, g_string );
}

stock SetPlayerBank( playerid, type[] = "+", _: cash )
{
	clean:<g_small_string>;
	
	new
		history 	[ 32 ],
		bool:flag = false;
	
	switch( type[0] )
	{
		case '+' :
		{
			Player[playerid][uBank] += cash;
			
			mysql_format( mysql, g_small_string, sizeof g_small_string, "UPDATE " #DB_USERS " SET uBank = uBank + %d WHERE uID = %d LIMIT 1",
				cash,
				Player[playerid][uID]
			);
			
			format:history( ""cGREEN"+ $%d", cash );
			
			for( new i; i < MAX_HISTORY; i++ )
			{
				if( !Payment[playerid][i][HistoryTime] )
				{
					Payment[playerid][i][HistoryTime] = gettime();
					
					clean:<Payment[playerid][i][HistoryName]>;
					strcat( Payment[playerid][i][HistoryName], history, 32 );
					
					flag = true;
					break;
				}
			}
			
			if( !flag )
			{
				Payment[playerid][0][HistoryTime] = gettime();
					
				clean:<Payment[playerid][0][HistoryName]>;
				strcat( Payment[playerid][0][HistoryName], history, 32 );
			}
		}
		
		case '-' :
		{
			Player[playerid][uBank] -= cash;
			
			mysql_format( mysql, g_small_string, sizeof g_small_string, "UPDATE " #DB_USERS " SET uBank = uBank - %d WHERE uID = %d LIMIT 1",
				cash,
				Player[playerid][uID]
			);
			
			format:history( ""cRED"- $%d", cash );
			
			for( new i; i < MAX_HISTORY; i++ )
			{
				if( !Payment[playerid][i][HistoryTime] )
				{
					Payment[playerid][i][HistoryTime] = gettime();
					
					clean:<Payment[playerid][i][HistoryName]>;
					strcat( Payment[playerid][i][HistoryName], history, 32 );
					
					flag = true;
					break;
				}
			}
			
			if( !flag )
			{
				Payment[playerid][0][HistoryTime] = gettime();
					
				clean:<Payment[playerid][0][HistoryName]>;
				strcat( Payment[playerid][0][HistoryName], history, 32 );
			}
		}
		
		case '=' :
		{
			Player[playerid][uBank] = cash;
			
			mysql_format( mysql, g_small_string, sizeof g_small_string, "UPDATE " #DB_USERS " SET uBank = %d WHERE uID = %d LIMIT 1",
				cash,
				Player[playerid][uID]
			);
		}
	}
	
	
	return mysql_tquery( mysql, g_small_string );
}

stock givePlayerWeapon( playerid, weaponid, ammo )
{
	GivePlayerWeapon( playerid, weaponid, ammo );
	printf( "[Log] Para %s[%d] - %d se le ha dado %d balas.", GetAccountName( playerid ), playerid, weaponid, ammo );
}

stock setPlayerSkin( playerid, skinid ) 
{
	return SetPlayerSkin( playerid, skinid );
}

stock togglePlayerSpectating( playerid, value ) 
{
	Player[playerid][tSpectate] = value;
	return TogglePlayerSpectating( playerid, value );
}

stock togglePlayerControllable( playerid, value )
{
	if(! value ) 
		SetPVarInt( playerid,"Player:Freeze", 0 );
	else 
		SetPVarInt( playerid,"Player:Freeze", 1 );
		
    return TogglePlayerControllable( playerid, value );
}

stock showPlayerDialog( playerid, dialogid, style, caption[], info[], button1[], button2[] ) 
{
	Player[playerid][tDialogId] = dialogid;
	
    return ShowPlayerDialog( playerid, dialogid, style, caption, info, button1, button2 );
}

stock setPlayerHealth( playerid, Float:health )
{
	if( health > 255.0 || health < 0.0 ) 
		return 0;
		
	Player[playerid][uHP] = health;
	
	SetPlayerHealth( playerid, health );
	return 1;
}

stock setPlayerArmour( playerid, Float: amount )
{
	if( amount > 255.0 || amount < 0.0 ) 
		return STATUS_ERROR;
		
	if( GetPVarInt( playerid, "Inv:unUseArmour" ) )
	{
		SetPlayerArmour( playerid, 0 );
		Player[playerid][uArmor] = 0;
		
		DeletePVar( playerid, "Inv:unUseArmour" );
		return STATUS_OK;
	}
		
	new 
		id;
		
	for( new i; i < MAX_INVENTORY_USE; i++ )
	{
		id = getInventoryId( UseInv[playerid][i][inv_id] );
			
		if( inventory[id][i_type] == INV_ARMOUR )
		{
			Player[playerid][uArmor] = amount;
			UseInv[playerid][i][inv_param_1] = floatround( amount );
			
			if( UseInv[playerid][i][inv_param_1] <= 0 )
			{
				if( UseInv[playerid][i][inv_bone] != INVALID_PARAM )
					RemovePlayerAttachedObject( playerid, i );
			
				saveInventory( playerid, i, INV_DELETE, TYPE_USE );
				clearSlot( playerid, i, TYPE_USE );
					
				if( GetPVarInt( playerid, "Inv:Show" ) )
				{
					updateImages( playerid, i, TYPE_USE );
					updateAmount( playerid, i, TYPE_USE );
			
					updateSelect( playerid, invSelect[playerid], 0 );
					invSelect[playerid] = INVALID_PTD;
				}
			}
				
			SetPlayerArmour( playerid, amount );
			return STATUS_OK;
		}
	}
	
    return STATUS_ERROR;
}

stock putPlayerInVehicle( playerid, vehicle, seat )
{
	foreach(new i: Player)
	{
		if( !IsLogged(i) || i == playerid ) continue;
	
		if( GetPlayerVehicleID( i ) == vehicle && GetPlayerVehicleSeat( i ) == seat )
			return STATUS_ERROR;
	}

	SetTimerEx( "OnputPlayerInVehicle", GetPlayerPing( playerid ) + 700, 0, "ddd", playerid, vehicle, seat );
	
	return 1;
}

function OnputPlayerInVehicle( playerid, vehicle, seat )
{
	if( !IsPlayerInAnyVehicle( playerid ) && Player[playerid][tEnterVehicle] == INVALID_VEHICLE_ID )
	{
		CheatDetected( playerid, "NOP putPlayerInVehicle", CHEAT_NOP_PUT_IN_VEH );
	}
	
	return PutPlayerInVehicle( playerid, vehicle, seat );
}

stock removePlayerFromVehicle( playerid ) 
{
	SetTimerEx( "OnPlayerRemoveFromVehicle", GetPlayerPing( playerid ) + 700, 0, "d", playerid );
	
    return 1;
}

function OnPlayerRemoveFromVehicle( playerid )
{
	if( IsPlayerInAnyVehicle( playerid ) && Player[playerid][tEnterVehicle] == INVALID_VEHICLE_ID )
	{
		CheatDetected( playerid, "NOP removePlayerFromVehicle", CHEAT_NOP_REMOVE_FROM_VEH );
	}
	
	RemovePlayerFromVehicle( playerid );
	Player[playerid][tEnterVehicle] = INVALID_VEHICLE_ID;
	
	return 1;
}

stock setVehiclePos( vehicleid, Float:x, Float:y, Float:z ) 
{
	ac_vehicle_pos[ vehicleid ][ vp_x ] = x;
	ac_vehicle_pos[ vehicleid ][ vp_y ] = y;
	ac_vehicle_pos[ vehicleid ][ vp_z ] = z;
	
	new 
		playerid = GetVehiclePlayerID( vehicleid );
	
	if( playerid != INVALID_PLAYER_ID )
	{
		g_player_airbreak_protect{playerid} = 7;
	}
	
	return SetVehiclePos( vehicleid, x, y, z );
}

stock setPlayerPos( playerid, Float: x, Float: y, Float: z ) 
{
	Player[playerid][tPos][0] = x;
	Player[playerid][tPos][1] = y;
	Player[playerid][tPos][2] = z;
	g_player_airbreak_protect{playerid} = ANTICHEAT_EXCEPTION_TIME;
	CancelEdit( playerid);
	
	SetPlayerPos( playerid, x, y, z );
	
	if( !g_player_airbreak_protect{playerid} && !IsAfk( playerid ) && !IsPlayerInRangeOfPoint( playerid, 15.0, x, y, z ) ) 
		return CheatDetected( playerid, "NOP setPlayerPos", CHEAT_NOP_TELEPORT );
	
	return 1;
}

stock setVehicleZAngle( vehicleid, Float:angle )
{
	SetTimerEx( "OnVehicleRotation", 200, false, "df", vehicleid, angle ); 
	return 1;
}

stock setVehicleHealth( vehicleid, Float: amount )
{
	Vehicle[vehicleid][vehicle_health] = amount;
	return SetVehicleHealth( vehicleid, amount );
}

function OnVehicleRotation( vehicleid, Float:angle )
{
	SetVehicleZAngle( vehicleid, angle );
	return 1;
}

#define SetPlayerSkin					setPlayerSkin
//#define SetPlayerPos					setPlayerPos
//#define SetVehiclePos					setVehiclePos
//#define RemovePlayerFromVehicle		removePlayerFromVehicle
//#define PutPlayerInVehicle			putPlayerInVehicle
//#define SetPlayerArmour				setPlayerArmour
//#define SetPlayerHealth				setPlayerHealth
//#define ShowPlayerDialog				showPlayerDialog
//#define TogglePlayerSpectating		togglePlayerSpectating
//#define TogglePlayerControllable		togglePlayerControllable
//#define GivePlayerWeapon				givePlayerWeapon
#define SetVehicleZAngle				setVehicleZAngle
//#define SetVehicleHealth				setVehicleHealth