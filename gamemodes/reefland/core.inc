/*
* Influx Core
* Developer: FR0Z3NH34R7
*
* 2018
*/

// Base
#include "reefland\server.pwn"
#include "reefland\connection.pwn"

// I-AC

#include "reefland\script\anticheat.pwn"

#include "reefland\intercepted_functions.pwn" 

#include "reefland\object.pwn"
#include "reefland\object_fraction.pwn"
#include "reefland\remove_object.pwn"
#include "reefland\textdraw.pwn"
#include "reefland\dialog.pwn"
//#include "reefland\date.pwn"
#include "reefland\functions.pwn"
#include "reefland\timer.pwn"
#include "reefland\dobject.pwn"
#include "reefland\overpass.pwn"
#include "reefland\sendmail.pwn"

#include "reefland\script\enters.pwn"
#include "reefland\script\licenses.pwn"
#include "reefland\script\user_menu.pwn"
#include "reefland\script\atm.pwn"
#include "reefland\script\pattach.pwn"
#include "reefland\script\payday.pwn"
#include "reefland\script\weather.pwn"
#include "reefland\script\logs.pwn"
#include "reefland\script\chat.pwn"
#include "reefland\script\target.pwn"
#include "reefland\script\skin.pwn"

#include "reefland\script\interface.pwn"
//#include "reefland\script\radio.pwn"
#include "reefland\script\gate.pwn"
#include "reefland\script\door.pwn"
#include "reefland\script\spawn.pwn"
#include "reefland\script\player.pwn"
#include "reefland\script\death.pwn"
#include "reefland\script\anim.pwn"
#include "reefland\script\gps.pwn"
#include "reefland\script\help.pwn"
#include "reefland\script\donat.pwn"

#include "reefland\script\admin\admin_menu.pwn"
#include "reefland\script\admin\admin_cmd.pwn"
#include "reefland\script\admin\admin_func.pwn"
#include "reefland\script\admin\admin_dialog.pwn"
#include "reefland\script\admin\admin_td.pwn"

#include "reefland\script\inventory\inv_td.pwn"
#include "reefland\script\inventory\inv_core.pwn"
#include "reefland\script\inventory\inv_show.pwn"
#include "reefland\script\inventory\inv_load.pwn"
#include "reefland\script\inventory\inv_dialog.pwn"
#include "reefland\script\inventory\inv_cmd.pwn"
#include "reefland\script\inventory\inv_buy_menu.pwn"

#include "reefland\script\job\job_core.pwn"
#include "reefland\script\job\job_dialog.pwn"
#include "reefland\script\job\job_cmd.pwn"

#include "reefland\script\phone\phone_td.pwn"
#include "reefland\script\phone\phone_core.pwn"
#include "reefland\script\phone\phone_dialog.pwn"

//#include "reefland\script\house\hostel.pwn"
#include "reefland\script\house\house_core.pwn"
#include "reefland\script\house\house_dialog.pwn"
#include "reefland\script\house\house_enter.pwn"
#include "reefland\script\house\house_buy.pwn"
#include "reefland\script\house\house_cmd.pwn"
#include "reefland\script\house\house_interior.pwn"
#include "reefland\script\house\house_texture.pwn"

#include "reefland\script\business\business_buy.pwn"
#include "reefland\script\business\business_core.pwn"
#include "reefland\script\business\business_enter.pwn"
#include "reefland\script\business\business_dialog.pwn"
#include "reefland\script\business\business_cmd.pwn"
#include "reefland\script\business\business_texture.pwn"
#include "reefland\script\business\business_interior.pwn"

#include "reefland\script\criminal\crime_core.pwn"
#include "reefland\script\criminal\crime_cmd.pwn"
#include "reefland\script\criminal\crime_dialog.pwn"

#include "reefland\script\vehicle\vehicle_dialog.pwn"
#include "reefland\script\vehicle\vehicle_core.pwn"
#include "reefland\script\vehicle\vehicle_cmd.pwn"
#include "reefland\script\vehicle\vehicle_hud.pwn"
#include "reefland\script\vehicle\tuning.pwn"

#include "reefland\script\furniture\furniture_dialog.pwn"
#include "reefland\script\furniture\furniture_core.pwn"

#include "reefland\script\fraction\fraction_core.pwn"
#include "reefland\script\fraction\fraction_dialog.pwn"
#include "reefland\script\fraction\fraction_cmd.pwn"
#include "reefland\script\fraction\fraction_object.pwn"
#include "reefland\script\fraction\fraction_attach.pwn"
#include "reefland\script\fraction\strobe.pwn"

#include "reefland\script\fraction\pd\pd_cmd.pwn"
#include "reefland\script\fraction\pd\pd_core.pwn"
#include "reefland\script\fraction\pd\pd_dialog.pwn"

#include "reefland\script\fraction\fd\fd_core.pwn"
#include "reefland\script\fraction\fd\fd_dialog.pwn"
#include "reefland\script\fraction\fd\fd_cmd.pwn"

#include "reefland\script\fraction\san\san_core.pwn"
#include "reefland\script\fraction\san\san_dialog.pwn"
#include "reefland\script\fraction\san\san_cmd.pwn"

#include "reefland\script\fraction\ch\ch_core.pwn"
#include "reefland\script\fraction\ch\ch_dialog.pwn"
#include "reefland\script\fraction\ch\ch_cmd.pwn"

#include "reefland\script\fraction\sadoc\prison_core.pwn"
#include "reefland\script\fraction\sadoc\prison_dialog.pwn"
#include "reefland\script\fraction\sadoc\prison_cmd.pwn"