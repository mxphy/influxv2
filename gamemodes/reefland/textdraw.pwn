new
	Text: logo				[ 3 ],
	Text: chooseSkin		[ 4 ],
	PlayerText: radio		[ 3 ][ MAX_PLAYERS ],
	Text: blind_background;
	
TextDraws_OnGameModeInit() 
{
	// New
	/* - TD: Logo - */
	/*logo[0] = TextDrawCreate(549.235107, 8.499971, "LD_BEAT:chit");
	TextDrawLetterSize(logo[0], 0.000000, 0.000000);
	TextDrawTextSize(logo[0], 15.000000, 15.000000);
	TextDrawAlignment(logo[0], 1);
	TextDrawColor(logo[0], td_cBLUE);
	TextDrawSetShadow(logo[0], 0);
	TextDrawSetOutline(logo[0], 0);
	TextDrawBackgroundColor(logo[0], 255);
	TextDrawFont(logo[0], 4);
	TextDrawSetProportional(logo[0], 0);
	TextDrawSetShadow(logo[0], 0);

	logo[1] = TextDrawCreate(554.482385, 11.499990, "G");
	TextDrawLetterSize(logo[1], 0.189412, 0.883165);
	TextDrawAlignment(logo[1], 1);
	TextDrawColor(logo[1], -1);
	TextDrawSetShadow(logo[1], 0);
	TextDrawSetOutline(logo[1], 0);
	TextDrawBackgroundColor(logo[1], 255);
	TextDrawFont(logo[1], 2);
	TextDrawSetProportional(logo[1], 1);
	TextDrawSetShadow(logo[1], 0);

	logo[2] = TextDrawCreate(562.705932, 11.499991, "-GAME.NET");
	TextDrawLetterSize(logo[2], 0.189412, 0.883165);
	TextDrawAlignment(logo[2], 1);
	TextDrawColor(logo[2], -1);
	TextDrawSetShadow(logo[2], 0);
	TextDrawSetOutline(logo[2], 0);
	TextDrawBackgroundColor(logo[2], 255);
	TextDrawFont(logo[2], 2);
	TextDrawSetProportional(logo[2], 1);
	TextDrawSetShadow(logo[2], 0);*/
	
	logo[0] = TextDrawCreate(554.482385, 11.499990, "INFLUX-RP.EU");
	TextDrawLetterSize(logo[0], 0.189412, 0.883165);
	TextDrawAlignment(logo[0], 1);
	TextDrawColor(logo[0], -1);
	TextDrawSetShadow(logo[0], 0);
	TextDrawSetOutline(logo[0], 0);
	TextDrawBackgroundColor(logo[0], 255);
	TextDrawFont(logo[0], 2);
	TextDrawSetProportional(logo[0], 1);
	TextDrawSetShadow(logo[0], 0);
	
	/* - TD: Logo - End */
	
	// Old
	
	SpeedFon[0] = TextDrawCreate(259.600891, 377.149902, "box");
	TextDrawLetterSize(SpeedFon[0], 0.000000, 4.682346);
	TextDrawTextSize(SpeedFon[0], 375.000000, 0.000000);
	TextDrawAlignment(SpeedFon[0], 1);
	TextDrawColor(SpeedFon[0], -1);
	TextDrawUseBox(SpeedFon[0], 1);
	//TextDrawBoxColor(SpeedFon[0], 0x4A86B650);
	TextDrawBoxColor(SpeedFon[0], 0x1E999930 );
	TextDrawSetShadow(SpeedFon[0], 0);
	TextDrawSetOutline(SpeedFon[0], 0);
	TextDrawBackgroundColor(SpeedFon[0], 95);
	TextDrawFont(SpeedFon[0], 1);
	TextDrawSetProportional(SpeedFon[0], 1);
	
	//Fondo del tax�metro
	TaxiBackground = TextDrawCreate(259.600891, 424.728281, "box");
	TextDrawLetterSize(TaxiBackground, 0.000000, 1.693349);
	TextDrawTextSize(TaxiBackground, 375.000000, 0.000000);
	TextDrawAlignment(TaxiBackground, 1);
	TextDrawColor(TaxiBackground, -1);
	TextDrawUseBox(TaxiBackground, 1);
	//TextDrawBoxColor(TaxiBackground, 0x4A86B650 );
	TextDrawBoxColor(TaxiBackground, 0x1E999930 );
	TextDrawSetShadow(TaxiBackground, 0);
	TextDrawSetOutline(TaxiBackground, 0);
	TextDrawBackgroundColor(TaxiBackground, 95);
	TextDrawFont(TaxiBackground, 1);
	TextDrawSetProportional(TaxiBackground, 1);
	
	SpeedFon[1] = TextDrawCreate(242.882369, 402.916687, "LD_BEAT:chit");
	TextDrawLetterSize(SpeedFon[1], 0.000000, 0.000000);
	TextDrawTextSize(SpeedFon[1], 148.000000, -2.000000);
	TextDrawAlignment(SpeedFon[1], 1);
	TextDrawColor(SpeedFon[1], -1);
	TextDrawSetOutline(SpeedFon[1], 0);
	TextDrawBackgroundColor(SpeedFon[1], 255);
	TextDrawFont(SpeedFon[1], 4);
	TextDrawSetProportional(SpeedFon[1], 0);
	TextDrawSetShadow(SpeedFon[1], 0);
	
	SpeedFon[2]= TextDrawCreate(307.711853, 384.250030, "MP/H");
	TextDrawLetterSize(SpeedFon[2], 0.150588, 1.016666);
	TextDrawAlignment(SpeedFon[2], 2);
	TextDrawColor(SpeedFon[2], -1);
	TextDrawSetShadow(SpeedFon[2], 0);
	TextDrawSetOutline(SpeedFon[2], 0);
	TextDrawBackgroundColor(SpeedFon[2], 255);
	TextDrawFont(SpeedFon[2], 2);
	TextDrawSetProportional(SpeedFon[2], 1);
	TextDrawSetShadow(SpeedFon[2], 0);
	
	MenuPlayer[0] = TextDrawCreate(265.000000, 131.000000, "_");
	TextDrawBackgroundColor(MenuPlayer[0], 255);
	TextDrawFont(MenuPlayer[0], 1);
	TextDrawLetterSize(MenuPlayer[0], 0.500000, 25.300025);
	TextDrawColor(MenuPlayer[0], -1);
	TextDrawSetOutline(MenuPlayer[0], 0);
	TextDrawSetProportional(MenuPlayer[0], 1);
	TextDrawSetShadow(MenuPlayer[0], 1);
	TextDrawUseBox(MenuPlayer[0], 1);
	TextDrawBoxColor(MenuPlayer[0], td_cBLUE);
	TextDrawTextSize(MenuPlayer[0], 76.000000, 0.000000);
	TextDrawSetSelectable(MenuPlayer[0], 0);
	
	MenuPlayer[1] = TextDrawCreate(262.000000, 134.000000, "_");
	TextDrawBackgroundColor(MenuPlayer[1], 255);
	TextDrawFont(MenuPlayer[1], 1);
	TextDrawLetterSize(MenuPlayer[1], 0.500000, 24.600023);
	TextDrawColor(MenuPlayer[1], -1);
	TextDrawSetOutline(MenuPlayer[1], 0);
	TextDrawSetProportional(MenuPlayer[1], 1);
	TextDrawSetShadow(MenuPlayer[1], 1);
	TextDrawUseBox(MenuPlayer[1], 1);
	TextDrawBoxColor(MenuPlayer[1], 96);
	TextDrawTextSize(MenuPlayer[1], 79.000000, 0.000000);
	TextDrawSetSelectable(MenuPlayer[1], 0);
	
	MenuPlayer[2] = TextDrawCreate(260.000000, 137.000000, "_");
	TextDrawBackgroundColor(MenuPlayer[2], 255);
	TextDrawFont(MenuPlayer[2], 1);
	TextDrawLetterSize(MenuPlayer[2], 0.500000, 1.300024);
	TextDrawColor(MenuPlayer[2], -1);
	TextDrawSetOutline(MenuPlayer[2], 0);
	TextDrawSetProportional(MenuPlayer[2], 1);
	TextDrawSetShadow(MenuPlayer[2], 1);
	TextDrawUseBox(MenuPlayer[2], 1);
	TextDrawBoxColor(MenuPlayer[2], td_cBLUE);
	TextDrawTextSize(MenuPlayer[2], 81.000000, 0.000000);
	TextDrawSetSelectable(MenuPlayer[2], 0);
	
	MenuPlayer[3] = TextDrawCreate(435.000000, 170.000000, "_");
	TextDrawAlignment(MenuPlayer[3], 2);
	TextDrawBackgroundColor(MenuPlayer[3], 255);
	TextDrawFont(MenuPlayer[3], 1);
	TextDrawLetterSize(MenuPlayer[3], 0.500000, 1.799999);
	TextDrawColor(MenuPlayer[3], -1);
	TextDrawSetOutline(MenuPlayer[3], 0);
	TextDrawSetProportional(MenuPlayer[3], 1);
	TextDrawSetShadow(MenuPlayer[3], 1);
	TextDrawUseBox(MenuPlayer[3], 1);
	TextDrawBoxColor(MenuPlayer[3], td_cBLUE);
	TextDrawTextSize(MenuPlayer[3], 6.000000, -128.000000);
	TextDrawSetSelectable(MenuPlayer[3], 0);
	
	MenuPlayer[4] = TextDrawCreate(435.000000, 195.000000, "_");
	TextDrawAlignment(MenuPlayer[4], 2);
	TextDrawBackgroundColor(MenuPlayer[4], 255);
	TextDrawFont(MenuPlayer[4], 1);
	TextDrawLetterSize(MenuPlayer[4], 0.500000, 1.799999);
	TextDrawColor(MenuPlayer[4], -1);
	TextDrawSetOutline(MenuPlayer[4], 0);
	TextDrawSetProportional(MenuPlayer[4], 1);
	TextDrawSetShadow(MenuPlayer[4], 1);
	TextDrawUseBox(MenuPlayer[4], 1);
	TextDrawBoxColor(MenuPlayer[4], td_cBLUE);
	TextDrawTextSize(MenuPlayer[4], 6.000000, -128.000000);
	TextDrawSetSelectable(MenuPlayer[4], 0);
	
	MenuPlayer[5] = TextDrawCreate(435.000000, 220.000000, "_");
	TextDrawAlignment(MenuPlayer[5], 2);
	TextDrawBackgroundColor(MenuPlayer[5], 255);
	TextDrawFont(MenuPlayer[5], 1);
	TextDrawLetterSize(MenuPlayer[5], 0.500000, 1.799999);
	TextDrawColor(MenuPlayer[5], -1);
	TextDrawSetOutline(MenuPlayer[5], 0);
	TextDrawSetProportional(MenuPlayer[5], 1);
	TextDrawSetShadow(MenuPlayer[5], 1);
	TextDrawUseBox(MenuPlayer[5], 1);
	TextDrawBoxColor(MenuPlayer[5], td_cBLUE);
	TextDrawTextSize(MenuPlayer[5], 6.000000, -128.000000);
	TextDrawSetSelectable(MenuPlayer[5], 0);
	
	MenuPlayer[6] = TextDrawCreate(435.000000, 245.000000, "_");
	TextDrawAlignment(MenuPlayer[6], 2);
	TextDrawBackgroundColor(MenuPlayer[6], 255);
	TextDrawFont(MenuPlayer[6], 1);
	TextDrawLetterSize(MenuPlayer[6], 0.500000, 1.799999);
	TextDrawColor(MenuPlayer[6], -1);
	TextDrawSetOutline(MenuPlayer[6], 0);
	TextDrawSetProportional(MenuPlayer[6], 1);
	TextDrawSetShadow(MenuPlayer[6], 1);
	TextDrawUseBox(MenuPlayer[6], 1);
	TextDrawBoxColor(MenuPlayer[6], td_cBLUE);
	TextDrawTextSize(MenuPlayer[6], 6.000000, -128.000000);
	TextDrawSetSelectable(MenuPlayer[6], 0);
	
	MenuPlayer[7] = TextDrawCreate(435.000000, 270.000000, "_");
	TextDrawAlignment(MenuPlayer[7], 2);
	TextDrawBackgroundColor(MenuPlayer[7], 255);
	TextDrawFont(MenuPlayer[7], 1);
	TextDrawLetterSize(MenuPlayer[7], 0.500000, 1.799999);
	TextDrawColor(MenuPlayer[7], -1);
	TextDrawSetOutline(MenuPlayer[7], 0);
	TextDrawSetProportional(MenuPlayer[7], 1);
	TextDrawSetShadow(MenuPlayer[7], 1);
	TextDrawUseBox(MenuPlayer[7], 1);
	TextDrawBoxColor(MenuPlayer[7], td_cBLUE);
	TextDrawTextSize(MenuPlayer[7], 6.000000, -128.000000);
	TextDrawSetSelectable(MenuPlayer[7], 0);
	
	MenuPlayer[8] = TextDrawCreate(435.000000, 295.000000, "_");
	TextDrawAlignment(MenuPlayer[8], 2);
	TextDrawBackgroundColor(MenuPlayer[8], 255);
	TextDrawFont(MenuPlayer[8], 1);
	TextDrawLetterSize(MenuPlayer[8], 0.500000, 1.799999);
	TextDrawColor(MenuPlayer[8], -1);
	TextDrawSetOutline(MenuPlayer[8], 0);
	TextDrawSetProportional(MenuPlayer[8], 1);
	TextDrawSetShadow(MenuPlayer[8], 1);
	TextDrawUseBox(MenuPlayer[8], 1);
	TextDrawBoxColor(MenuPlayer[8], td_cBLUE);
	TextDrawTextSize(MenuPlayer[8], 6.000000, -128.000000);
	TextDrawSetSelectable(MenuPlayer[8], 0);
	
	MenuPlayer[9] = TextDrawCreate(435.000000, 172.000000, "_");
	TextDrawAlignment(MenuPlayer[9], 2);
	TextDrawBackgroundColor(MenuPlayer[9], 255);
	TextDrawFont(MenuPlayer[9], 1);
	TextDrawLetterSize(MenuPlayer[9], 0.500000, 1.399999);
	TextDrawColor(MenuPlayer[9], -1);
	TextDrawSetOutline(MenuPlayer[9], 0);
	TextDrawSetProportional(MenuPlayer[9], 1);
	TextDrawSetShadow(MenuPlayer[9], 1);
	TextDrawUseBox(MenuPlayer[9], 1);
	TextDrawBoxColor(MenuPlayer[9], 96);
	TextDrawTextSize(MenuPlayer[9], 6.000000, -125.000000);
	TextDrawSetSelectable(MenuPlayer[9], 0);
	
	MenuPlayer[10] = TextDrawCreate(435.000000, 197.000000, "_");
	TextDrawAlignment(MenuPlayer[10], 2);
	TextDrawBackgroundColor(MenuPlayer[10], 255);
	TextDrawFont(MenuPlayer[10], 1);
	TextDrawLetterSize(MenuPlayer[10], 0.500000, 1.399999);
	TextDrawColor(MenuPlayer[10], -1);
	TextDrawSetOutline(MenuPlayer[10], 0);
	TextDrawSetProportional(MenuPlayer[10], 1);
	TextDrawSetShadow(MenuPlayer[10], 1);
	TextDrawUseBox(MenuPlayer[10], 1);
	TextDrawBoxColor(MenuPlayer[10], 96);
	TextDrawTextSize(MenuPlayer[10], 6.000000, -125.000000);
	TextDrawSetSelectable(MenuPlayer[10], 0);
	
	MenuPlayer[11] = TextDrawCreate(435.000000, 222.000000, "_");
	TextDrawAlignment(MenuPlayer[11], 2);
	TextDrawBackgroundColor(MenuPlayer[11], 255);
	TextDrawFont(MenuPlayer[11], 1);
	TextDrawLetterSize(MenuPlayer[11], 0.500000, 1.299999);
	TextDrawColor(MenuPlayer[11], -1);
	TextDrawSetOutline(MenuPlayer[11], 0);
	TextDrawSetProportional(MenuPlayer[11], 1);
	TextDrawSetShadow(MenuPlayer[11], 1);
	TextDrawUseBox(MenuPlayer[11], 1);
	TextDrawBoxColor(MenuPlayer[11], 96);
	TextDrawTextSize(MenuPlayer[11], 6.000000, -125.000000);
	TextDrawSetSelectable(MenuPlayer[11], 0);
	
	MenuPlayer[12] = TextDrawCreate(435.000000, 247.000000, "_");
	TextDrawAlignment(MenuPlayer[12], 2);
	TextDrawBackgroundColor(MenuPlayer[12], 255);
	TextDrawFont(MenuPlayer[12], 1);
	TextDrawLetterSize(MenuPlayer[12], 0.500000, 1.299999);
	TextDrawColor(MenuPlayer[12], -1);
	TextDrawSetOutline(MenuPlayer[12], 0);
	TextDrawSetProportional(MenuPlayer[12], 1);
	TextDrawSetShadow(MenuPlayer[12], 1);
	TextDrawUseBox(MenuPlayer[12], 1);
	TextDrawBoxColor(MenuPlayer[12], 96);
	TextDrawTextSize(MenuPlayer[12], 6.000000, -125.000000);
	TextDrawSetSelectable(MenuPlayer[12], 0);
	
	MenuPlayer[13] = TextDrawCreate(435.000000, 272.000000, "_");
	TextDrawAlignment(MenuPlayer[13], 2);
	TextDrawBackgroundColor(MenuPlayer[13], 255);
	TextDrawFont(MenuPlayer[13], 1);
	TextDrawLetterSize(MenuPlayer[13], 0.500000, 1.299999);
	TextDrawColor(MenuPlayer[13], -1);
	TextDrawSetOutline(MenuPlayer[13], 0);
	TextDrawSetProportional(MenuPlayer[13], 1);
	TextDrawSetShadow(MenuPlayer[13], 1);
	TextDrawUseBox(MenuPlayer[13], 1);
	TextDrawBoxColor(MenuPlayer[13], 96);
	TextDrawTextSize(MenuPlayer[13], 6.000000, -125.000000);
	TextDrawSetSelectable(MenuPlayer[13], 0);
	
	MenuPlayer[14] = TextDrawCreate(435.000000, 297.000000, "_");
	TextDrawAlignment(MenuPlayer[14], 2);
	TextDrawBackgroundColor(MenuPlayer[14], 255);
	TextDrawFont(MenuPlayer[14], 1);
	TextDrawLetterSize(MenuPlayer[14], 0.500000, 1.299999);
	TextDrawColor(MenuPlayer[14], -1);
	TextDrawSetOutline(MenuPlayer[14], 0);
	TextDrawSetProportional(MenuPlayer[14], 1);
	TextDrawSetShadow(MenuPlayer[14], 1);
	TextDrawUseBox(MenuPlayer[14], 1);
	TextDrawBoxColor(MenuPlayer[14], 96);
	TextDrawTextSize(MenuPlayer[14], 6.000000, -125.000000);
	TextDrawSetSelectable(MenuPlayer[14], 0);
	
	MenuPlayer[15] = TextDrawCreate(410.000000, 170.500000, "Enviar_Duda");
	TextDrawBackgroundColor(MenuPlayer[15], 255);
	TextDrawFont(MenuPlayer[15], 2);
	TextDrawLetterSize(MenuPlayer[15], 0.209999, 1.399999);
	TextDrawColor(MenuPlayer[15], -1);
	TextDrawSetOutline(MenuPlayer[15], 0);
	TextDrawSetProportional(MenuPlayer[15], 1);
	TextDrawSetShadow(MenuPlayer[15], 0);
	TextDrawUseBox(MenuPlayer[15], 1);
	TextDrawBoxColor(MenuPlayer[15], 0);
	TextDrawTextSize(MenuPlayer[15], 492.000000, 10.000000);
	TextDrawSetSelectable(MenuPlayer[15], 1);
	
	MenuPlayer[16] = TextDrawCreate(416.000000, 195.500000, "Ayuda");
	TextDrawBackgroundColor(MenuPlayer[16], 255);
	TextDrawFont(MenuPlayer[16], 2);
	TextDrawLetterSize(MenuPlayer[16], 0.209999, 1.399999);
	TextDrawColor(MenuPlayer[16], -1);
	TextDrawSetOutline(MenuPlayer[16], 0);
	TextDrawSetProportional(MenuPlayer[16], 1);
	TextDrawSetShadow(MenuPlayer[16], 0);
	TextDrawUseBox(MenuPlayer[16], 1);
	TextDrawBoxColor(MenuPlayer[16], 0);
	TextDrawTextSize(MenuPlayer[16], 470.000000, 10.000000);
	TextDrawSetSelectable(MenuPlayer[16], 1);
	
	MenuPlayer[17] = TextDrawCreate(410.000000, 220.500000, "Mis_Ajustes");
	TextDrawBackgroundColor(MenuPlayer[17], 255);
	TextDrawFont(MenuPlayer[17], 2);
	TextDrawLetterSize(MenuPlayer[17], 0.209999, 1.399999);
	TextDrawColor(MenuPlayer[17], -1);
	TextDrawSetOutline(MenuPlayer[17], 0);
	TextDrawSetProportional(MenuPlayer[17], 1);
	TextDrawSetShadow(MenuPlayer[17], 0);
	TextDrawUseBox(MenuPlayer[17], 1);
	TextDrawBoxColor(MenuPlayer[17], 0);
	TextDrawTextSize(MenuPlayer[17], 469.000000, 10.000000);
	TextDrawSetSelectable(MenuPlayer[17], 1);
	
	MenuPlayer[18] = TextDrawCreate(415.000000, 245.500000, "Comandos");
	TextDrawBackgroundColor(MenuPlayer[18], 255);
	TextDrawFont(MenuPlayer[18], 2);
	TextDrawLetterSize(MenuPlayer[18], 0.209999, 1.399999);
	TextDrawColor(MenuPlayer[18], -1);
	TextDrawSetOutline(MenuPlayer[18], 0);
	TextDrawSetProportional(MenuPlayer[18], 1);
	TextDrawSetShadow(MenuPlayer[18], 0);
	TextDrawUseBox(MenuPlayer[18], 1);
	TextDrawBoxColor(MenuPlayer[18], 0);
	TextDrawTextSize(MenuPlayer[18], 475.000000, 10.000000);
	TextDrawSetSelectable(MenuPlayer[18], 1);
	
	MenuPlayer[19] = TextDrawCreate( 407.000000, 270.500000, "Estadisticas" );
	TextDrawBackgroundColor(MenuPlayer[19], 255);
	TextDrawFont(MenuPlayer[19], 2);
	TextDrawLetterSize(MenuPlayer[19], 0.209999, 1.399999);
	TextDrawColor(MenuPlayer[19], -1);
	TextDrawSetOutline(MenuPlayer[19], 0);
	TextDrawSetProportional(MenuPlayer[19], 1);
	TextDrawSetShadow(MenuPlayer[19], 0);
	TextDrawUseBox(MenuPlayer[19], 1);
	TextDrawBoxColor(MenuPlayer[19], 0);
	TextDrawTextSize(MenuPlayer[19], 488.000000, 10.000000);
	TextDrawSetSelectable(MenuPlayer[19], 1);
	
	MenuPlayer[20] = TextDrawCreate( 416.000000, 295.500000, "Tienda" );
	TextDrawBackgroundColor(MenuPlayer[20], 255);
	TextDrawFont(MenuPlayer[20], 2);
	TextDrawLetterSize(MenuPlayer[20], 0.209999, 1.399999);
	TextDrawColor(MenuPlayer[20], -1);
	TextDrawSetOutline(MenuPlayer[20], 0);
	TextDrawSetProportional(MenuPlayer[20], 1);
	TextDrawSetShadow(MenuPlayer[20], 0);
	TextDrawUseBox(MenuPlayer[20], 1);
	TextDrawBoxColor(MenuPlayer[20], 0);
	TextDrawTextSize(MenuPlayer[20], 449.000000, 10.000000);
	TextDrawSetSelectable(MenuPlayer[20], 1);
	
	
	
	furnitureBuy[1] = TextDrawCreate(320.000000, 380.000000, "_");
	TextDrawAlignment(furnitureBuy[1], 2);
	TextDrawBackgroundColor(furnitureBuy[1], 255);
	TextDrawFont(furnitureBuy[1], 1);
	TextDrawLetterSize(furnitureBuy[1], 30.500000, 2.100000);
	TextDrawColor(furnitureBuy[1], -1);
	TextDrawSetOutline(furnitureBuy[1], 0);
	TextDrawSetProportional(furnitureBuy[1], 1);
	TextDrawSetShadow(furnitureBuy[1], 0);
	TextDrawUseBox(furnitureBuy[1], 1);
	//TextDrawBoxColor(furnitureBuy[1], 0x4A86B630);
	TextDrawBoxColor(furnitureBuy[1], 0x1E999930);
	TextDrawTextSize(furnitureBuy[1], 0.000000, -179.000000);
	TextDrawSetSelectable(furnitureBuy[1], 0);
	
	furnitureBuy[6] = TextDrawCreate(320.000000, 356.000000, "_");
	TextDrawAlignment(furnitureBuy[6], 2);
	TextDrawBackgroundColor(furnitureBuy[6], 255);
	TextDrawFont(furnitureBuy[6], 1);
	TextDrawLetterSize(furnitureBuy[6], 30.500000, 2.100000);
	TextDrawColor(furnitureBuy[6], -1);
	TextDrawSetOutline(furnitureBuy[6], 0);
	TextDrawSetProportional(furnitureBuy[6], 1);
	TextDrawSetShadow(furnitureBuy[6], 0);
	TextDrawUseBox(furnitureBuy[6], 1);
	//TextDrawBoxColor(furnitureBuy[6], 0x4A86B630);
	TextDrawBoxColor(furnitureBuy[6], 0x1E999930);
	TextDrawTextSize(furnitureBuy[6], 0.000000, -179.000000);
	TextDrawSetSelectable(furnitureBuy[6], 0);
	
	furnitureBuy[0] = TextDrawCreate(320.000000, 404.000000, "_");
	TextDrawLetterSize(furnitureBuy[0], 0.500000, 2.100000);
	TextDrawTextSize(furnitureBuy[0], 0.000000, -179.000000);
	TextDrawAlignment(furnitureBuy[0], 2);
	TextDrawColor(furnitureBuy[0], -1);
	TextDrawUseBox(furnitureBuy[0], 1);
	//TextDrawBoxColor(furnitureBuy[0], 0x4A86B630);
	TextDrawBoxColor(furnitureBuy[0], 0x1E999930);
	TextDrawSetShadow(furnitureBuy[0], 0);
	TextDrawSetOutline(furnitureBuy[0], 0);
	TextDrawBackgroundColor(furnitureBuy[0], 255);
	TextDrawFont(furnitureBuy[0], 1);
	TextDrawSetProportional(furnitureBuy[0], 1);
	
	furnitureBuy[2] = TextDrawCreate(234.000000, 403.000000, "ld_beat:left");
	TextDrawBackgroundColor(furnitureBuy[2], 255);
	TextDrawFont(furnitureBuy[2], 4);
	TextDrawLetterSize(furnitureBuy[2], 0.500000, 1.000000);
	TextDrawColor(furnitureBuy[2], 0x1E999930);
	TextDrawSetOutline(furnitureBuy[2], 0);
	TextDrawSetProportional(furnitureBuy[2], 1);
	TextDrawSetShadow(furnitureBuy[2], 1);
	TextDrawUseBox(furnitureBuy[2], 1);
	TextDrawBoxColor(furnitureBuy[2], 255);
	TextDrawTextSize(furnitureBuy[2], 21.000000, 22.000000);
	TextDrawSetSelectable(furnitureBuy[2], 1);
	
	furnitureBuy[3] = TextDrawCreate(386.000000, 403.000000, "ld_beat:right");
	TextDrawBackgroundColor(furnitureBuy[3], 255);
	TextDrawFont(furnitureBuy[3], 4);
	TextDrawLetterSize(furnitureBuy[3], 0.500000, 1.000000);
	TextDrawColor(furnitureBuy[3], 0x1E999930);
	TextDrawSetOutline(furnitureBuy[3], 0);
	TextDrawSetProportional(furnitureBuy[3], 1);
	TextDrawSetShadow(furnitureBuy[3], 1);
	TextDrawUseBox(furnitureBuy[3], 1);
	TextDrawBoxColor(furnitureBuy[3], 255);
	TextDrawTextSize(furnitureBuy[3], 21.000000, 22.000000);
	TextDrawSetSelectable(furnitureBuy[3], 1);
	
	furnitureBuy[4] = TextDrawCreate(265.000000, 404.000000, "Comprar");
	TextDrawBackgroundColor(furnitureBuy[4], 255);
	TextDrawFont(furnitureBuy[4], 2);
	TextDrawLetterSize(furnitureBuy[4], 0.320000, 1.899999);
	TextDrawColor(furnitureBuy[4], -1);
	TextDrawSetOutline(furnitureBuy[4], 0);
	TextDrawSetProportional(furnitureBuy[4], 1);
	TextDrawSetShadow(furnitureBuy[4], 0);
	TextDrawUseBox(furnitureBuy[4], 1);
	TextDrawBoxColor(furnitureBuy[4], 0);
	TextDrawTextSize(furnitureBuy[4], 310.000000, 20.000000);
	TextDrawSetSelectable(furnitureBuy[4], 1);
	
	furnitureBuy[5] = TextDrawCreate(335.000000, 404.000000, "Salir");
	TextDrawBackgroundColor(furnitureBuy[5], 255);
	TextDrawFont(furnitureBuy[5], 2);
	TextDrawLetterSize(furnitureBuy[5], 0.320000, 1.899999);
	TextDrawColor(furnitureBuy[5], -1);
	TextDrawSetOutline(furnitureBuy[5], 0);
	TextDrawSetProportional(furnitureBuy[5], 1);
	TextDrawSetShadow(furnitureBuy[5], 0);
	TextDrawUseBox(furnitureBuy[5], 1);
	TextDrawBoxColor(furnitureBuy[5], 0);
	TextDrawTextSize(furnitureBuy[5], 376.000000, 20.000000);
	TextDrawSetSelectable(furnitureBuy[5], 1);
	
	//Diferente para los negocios
	furnitureOther[0] = TextDrawCreate(234.000000, 403.000000, "ld_beat:left");
	TextDrawBackgroundColor(furnitureOther[0], 255);
	TextDrawFont(furnitureOther[0], 4);
	TextDrawLetterSize(furnitureOther[0], 0.500000, 1.000000);
	TextDrawColor(furnitureOther[0], 0x1E999930);
	TextDrawSetOutline(furnitureOther[0], 0);
	TextDrawSetProportional(furnitureOther[0], 1);
	TextDrawSetShadow(furnitureOther[0], 1);
	TextDrawUseBox(furnitureOther[0], 1);
	TextDrawBoxColor(furnitureOther[0], 255);
	TextDrawTextSize(furnitureOther[0], 21.000000, 22.000000);
	TextDrawSetSelectable(furnitureOther[0], 1);
	
	furnitureOther[1] = TextDrawCreate(386.000000, 403.000000, "ld_beat:right");
	TextDrawBackgroundColor(furnitureOther[1], 255);
	TextDrawFont(furnitureOther[1], 4);
	TextDrawLetterSize(furnitureOther[1], 0.500000, 1.000000);
	TextDrawColor(furnitureOther[1], 0x1E999930);
	TextDrawSetOutline(furnitureOther[1], 0);
	TextDrawSetProportional(furnitureOther[1], 1);
	TextDrawSetShadow(furnitureOther[1], 1);
	TextDrawUseBox(furnitureOther[1], 1);
	TextDrawBoxColor(furnitureOther[1], 255);
	TextDrawTextSize(furnitureOther[1], 21.000000, 22.000000);
	TextDrawSetSelectable(furnitureOther[1], 1);
	
	furnitureOther[2] = TextDrawCreate(265.000000, 404.000000, "Comprar");
	TextDrawBackgroundColor(furnitureOther[2], 255);
	TextDrawFont(furnitureOther[2], 2);
	TextDrawLetterSize(furnitureOther[2], 0.320000, 1.899999);
	TextDrawColor(furnitureOther[2], -1);
	TextDrawSetOutline(furnitureOther[2], 0);
	TextDrawSetProportional(furnitureOther[2], 1);
	TextDrawSetShadow(furnitureOther[2], 0);
	TextDrawUseBox(furnitureOther[2], 1);
	TextDrawBoxColor(furnitureOther[2], 0);
	TextDrawTextSize(furnitureOther[2], 310.000000, 20.000000);
	TextDrawSetSelectable(furnitureOther[2], 1);
	
	furnitureOther[3] = TextDrawCreate(335.000000, 404.000000, "Salir");
	TextDrawBackgroundColor(furnitureOther[3], 255);
	TextDrawFont(furnitureOther[3], 2);
	TextDrawLetterSize(furnitureOther[3], 0.320000, 1.899999);
	TextDrawColor(furnitureOther[3], -1);
	TextDrawSetOutline(furnitureOther[3], 0);
	TextDrawSetProportional(furnitureOther[3], 1);
	TextDrawSetShadow(furnitureOther[3], 0);
	TextDrawUseBox(furnitureOther[3], 1);
	TextDrawBoxColor(furnitureOther[3], 0);
	TextDrawTextSize(furnitureOther[3], 376.000000, 20.000000);
	TextDrawSetSelectable(furnitureOther[3], 1);
	
	//Bot�n de compra para el hogar
	furnitureOther[4] = TextDrawCreate(265.000000, 404.000000, "Comprar");
	TextDrawBackgroundColor(furnitureOther[4], 255);
	TextDrawFont(furnitureOther[4], 2);
	TextDrawLetterSize(furnitureOther[4], 0.320000, 1.899999);
	TextDrawColor(furnitureOther[4], -1);
	TextDrawSetOutline(furnitureOther[4], 0);
	TextDrawSetProportional(furnitureOther[4], 1);
	TextDrawSetShadow(furnitureOther[4], 0);
	TextDrawUseBox(furnitureOther[4], 1);
	TextDrawBoxColor(furnitureOther[4], 0);
	TextDrawTextSize(furnitureOther[4], 310.000000, 20.000000);
	TextDrawSetSelectable(furnitureOther[4], 1);
	
	//Puesta a punto
	car_tuning[0] = TextDrawCreate(234.000000, 403.000000, "ld_beat:left");
	TextDrawBackgroundColor(car_tuning[0], 255);
	TextDrawFont(car_tuning[0], 4);
	TextDrawLetterSize(car_tuning[0], 0.500000, 1.000000);
	TextDrawColor(car_tuning[0], 0x1E999930);
	TextDrawSetOutline(car_tuning[0], 0);
	TextDrawSetProportional(car_tuning[0], 1);
	TextDrawSetShadow(car_tuning[0], 1);
	TextDrawUseBox(car_tuning[0], 1);
	TextDrawBoxColor(car_tuning[0], 255);
	TextDrawTextSize(car_tuning[0], 21.000000, 22.000000);
	TextDrawSetSelectable(car_tuning[0], 1);
	
	car_tuning[1] = TextDrawCreate(386.000000, 403.000000, "ld_beat:right");
	TextDrawBackgroundColor(car_tuning[1], 255);
	TextDrawFont(car_tuning[1], 4);
	TextDrawLetterSize(car_tuning[1], 0.500000, 1.000000);
	TextDrawColor(car_tuning[1], 0x1E999930);
	TextDrawSetOutline(car_tuning[1], 0);
	TextDrawSetProportional(car_tuning[1], 1);
	TextDrawSetShadow(car_tuning[1], 1);
	TextDrawUseBox(car_tuning[1], 1);
	TextDrawBoxColor(car_tuning[1], 255);
	TextDrawTextSize(car_tuning[1], 21.000000, 22.000000);
	TextDrawSetSelectable(car_tuning[1], 1);
	
	car_tuning[2] = TextDrawCreate(258.000000, 404.000000, "ajuste");
	TextDrawBackgroundColor(car_tuning[2], 255);
	TextDrawFont(car_tuning[2], 2);
	TextDrawLetterSize(car_tuning[2], 0.320000, 1.899999);
	TextDrawColor(car_tuning[2], -1);
	TextDrawSetOutline(car_tuning[2], 0);
	TextDrawSetProportional(car_tuning[2], 1);
	TextDrawSetShadow(car_tuning[2], 0);
	TextDrawUseBox(car_tuning[2], 1);
	TextDrawBoxColor(car_tuning[2], 0);
	TextDrawTextSize(car_tuning[2], 310.000000, 20.000000);
	TextDrawSetSelectable(car_tuning[2], 1);
	
	car_tuning[3] = TextDrawCreate(337.000000, 404.000000, "atras");
	TextDrawBackgroundColor(car_tuning[3], 255);
	TextDrawFont(car_tuning[3], 2);
	TextDrawLetterSize(car_tuning[3], 0.320000, 1.899999);
	TextDrawColor(car_tuning[3], -1);
	TextDrawSetOutline(car_tuning[3], 0);
	TextDrawSetProportional(car_tuning[3], 1);
	TextDrawSetShadow(car_tuning[3], 0);
	TextDrawUseBox(car_tuning[3], 1);
	TextDrawBoxColor(car_tuning[3], 0);
	TextDrawTextSize(car_tuning[3], 376.000000, 20.000000);
	TextDrawSetSelectable(car_tuning[3], 1);
	
	car_tuning[4] = TextDrawCreate(320.000000, 380.000000, "_");
	TextDrawAlignment(car_tuning[4], 2);
	TextDrawBackgroundColor(car_tuning[4], 255);
	TextDrawFont(car_tuning[4], 1);
	TextDrawLetterSize(car_tuning[4], 30.500000, 2.100000);
	TextDrawColor(car_tuning[4], -1);
	TextDrawSetOutline(car_tuning[4], 0);
	TextDrawSetProportional(car_tuning[4], 1);
	TextDrawSetShadow(car_tuning[4], 0);
	TextDrawUseBox(car_tuning[4], 1);
	TextDrawBoxColor(car_tuning[4], 0x1E999930);
	TextDrawTextSize(car_tuning[4], 0.000000, -179.000000);
	TextDrawSetSelectable(car_tuning[4], 0);
	
	car_tuning[5] = TextDrawCreate(320.000000, 356.000000, "_");
	TextDrawAlignment(car_tuning[5], 2);
	TextDrawBackgroundColor(car_tuning[5], 255);
	TextDrawFont(car_tuning[5], 1);
	TextDrawLetterSize(car_tuning[5], 30.500000, 2.100000);
	TextDrawColor(car_tuning[5], -1);
	TextDrawSetOutline(car_tuning[5], 0);
	TextDrawSetProportional(car_tuning[5], 1);
	TextDrawSetShadow(car_tuning[5], 0);
	TextDrawUseBox(car_tuning[5], 1);
	TextDrawBoxColor(car_tuning[5], 0x1E999930);
	TextDrawTextSize(car_tuning[5], 0.000000, -179.000000);
	TextDrawSetSelectable(car_tuning[5], 0);
	
	car_tuning[6] = TextDrawCreate(320.000000, 404.000000, "_");
	TextDrawLetterSize(car_tuning[6], 0.500000, 2.100000);
	TextDrawTextSize(car_tuning[6], 0.000000, -179.000000);
	TextDrawAlignment(car_tuning[6], 2);
	TextDrawColor(car_tuning[6], -1);
	TextDrawUseBox(car_tuning[6], 1);
	TextDrawBoxColor(car_tuning[6], 0x1E999930);
	TextDrawSetShadow(car_tuning[6], 0);
	TextDrawSetOutline(car_tuning[6], 0);
	TextDrawBackgroundColor(car_tuning[6], 255);
	TextDrawFont(car_tuning[6], 1);
	TextDrawSetProportional(furnitureBuy[0], 1);
	
	car_tuning[7] = TextDrawCreate(260.000000, 404.000000, "pintando");
	TextDrawBackgroundColor(car_tuning[7], 255);
	TextDrawFont(car_tuning[7], 2);
	TextDrawLetterSize(car_tuning[7], 0.320000, 1.899999);
	TextDrawColor(car_tuning[7], -1);
	TextDrawSetOutline(car_tuning[7], 0);
	TextDrawSetProportional(car_tuning[7], 1);
	TextDrawSetShadow(car_tuning[7], 0);
	TextDrawUseBox(car_tuning[7], 1);
	TextDrawBoxColor(car_tuning[7], 0);
	TextDrawTextSize(car_tuning[7], 310.000000, 20.000000);
	TextDrawSetSelectable(car_tuning[7], 1);
	
	car_tuning[8] = TextDrawCreate(336.000000, 404.000000, "atras");
	TextDrawBackgroundColor(car_tuning[8], 255);
	TextDrawFont(car_tuning[8], 2);
	TextDrawLetterSize(car_tuning[8], 0.320000, 1.899999);
	TextDrawColor(car_tuning[8], -1);
	TextDrawSetOutline(car_tuning[8], 0);
	TextDrawSetProportional(car_tuning[8], 1);
	TextDrawSetShadow(car_tuning[8], 0);
	TextDrawUseBox(car_tuning[8], 1);
	TextDrawBoxColor(car_tuning[8], 0);
	TextDrawTextSize(car_tuning[8], 376.000000, 20.000000);
	TextDrawSetSelectable(car_tuning[8], 1);
	
	carshop[0] = TextDrawCreate(320.000000, 380.000000, "_");
	TextDrawAlignment(carshop[0], 2);
	TextDrawBackgroundColor(carshop[0], 255);
	TextDrawFont(carshop[0], 1);
	TextDrawLetterSize(carshop[0], 30.500000, 2.100000);
	TextDrawColor(carshop[0], -1);
	TextDrawSetOutline(carshop[0], 0);
	TextDrawSetProportional(carshop[0], 1);
	TextDrawSetShadow(carshop[0], 0);
	TextDrawUseBox(carshop[0], 1);
	TextDrawBoxColor(carshop[0], 0x1E999930);
	TextDrawTextSize(carshop[0], 0.000000, -179.000000);
	TextDrawSetSelectable(carshop[0], 0);
	
	carshop[1] = TextDrawCreate(320.000000, 356.000000, "_");
	TextDrawAlignment(carshop[1], 2);
	TextDrawBackgroundColor(carshop[1], 255);
	TextDrawFont(carshop[1], 1);
	TextDrawLetterSize(carshop[1], 30.500000, 2.100000);
	TextDrawColor(carshop[1], -1);
	TextDrawSetOutline(carshop[1], 0);
	TextDrawSetProportional(carshop[1], 1);
	TextDrawSetShadow(carshop[1], 0);
	TextDrawUseBox(carshop[1], 1);
	TextDrawBoxColor(carshop[1], 0x1E999930);
	TextDrawTextSize(carshop[1], 0.000000, -179.000000);
	TextDrawSetSelectable(carshop[1], 0);
	
	carshop[2] = TextDrawCreate(320.000000, 404.000000, "_");
	TextDrawLetterSize(carshop[2], 0.500000, 2.100000);
	TextDrawTextSize(carshop[2], 0.000000, -179.000000);
	TextDrawAlignment(carshop[2], 2);
	TextDrawColor(carshop[2], -1);
	TextDrawUseBox(carshop[2], 1);
	TextDrawBoxColor(carshop[2], 0x1E999930);
	TextDrawSetShadow(carshop[2], 0);
	TextDrawSetOutline(carshop[2], 0);
	TextDrawBackgroundColor(carshop[2], 255);
	TextDrawFont(carshop[2], 1);
	TextDrawSetProportional(carshop[2], 1);
	
	carshop[3] = TextDrawCreate(234.000000, 403.000000, "ld_beat:left");
	TextDrawBackgroundColor(carshop[3], 255);
	TextDrawFont(carshop[3], 4);
	TextDrawLetterSize(carshop[3], 0.500000, 1.000000);
	TextDrawColor(carshop[3], 0x1E999930);
	TextDrawSetOutline(carshop[3], 0);
	TextDrawSetProportional(carshop[3], 1);
	TextDrawSetShadow(carshop[3], 1);
	TextDrawUseBox(carshop[3], 1);
	TextDrawBoxColor(carshop[3], 255);
	TextDrawTextSize(carshop[3], 21.000000, 22.000000);
	TextDrawSetSelectable(carshop[3], 1);
	
	carshop[4] = TextDrawCreate(386.000000, 403.000000, "ld_beat:right");
	TextDrawBackgroundColor(carshop[4], 255);
	TextDrawFont(carshop[4], 4);
	TextDrawLetterSize(carshop[4], 0.500000, 1.000000);
	TextDrawColor(carshop[4], 0x1E999930);
	TextDrawSetOutline(carshop[4], 0);
	TextDrawSetProportional(carshop[4], 1);
	TextDrawSetShadow(carshop[4], 1);
	TextDrawUseBox(carshop[4], 1);
	TextDrawBoxColor(carshop[4], 255);
	TextDrawTextSize(carshop[4], 21.000000, 22.000000);
	TextDrawSetSelectable(carshop[4], 1);
	
	carshop[5] = TextDrawCreate(265.000000, 404.000000, "Comprar");
	TextDrawBackgroundColor(carshop[5], 255);
	TextDrawFont(carshop[5], 2);
	TextDrawLetterSize(carshop[5], 0.320000, 1.899999);
	TextDrawColor(carshop[5], -1);
	TextDrawSetOutline(carshop[5], 0);
	TextDrawSetProportional(carshop[5], 1);
	TextDrawSetShadow(carshop[5], 0);
	TextDrawUseBox(carshop[5], 1);
	TextDrawBoxColor(carshop[5], 0);
	TextDrawTextSize(carshop[5], 310.000000, 20.000000);
	TextDrawSetSelectable(carshop[5], 1);
	
	carshop[6] = TextDrawCreate(335.000000, 404.000000, "Salir");
	TextDrawBackgroundColor(carshop[6], 255);
	TextDrawFont(carshop[6], 2);
	TextDrawLetterSize(carshop[6], 0.320000, 1.899999);
	TextDrawColor(carshop[6], -1);
	TextDrawSetOutline(carshop[6], 0);
	TextDrawSetProportional(carshop[6], 1);
	TextDrawSetShadow(carshop[6], 0);
	TextDrawUseBox(carshop[6], 1);
	TextDrawBoxColor(carshop[6], 0);
	TextDrawTextSize(carshop[6], 376.000000, 20.000000);
	TextDrawSetSelectable(carshop[6], 1);
	
	
	/* - TD: La elecci�n de la piel en el registro. - */
	chooseSkin[1] = TextDrawCreate(320.000000, 390.000000, "_");
	TextDrawLetterSize(chooseSkin[1], 0.500000, 2.099999);
	TextDrawTextSize(chooseSkin[1], -179.000000, 150.000000);
	TextDrawAlignment(chooseSkin[1], 2);
	TextDrawColor(chooseSkin[1], -1);
	TextDrawUseBox(chooseSkin[1], 1);
	TextDrawBoxColor(chooseSkin[1], 0x1E999920);
	TextDrawSetShadow(chooseSkin[1], 0);
	TextDrawSetOutline(chooseSkin[1], 0);
	TextDrawBackgroundColor(chooseSkin[1], 255);
	TextDrawFont(chooseSkin[1], 1);
	TextDrawSetProportional(chooseSkin[1], 1);
	TextDrawSetShadow(chooseSkin[1], 0);
	
	chooseSkin[0] = TextDrawCreate(239.499908, 384.767364, ""); //De vuelta
	TextDrawLetterSize(chooseSkin[0], 0.000000, 0.000000);
	TextDrawTextSize(chooseSkin[0], 30.000000, 30.000000);
	TextDrawAlignment(chooseSkin[0], 1);
	TextDrawColor(chooseSkin[0], 204624);
	TextDrawSetShadow(chooseSkin[0], 0);
	TextDrawSetOutline(chooseSkin[0], 0);
	TextDrawBackgroundColor(chooseSkin[0], 0);
	TextDrawFont(chooseSkin[0], 5);
	TextDrawSetProportional(chooseSkin[0], 0);
	TextDrawSetShadow(chooseSkin[0], 0);
	TextDrawSetSelectable(chooseSkin[0], true);
	TextDrawSetPreviewModel(chooseSkin[0], 19134);
	TextDrawSetPreviewRot(chooseSkin[0], 0.000000, 90.000000, 90.000000, 0.85);

	chooseSkin[2] = TextDrawCreate(370.307891, 384.767364, ""); //Hacia adelante
	TextDrawLetterSize(chooseSkin[2], 0.000000, 0.000000);
	TextDrawTextSize(chooseSkin[2], 30.000000, 30.000000);
	TextDrawAlignment(chooseSkin[2], 1);
	TextDrawColor(chooseSkin[2], 204624);
	TextDrawSetShadow(chooseSkin[2], 0);
	TextDrawSetOutline(chooseSkin[2], 0);
	TextDrawBackgroundColor(chooseSkin[2], 0);
	TextDrawFont(chooseSkin[2], 5);
	TextDrawSetProportional(chooseSkin[2], 0);
	TextDrawSetShadow(chooseSkin[2], 0);
	TextDrawSetSelectable(chooseSkin[2], true);
	TextDrawSetPreviewModel(chooseSkin[2], 19134);
	TextDrawSetPreviewRot(chooseSkin[2], 0.000000, -90.000000, 90.000000, 0.85);

	chooseSkin[3] = TextDrawCreate(319.500091, 389.772277, "elegir");
	TextDrawLetterSize(chooseSkin[3], 0.319999, 1.899999);
	TextDrawTextSize(chooseSkin[3], 20.000000, 50.000000);
	TextDrawAlignment(chooseSkin[3], 2);
	TextDrawColor(chooseSkin[3], -1);
	TextDrawSetShadow(chooseSkin[3], 0);
	TextDrawSetOutline(chooseSkin[3], 0);
	TextDrawBackgroundColor(chooseSkin[3], 255);
	TextDrawFont(chooseSkin[3], 2);
	TextDrawSetProportional(chooseSkin[3], 1);
	TextDrawSetShadow(chooseSkin[3], 0);
	TextDrawSetSelectable(chooseSkin[3], 1);
	
	blind_background = TextDrawCreate(641.199951, 1.500000, "usebox");
	TextDrawLetterSize( blind_background, 0.000000, 49.378147);
	TextDrawTextSize( blind_background, -2.000000, 0.000000);
	TextDrawAlignment( blind_background, 3);
	TextDrawColor( blind_background, -1);
	TextDrawUseBox( blind_background, true);
	TextDrawBoxColor( blind_background, 255);
	TextDrawSetShadow( blind_background, 0);
	TextDrawSetOutline( blind_background, 0);
	TextDrawBackgroundColor( blind_background, 255);
	TextDrawFont( blind_background, 1);
	
	return 1;
}

TextDraws_OnPlayerConnect( playerid )
{
	// New
	/* - TD: Radio(slot,channel,radio info) - */
	radio[0][playerid] = CreatePlayerTextDraw(playerid, 530.470153, 105.999961, "radio");
	PlayerTextDrawLetterSize(playerid, radio[0][playerid], 0.254117, 1.191666);
	PlayerTextDrawAlignment(playerid, radio[0][playerid], 2);
	PlayerTextDrawColor(playerid, radio[0][playerid], td_cBLUE);
	PlayerTextDrawSetShadow(playerid, radio[0][playerid], 0);
	PlayerTextDrawSetOutline(playerid, radio[0][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, radio[0][playerid], 255);
	PlayerTextDrawFont(playerid, radio[0][playerid], 2);
	PlayerTextDrawSetProportional(playerid, radio[0][playerid], 1);
	PlayerTextDrawSetShadow(playerid, radio[0][playerid], 0);

	radio[1][playerid] = CreatePlayerTextDraw(playerid, 502.705841, 118.833343, "freq: 0");
	PlayerTextDrawLetterSize(playerid, radio[1][playerid], 0.167529, 0.929166);
	PlayerTextDrawAlignment(playerid, radio[1][playerid], 1);
	PlayerTextDrawColor(playerid, radio[1][playerid], td_cWHITE);
	PlayerTextDrawSetShadow(playerid, radio[1][playerid], 0);
	PlayerTextDrawSetOutline(playerid, radio[1][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, radio[1][playerid], 255);
	PlayerTextDrawFont(playerid, radio[1][playerid], 2);
	PlayerTextDrawSetProportional(playerid, radio[1][playerid], 1);
	PlayerTextDrawSetShadow(playerid, radio[1][playerid], 0);

	radio[2][playerid] = CreatePlayerTextDraw(playerid, 502.705841, 128.750030, "canal: 12");
	PlayerTextDrawLetterSize(playerid, radio[2][playerid], 0.167529, 0.929166);
	PlayerTextDrawAlignment(playerid, radio[2][playerid], 1);
	PlayerTextDrawColor(playerid, radio[2][playerid], td_cWHITE);
	PlayerTextDrawSetShadow(playerid, radio[2][playerid], 0);
	PlayerTextDrawSetOutline(playerid, radio[2][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, radio[2][playerid], 255);
	PlayerTextDrawFont(playerid, radio[2][playerid], 2);
	PlayerTextDrawSetProportional(playerid, radio[2][playerid], 1);
	PlayerTextDrawSetShadow(playerid, radio[2][playerid], 0);
	/* - TD: Radio(slot,channel,radio info) - End */
	
	// Veloc�metro
	
	Speed[0][playerid] = CreatePlayerTextDraw(playerid,289.823303, 405.850128, "ENG");
	PlayerTextDrawLetterSize(playerid,Speed[0][playerid], 0.185881, 1.168331);
	PlayerTextDrawAlignment(playerid,Speed[0][playerid], 1);
	PlayerTextDrawColor(playerid,Speed[0][playerid], -1);
	PlayerTextDrawSetShadow(playerid,Speed[0][playerid], 0);
	PlayerTextDrawSetOutline(playerid,Speed[0][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,Speed[0][playerid], 255);
	PlayerTextDrawFont(playerid,Speed[0][playerid], 2);
	PlayerTextDrawSetProportional(playerid,Speed[0][playerid], 1);
	
	Speed[1][playerid] = CreatePlayerTextDraw(playerid,350.017425, 405.866821, "LIMIT");
	PlayerTextDrawLetterSize(playerid,Speed[1][playerid], 0.185881, 1.168331);
	PlayerTextDrawAlignment(playerid,Speed[1][playerid], 1);
	PlayerTextDrawColor(playerid,Speed[1][playerid], -1);
	PlayerTextDrawSetShadow(playerid,Speed[1][playerid], 0);
	PlayerTextDrawSetOutline(playerid,Speed[1][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,Speed[1][playerid], 255);
	PlayerTextDrawFont(playerid,Speed[1][playerid], 2);
	PlayerTextDrawSetProportional(playerid,Speed[1][playerid], 1);
	
	Speed[2][playerid] = CreatePlayerTextDraw(playerid,264.240997, 406.150177, "LIGHT");
	PlayerTextDrawLetterSize(playerid,Speed[2][playerid], 0.185881, 1.168331);
	PlayerTextDrawAlignment(playerid,Speed[2][playerid], 1);
	PlayerTextDrawColor(playerid,Speed[2][playerid], -1);
	PlayerTextDrawSetShadow(playerid,Speed[2][playerid], 0);
	PlayerTextDrawSetOutline(playerid,Speed[2][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,Speed[2][playerid], 255);
	PlayerTextDrawFont(playerid,Speed[2][playerid], 2);
	PlayerTextDrawSetProportional(playerid,Speed[2][playerid], 1);
	
	Speed[3][playerid] = CreatePlayerTextDraw(playerid,317.829101, 405.850128, "LOCK");
	PlayerTextDrawLetterSize(playerid,Speed[3][playerid], 0.185881, 1.168331);
	PlayerTextDrawAlignment(playerid,Speed[3][playerid], 1);
	PlayerTextDrawColor(playerid,Speed[3][playerid], -1);
	PlayerTextDrawSetShadow(playerid,Speed[3][playerid], 0);
	PlayerTextDrawSetOutline(playerid,Speed[3][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,Speed[3][playerid], 255);
	PlayerTextDrawFont(playerid,Speed[3][playerid], 2);
	PlayerTextDrawSetProportional(playerid,Speed[3][playerid], 1);
	
	Speed[4][playerid] = CreatePlayerTextDraw(playerid,326.505889, 388.333465, "MILE: 1000.0");
	PlayerTextDrawLetterSize(playerid,Speed[4][playerid], 0.177882, 1.156666);
	PlayerTextDrawAlignment(playerid,Speed[4][playerid], 1);
	PlayerTextDrawColor(playerid,Speed[4][playerid], -1);
	PlayerTextDrawSetShadow(playerid,Speed[4][playerid], 0);
	PlayerTextDrawSetOutline(playerid,Speed[4][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,Speed[4][playerid], 255);
	PlayerTextDrawFont(playerid,Speed[4][playerid], 2);
	PlayerTextDrawSetProportional(playerid,Speed[4][playerid], 1);
	
	Speed[5][playerid] = CreatePlayerTextDraw(playerid,297.888214, 376.133270, "100");
	PlayerTextDrawLetterSize(playerid,Speed[5][playerid], 0.450352, 2.603336);
	PlayerTextDrawAlignment(playerid,Speed[5][playerid], 3);
	PlayerTextDrawColor(playerid,Speed[5][playerid], -1);
	PlayerTextDrawSetShadow(playerid,Speed[5][playerid], 0);
	PlayerTextDrawSetOutline(playerid,Speed[5][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,Speed[5][playerid], 255);
	PlayerTextDrawFont(playerid,Speed[5][playerid], 2);
	PlayerTextDrawSetProportional(playerid,Speed[5][playerid], 1);
	
	Speed[6][playerid] = CreatePlayerTextDraw(playerid,326.705780, 377.833404, "FUEL: 100.0");
	PlayerTextDrawLetterSize(playerid,Speed[6][playerid], 0.177882, 1.156666);
	PlayerTextDrawAlignment(playerid,Speed[6][playerid], 1);
	PlayerTextDrawColor(playerid,Speed[6][playerid], -1);
	PlayerTextDrawSetShadow(playerid,Speed[6][playerid], 0);
	PlayerTextDrawSetOutline(playerid,Speed[6][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,Speed[6][playerid], 255);
	PlayerTextDrawFont(playerid,Speed[6][playerid], 2);
	PlayerTextDrawSetProportional(playerid,Speed[6][playerid], 1);
	
	//Tax�metro
	Taximeter[playerid] = CreatePlayerTextDraw(playerid, 286.058532, 426.405700, "taximeter: $0");
	PlayerTextDrawLetterSize(playerid, Taximeter[playerid], 0.185881, 1.168331);
	PlayerTextDrawAlignment(playerid, Taximeter[playerid], 1);
	PlayerTextDrawColor(playerid, Taximeter[playerid], -1);
	PlayerTextDrawSetShadow(playerid, Taximeter[playerid], 0);
	PlayerTextDrawSetOutline(playerid, Taximeter[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, Taximeter[playerid], 255);
	PlayerTextDrawFont(playerid, Taximeter[playerid], 2);
	PlayerTextDrawSetProportional(playerid, Taximeter[playerid], 1);
	
	//Camionero
	Trucker[playerid] = CreatePlayerTextDraw(playerid, 276.058532, 426.405700, "none");
	PlayerTextDrawLetterSize(playerid, Trucker[playerid], 0.185881, 1.168331);
	PlayerTextDrawAlignment(playerid, Trucker[playerid], 1);
	PlayerTextDrawColor(playerid, Trucker[playerid], -1);
	PlayerTextDrawSetShadow(playerid, Trucker[playerid], 0);
	PlayerTextDrawSetOutline(playerid, Trucker[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, Trucker[playerid], 255);
	PlayerTextDrawFont(playerid, Trucker[playerid], 2);
	PlayerTextDrawSetProportional(playerid, Trucker[playerid], 1);
	
	//Hombre de negocios
	Drivebus[playerid] = CreatePlayerTextDraw(playerid, 292.058532, 426.405700, "none");
	PlayerTextDrawLetterSize(playerid, Drivebus[playerid], 0.185881, 1.168331);
	PlayerTextDrawAlignment(playerid, Drivebus[playerid], 1);
	PlayerTextDrawColor(playerid, Drivebus[playerid], -1);
	PlayerTextDrawSetShadow(playerid, Drivebus[playerid], 0);
	PlayerTextDrawSetOutline(playerid, Drivebus[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, Drivebus[playerid], 255);
	PlayerTextDrawFont(playerid, Drivebus[playerid], 2);
	PlayerTextDrawSetProportional(playerid, Drivebus[playerid], 1);
	
	/*NewSkinSelect[0][playerid] = CreatePlayerTextDraw(playerid, 139.000000, 146.429595, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[0][playerid], 0.095666, 3.364151);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[0][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[0][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[0][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[0][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[0][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[0][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[0][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[0][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[0][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[0][playerid], true);

	NewSkinSelect[1][playerid] = CreatePlayerTextDraw(playerid, 203.666671, 146.600006, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[1][playerid], 0.060333, 0.647111);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[1][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[1][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[1][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[1][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[1][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[1][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[1][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[1][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[1][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[1][playerid], true);

	NewSkinSelect[2][playerid] = CreatePlayerTextDraw(playerid, 268.666473, 146.770355, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[2][playerid], 0.060666, 0.672000);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[2][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[2][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[2][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[2][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[2][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[2][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[2][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[2][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[2][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[2][playerid], true);

	NewSkinSelect[3][playerid] = CreatePlayerTextDraw(playerid,332.999816, 146.940734, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[3][playerid], 0.059333, 0.684444);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[3][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[3][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[3][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[3][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[3][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[3][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[3][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[3][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[3][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[3][playerid], true);

	NewSkinSelect[4][playerid] = CreatePlayerTextDraw(playerid,397.333129, 146.696365, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[4][playerid], 0.123999, 0.754962);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[4][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[4][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[4][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[4][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[4][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[4][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[4][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[4][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[4][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[4][playerid], true);

	NewSkinSelect[5][playerid] = CreatePlayerTextDraw(playerid,461.999725, 146.866729, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[5][playerid], 0.066999, 1.335703);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[5][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[5][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[5][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[5][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[5][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[5][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[5][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[5][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[5][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[5][playerid], true);

	NewSkinSelect[6][playerid] = CreatePlayerTextDraw(playerid,139.000000, 221.681442, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[6][playerid], 0.155333, 4.048594);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[6][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[6][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[6][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[6][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[6][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[6][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[6][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[6][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[6][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[6][playerid], true);

	NewSkinSelect[7][playerid] = CreatePlayerTextDraw(playerid,204.000045, 221.851806, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[7][playerid], 0.158666, 4.023706);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[7][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[7][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[7][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[7][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[7][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[7][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[7][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[7][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[7][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[7][playerid], true);

	NewSkinSelect[8][playerid] = CreatePlayerTextDraw(playerid,269.333374, 221.607376, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[8][playerid], 0.158999, 4.011263);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[8][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[8][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[8][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[8][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[8][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[8][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[8][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[8][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[8][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[8][playerid], true);

	NewSkinSelect[9][playerid] = CreatePlayerTextDraw(playerid,334.666625, 221.777725, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[9][playerid], 0.157999, 4.048594);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[9][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[9][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[9][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[9][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[9][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[9][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[9][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[9][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[9][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[9][playerid], true);

	NewSkinSelect[10][playerid] = CreatePlayerTextDraw(playerid,400.333312, 221.948089, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[10][playerid], 0.161333, 4.023707);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[10][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[10][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[10][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[10][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[10][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[10][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[10][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[10][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[10][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[10][playerid], true);

	NewSkinSelect[11][playerid] = CreatePlayerTextDraw(playerid,465.333190, 221.703628, "PreviewModel");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[11][playerid], 0.159999, 4.048596);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[11][playerid], 61.666648, 69.274085);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[11][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[11][playerid], -1);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[11][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[11][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[11][playerid], -707406934);
	PlayerTextDrawFont(playerid,NewSkinSelect[11][playerid], 5);
	PlayerTextDrawSetPreviewModel(playerid,NewSkinSelect[11][playerid], 0);
	PlayerTextDrawSetPreviewRot(playerid,NewSkinSelect[11][playerid], 0.000000, 0.000000, 0.000000, 1.000000);
	PlayerTextDrawSetSelectable(playerid,NewSkinSelect[11][playerid], true);

	NewSkinSelect[12][playerid] = CreatePlayerTextDraw(playerid,535.666625, 144.611114, "usebox");
	PlayerTextDrawLetterSize(playerid,NewSkinSelect[12][playerid], 0.000000, 16.829011);
	PlayerTextDrawTextSize(playerid,NewSkinSelect[12][playerid], 131.333328, 0.000000);
	PlayerTextDrawAlignment(playerid,NewSkinSelect[12][playerid], 1);
	PlayerTextDrawColor(playerid,NewSkinSelect[12][playerid], 0);
	PlayerTextDrawUseBox(playerid,NewSkinSelect[12][playerid], true);
	PlayerTextDrawBoxColor(playerid,NewSkinSelect[12][playerid], 102);
	PlayerTextDrawSetShadow(playerid,NewSkinSelect[12][playerid], 0);
	PlayerTextDrawSetOutline(playerid,NewSkinSelect[12][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid,NewSkinSelect[12][playerid], td_cBLUE);
	PlayerTextDrawFont(playerid,NewSkinSelect[12][playerid], 0);*/
	
	menuPlayer[0][playerid] = CreatePlayerTextDraw(playerid,84.000000, 154.000000, "none");PlayerTextDrawBackgroundColor(playerid,menuPlayer[0][playerid], 255);PlayerTextDrawFont(playerid,menuPlayer[0][playerid], 2);PlayerTextDrawLetterSize(playerid,menuPlayer[0][playerid], 0.200000, 1.200000);PlayerTextDrawColor(playerid,menuPlayer[0][playerid], -1);
	PlayerTextDrawSetOutline(playerid,menuPlayer[0][playerid], 0);PlayerTextDrawSetProportional(playerid,menuPlayer[0][playerid], 1);PlayerTextDrawSetShadow(playerid,menuPlayer[0][playerid], 0);PlayerTextDrawSetSelectable(playerid,menuPlayer[0][playerid], 0);
	menuPlayer[1][playerid] = CreatePlayerTextDraw(playerid,256.000000, 154.000000, "none");PlayerTextDrawAlignment(playerid,menuPlayer[1][playerid], 3);PlayerTextDrawBackgroundColor(playerid,menuPlayer[1][playerid], 255);PlayerTextDrawFont(playerid,menuPlayer[1][playerid], 2);PlayerTextDrawLetterSize(playerid,menuPlayer[1][playerid], 0.200000, 1.200000);
	PlayerTextDrawColor(playerid,menuPlayer[1][playerid], -1);PlayerTextDrawSetOutline(playerid,menuPlayer[1][playerid], 0);PlayerTextDrawSetProportional(playerid,menuPlayer[1][playerid], 1);PlayerTextDrawSetShadow(playerid,menuPlayer[1][playerid], 0);PlayerTextDrawSetSelectable(playerid,menuPlayer[1][playerid], 0);
	menuPlayer[2][playerid] = CreatePlayerTextDraw(playerid,172.000000, 135.000000, "nick_names");PlayerTextDrawAlignment(playerid,menuPlayer[2][playerid], 2);PlayerTextDrawBackgroundColor(playerid,menuPlayer[2][playerid], 255);PlayerTextDrawFont(playerid,menuPlayer[2][playerid], 2);PlayerTextDrawLetterSize(playerid,menuPlayer[2][playerid], 0.230000, 1.399999);
	PlayerTextDrawColor(playerid,menuPlayer[2][playerid], -1);PlayerTextDrawSetOutline(playerid,menuPlayer[2][playerid], 0);PlayerTextDrawSetProportional(playerid,menuPlayer[2][playerid], 1);PlayerTextDrawSetShadow(playerid,menuPlayer[2][playerid], 0);PlayerTextDrawSetSelectable(playerid,menuPlayer[2][playerid], 0);PlayerTextDrawSetShadow(playerid,menuPlayer[2][playerid], 0);PlayerTextDrawSetSelectable(playerid,menuPlayer[2][playerid], 0);PlayerTextDrawSetSelectable(playerid,menuPlayer[2][playerid], 0);
	
	/*
	HungerProgres[playerid] = CreatePlayerTextDraw(playerid,549.500000, 60.000000, "____");PlayerTextDrawBackgroundColor(playerid,HungerProgres[playerid], 255);PlayerTextDrawFont(playerid,HungerProgres[playerid], 1);PlayerTextDrawLetterSize(playerid,HungerProgres[playerid], 0.490000, -0.000000);
	PlayerTextDrawColor(playerid,HungerProgres[playerid], -1);PlayerTextDrawSetOutline(playerid,HungerProgres[playerid], 0);PlayerTextDrawSetProportional(playerid,HungerProgres[playerid], 1);PlayerTextDrawSetShadow(playerid,HungerProgres[playerid], 1);
	PlayerTextDrawUseBox(playerid,HungerProgres[playerid], 1);PlayerTextDrawBoxColor(playerid,HungerProgres[playerid], 0x559cd4AA);PlayerTextDrawTextSize(playerid,HungerProgres[playerid], 604.000000, 40.000000);PlayerTextDrawSetSelectable(playerid,HungerProgres[playerid], 0);*/
	
	furniturePrice[playerid] = CreatePlayerTextDraw(playerid,321.000000, 379.000000, "price");
	PlayerTextDrawAlignment(playerid,furniturePrice[playerid], 2);
	PlayerTextDrawBackgroundColor(playerid,furniturePrice[playerid], 255);
	PlayerTextDrawFont(playerid,furniturePrice[playerid], 2);
	PlayerTextDrawLetterSize(playerid,furniturePrice[playerid], 0.290000, 2.000000);
	PlayerTextDrawColor(playerid,furniturePrice[playerid], -1);
	PlayerTextDrawSetOutline(playerid,furniturePrice[playerid], 0);
	PlayerTextDrawSetProportional(playerid,furniturePrice[playerid], 1);
	PlayerTextDrawSetShadow(playerid,furniturePrice[playerid], 0);
	PlayerTextDrawSetSelectable(playerid,furniturePrice[playerid], 0);
	
	furnitureName[playerid] = CreatePlayerTextDraw(playerid,321.000000, 359.000000, "n");
	PlayerTextDrawAlignment(playerid,furnitureName[playerid], 2);
	PlayerTextDrawBackgroundColor(playerid,furnitureName[playerid], 255);
	PlayerTextDrawFont(playerid,furnitureName[playerid], 2);
	PlayerTextDrawLetterSize(playerid,furnitureName[playerid], 0.290000, 2.000000);
	PlayerTextDrawColor(playerid,furnitureName[playerid], -1);
	PlayerTextDrawSetOutline(playerid,furnitureName[playerid], 0);
	PlayerTextDrawSetProportional(playerid,furnitureName[playerid], 1);
	PlayerTextDrawSetShadow(playerid,furnitureName[playerid], 0);
	PlayerTextDrawSetSelectable(playerid,furnitureName[playerid], 0);
	
	//Nombre de Tuning - Precio
	tuning_price[playerid] = CreatePlayerTextDraw(playerid,321.000000, 379.000000, "price");
	PlayerTextDrawAlignment(playerid,tuning_price[playerid], 2);
	PlayerTextDrawBackgroundColor(playerid,tuning_price[playerid], 255);
	PlayerTextDrawFont(playerid,tuning_price[playerid], 2);
	PlayerTextDrawLetterSize(playerid,tuning_price[playerid], 0.290000, 2.000000);
	PlayerTextDrawColor(playerid,tuning_price[playerid], -1);
	PlayerTextDrawSetOutline(playerid,tuning_price[playerid], 0);
	PlayerTextDrawSetProportional(playerid,tuning_price[playerid], 1);
	PlayerTextDrawSetShadow(playerid,tuning_price[playerid], 0);
	PlayerTextDrawSetSelectable(playerid,tuning_price[playerid], 0);
	
	tuning_name[playerid] = CreatePlayerTextDraw(playerid,321.000000, 359.000000, "n");
	PlayerTextDrawAlignment(playerid,tuning_name[playerid], 2);
	PlayerTextDrawBackgroundColor(playerid,tuning_name[playerid], 255);
	PlayerTextDrawFont(playerid,tuning_name[playerid], 2);
	PlayerTextDrawLetterSize(playerid,tuning_name[playerid], 0.290000, 2.000000);
	PlayerTextDrawColor(playerid,tuning_name[playerid], -1);
	PlayerTextDrawSetOutline(playerid,tuning_name[playerid], 0);
	PlayerTextDrawSetProportional(playerid,tuning_name[playerid], 1);
	PlayerTextDrawSetShadow(playerid,tuning_name[playerid], 0);
	PlayerTextDrawSetSelectable(playerid,tuning_name[playerid], 0);
	
	carshop_info[playerid] = CreatePlayerTextDraw( playerid, 380.123229, 80.916595, "_" );
	PlayerTextDrawLetterSize( playerid, carshop_info[playerid], 0.175058, 1.034166 );
	PlayerTextDrawAlignment( playerid, carshop_info[playerid], 1 );
	PlayerTextDrawColor( playerid, carshop_info[playerid], -1 );
	PlayerTextDrawSetShadow( playerid, carshop_info[playerid], 0 );
	PlayerTextDrawSetOutline( playerid, carshop_info[playerid], 0 );
	PlayerTextDrawBackgroundColor( playerid, carshop_info[playerid], 60 );
	PlayerTextDrawFont( playerid, carshop_info[playerid], 2 );
	PlayerTextDrawSetProportional( playerid, carshop_info[playerid], 1 );
	PlayerTextDrawSetShadow( playerid, carshop_info[playerid], 0 );
	
	//Informaci�n al cambiar el interior / textura.
	interior_info[playerid] = CreatePlayerTextDraw(playerid, 555.693359, 413.121826, "interior 19 $100000");
	PlayerTextDrawLetterSize(playerid, interior_info[playerid], 0.272000, 1.456888);
	PlayerTextDrawAlignment(playerid, interior_info[playerid], 2);
	PlayerTextDrawColor(playerid, interior_info[playerid], -1);
	PlayerTextDrawSetShadow(playerid, interior_info[playerid], 0);
	PlayerTextDrawSetOutline(playerid, interior_info[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, interior_info[playerid], 255);
	PlayerTextDrawFont(playerid, interior_info[playerid], 2);
	PlayerTextDrawSetProportional(playerid, interior_info[playerid], 1);
	PlayerTextDrawSetShadow(playerid, interior_info[playerid], 0);
	
	//Redacci�n de textos en prisi�n sobre c�maras de CCTV.
	TDPrisonCCTV[playerid] = CreatePlayerTextDraw( playerid, 20.0, 360.0, "~y~Camera ~w~#1~n~~y~Date: ~w~01.01.2016 01:55~n~~y~Location: ~w~Block ~n~");
	PlayerTextDrawLetterSize( playerid, TDPrisonCCTV[playerid], 0.8, 2.2 );
    PlayerTextDrawSetShadow( playerid, TDPrisonCCTV[playerid], 0);
    PlayerTextDrawUseBox( playerid, TDPrisonCCTV[playerid], 0 );
	PlayerTextDrawBoxColor( playerid, TDPrisonCCTV[playerid],0x00000055);
	PlayerTextDrawTextSize( playerid, TDPrisonCCTV[playerid], 380, 400);
	
	return 1;
}
