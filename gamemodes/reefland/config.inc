// -- Main
#define GGAME_VERSION						"Influx RP 2.0"
#define GGAME_VERSION_AC					"IAC 2.1"
#define GGAME_DESCRIPTION					"Juego de Rol"



// -- Ajustes

#define ANTICHEAT_MIN_ADMIN_LEVEL			( 1 )
#define ANTICHEAT_EXCEPTION_TIME			( 10 ) 
#define ANTICHEAT_EXCEPTION_CARSHOT			( 2 ) 

#define BENEFIT_VALUE						( 100 ) 
#define BENEFIT_NORANK						( 200 ) 

#define DELETE_CAR_VALUE					( 900 )

// -- Tablas
#define DB_USERS							"gb_users"
#define DB_ADMINS               			"gb_admins"
#define DB_PERMISSIONS          			"gb_permissions"
#define DB_SUPPORTS             			"gb_supports"
#define DB_ADMIN_LOGS           			"gb_admin_logs"
#define DB_BANS                 			"gb_bans"
#define DB_INVENTORY						"gb_inventory"
#define DB_IPBANS							"gb_ipbans"
#define DB_VEHICLES							"gb_vehicles"
#define DB_ITEMS							"gb_items"
#define DB_SESSIONS							"gb_sessions"
#define DB_FRACTIONS						"gb_fractions"
#define DB_ENTERS							"gb_enters"
#define DB_ENTERS_INSIDE					"gb_enters_inside"
#define DB_LICENSES							"gb_licenses"
#define DB_APPLICATION						"gb_licenses_apply"
#define DB_LOGS								"gb_logs"
#define DB_ANTICHEAT_LOGS					"gb_anticheat_logs"
#define DB_BUSINESS							"gb_business"
#define DB_BUSINESS_FURN					"gb_business_furn"
#define DB_HOUSE_FURN						"gb_house_furn"
#define DB_HOUSE							"gb_houses"
#define DB_BUSINESS_ITEMS					"gb_business_items"
#define DB_SERVER							"gb_server"
#define DB_BUSINESS_ORDERS					"gb_business_orders"
#define DB_ROUTES							"gb_routes"
#define DB_ROUTES_DESCRIPT					"gb_routes_descript"
#define DB_PHONES							"gb_phones"
#define DB_PHONES_CONTACTS					"gb_phones_contacts"
#define DB_PHONES_MESSAGES					"gb_phones_messages"
#define DB_RANKS							"gb_ranks"
#define DB_FRAC_INFO						"gb_frac_info"
#define DB_PENALTIES						"gb_penalties"
#define DB_VEHICLES_ARREST					"gb_vehicles_arrest"
#define DB_SAN								"gb_san"
#define DB_SUSPECT							"gb_suspect"
#define DB_SUSPECT_VEHICLE					"gb_suspect_vehicle"
#define DB_RECOURSE							"gb_recourse"
#define DB_DOCUMENT							"gb_document"
#define DB_PRISON							"gb_prison"
#define DB_CRIME							"gb_crime"
#define DB_CRIME_RANKS						"gb_crime_ranks"
#define DB_CRIME_GUNDEALER					"gb_crime_gundealer"
#define DB_CRIME_ORDER						"gb_crime_order"
#define DB_PREMIUM							"gb_premium"

// -- Colores
#define C_PURPLE							( 0xC2A2DAFF )
#define C_LIGHTRED          				( 0xFF6347AA )
#define C_LIGHTGREEN 						( 0x9ACD32AA )
#define C_YELLOW                			( 0xFFFF00AA )
#define C_GRAY 								( 0xAFAFAFAA )
#define C_DARKGRAY              			( 0x989898FF )
#define C_GREEN 							( 0x46BD38FF )
#define C_PINK 								( 0xE75480FF )
#define C_RED 								( 0xE05A5AFF )
#define C_DARKRED 							( 0xAA3333AA )
#define C_WHITE 							( 0xFFFFFFFF )
#define C_TAXI 								( 0xFFCC00FF )

#define C_BLUE                  			( 0x1E9999FF )
#define C_ORANGE                			( 0xFF9945FF )
#define C_LIGHTORANGE           			( 0xDEB047FF )
#define C_OPACITY_GRAY						( 0xD0D0D0AA )
#define C_LIGHTBLUE							( 0xCCDCFFAA )
#define C_LIGHTBLUE_TWO						( 0xC2CEEAAA )

#define COLOR_FADE1            				( 0xE6E6E6E6 )
#define COLOR_FADE2             			( 0xC8C8C8C8 )
#define COLOR_FADE3             			( 0xAAAAAAAA )
#define COLOR_FADE4             			( 0x8C8C8C8C )
#define COLOR_FADE5             			( 0x6E6E6E6E )
 
#define cPURPLE								"{C2A2DA}"
#define cYELLOW                 			"{FFFF00}"
#define cGRAY                   			"{AFAFAF}"
#define cPINK                   			"{E75480}"
#define cWHITE                  			"{FFFFFF}"
#define cTAXI                   			"{FFCC00}"
#define cDARKGRAY              	 			"{989898}"

#define cBLUE                 				"{04cabf}"
#define cDARKRED							"{AA3333}"
#define cRED                  				"{E05A5A}"
#define cGREEN                				"{46BD38}"
#define cORANGE								"{FF9945}"
#define cDARKORANGE             			"{DEB047}"

// -- TextDraw Colors

#define td_cBLUE							( 0x1E9999FF )
#define td_cGREEN							( 1166829567 )
#define td_cWHITE							( -1 )

// -- Chat
#define gbError                 			""cRED"* "cWHITE""
#define gbSuccess                			""cGREEN"* "cWHITE""
#define gbDefault               			""cGRAY"* "cWHITE""
#define gbDialog							""cGRAY"* "
#define gbDialogError						""cRED"* "
#define gbDialogSuccess						""cGREEN"* "
#define gbPhone								""cGRAY"[Tel�fono] "
#define gbRadio								""cWHITE"[Radio] "
#define gbVehicle							""cWHITE"[Vehiculo] "

#define ADMIN_PREFIX            			"[A]"
#define SUPPORT_PREFIX          			"[S]"
#define FRACTION_PREFIX						"[F]"
#define DEPARTMENT_PREFIX					"[D]"

// -- Dialogs ID
#define d_auth                  			( 30 )
#define d_death           					( 60 )
#define d_menu                  			( 90 )
#define d_bank                  			( 260 )
#define d_house                 			( 310 )
#define d_a_menu                			( 430 )
#define d_buy_menu              			( 460 )
#define d_fpanel							( 500 )
#define d_fill                  			( 600 )
#define d_frac                  			( 630 )
#define d_medical               			( 700 )
#define d_cnn                   			( 710 )
#define d_licenses              			( 740 )
#define d_admin                 			( 760 )
#define d_support							( 800 )
#define d_mebelbuy              			( 830 )
#define d_fire	                			( 860 )
#define d_gps 								( 890 )
#define d_meria                 			( 910 )
#define d_target                			( 940 )
#define d_buy_menu_select          			( 980 )
#define d_salon                 			( 1000 )
#define d_tune	                 			( 1050 )
#define d_repair                			( 1100 )
#define d_help                 				( 1010 )
#define d_police							( 1100 )
#define d_radio           					( 1400 )
#define d_donate                			( 1500 )
#define d_carjack               			( 1600 )
#define d_priton                			( 1700 )
#define d_mdc                   			( 1900 )
#define d_spec             					( 2100 )
#define d_makeleader            			( 2200 )
#define d_makeroute	            			( 2250 )
#define d_apartment             			( 2300 )
#define d_enterfrac             			( 2400 )
#define d_checkdamages          			( 2450 )
#define d_accept                			( 2540 )
#define d_fobject	            			( 2580 )
#define d_anim                 	 			( 3020 )
#define d_crime								( 3070 )
#define d_cars								( 3400 )
#define d_prison							( 3500 )
#define d_prison_duty						( 3995 )
#define d_arrest							( 4000 )
#define d_house_panel           			( 4050 )
#define d_business_panel					( 4400 )
#define	d_inv  								( 4500 )
#define d_buy_business						( 4550 )
#define	d_inv_settings 						( 5570 )
#define	d_inv_attach 						( 5600 )
#define d_attach							( 5700 )
#define d_commands							( 6000 )
#define d_daln								( 6100 )
#define d_bus								( 6130 )
#define d_wood								( 6140 )
#define d_taxi								( 6160 )
#define d_mech								( 6170 )
#define d_phone								( 6180 )
#define d_food								( 6220 )
#define d_fish								( 6230 )
#define d_createcars						( 7000 )

// -- Prepare Coords
#define RANGE_ADD_ADVERT					1670.8842, -1646.3506, 698.9738
#define RANGE_DE_MORGAN						3958.6768, -942.3129, 3.8427
#define RANGE_CAR_ARREST_1					1524.1223, -1481.3204, 9.5070
#define RANGE_CAR_ARREST_2					1509.8217, -1474.7585, 9.5170

// -- Pickups
#define PICKUP_FURNITURE					-489.8045, -262.2372, 3095.8960
#define PICKUP_SALON						1490.7559, 764.9916, 2001.0859
#define PICKUP_UTILIZATION					1333.8860, 396.6924, 19.7529
#define PICKUP_LICENSES						1494.8933, 1375.8115, 3116.3240
#define PICKUP_PARKING						-1551.4991, 1503.2120, 2001.0869
#define PICKUP_NETWORK						1671.2676, -1646.3319, 1701.9729
#define PICKUP_RECEPTION					1484.9921, -1744.5530, 3910.0859
#define PICKUP_WEAPON						-1503.8457, 95.5487, 3201.9858
#define PICKUP_POLICE						1543.4025, -1680.0443, 2494.2449
#define PICKUP_POLICE_2						-138.8494, -1874.2231, 2501.0859
#define PICKUP_REFILL						2510.0686, 1226.6754, 1801.0859
#define PICKUP_REFILL_2						664.2344, -568.3286, 16.3363

// -- Keys in public KeyStateChange
#define KEY_F								( 16 )

// -- Prepare Numbers

#define UNIX_MONTH							( 2592000 )

#define IC									( 0 )
#define OOC									( 1 )

#define RP_TYPE_ME							( 0 )
#define RP_TYPE_AME							( 1 )
#define RP_TYPE_DO							( 2 )
#define RP_TYPE_TODO						( 3 )

#define TYPE_CMD							( 1 )
#define TYPE_ADMIN							( 2 )

#define MAX_RAD_NAME 						( 16 )
#define TIME_UPDATE 						( 1500 )

#define MAX_PLAYER_VEHICLES					( 2 )
#define MAX_PLAYER_SETTINGS					( 11 )
#define MAX_PLAYER_BUSINESS					( 2 )
#define MAX_PLAYER_HOUSE					( 2 )

// -- Logs
#define LOG_TRANSFER_BANK_MONEY				( 1 )
#define LOG_BUY_HOUSE						( 2 )
#define LOG_BUY_HOUSE_FROM_PLAYER			( 3 )
#define LOG_SELL_HOUSE						( 4 )
#define LOG_TRANSFER_MONEY					( 5 )
#define LOG_TRANSFER_RCOIN					( 6 )
#define LOG_CHANGE_NAME						( 7 )
#define LOG_BUY_PREMIUM						( 8 )
#define LOG_BUY_BUSINESS					( 9 )
#define LOG_BUY_BUSINESS_FROM_PLAYER		( 10 )
#define LOG_RENT_HOUSE						( 11 )
#define LOG_BUY_VEHICLE						( 12 )
#define LOG_BUY_VEHICLE_FROM_PLAYER			( 13 )
#define LOG_SWAP_VEHICLE					( 14 )

// -- Prepare Message
#define PLAYER_DISTANCE						""gbError"Este jugador no est� cerca de ti."
#define MAX_MASS							""gbError"Ya llevas suficientes objetos contigo."
#define MAX_BAG_MASS						""gbError"Ya llevas suficientes art�culos en tu bolsa."
#define MAX_MASS_ON_TARGET					""gbError"Este jugador ya lleva suficientes objetos."
#define TIME_CMD							""gbError"No puedes usar este comando tan a menudo, espera."
#define CHAT_MUTE_IC						""gbError"Has bloqueado el acceso al chat IC, no puedes usarlo."
#define CHAT_MUTE_OOC						""gbError"Has bloqueado el acceso al chat OOC, no puedes usarlo"
#define NO_ACCESS_CMD       				""gbError"No tienes permisos para utilizar este comando."
#define NO_ACCESS							""gbError"No tienes acceso, no puedes realizar esta acci�n."
#define NO_MONEY							""gbError"No tienes fondos suficientes para realizar esta operaci�n."
#define INCORRECT_PLAYERID      			""gbError"ID de jugador inv�lido"
#define INCORRECT_VEHICLEID      			""gbError"ID de transporte no v�lida."
#define	NO_VEHICLE_FRACTION					""gbError"Este transporte no pertenece a su organizaci�n."
#define NOT_CHOOSE_ITEM						""gbError"Seleccione un elemento para realizar esta acci�n."
#define NOT_MOVE_ACT_ITEM					""gbError"Este elemento no se puede mover a la ranura seleccionada."
#define USE_ACTIVE_SLOT_NOW					""gbError"Ya est�s utilizando un elemento similar en la ranura activa."
#define USE_ACTIVE_SLOT_ACTION				""gbError"Para realizar cualquier acci�n con este elemento, elim�nelo de la ranura activa."
#define INCORRECT_AMMO						""gbError"Los cartuchos actuales no son adecuados para armas en la ranura activa."
#define INCORRECT_USE_ITEM					""gbError"Este art�culo no se utiliza de esta manera."
#define INCORRECT_USE_CAR					""gbError"No se puede utilizar el maletero de un veh�culo temporal."
#define INCORRECT_FRACTION_ITEM				""gbError"No puedes mover este objeto al transporte."

#define PLAYER_NEED_VEHICLE					""gbDefault"Para utilizar este comando, debes estar en el transporte."
#define NO_PERSONAL_TRANSPORT				""gbError"No tienes transporte personal."

#define NO_PERSONAL_BUSINESS				""gbError"Usted no es due�o de un negocio."

#define NO_MONEY_PLAYER						""gbError"El jugador no tiene fondos suficientes para realizar esta operaci�n."

#define HELP_EDITOR							""gbDefault"Utilice el "cBLUE" SPACE "cWHITE" para girar la c�mara, "cBLUE" ESC "cWHITE" para salir del editor."


#define TRANSFER_ITEM_ACCEPT				""gbSuccess"El jugador "cBLUE"%s [%d] "cWHITE" ha aceptado tu oferta."
#define TAKE_ITEM_ACCEPT					""gbSuccess"Has aceptado la oferta del jugador "cBLUE"%s[%d]"cWHITE"."


#define TRANSFER_ITEM_CANCEL 				""gbError"El jugador "cBLUE"%s [%d]"cWHITE" rechaz� su oferta."
#define TAKE_ITEM_CANCEL 					""gbError"Has rechazado la oferta del jugador"cBLUE"%s [%d]"cWHITE"."

#define OFFERED_TRANSFER 					""gbDefault"Le ha ofrecido al jugador"cBLUE"%s [%d]"cWHITE"el asunto"cBLUE"%s (%d)"cWHITE"."

#define TRANSFER_IS_BROKEN 					""gbError"Es imposible completar la transferencia del elemento, por favor repita."
#define NO_ACCESS_INTERACTION 				""gbError"No puede interactuar con este jugador."

#define NO_KEY_CAR 							""gbError"No tiene las llaves de encendido para este veh�culo."
#define NO_LEADER 							""gbError"Usted no es el l�der de la organizaci�n."

#define NO_NETWORK 							""gbError"Est� fuera de la cobertura de la red."
#define NO_HAVE_RCOIN 						""gbError"Tus ICoins no es suficiente en su cuenta, deposite fondos sin dejar el juego en"cBLUE"reeflandrp.ru/donate"
// -- Prepare Dialog Content

#define acontent_login 			"\
									"cBLUE" Inicia sesi�n \n \n \
									"cWHITE" Debe iniciar sesi�n para continuar. \n \n \
									Ingrese su contrase�a de administraci�n: \
								"
#define acontent_reg 			"\
									"cBLUE" Registro \n \n \
									"cWHITE" Debe registrarse como administrador. \n \n \
									Ingrese su contrase�a de administraci�n futura: \
								"

#define acontent_acp 			"\
									"cBLUE" - "cWHITE" Admins en linea \n \
									"cBLUE" - "cWHITE" Lista de comandos \n \
									"cBLUE" - "cWHITE" Estad�sticas \n \
									"cBLUE" - "cWHITE" Configuraci�n \n \
									"cBLUE" - "cWHITE" Kill Log \n \
									"cBLUE" - "cWHITE" Registro de desconexiones \n \
									"cBLUE" - "cWHITE" Actualizar permisos \n \
								"
#define hcontent_hcp 			"\
									"cBLUE" - "cWHITE" Staff en linea \n \
									"cBLUE" - "cWHITE" Lista de comandos \n \
									"cBLUE" - "cWHITE" Estad�sticas \n \
								"

#define acontent_giveitem 		"\
									"gbDialog" Seleccione una partici�n \n \
									"cBLUE" - "cWHITE" Buscar elemento \n \
									"cBLUE" - "cWHITE" Ropa \n \
									"cBLUE" - "cWHITE" Armas blancas \n \
									"cBLUE" - "cWHITE" Armas de fuego \n \
									"cBLUE" - "cWHITE" Ammo \n \
									"cBLUE" - "cWHITE" Bags \n \
									"cBLUE" - "cWHITE" Food \n \
									"cBLUE" - "cWHITE" Especial \n \
									"cBLUE" - "cWHITE" Otro \
								"
							
#define regtext 			""cBLUE"Informaci�n \n \n \
								"cWHITE" Te registraste correctamente en "cBLUE" Influx RP "cWHITE". \n \n \
								Para un juego sano y divertido, conf�e en un rol reflexivo, tenga en cuenta el car�cter y las cualidades personales de su personaje. \n \
								Observe el entorno de juego, eval�e adecuadamente la situaci�n del rol y sus acciones, conozca y haga nuevos amigos. \n \n \
								Toda la informaci�n adicional se puede encontrar en el men� del juego (Y - Men� del juego). \n \n \
								Disfruta el juego en "cBLUE" Influx RP "cWHITE"! \
							"
					
#define logtext 			""cBLUE"Login \n \n \
								%s, "cWHITE" bienvenido a "cBLUE" Influx RP "cWHITE". \n \n \
								Para continuar el juego, debes iniciar sesi�n. \n \
								Introduzca su contrase�a a continuaci�n: \
							"

#define logtextold 			""cBLUE"Registro \n \n \
								"cWHITE" Bienvenido a "cBLUE" Influx RP "cWHITE". \n \n \
								La cuenta "cBLUE"%s "cWHITE" se encontr� en la base de datos. \n \
								Para continuar con el registro, ingrese su contrase�a anterior: \n \
								"gbDialog" Se enviar� un c�digo de confirmaci�n al correo asociado con su cuenta. \
							"

#define logtexmail 			""cBLUE"Registro: C�digo \n \n \
								"cWHITE" Se ha enviado un correo electr�nico con un c�digo de verificaci�n a su direcci�n de correo electr�nico "cBLUE"% s "cWHITE". \n \n \
								Para completar el registro, ingrese el c�digo de confirmaci�n: \n \
								"gbDialog" Si no encontr� la letra con el c�digo de confirmaci�n, verifique la carpeta Spam. \
							"
							

#define logerror 			""cBLUE"Registro: C�digo \n \n \
								"cWHITE" Se ha enviado un correo electr�nico con un c�digo de verificaci�n a su direcci�n de correo electr�nico "cBLUE"% s "cWHITE". \n \n \
								Para completar el registro, ingrese el c�digo de confirmaci�n: \n \
								"gbDialogError" C�digo de verificaci�n no v�lido. \
							"
							
#define regpass				""cBLUE"Registro: Cuenta\n\n\
								"cWHITE"Bienvenido al servidor "cBLUE"%s"cWHITE", est�s a punto de registrar una cuenta.\n\n\
								Antes de registrarte te recomendamos leer nuestras normativas en "cBLUE"influx-rp.eu\n\
								"cWHITE"Para continuar ingrese una contrase�a que cumpla las siguientes condiciones:\n\n\
								"gbDialog"Debe tener un m�nimo de 8 car�cteres y un m�ximo de 20.\n\
								"gbDialog"No deber� tener simbolos o car�cteres especiales.\
							"
							
#define regpassrepeat		""cBLUE"Registro: Confirmaci�n\n\n\
								"cWHITE"Antes de registrarte te recomendamos leer nuestras normativas en "cBLUE"influx-rp.eu\n\
								"cWHITE"Para continuar, ingrese denuevo su contrase�a:\
							"
							
#define regmail				""cBLUE"Registro: Email\n\n\
								"cWHITE"Ingrese su direcci�n de correo electr�nico:\n\n\
								"gbDialog"En caso de que olvides tu contrase�a, podr�s recuperarla con tu email registrado.\n\
								"gbDialog"Asegurate de ingresar tu email en el formato adecuado. Ej: example@gmail.com\
							"

#define regsex				""cBLUE"Registro: Sexo\n\n\
								"cWHITE"Seleccione el sexo del personaje:\
							"
							
#define regcolor 		""cBLUE"Registro: skin color \n \n \
							"cWHITE" Elige el color de piel de tu personaje: \
						"

#define regnation 		"\
							"gbDialog" Elige la nacionalidad de tu personaje: \n \
							"cBLUE" - "cWHITE" Australiano \n \
							"cBLUE" - "cWHITE" Alban�s \n \
							"cBLUE" - "cWHITE" Americano \n \
							"cBLUE" - "cWHITE" Ingl�s \n \
							"cBLUE" - "cWHITE" Armenio \n \
							"cBLUE" - "cWHITE" Brasilero \n \
							"cBLUE" - "cWHITE" Vietnamita \n \
							"cBLUE" - "cWHITE" Alem�n \n \
							"cBLUE" - "cWHITE" Hebreo \n \
							"cBLUE" - "cWHITE" Espa�ol \n \
							"cBLUE" - "cWHITE" Italiano \n \
							"cBLUE" - "cWHITE" Chino \n \
							"cBLUE" - "cWHITE" Colombiano \n \
							"cBLUE" - "cWHITE" Coreano \n \
							"cBLUE" - "cWHITE" Latino \n \
							"cBLUE" - "cWHITE" Mexicano \n \
							"cBLUE" - "cWHITE" Alem�n \n \
							"cBLUE" - "cWHITE" Poland�s \n \
							"cBLUE" - "cWHITE" Portugu�s \n \
							"cBLUE" - "cWHITE" Ruso \n \
							"cBLUE" - "cWHITE" Turco \n \
							"cBLUE" - "cWHITE" Ucraniano \n \
							"cBLUE" - "cWHITE" Franc�s \n \
							"cBLUE" - "cWHITE" Checo \n \
							"cBLUE" - "cWHITE" Japon�s \
						"
							

#define regcountry 		"\
							"gbDialog" Seleccione el pa�s de nacimiento de su personaje: \n \
							"cBLUE" - "cWHITE" Australia \n \
							"cBLUE" - "cWHITE" Albania \n \
							"cBLUE" - "cWHITE" Inglaterra \n \
							"cBLUE" - "cWHITE" Armenia \n \
							"cBLUE" - "cWHITE" Brasil \n \
							"cBLUE" - "cWHITE" Vietnam \n \
							"cBLUE" - "cWHITE" Alemania \n \
							"cBLUE" - "cWHITE" Israel \n \
							"cBLUE" - "cWHITE" Espa�a \n \
							"cBLUE" - "cWHITE" Italia \n \
							"cBLUE" - "cWHITE" China \n \
							"cBLUE" - "cWHITE" Colombia \n \
							"cBLUE" - "cWHITE" Corea \n \
							"cBLUE" - "cWHITE" Mexico \n \
							"cBLUE" - "cWHITE" Pa�ses Bajos \n \
							"cBLUE" - "cWHITE" Polonia \n \
							"cBLUE" - "cWHITE" Portugal \n \
							"cBLUE" - "cWHITE" Rusia \n \
							"cBLUE" - "cWHITE" US \n \
							"cBLUE" - "cWHITE" Turqu�a \n \
							"cBLUE" - "cWHITE" Francia \n \
							"cBLUE" - "cWHITE" Ucrania \n \
							"cBLUE" - "cWHITE" Rep�blica Checa \n \
							"cBLUE" - "cWHITE" Jap�n \
						"

#define regage 			""cBLUE"Registro: Edad \n \n \
							"cWHITE" Ingresa la edad de tu personaje: \n \
							"gbDialog" De 16 a 70 a�os \
						"
							
#define regnick 		""cBLUE"Registro: Nombre y Apellido \n \n \
							"cWHITE" Est� utilizando un Nombre y / o Apellido incorrectos para registrar un car�cter - "cBLUE"% s "cWHITE". \n \n \
							Verifique la ortograf�a del nombre / apellido y vuelva a intentarlo: \n \
							"cBLUE" - el apodo "cGRAY" debe constar solo de letras latinas, \n \
							"cBLUE" - "cGRAY" entre el Nombre y el Apellido debe usar el car�cter '_', \n \
							"cBLUE" - el apodo "cGRAY" no debe contener m�s de 24 y menos de 6 caracteres, \n \
							"cBLUE" - "cGRAY" El nombre y / o el Apellido no deben contener menos de 3 caracteres, \n \
							"cBLUE" - "cGRAY" El nombre y el apellido deben comenzar con may�sculas. \
						"

#define retesttext 		""gbDefault"Informaci�n \n \n \
							"cWHITE" Bienvenido a "cBLUE" Influx RP "cWHITE". \n \
							Su cuenta ha sido enviada para volver a probar. \n \
							Puede completar la nueva prueba en el sitio: "cBLUE" www.influx-rp.eu "cWHITE". \
						"

#define checktext 	""gbDefault"Informaci�n \n \n \
						"cWHITE" Bienvenido a "cBLUE" Influx RP "cWHITE". \n \n \
						Tu cuenta a�n no est� activada. El tiempo de activaci�n puede demorar hasta 48 horas. \n \
						Puede hacer un seguimiento del estado de su cuenta en el sitio web: "cBLUE" www.influx-rp.eu "cWHITE". \
					"

#define unactive 	""gbDefault"Informaci�n \n \n \
						"cWHITE" Bienvenido a "cBLUE" Influx RP "cWHITE". \n \n \
						Su cuenta ha sido denegada. \n \
						Puede volver a crear su cuenta en el sitio: "cBLUE" www.influx-rp.eu "cWHITE". \
					"

#define errortext 	""gbError"Informaci�n \n \n \
						Fue desconectado del servidor por una entrada de datos incorrecta. \n \
						Puede restaurar el acceso a su cuenta en el sitio web: "cBLUE" www.influx-rp.eu "cWHITE". \
					"
#define cktext 	"\
					Informaci�n de "gbError" \n \n \
					Tu personaje del juego muri� por \"IC \" y recibi� el estado - \"CK \". \n \
					No puedes iniciar sesi�n. \
				"

#define static_gps ""cBLUE"-"cWHITE" Buscar"

#define invcontent_bone ""gbDialog"Seleccione una parte del cuerpo: \n \
							"cBLUE" - "cWHITE" Espalda \n \
							"cBLUE" - "cWHITE" Cabeza \n \
							"cBLUE" - "cWHITE" Hombro izquierdo \n \
							"cBLUE" - "cWHITE" Hombro derecho \n \
							"cBLUE" - "cWHITE" Mano izquierda \n \
							"cBLUE" - "cWHITE" Mano derecha \n \
							"cBLUE" - "cWHITE" Cadera izquierda \n \
							"cBLUE" - "cWHITE" Cadera derecha \n \
							"cBLUE" - "cWHITE" Pierna izquierda \n \
							"cBLUE" - "cWHITE" Pierna derecha \n \
							"cBLUE" - "cWHITE" Muslo derecho \n \
							"cBLUE" - "cWHITE" Muslo izquierdo \n \
							"cBLUE" - "cWHITE" Antebrazo izquierdo \n \
							"cBLUE" - "cWHITE" Antebrazo derecho \n \
							"cBLUE" - "cWHITE" Clav�cula izquierda \n \
							"cBLUE" - "cWHITE" Clav�cula derecha \n \
							"cBLUE" - "cWHITE" Cuello \n \
							"cBLUE" - "cWHITE" Mand�bula \
						"
#define vehcontent_cpanel 	"\
								"cBLUE" - "cWHITE" Informaci�n \n \
								"cBLUE" - "cWHITE" Gesti�n de vehiculo \n \
								"cBLUE" - "cWHITE" Respawnear vehiculos \n \
								"cBLUE" - "cWHITE" Encontrar un vehiculo en el mapa\n \
								"cBLUE" - "cWHITE" Estacionar un veh�culo \n \
								"cBLUE" - "cWHITE" Cambiar de vehiculo \n \
								"cBLUE" - "cWHITE" Vender vehiculo a un jugador \
							"
							
#define b_panel 		"\
							"cBLUE" - "cWHITE" Informaci�n \n \
							"cBLUE" - "cWHITE" Expansi�n empresarial \n \
							"cBLUE" - "cWHITE" Operaciones monetarias \n \
							"cBLUE" - "cWHITE" Configuraci�n \
						"

#define b_panel_p2 		"\
							"cWHITE" Nombre \t "cWHITE" Disponibilidad \n \
							"cWHITE" Tienda extendida \t%s \n \
							"cWHITE" Contador extendido  \t%s \n \
							"cWHITE" Muebles adicionales \
						"

#define b_panel_p3 		"\
							"cWHITE" Depositar en caja fuerte \n \
							"cWHITE" Retirar de caja fuerte \
						"

#define b_panel_p4 		"\
							"cWHITE" Cambiar el nombre del negocio \n \
							"cWHITE" Cambiar precio del producto (Actual "cBLUE" $%d "cWHITE") \n \
							"cWHITE" Ordenar los productos \n \
							"cWHITE" Modificaci�n del negocio \
						"

#define b_panel_plan 	"\
							"cWHITE" Refacciones \n \
							"cWHITE" Colocar muebles \n \
							"cWHITE" Instalaciones \
						"

#define b_panel_texture "\
							"cWHITE" Cambiar paredes \n \
							"cWHITE" Cambiar pisos \n \
							"cWHITE" Cambiar techos \n \
							"cWHITE" Cambiar escaleras \
						"
			

#define furniture_type 	"\
							"cBLUE" 1. "cWHITE"Sof�s y sillones \n \
							"cBLUE" 2. "cWHITE"Dormitorio \n \
							"cBLUE" 3. "cWHITE"Mesas  \n \
							"cBLUE" 4. "cWHITE"Sillas  \n \
							"cBLUE" 5. "cWHITE"Gabinetes y cofres  \n \
							"cBLUE" 6. "cWHITE"Electr�nica \n \
							"cBLUE" 7. "cWHITE"Cocina \n \
							"cBLUE" 8. "cWHITE"Ba�o\n \
							"cBLUE" 9. "cWHITE"Lighting \n \
							"cBLUE" 10. "cWHITE"Decoraci�n  \n \
							"cBLUE" 11. "cWHITE"Ventanas y vidrios \n \
							"cBLUE" 12. "cWHITE"Puertas y Cercas \n \
							"cBLUE" 13. "cWHITE"Oficina \n \
							"cBLUE" 14. "cWHITE"Catering p�blico \n \
							"cBLUE" 15. "cWHITE"Contadores y vitrinas  \n \
							"cBLUE" 16. "cWHITE"Deportes y Recreaci�n \
						"

#define furniture_other "\
							"cBLUE" 1. "cWHITE" Sombreros  \n \
							"cBLUE" 2. "cWHITE" Electrodom�sticos de cocina \n \
							"cBLUE" 3. "cWHITE" Comida y bebida \n \
							"cBLUE" 4. "cWHITE" Para empresas \n \
							"cBLUE" 5. "cWHITE" Anuncio \n \
							"cBLUE" 6. "cWHITE" Deportes y Recreaci�n \n \
							"cBLUE" 7. "cWHITE" Para las vacaciones \n \
							"cBLUE" 8. "cWHITE" Basura \n \
							"cBLUE" 9. "cWHITE" Productos de uso diario # 1 \n \
							"cBLUE" 10. "cWHITE" Productos de uso diario # 2 \n \
						"
							
							
#define job_dialog 	"\
						"cBLUE" 1. "cWHITE" %s dia de trabajo\n \
						"cBLUE" 2. "cWHITE" Informaci�n \n \
						"cBLUE" 3. "cWHITE" Conseguir el trabajo \
					"

#define dialog_stocks 	"\
							"cWHITE" Informaci�n General \n \
							"cWHITE" Seleccione las mercanc�as para el transporte \
						"