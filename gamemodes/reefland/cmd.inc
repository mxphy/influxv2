CMD:seguro( playerid, params[] )
{
	if( sscanf( params, "i", params[0] ) ) return SendClient:( playerid, C_WHITE, !""gbDefault"Sintaxis: /seguro [n�mero] (1 - casa / 2 - auto personal / 3 - negocio / 4 - auto de trabajo)" );
		

	
	switch( params[0] )
	{
		case 1 : LockHouse( playerid );

		case 2 : 
		{
			if( !IsOwnerVehicleCount( playerid ) )
				return SendClient:( playerid, C_WHITE, !""gbError"No tienes un veh�culo." );

			new
				id,
				Float:x,
				Float:y,
				Float:z,
				bool:flag = false;
		
			for( new i; i < MAX_PLAYER_VEHICLES; i++ ) 
			{
				id = Player[playerid][tVehicle][i];
				
				if( id == INVALID_VEHICLE_ID )
					continue;
				
				GetVehiclePos( id, x, y, z );
				CheckVehicleParams( id );

				if( IsPlayerInRangeOfPoint( playerid, 4.0, x, y, z ) )
				{
					if( Vehicle[id][vehicle_arrest] )
						return SendClient:( playerid, C_WHITE, !""gbError"No se puede desbloquear la puerta de un autom�vil en un interruptor autom�tico." );
				
					if( !Vehicle[id][vehicle_state_door] )
					{
						GameTextForPlayer( playerid, "~r~SEGURO", 1000, 1 );
						Vehicle[id][vehicle_state_door] = 1;
						format:g_small_string( "le pone el seguro a su vehiculo." );
						
						//Vehicle[id][vehicle_state_boot] = false;
						//Vehicle[id][vehicle_state_hood] = false;
					}
					else
					{
						GameTextForPlayer( playerid, "~g~DOORS UNLOCK", 1000, 1 );
						Vehicle[id][vehicle_state_door] = 0;
						format:g_small_string( "le quita el seguro a su vehiculo." );
					}
					MeAction( playerid, g_small_string, 1 );
					SetVehicleParams( id );
				
					flag = true;
					
					break;
				}
				else continue;
			}
			
			if( !flag )
				return SendClient:( playerid, C_WHITE, ""gbError"Su vehiculo est� demasiado lejos." );
		}
		
		case 3 : LockBusiness( playerid );
	
		case 4 : LockJobCar( playerid );
	}
	
	return 1;
}
	
CMD:desenganchar( playerid, params[] )
{
	if( IsTrailerAttachedToVehicle( GetPlayerVehicleID(playerid) ) ) 
		DetachTrailerFromVehicle( GetPlayerVehicleID(playerid) );
	
	return 1;
}


CMD:id( playerid, params[] )
{
	if( sscanf( params, "s[25]", params[0] ) )
		return SendClientMessage(playerid, C_WHITE, !""gbDefault"Sintaxis: /id <Nombre/ID>");
		
	new
		id = 0,
		fmt_afk[ 32 ];
	

	
	SendClient:( playerid, C_WHITE, !""gbDefault"Resultado de la b�squeda:" );
	
	if( !IsNumeric( params[0] ) )
	{
		foreach (new i : Player)
		{
			if( strfind( GetAccountName( i ), params[0], true ) != -1 )
			{

				
				if( IsAfk( i ) ) 
				{
					format( fmt_afk, sizeof fmt_afk, " - "cGREEN"[AFK: %d]",
						GetPVarInt( i, "Player:Afk" )
					);
				}
				
				format:g_small_string(  "%d. "cBLUE"%s[%d]%s"cWHITE"",
					id + 1,
					GetAccountName( i ),
					i,
					IsAfk( i ) ? fmt_afk : ("")
				);
				

				
				SendClient:( playerid, C_GRAY, g_small_string );
				
				id++;
			}
		}
		
		if( !id )
		{
			
			SendClient:( playerid, C_WHITE, ""gbError"No se encontraron resultados para su solicitud." );
		}

		
	}
	else
	{
		if( !IsLogged( strval( params[0] ) ) )
			return SendClientMessage( playerid, C_WHITE, !INCORRECT_PLAYERID );
		
		if( GetPVarInt( strval( params[0] ), "Player:Afk" ) > 0 ) 
		{
			format( fmt_afk, sizeof fmt_afk, " - "cGREEN"[AFK: %d]"cWHITE"",
				GetPVarInt( strval( params[0] ), "Player:Afk" )
			);
		}
		
		format:g_small_string(  ""cBLUE"%s[%d]%s"cWHITE"", 
			GetAccountName( strval( params[0] ) ),
			strval( params[0] ),
			GetPVarInt( strval( params[0] ), "Player:Afk" ) > 0 ? fmt_afk : ("")
		);
		
		//strcat( g_big_string, g_small_string );
		SendClient:( playerid, C_WHITE, g_small_string );
	}
	
	//showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Cerrar", "");
	
	return 1;
}
CMD:re( playerid, params[] ) return cmd_reportar(playerid, params);
CMD:duda( playerid, params[] ) return cmd_reportar(playerid, params);
CMD:reportar( playerid, params[] )
{
	new
	    server_tick = GetTickCount();
	
	if( GetPVarInt( playerid, "Player:ReportTime" ) > server_tick )
		return SendClientMessage(playerid, C_WHITE, !""gbError"Puede enviar un reporte una vez cada 20 segundos.");
	
	if( IsMuted( playerid, OOC ) )
		return SendClientMessage(playerid, C_WHITE, !CHAT_MUTE_OOC );
	
	if( isnull( params ) )
		return SendClientMessage(playerid, C_WHITE, !""gbDefault"Sintaxis: /reportar <Texto>");
	
	switch( Premium[playerid][prem_color] )
	{
		case 0:
		{
			format:g_small_string( "<Duda/Reporte> %s [%d]: "gbDefault"%s",
				GetAccountName( playerid ),
				playerid,
				params
			);
			SendSupport:( C_PURPLE, g_small_string, false );
					
			format:g_small_string( "<Duda/Reporte> %s [%d]: "gbDefault"%s",
				GetAccountName(playerid),
				playerid,
				params
			);
			SendAdmin:( C_PURPLE, g_small_string );
		}
		
		case 1:
		{
			format:g_small_string( "<Duda/Reporte> %s [%d]: "gbDefault"%s",
				GetAccountName( playerid ),
				playerid,
				params
			);
			SendSupport:( C_LIGHTGREEN, g_small_string, false );
					
			format:g_small_string( "<Duda/Reporte> %s [%d]: "gbDefault"%s",
				GetAccountName(playerid),
				playerid,
				params
			);
			SendAdmin:( C_LIGHTGREEN, g_small_string );
		}
		
		case 2:
		{
			format:g_small_string( "<Duda/Reporte> %s [%d]: "gbDefault"%s",
				GetAccountName( playerid ),
				playerid,
				params
			);
			SendSupport:( C_LIGHTORANGE, g_small_string, false );
					
			format:g_small_string( "<Duda/Reporte> %s [%d]: "gbDefault"%s",
				GetAccountName(playerid),
				playerid,
				params
			);
			SendAdmin:( C_LIGHTORANGE, g_small_string );
		}
		
		case 3:
		{
			format:g_small_string( "<Duda/Reporte> %s[%d]: "gbDefault"%s",
				GetAccountName( playerid ),
				playerid,
				params
			);
			SendSupport:( C_LIGHTRED, g_small_string, false );
			
			format:g_small_string( "<Duda/Reporte> %s[%d]: "gbDefault"%s",
				GetAccountName(playerid),
				playerid,
				params
			);
			SendAdmin:( C_LIGHTRED, g_small_string );
		}
	}
	
	format:g_small_string( ""gbDefault"Texto: "cBLUE"%s"cWHITE"",
		params
	);
	
	SendClient:( playerid, C_WHITE, g_small_string );
	SendClient:( playerid, C_WHITE, !""gbDefault"Usted ha enviado con �xito un mensaje a soporte y administradores.");
	
	SetPVarInt( playerid, "Player:ReportTime", server_tick + 20000 );
	return 1;
}

CMD:me( playerid, params[] )
{
	if( IsMuted( playerid, IC ) )
		return SendClientMessage( playerid, C_WHITE, !CHAT_MUTE_IC );
		
	if( isnull( params ) )
		return SendClientMessage( playerid, C_WHITE, !""gbDefault"Sintaxis: /me <Accion>");
		
	SendRolePlayAction( playerid, params, 0 );
	
	return 1;
}

CMD:coin( playerid, params[] )
{
	if( IsMuted( playerid, IC ) )
		return SendClientMessage( playerid, C_WHITE, !CHAT_MUTE_IC );
		
	clean:<g_small_string>;
	strcat( g_small_string, "arroja una moneda al aire y cae " );
	
	if( random( 2 ) == 1 )
		strcat( g_small_string, ""cGRAY"cara"cPURPLE"." );
	else 
		strcat( g_small_string, ""cGRAY"cruz"cPURPLE"." );
		
	SendRolePlayAction( playerid, g_small_string, 0 );
	
	return 1;
}

CMD:ame( playerid, params[] )
{
	if( IsMuted( playerid, IC ) )
		return SendClientMessage( playerid, C_WHITE, !CHAT_MUTE_IC );
		
	if( isnull( params ) )
		return SendClientMessage( playerid, C_WHITE, !""gbDefault"Sintaxis: /ame <Acci�n>");
	
	format:g_small_string( "> %s %s", 
		Player[playerid][uRPName], 
		params 
	);
	
	SendClient:( playerid, C_PURPLE, g_small_string );
	SendRolePlayAction( playerid, params, 1 );
	
	return 1;
}

CMD:do( playerid, params[] )
{
	if( IsMuted( playerid, IC ) )
		return SendClientMessage( playerid, C_WHITE, !CHAT_MUTE_IC );
		
	if( isnull( params ) )
		return SendClientMessage( playerid, C_WHITE, !""gbDefault"Sintaxis: /do <Acci�n>");
		
	SendRolePlayAction( playerid, params, 2 );
	
	return 1;
}

CMD:todo( playerid, params[] )
{
	static 
		string_text		[ 128 ],
		string_action	[ 128 ];
	
	if( IsMuted( playerid, IC ) )
		return SendClientMessage( playerid, C_WHITE, !CHAT_MUTE_IC );
		
	if( sscanf( params, "p<*>s[128]s[128]", string_text, string_action ) )
		return SendClientMessage( playerid, C_WHITE, !""gbDefault"Sintaxis: /todo [ Acci�n ]");	
	
	if( strlen( params ) <= 72 )
	{
		format:g_small_string( "\"%s\" dijo %s"cPURPLE" %s",
			string_text,
			Player[playerid][uRPName],
			string_action
		);
		
		ProxDetector( 20.0, playerid, g_small_string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5 );
	}
	else if( strlen( params ) > 72 )
	{
		format:g_small_string( "\"%s\" dijo %s ...",
			string_text,
			GetAccountName( playerid )
		);
		
		ProxDetector( 20.0, playerid, g_small_string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5 );
		
		format:g_small_string( "... %s",
			string_action
		);
		
		ProxDetector( 20.0, playerid, g_small_string, C_PURPLE, C_PURPLE, C_PURPLE, C_PURPLE, C_PURPLE );
	}
	
	return 1;
}

CMD:ab( playerid, params[] ) 
{
	if( IsMuted( playerid, OOC ) ) 
		return SendClient:( playerid, C_WHITE, !CHAT_MUTE_OOC );
		
	if( sscanf( params, "s[128]", params[0] ) ) 
		return SendClient:( playerid, C_WHITE, ""gbDefault"Sintaxis: /ab <Texto>" );
		
	format:g_small_string( "> (( %s[%d]: %s ))", 
		Player[playerid][uName],
		playerid,
		params[0] 
	);
	
	SendClient:( playerid, COLOR_FADE1, g_small_string );
	
	format:g_small_string( "(( %s ))", params[ 0 ] );
	SetPlayerChatBubble( playerid, g_small_string, COLOR_FADE1, 10.0, 5000 );
	
	return 1;
}

CMD:s( playerid, params[] )
{
	if( IsMuted( playerid, IC ) )
		return SendClient:( playerid, C_WHITE, !CHAT_MUTE_IC );
		
	if( isnull( params ) )
		return SendClientMessage( playerid, C_WHITE, !""gbDefault"Sintaxis: /s <Texto>");	
	
	format:g_small_string( "%s[%d] susurra: %s",
		Player[playerid][uRPName],
		playerid,
		params
	);

	SendLong:( playerid, 3.0, g_small_string, COLOR_FADE3, COLOR_FADE3, COLOR_FADE3, COLOR_FADE3, COLOR_FADE3 );
	
	return 1;
}

CMD:b( playerid, params[] )
{
	if( IsMuted( playerid, OOC ) )
		return SendClientMessage( playerid, C_WHITE, !CHAT_MUTE_OOC );
	
	if( isnull( params ) )
		return SendClientMessage( playerid, C_WHITE, !""gbDefault"Sintaxis: /b <Texto>");	
	
	format:g_small_string( "(( %s[%d]: %s ))",
		GetAccountName( playerid ),
		playerid,
		params
	);
	
	SendLong:( playerid, 18.0, g_small_string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5, 1 );
	
	return 1;
}

CMD:g( playerid, params[] )
{
	if( IsMuted( playerid, IC ) )
		return SendClientMessage( playerid, C_WHITE, !CHAT_MUTE_IC );
		
	if( isnull( params ) )
		return SendClientMessage( playerid, C_WHITE, !""gbDefault"Sintaxis: /g <Texto>");	
	
	format:g_small_string( "%s grita: �%s!",
		Player[playerid][uRPName],
		params
	);

	SendLong:( playerid, 35.0, g_small_string, COLOR_FADE1, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE3 );
	
	if( !IsPlayerInAnyVehicle( playerid ) ) 
		ApplyAnimation( playerid, "RIOT", "RIOT_shout", 4.0, 0, 1, 1, 0, 0, 1 );
		
	return 1;
}

CMD:w( playerid, params[] )
{	
	new
	    server_tick = GetTickCount();
		
	if( GetPVarInt( playerid, "Player:PMTime" ) > server_tick )
		return SendClientMessage( playerid, C_WHITE, !""gbError"Intentalo despu�s de unos segundos.");
	
	if( IsMuted( playerid, OOC ))
		return SendClientMessage( playerid, C_WHITE, !CHAT_MUTE_OOC );
		
	if( !Player[playerid][uPM] )
		return SendClient:(playerid, C_WHITE, ""gbError"Error Habilitar la recepci�n de mensajes privados." );
		
	if( sscanf( params, "us[144]", params[0], params[1] ) ) 
		return SendClientMessage(playerid, C_WHITE, !""gbDefault"Sintaxis: /w <ID> <Texto>");
		
	if( !IsLogged( params[0] ) || params[0] == playerid || IsPlayerNPC(params[0]) ) 
		return SendClientMessage( playerid, C_WHITE, !INCORRECT_PLAYERID );
	
	if( !Player[params[0]][uPM] )
		return SendClientMessage(playerid, C_WHITE, ""gbError"Este jugador ha desactivado la recepci�n de mensajes privados.");
		
	format:g_small_string( "(( PM a %s[%d]: %s ))",
		GetAccountName( params[0] ),
		params[0],
		params[1]
	);
	
	SendLongNoRadius:( playerid, C_YELLOW, g_small_string );
	
	format:g_small_string( "(( PM de %s[%d]: %s ))",
		GetAccountName( playerid ),
		playerid,
		params[1]
	);

	SendLongNoRadius:( params[0], C_YELLOW, g_small_string );
	
	SetPVarInt( playerid, "Player:PMTime", server_tick + 3500 );
	return 1;
}

CMD:afk( playerid, params[] ) 
{
    if( sscanf( params, "u", params[0] ) ) 
		return SendClientMessage( playerid, C_WHITE, !""gbDefault"Sintaxis: /afk <ID>" );
	
	if( !IsLogged( params[0] ) || params[0] == playerid || IsPlayerNPC(params[0]) ) 
		return SendClientMessage( playerid, C_WHITE, !INCORRECT_PLAYERID );
	
	if( !IsAfk( params[0] ) )
		return SendClientMessage( playerid, C_WHITE, !""gbDefault"Este jugador no est� en AFK." );
		
	format:g_small_string( ""gbDefault"Jugador "cBLUE"%s[%d]"cWHITE" - AFK: "cBLUE"%d"cWHITE" segundos.",
		Player[params[0]][uName],
		params[0],
		GetPVarInt( params[0], "Player:Afk" )
	);
	
	SendClient:( playerid, C_WHITE, g_small_string );
	
	return 1;
}



CMD:desc( playerid, params[] )
{
	if( sscanf( params, "d", params[0] ) )
		return SendClient:( playerid, C_WHITE, ""gbDefault"Sintaxis: /desc <id>" );
	
	if( !IsLogged( params[0] ) || IsPlayerNPC(params[0]) ) 
		return SendClientMessage( playerid, C_WHITE, !INCORRECT_PLAYERID );
	
	if( isnull( Player[params[0]][uPame] ) )
		return SendClient:( playerid, C_WHITE, !""gbError"Falta la descripci�n del jugador actual." );
	
	
	format:g_small_string( ""cWHITE"Descripci�n "cBLUE"%s"cWHITE":\n\n%s", Player[params[0]][uName], Player[params[0]][uPame] );
	
	showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_small_string, "Cerrar", "" );
	
	return 1;
}

CMD:estado( playerid )
{
	if( IsMuted( playerid, IC ) )
		return SendClientMessage( playerid, C_WHITE, !CHAT_MUTE_IC );
	
	if( isnull( Player[playerid][uPame] ) )
	{
		showPlayerDialog( playerid, d_commands + 4, DIALOG_STYLE_INPUT, " ", 
		""cBLUE" Establecer la descripci�n del personaje.\n\n\
		"cWHITE" Introduzca el texto de la descripci�n:", 
		"Siguiente", "Cerrar");
	}
	else
	{
		showPlayerDialog( playerid, d_commands + 5, DIALOG_STYLE_LIST, " ", 
		""cBLUE"-"cWHITE" Descripci�n actual\n\
		 "cBLUE"-"cWHITE" Establecer nueva descripci�n\n\
		 "cBLUE"-"cWHITE" Eliminar descripci�n", 
		"Siguiente", "Cerrar");
	}
	
	return 1;
}



CMD:lideres( playerid, params[] ) 
{
	new 
		leaders;
		
	clean:<g_big_string>;
	
	foreach(new i: Player) 
	{
		if( !IsLogged(i) || !Player[i][uMember] ) continue;
			
		if( !PlayerLeaderFraction( i, Player[i][uMember] - 1 ) ) continue;
			
		format:g_small_string( ""cBLUE"-"cWHITE" %s\t"cBLUE"%s[%d]"cWHITE"\n", 
			Fraction[ Player[i][uMember] - 1 ][f_name], 
			Player[i][uName], i
		);
		
		strcat( g_big_string, g_small_string );
		
		leaders++;
	}
	
	if( !leaders )
		return SendClient:( playerid, C_WHITE, !""gbError"Por el momento, los l�deres de las organizaciones no est�n en l�nea." );

	showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, "L�deres de organizaciones", g_big_string, "Cerrar", "" );
	
	return 1;
}

CMD:tiempo( playerid, params[] )
{
	static
		year, 
		month, 
		day, 
		hour, 
		minute, 
		second;
	
	getdate( year, month, day );
	gettime( hour, minute, second );

	clean:<g_big_string>;
	
	strcat( g_big_string, ""gbDefault"Informaci�n sobre la hora y fecha:\n\n" );
	format:g_small_string( " - Fecha: "cBLUE"%02d:%02d:%02d"cWHITE"\n", 
		hour, 
		minute,
		second
	);
	strcat( g_big_string, g_small_string );
	
	format:g_small_string( " - Fecha: "cBLUE"%02d/%02d/%d"cWHITE"\n\n", 
		day, 
		month, 
		year
	);
	strcat( g_big_string, g_small_string );
	
	if( Player[playerid][uJailTime] > 0 )
	{
		new
			Float:interval = float( Player[playerid][uJailTime] - gettime() ) / 3600.0,
			jail_hour = floatround( interval, floatround_floor ),
			Float:interval_2 = ( interval - float( jail_hour ) ) * 3600.0 / 60.0,
			jail_minute = floatround( interval_2 );
			
		if( jail_hour < 0 ) jail_hour = jail_minute = 0;
	
		format:g_small_string( " - Tiempo restante de carcel: "cBLUE"%d h. %d min."cWHITE"\n", jail_hour, jail_minute );
		strcat( g_big_string, g_small_string );
	}
	else if( Player[playerid][uDMJail] )
	{
		format:g_small_string( " - Tiempo restante de carcel: "cBLUE"%d"cWHITE" min.\n", Player[playerid][uDMJail] );
		strcat( g_big_string, g_small_string );
	}
	else if( Player[playerid][uMute] )
	{
		format:g_small_string( " - Mute IC: "cBLUE"%d"cWHITE" min.\n", Player[playerid][uMute] );
		strcat( g_big_string, g_small_string );
	}
	else if( Player[playerid][uBMute] )
	{
		format:g_small_string( " - Mute OOC: "cBLUE"%d"cWHITE" min.\n", Player[playerid][uBMute] );
		strcat( g_big_string, g_small_string );
	}
	
	format:g_small_string( " - Antes del salario: "cBLUE"%d"cWHITE" minutos.", 60 - Player[playerid][uPayTime] );
	strcat( g_big_string, g_small_string );
	
	if( Job[playerid][j_time] )
	{
		gmtime( Job[playerid][j_time], _, _, _, hour, minute );
	
		format:g_small_string( "\n - El contrato de seis horas expira a las "cBLUE"%02d:%02d"cWHITE" minutos.", hour, minute );
		strcat( g_big_string, g_small_string );
	}
	
	if( Premium[playerid][prem_time] )
	{
		gmtime( Premium[playerid][prem_time], year, month, day );
		
		format:g_small_string( "\n - Tu premium expira el "cBLUE"%02d.%02d.%d", day, month, year );
		strcat( g_big_string, g_small_string );
	}
	
	showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Cerrar", "" );
	
	return 1;
}

CMD:ayudantes( playerid )
{
	if( !Premium[playerid][prem_supports] )
		return SendClient:( playerid, C_WHITE, !NO_ACCESS_CMD );

	new
		support_count = 0;
				
	clean:<g_big_string>;
	strcat( g_big_string, ""gbDefault"Ayudantes en linea:\n\n" );

	foreach(new i : Player) 
	{
		if( !IsLogged( i ) || !GetAccessSupport( i ) ) 
			continue;
		
		format:g_string( ""cBLUE"%s[%d]%s%s"cWHITE"\n",
			Player[i][uName],
			i,
			!GetPVarInt( i, "Support:Duty" ) ? (" - "cGRAY"[Fuera de servicio]"cWHITE"") : ("")
		);
		
		++support_count;
		
		strcat( g_big_string, g_string );
	}
	
	if( support_count == 0 )
		strcat( g_big_string, "Total de ayudantes en linea:\n" );
	
	format:g_small_string(  "\n\n"cWHITE"Soporte en linea: "cBLUE"%d",
		support_count
	);
	
	strcat( g_big_string, g_small_string );
	
	showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Cerrar", "");
	
	return 1;
}

CMD:limpiarmichat( playerid, params[] ) 
{
	for( new i; i < 20; i++ ) 
		SendClient:( playerid, -1, " " );
		
	printf( "[Log]: El jugador %s[%d] limpio el chat.", Player[playerid][uName], playerid );
	
	return 1;
}

CMD:pagar( playerid, params[] ) 
{
	if( !Player[playerid][uLevel] )
		return SendClient:( playerid, C_WHITE, !NO_ACCESS_CMD );

	if( sscanf( params, "ud", params[0], params[1] ) ) 
		return SendClient:( playerid, C_WHITE, !""gbDefault"Sintaxis: /pagar <id> <cantidad>");
		
	if( !IsLogged( playerid ) || params[0] == playerid || IsPlayerNPC(params[0]) ) 
		return SendClient:( playerid, C_WHITE, !INCORRECT_PLAYERID );
		
	if( !Player[params[0]][uLevel] )
		return SendClient:( playerid, C_WHITE, !""gbError"No puedes transferir dinero a este jugador." );
		
	if( GetDistanceBetweenPlayers( playerid,params[0] ) > 3.0 || GetPlayerVirtualWorld(playerid) != GetPlayerVirtualWorld( params[0] ) ) 
		return SendClient:( playerid, C_WHITE, !PLAYER_DISTANCE );
		
	if( Player[playerid][uMoney] < params[1] ) 
		return SendClient:( playerid, C_WHITE, !NO_MONEY);
		
	if( params[1] < 1 || params[1] > 100000 ) 
		return SendClient:( playerid, C_WHITE, !""gbError"El monto m�nimo de transferencia es de $ 1 | El m�ximo es de $ 100,000.");

	SetPlayerCash( playerid, "-", params[1] );
	SetPlayerCash( params[0], "+", params[1] );

	pformat:( ""gbSuccess"Le diste al jugador "cBLUE"%s[%d]"cWHITE" - "cGREEN"$%d"cWHITE".",
		Player[params[0]][uName],
		params[0],
		params[1]
	);
	
	psend:( playerid, C_WHITE );
	
	pformat:( ""gbSuccess"El jugador "cBLUE"%s[%d]"cWHITE" te dio - "cGREEN"$%d"cWHITE".",
		Player[playerid][uName],
		playerid,
		params[1]
	);
	
	psend:( params[0], C_WHITE );

	ApplyAnimation( playerid,"DEALER", "shop_pay",4.0, 0, 1, 1, 0, 0, 1 );
	
	format:g_small_string( "Le di� dinero a %s", Player[params[0]][uName] );
	SendRolePlayAction( playerid, g_small_string, RP_TYPE_AME );
	
	log( LOG_TRANSFER_MONEY, "Lista de empleados", Player[playerid][uID], Player[params[0]][uID], params[1] ); 
	
	return 1;
}

CMD:miembros( playerid ) 
{
	if( !Player[playerid][uMember] && !Player[playerid][uCrimeM] ) 
		return SendClient:( playerid, C_WHITE, !NO_ACCESS_CMD );
	
	if( Player[playerid][uMember] )
	{
		new
			fid = Player[playerid][uMember] - 1,
			year, month, day,
			id;
		
		clean:<g_big_string>;
				
		format:g_small_string( ""cWHITE"Lista de empleados "cBLUE"%s", Fraction[fid][f_name] );
		strcat( g_big_string, g_small_string );
						
		strcat( g_big_string, "\n\n"cWHITE"En linea:\n"cBLUE"" );
						
		foreach(new i: Player)
		{
			if( !IsLogged(i) ) continue;
							
			if( Player[i][uMember] == fid + 1 )
			{
				id = getRankId( i, fid );
							
				if( Player[i][uRank] ) format:g_string( "%s", FRank[fid][id][r_name] );
								
				format:g_small_string( "%s[%d] - %s\n", Player[i][uName], i, 
					!Player[i][uRank] ? ("Sin rango") : g_string );
									
				strcat( g_big_string, g_small_string );
			}
		}
						
		strcat( g_big_string, "\n\n"cGRAY"" );
						
		for( new i; i < MAX_MEMBERS; i++ )
		{
			if( FMember[fid][i][m_id] )
			{
				year = month = day = 0;
				gmtime( FMember[fid][i][m_lasttime], year, month, day );
		
				if( FMember[fid][i][m_rank] ) format:g_string( "%s", FRank[fid][ FMember[fid][i][m_rank] - 1 ][r_name] );
		
				format:g_small_string( "%s[ac. %d] - %s - %02d.%02d.%d\n", FMember[fid][i][m_name], FMember[fid][i][m_id],
					!FMember[fid][i][m_rank] ? ("Sin rango") : g_string,
					day, month, year );
									
				strcat( g_big_string, g_small_string );
			}
		}
						
		showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Cerrar", "" );
	}
	else
	{
		ShowCrimeMembers( playerid, INVALID_DIALOG_ID, "Cerrar", "" );
	}
	
	return 1;
}

CMD:doc( playerid )
{
	new
		amount,
		year, month, day;
		
	clean:<g_string>;
	strcat( g_string, ""cWHITE"Tipo\t"cWHITE"Compilado por\t"cWHITE"Fecha" );
	
	for( new i; i < MAX_DOCUMENT; i++ )
	{
		if( Document[playerid][i][d_id] )
		{
			gmtime( Document[playerid][i][d_date], year, month, day );
		
			format:g_small_string( "\n"cGRAY"%d. "cWHITE"%s\t"cBLUE"%s\t%02d.%02d.%d", 
				amount + 1, 
				document_type[ Document[playerid][i][d_type] ],
				Document[playerid][i][d_name], 
				day, month, year );
			strcat( g_string, g_small_string );
			
			g_dialog_select[playerid][amount] = i;
			amount++;
		}
	}
	
	if( !amount )
		return SendClient:( playerid, C_WHITE, !""gbError"No tienes documentos." );
		
	showPlayerDialog( playerid, d_meria + 13, DIALOG_STYLE_TABLIST_HEADERS, "Mis documentos", g_string, "Seleccionar", "Cerrar" );

	return 1;
}



CMD:dfumar( playerid )
{
	if( !GetPVarInt( playerid, "Inv:UseSmokeId" ) )
		return SendClient:( playerid, C_WHITE, !""gbDefault"No est�s fumando." );
		
	DeletePVar( playerid, "Inv:UseSmokeId" );
	DeletePVar( playerid, "Inv:UseSmokeCount" );

	ClearAnimations( playerid );

	RemovePlayerAttachedObject( playerid, 5 );
		
	SendClient:( playerid, C_WHITE, !""gbSuccess"Tiraste el cigarrillo." );

	return 1;
}

CMD:dbeber( playerid )
{
	if( !GetPVarInt( playerid, "Inv:UseDrinkId" ) )
		return SendClient:( playerid, C_WHITE, !""gbDefault"No est�s bebiendo en este momento." );
		
	DeletePVar( playerid, "Inv:UseDrinkId" );
	DeletePVar( playerid, "Inv:UseDrinkCount" );
	
	ClearAnimations( playerid );
	
	RemovePlayerAttachedObject( playerid, 5 );
		
	SendClient:( playerid, C_WHITE, !""gbSuccess"Tiraste la botella." );

	return 1;
}

CMD:llenar( playerid, params[] )
{
	if( IsPlayerInAnyVehicle( playerid ) )
		return SendClient:( playerid, C_WHITE, !NO_ACCESS_CMD );
		
	if( !getItem( playerid, INV_SPECIAL, PARAM_JERRYCAN ) )
		return SendClient:( playerid, C_WHITE, !""gbError"No tienes latas contigo." );

	if( sscanf( params, "d", params[0] ) ) 
		return SendClient:( playerid, C_WHITE, !""gbDefault"Ingresar: /llenar <ID del veh�culo (/dl)>" );
		
	if( !GetVehicleModel( params[0] ) || IsVelo( params[0] ) )
		return SendClient:( playerid, C_WHITE, !INCORRECT_VEHICLEID );
		
	if( !Vehicle[ params[0] ][vehicle_id] )
		return SendClient:( playerid, C_WHITE, !""gbError"No se puede repostar el transporte temporal." );
		
	if( Vehicle[ params[0] ][vehicle_fuel] + 5.0 > VehicleInfo[ GetVehicleModel( params[0] ) - 400 ][v_fuel] )
		return SendClient:( playerid, C_WHITE, !""gbError"El tanque de combustible no puede acumular esa cantidad." );
		
	new
		Float:X, Float:Y, Float:Z;
	GetPlayerPos( playerid, X, Y, Z );
		
	if( GetVehicleDistanceFromPoint( params[0], X, Y, Z ) > 5.0 )
		return SendClient:( playerid, C_WHITE, !""gbError"Debes estar cerca del transporte." );
		
	Vehicle[params[0]][vehicle_fuel] += 5.0;
	UpdateVehicleFloat( params[0], "vehicle_fuel", Vehicle[ params[0] ][vehicle_fuel] );
	
	pformat:( ""gbSuccess"Tu recargaste "cBLUE"%s"cWHITE" usando el bote de "cBLUE"5"cWHITE" l.", GetVehicleModelName( GetVehicleModel( params[0] ) ) );
	psend:( playerid, C_WHITE );
	
	useSpecialItem( playerid, PARAM_JERRYCAN );

	return 1;
}

CMD:donar( playerid ) return cmd_donate( playerid );

CMD:donate( playerid )
{
	mysql_format:g_small_string( "SELECT `uGMoney` FROM `"DB_USERS"` WHERE `uID` = %d", Player[playerid][uID] );
	mysql_tquery( mysql, g_small_string, "showDonatPanel", "i", playerid );

	return 1;
}

CMD:admins( playerid )
{
	if( !Premium[playerid][prem_admins] )
		return SendClient:( playerid, C_WHITE, !NO_ACCESS_CMD );

	new
		fmt_player[32 + MAX_PLAYER_NAME + 2],
		admin_count = 0;
		
	clean:<g_big_string>;
	strcat( g_big_string, ""gbDefault"Administradores en linea:\n\n" );
	
	foreach(new i : Player) 
	{
		if( !IsLogged( i ) || !GetAccessAdmin( i, 1, false ) ) 
			continue;

		format( fmt_player, sizeof fmt_player, "(%d) "cBLUE"%s[%d]"cWHITE"",
			Admin[i][aLevel],
			Player[i][uName],
			i
		);
		
		format:g_small_string(  "%s%s\n",
			fmt_player,
			!GetPVarInt( i, "Admin:Duty" ) ? (" - "cRED"[Fuera de servicio]"cGRAY"") : ("")
		);
	
		++admin_count;
		strcat( g_big_string, g_small_string );
	}
	
	if( !admin_count )
		strcat( g_big_string, ""cBLUE"No" );
		
	showPlayerDialog( playerid, INVALID_DIALOG_ID, DIALOG_STYLE_MSGBOX, " ", g_big_string, "Cerrar", "" );

	return 1;
}

CMD:limite( playerid, params[] )
{
	if( !IsPlayerInAnyVehicle( playerid ) || GetPlayerState( playerid ) != PLAYER_STATE_DRIVER )
		return SendClient:( playerid, C_WHITE, !NO_ACCESS_CMD );
		
	if( sscanf( params, "d", params[0] ) ) 
		return SendClient:( playerid, C_WHITE, !""gbDefault"Entrar: /limite <Velocidad - 0 para eliminar>" );
	
	new
		vehicleid = GetPlayerVehicleID( playerid );
	
	if( IsVelo( vehicleid ) )
		return SendClient:( playerid, C_WHITE, !NO_ACCESS_CMD );
	
	if( !params[0] )
	{
		Vehicle[vehicleid][vehicle_limit] = 0;
		
		pformat:( ""gbSuccess"Has inhabilitado el limitador de velocidad en "cBLUE"%s"cWHITE".", GetVehicleModelName( GetVehicleModel(vehicleid) ) );
		psend:( playerid, C_WHITE );
	}
	else
	{
		if( params[0] < 20 || params[0] > 100 )
			return SendClient:( playerid, C_WHITE, !""gbDefault"El limitador de velocidad se puede ajustar de 20 a 100 mp/h." );
	
		Vehicle[vehicleid][vehicle_limit] = params[0];
		
		pformat:( ""gbSuccess"Has habilitado el limitador de velocidad en "cBLUE"%s"cWHITE": "cBLUE"%d"cWHITE" mp/h.", GetVehicleModelName( GetVehicleModel(vehicleid) ), params[0] );
		psend:( playerid, C_WHITE );
	}

	return 1;
}

CMD:blind( playerid )
{
	if( !GetPVarInt( playerid, "Player:Blinded" ) )
	{
		TextDrawShowForPlayer( playerid, blind_background );
		SetPVarInt( playerid, "Player:Blinded", 1 );
	}
	else 
	{
		TextDrawHideForPlayer( playerid, blind_background );
		SetPVarInt( playerid, "Player:Blinded", 0 );
		GameTextForPlayer(playerid, "Unblind", 2000, 5 );
	}
	
	return 1;
}

CMD:ayuda( playerid )
{
	return showPlayerDialog( playerid, d_help + 7, DIALOG_STYLE_MSGBOX, " ", d_help_start, "Siguiente", "" );
}